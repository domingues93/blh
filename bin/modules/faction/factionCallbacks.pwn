#include <YSI_Coding\y_hooks>

hook OnGameModeInit()
{
	print("� Carregando fac��es.");

	new query[255];
	mysql_format(getConexao(), query, sizeof(query), "SELECT f.*, u.username as owner_name FROM "TABLE_FACTIONS" as f LEFT JOIN "TABLE_USERS" as u ON u.id = f.owner WHERE 1;");
	new Cache:cache_id = mysql_query(getConexao(), query, true), rows = cache_num_rows();
	if ( rows )
	{
		for(new i; i < rows; i ++) {

			Iter_Add(Faction, i);

			cache_get_value_name_int(i, "id", Faction[i][ID]);

			cache_get_value_name_int(i, "owner", Faction[i][Owner][ID]);
			cache_get_value_name(i, "owner_name", Faction[i][Owner][Name], MAX_PLAYER_NAME);

			cache_get_value_name_int(i, "color", Faction[i][Color]);
			cache_get_value_name_int(i, "safebox", Faction[i][SafeBox]);
		}
	}
	printf("\tL %d Fac��es carregadas.\n", Iter_Count(Faction));

	cache_delete(cache_id);
	return Y_HOOKS_CONTINUE_RETURN_1;
}
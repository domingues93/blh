#if defined _inc_policia
	#error arquivo gerado
#endif

static const Float:CelasLosSantos[][] =
{
	{264.1914, 86.2944, 1001.0391, 90.0000},
	{264.2642, 81.0629, 1001.0391, 90.0000},
	{264.7795, 81.7773, 1001.0457, 90.0000}
};

static const Float:CelasSF[][] = 
{
	{219.11975, 108.59809, 998.01532},
	{214.76483, 108.13013, 998.01532},
	{223.02049, 108.45776, 998.01532},
	{226.95946, 108.58559, 998.01532}
};

static const Float:CelasLV[][]=
{
	{194.54076, 162.35536, 1002.02435},
	{190.50098, 162.24921, 1002.02435},
	{198.91145, 162.25813, 1002.02435},
	{188.76086, 174.33563, 1002.02344},
	{193.05070, 174.45988, 1002.02344},
	{197.13063, 174.70404, 1002.02344}
};

new static 
	PlayerText:TimePrison[MAX_PLAYERS], TimerPerseguicao[MAX_PLAYERS], Localizando[MAX_PLAYERS], PmID[MAX_PLAYERS];

#include <YSI_Coding\y_hooks>

hook OnGameModeInit()
{
	CreateDynamicPickup(1247, 23, 1525.9398, -1677.3005, 5.8906, 0, 0, .streamdistance=30.0); // /prender Los Santos
	CreateDynamicPickup(1247, 23, -1606.1379, 673.2057, -5.2422, 0, 0, .streamdistance=30.0); // /prender San Fierro
	CreateDynamicPickup(1247, 23, 2282.2183, 2424.3428, 3.4766, 0, 0, .streamdistance=30.0); // /prender Las Venturas

	// Veiculos
	call::JOB->SetVehicleJob(CreateVehicle(596, 1535.9813, -1667.6187, 12.9729, 0.1341, -1, -1, 	TIME_VEHICLE_SPAWN), 	POLICIA_MILITAR);
	call::JOB->SetVehicleJob(CreateVehicle(596, 1536.0015, -1678.1123, 12.9729, 359.7990, -1, -1, 	TIME_VEHICLE_SPAWN),	POLICIA_MILITAR);
	call::JOB->SetVehicleJob(CreateVehicle(596, 1552.4932, -1614.6538, 13.0397, 0.0000, -1, -1, 	TIME_VEHICLE_SPAWN), 	POLICIA_MILITAR);
	call::JOB->SetVehicleJob(CreateVehicle(596, 1557.0504, -1614.6760, 13.0397, 0.0000, -1, -1, 	TIME_VEHICLE_SPAWN), 	POLICIA_MILITAR);
	call::JOB->SetVehicleJob(CreateVehicle(596, 1561.7593, -1614.6534, 13.0397, 0.0000, -1, -1, 	TIME_VEHICLE_SPAWN), 	POLICIA_MILITAR);
	call::JOB->SetVehicleJob(CreateVehicle(596, 1566.4622, -1614.6753, 13.0397, 0.0000, -1, -1, 	TIME_VEHICLE_SPAWN), 	POLICIA_MILITAR);
	call::JOB->SetVehicleJob(CreateVehicle(596, 1571.0800, -1614.7461, 13.0397, 0.0000, -1, -1, 	TIME_VEHICLE_SPAWN), 	POLICIA_MILITAR);
	call::JOB->SetVehicleJob(CreateVehicle(596, 1575.4910, -1614.7804, 13.0397, 0.0000, -1, -1, 	TIME_VEHICLE_SPAWN), 	POLICIA_MILITAR);
	call::JOB->SetVehicleJob(CreateVehicle(523, 1580.2361, -1604.1326, 12.9488, 177.3033, -1, -1, 	TIME_VEHICLE_SPAWN), 	POLICIA_MILITAR);
	call::JOB->SetVehicleJob(CreateVehicle(523, 1582.0455, -1604.2159, 12.9496, 178.2768, -1, -1, 	TIME_VEHICLE_SPAWN), 	POLICIA_MILITAR);
	call::JOB->SetVehicleJob(CreateVehicle(523, 1583.9740, -1604.1248, 12.9521, 179.4607, -1, -1, 	TIME_VEHICLE_SPAWN), 	POLICIA_MILITAR);
	call::JOB->SetVehicleJob(CreateVehicle(523, 1585.9513, -1604.1871, 12.9523, 179.9288, -1, -1, 	TIME_VEHICLE_SPAWN), 	POLICIA_MILITAR);
	call::JOB->SetVehicleJob(CreateVehicle(523, 1587.7197, -1604.1445, 12.9509, 179.8583, -1, -1, 	TIME_VEHICLE_SPAWN), 	POLICIA_MILITAR);
	call::JOB->SetVehicleJob(CreateVehicle(599, -1399.9597, 2631.7783, 55.9460,92.1383, -1, -1, 	TIME_VEHICLE_SPAWN), 	POLICIA_MILITAR);
	call::JOB->SetVehicleJob(CreateVehicle(599, -1399.9863, 2637.8792, 55.8776, 90.7791, -1, -1, 	TIME_VEHICLE_SPAWN), 	POLICIA_MILITAR);
	call::JOB->SetVehicleJob(CreateVehicle(599, -1399.6434, 2650.2114, 55.8786,88.6543, -1, -1, 	TIME_VEHICLE_SPAWN), 	POLICIA_MILITAR);
	call::JOB->SetVehicleJob(CreateVehicle(599, -1399.6257, 2656.6499, 55.8735,90.1429, -1, -1, 	TIME_VEHICLE_SPAWN), 	POLICIA_MILITAR);
	CreateObject(1522, -1390.80627, 2639.21533, 54.97758,   0.00000, 0.00000, 0.00000);
}

hook OnPlayerConnect(playerid)
{
	TimePrison[playerid] = PlayerText:INVALID_TEXT_DRAW;
	PmID[playerid] = INVALID_PLAYER_ID;	
}

hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	if ( PRESSED(KEY_CROUCH))
	{
		if ( call::PLAYER->GetPlayerVarInt(playerid, Profissao) != POLICIA_MILITAR)
			return Y_HOOKS_CONTINUE_RETURN_1;

		call::MAP->AbrirPortaoDP(playerid);
		return Y_HOOKS_CONTINUE_RETURN_1;
	}
	return Y_HOOKS_CONTINUE_RETURN_1;
}

forward ResetableVarsPerseguicao(pmid, playerid);
public ResetableVarsPerseguicao(pmid, playerid)
{
	if ( call::PLAYER->GetPlayerVarBool(playerid, Perseguido) )
	{
		TimerPerseguicao[pmid] = -1;
		TimerPerseguicao[playerid] = -1;

		call::PLAYER->SetPlayerVarBool(pmid, Perseguindo, false);
		SendClientMessage(pmid, GetPlayerColor(pmid), "O(A) Jogador(a) %s não está mais sendo perseguido.", GetUserName(playerid));

		call::PLAYER->SetPlayerVarInt(playerid, TempoPerseguido, gettime() + 120);

		call::PLAYER->SetPlayerVarBool(playerid, Perseguido, false);
		SendClientMessage(playerid, GetPlayerColor(pmid), "O(A) Policial %s não está mais lhe perseguindo.", GetUserName(pmid));

		SendClientMessageToAll(GetPlayerColor(playerid), "O(A) Policial %s não está mais perseguindo o suspeito %s", GetUserName(pmid), GetUserName(playerid));
	}
	return true;
}


stock function PM::resetVarsPerseguicao(playerid)
{
	if( call::PLAYER->GetPlayerVarInt(playerid, Perseguido) )
	{
		new pmid = PmID[playerid];
		PmID[playerid] = INVALID_PLAYER_ID;

		TimerPerseguicao[pmid] = -1;
		TimerPerseguicao[playerid] = -1;

		call::PLAYER->SetPlayerVarBool(pmid, Perseguindo, false);
		call::PLAYER->SetPlayerVarBool(playerid, Perseguido, false);
	}
	return true;
}

// ============================== [ COMMANDs ] ============================== //

CMD:prender(playerid, params[])
{
	if(call::PLAYER->GetPlayerVarInt(playerid, Profissao) != POLICIA_MILITAR)
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

	if(!call::JOB->IsPlayerInWorking(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está trabalhando.");

	new pid;
	if(sscanf(params, "u", pid))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/prender [playerid / playername]");

	if(call::ADMIN->IsAdminInJob(pid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não pode prender um Administrador em modo trabalho.");

	if ( GetPlayerWantedLevel(pid) < 1)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Esse playerid não está sendo procurado pela policia.");
	
	if(!IsPlayerInRangeOfPoint(playerid, 5.0, 1525.9398, -1677.3005, 5.8906))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você tem que levar o jogador para a delegacia.");
	
	if(!call::PLAYER->IsPlayerLogged(pid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) não está logado(a).");

	if(!IsPlayerInRangeOfPoint(pid, 5.0, 1525.9398, -1677.3005, 5.8906))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Suspeito(a) não está proximo.");

	if(GetPlayerWantedLevel(pid) == 0)
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) não está sendo procurado.");

	if(!call::PLAYER->GetPlayerVarBool(pid, Algemado))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) não está algemado.");

	call::PM->PrenderJogador(playerid, pid);
	return true;
}

CMD:algemar(playerid, params[])
{
	if(call::PLAYER->GetPlayerVarInt(playerid, Profissao) != POLICIA_MILITAR)
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

	if(!call::JOB->IsPlayerInWorking(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está trabalhando.");

	if ( IsPlayerInAnyVehicle(playerid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não pode algemar um suspeito estando dentro do veiculo.");

	new pid;
	if(sscanf(params, "d", pid))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/algemar [playerid]");

	if(call::ADMIN->IsAdminInJob(pid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não pode algemar um Administrador em modo trabalho.");

	if ( GetPlayerWantedLevel(pid) < 1)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Esse playerid não está sendo procurado pela policia.");

	if(playerid == pid)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não pode algemar Você mesmo.");

	if(!call::PLAYER->IsPlayerLogged(pid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) não está logado(a).");

	if(call::PLAYER->GetPlayerVarBool(pid, Algemado))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) já está algemado(a).");

	new Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x, y, z);

	if( GetPlayerDistanceFromPoint(pid, x, y, z) > 5.0 )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está proximo ao jogador.");

	SetPlayerSpecialAction(pid, SPECIAL_ACTION_CUFFED);
	call::PLAYER->SetPlayerVarBool(pid, Algemado, true);
	SendClientMessage(pid, COR_SISTEMA, "» O(A) Policial {"COR_BRANCO_INC"}%s{"COR_SISTEMA_INC"} algemou Você.", GetUserName(playerid));
	TogglePlayerControllable(pid, false);

	SendClientMessage(playerid, COR_SISTEMA, "» O(A) suspeito(a) {"COR_BRANCO_INC"}%s{"COR_SISTEMA_INC"} foi algemado(a).", GetUserName(pid));
	return true;
}

CMD:desalgemar(playerid, params[])
{
	if(call::PLAYER->GetPlayerVarInt(playerid, Profissao) != POLICIA_MILITAR)
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

	if(!call::JOB->IsPlayerInWorking(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está trabalhando.");

	new pid;
	if(sscanf(params, "d", pid))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/desalgemar [playerid]");

	if ( GetPlayerWantedLevel(pid) < 1)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Esse playerid não está sendo procurado pela policia.");

	if(playerid == pid)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não pode desalgemar Você mesmo.");

	if(!call::PLAYER->IsPlayerLogged(pid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) jogador(a) não está logado(a).");

	if(!call::PLAYER->GetPlayerVarBool(pid, Algemado))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) jogador(a) não está algemado.");

	new Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x, y, z);

	if(GetPlayerDistanceFromPoint(pid, x, y, z) > 5.0)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está proximo ao jogador.");

	SetPlayerSpecialAction(pid, SPECIAL_ACTION_NONE), call::PLAYER->SetPlayerVarBool(pid, Algemado, false);
	SendClientMessage(pid, COR_SISTEMA, "» O(A) Policial {"COR_BRANCO_INC"}%s{"COR_SISTEMA_INC"} desalgemou Você.", GetUserName(playerid));
	TogglePlayerControllable(pid, true);

	SendClientMessage(playerid, COR_SISTEMA, "» O(A) suspeito(a) {"COR_BRANCO_INC"}%s{"COR_SISTEMA_INC"} foi desalgemado(a).", GetUserName(pid));
	return true;
}

CMD:procurar(playerid, params[])
{
	if(call::PLAYER->GetPlayerVarInt(playerid, Profissao) != POLICIA_MILITAR)
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

	if(!call::JOB->IsPlayerInWorking(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está trabalhando.");

	new pid, nivel, motivo[30];
	if(sscanf(params, "uds[30]", pid, nivel, motivo))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/procurar [playerid / playername] [nivel procurado] [motivo]");

	if(call::ADMIN->IsAdminInJob(pid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não pode colocar estrelas em um Administrador em modo trabalho.");

	if(playerid == pid)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não pode se colocar na lista de procurados.");

	if( !call::PLAYER->IsPlayerLogged(pid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) não está conectado(a).");

	if(call::PLAYER->GetPlayerVarInt(pid, Preso) != SOLTO )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não pode colocar o jogador na lista de procurados, pos ele está preso.");

	if (nivel > 6)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você pode adicionar o máximo de 6 estrelas de procurado.");

	SetPlayerWantedLevel(pid, GetPlayerWantedLevel(pid)+nivel);
	SendClientMessage(pid, GetPlayerColor(playerid), "» {"COR_BRANCO_INC"}O(A) Policial {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"} colocou você na lista de procurado.", GetUserName(playerid));
	SendClientMessage(pid, GetPlayerColor(playerid), "» {"COR_BRANCO_INC"}Nivel: {"COR_SISTEMA_INC"}%d{"COR_BRANCO_INC"} Motivo: {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}", nivel, motivo);

	SendClientMessage(playerid, GetPlayerColor(playerid), "» {"COR_BRANCO_INC"}Você adiciou +{"COR_SISTEMA_INC"}%d{"COR_BRANCO_INC"} nivel de procurados em {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}.", nivel, GetUserName(pid) );
	return true;
}

CMD:perseguir(playerid, params[])
{
	if(call::PLAYER->GetPlayerVarInt(playerid, Profissao) != POLICIA_MILITAR)
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

	if(!call::JOB->IsPlayerInWorking(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está trabalhando.");

	new pid;
	if(sscanf(params, "u", pid))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/perseguir [playerid / playername]");

	if ( !call::PLAYER->IsPlayerLogged(pid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) não está conectado.");

	if(playerid == pid)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não pode perseguir Você mesmo.");

	if ( GetPlayerWantedLevel(pid) < 1)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Esse playerid não está sendo procurado pela policia.");

	if(call::ADMIN->IsAdminInJob(pid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não pode perseguir um Administrador em modo trabalho.");

	if(!call::PLAYER->IsPlayerLogged(pid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) não está conectado(a).");

	if(!GetPlayerWantedLevel(pid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) não está sendo procurado(a).");

	if( call::PLAYER->GetPlayerVarBool(pid, Perseguido) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) já está sendo perseguido(a).");

	if(call::PLAYER->GetPlayerVarBool(playerid, Perseguindo))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você já está perseguindo um(a) suspeito(a).");

	if(call::PLAYER->GetPlayerVarInt(pid, TempoPerseguido) > gettime() )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você precisa aguardar {"COR_BRANCO_INC"}%d{"COR_ERRO_INC"} segundos para perseguir o jogador novamente.", call::PLAYER->GetPlayerVarInt(playerid, TempoPerseguido) - gettime());
 	
	if ( !IsPlayerStreamedIn(playerid, pid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você precisa está mais próximo ao jogador.");

	call::PLAYER->SetPlayerVarBool(pid, Perseguido, true);
	SendClientMessage(pid, COR_SISTEMA, "» {"COR_BRANCO_INC"}O(A) Policial {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"} está lhe perseguindo, você precisa fugir dele por 10 minutos.", GetUserName(playerid));

	call::PLAYER->SetPlayerVarBool(playerid, Perseguindo, true);
	SendClientMessage(playerid, COR_SISTEMA, "» {"COR_BRANCO_INC"}Você está perseguindo o(a) jogador(a) {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"}, Você tem 10 minutos para captura-lo.", GetUserName(pid));

	SendClientMessageToAll(GetPlayerColor(playerid), "» O(A) Policial {"COR_BRANCO_INC"}%s{%06x} está perseguindo o(a) suspeito(a) {"COR_BRANCO_INC"}%s{%06x}", GetUserName(playerid), (GetPlayerColor(playerid) >>> 8), GetUserName(pid), (GetPlayerColor(playerid) >>> 8));
	
	PmID[pid] = playerid;

	KillTimer(TimerPerseguicao[playerid]);
	KillTimer(TimerPerseguicao[pid]);
	TimerPerseguicao[playerid] = TimerPerseguicao[pid] = SetTimerEx("ResetableVarsPerseguicao", (10 * (60 * 1000)), false, "dd", playerid, pid);
	return true;
}

CMD:arrastar(playerid, params[])
{
	if(call::PLAYER->GetPlayerVarInt(playerid, Profissao) != POLICIA_MILITAR)
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

	if(!call::JOB->IsPlayerInWorking(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está trabalhando.");

	if(isnull(params) || !IsNumeric(params))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/arrastar [playerid]");

	new pid = strval(params);

	if(!IsPlayerInAnyVehicle(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está em um veiculo.");

	if(call::ADMIN->IsAdminInJob(pid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não pode arrastar um Administrador em modo trabalho.");

	if ( GetPlayerWantedLevel(pid) < 1)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Esse playerid não está sendo procurado pela policia.");

	new vehicleid = GetPlayerVehicleID(playerid);

	if( call::JOB->GetVehicleJob(vehicleid) != POLICIA_MILITAR )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está em uma viatura da Policia Militar.");

	if(!call::PLAYER->GetPlayerVarBool(pid, Algemado))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) jogador(a) não está algemado.");

	new Float:x, Float:y, Float:z;
	GetPlayerPos(pid, x, y, z);
	if ( GetPlayerDistanceFromPoint(playerid, x, y, z) > 10.0 )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você precisa esta mais próximo do(a) jogador(a).");


	if(!IsVehicleSeatOccupied(vehicleid, 3)){
		PutPlayerInVehicle(pid, vehicleid, 3);
	}
	else if(!IsVehicleSeatOccupied(vehicleid, 4)){
		PutPlayerInVehicle(pid, vehicleid, 4);
	}
	else{
		SendClientMessage(playerid, COR_ERRO, "Erro: não tem mais vaga no veiculo.");
		return true;
	}
	TogglePlayerControllable(pid, false);
	SendClientMessage(pid, GetPlayerColor(playerid), "» {"COR_BRANCO_INC"}O Policial %s te arrastou a força para a viatura.", GetUserName(playerid));

	SendClientMessage(playerid, GetPlayerColor(playerid), "» {"COR_BRANCO_INC"}Você arrastou o suspeito %s até a viatura.", GetUserName(pid));
	return true;
}

CMD:procurados(playerid)
{
	if( call::PLAYER->GetPlayerVarInt(playerid, Profissao) != POLICIA_MILITAR )
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

	if( !call::JOB->IsPlayerInWorking(playerid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está trabalhando.");

	if( call::JOB->GetVehicleJob(GetPlayerVehicleID(playerid)) != POLICIA_MILITAR )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está em uma viatura da Policia Militar.");

	new str[1045] = "id\tnome\testrelas\tdistância\n", Float:x, Float:y, Float:z, count;
	foreach(new i: Player)
	{
		if(GetPlayerWantedLevel(i) > 0 && !call::ADMIN->IsAdminInJob(i) )
		{
			GetPlayerPos(i, x, y, z);
			format(str, sizeof(str), "%s%d\t{"COR_VERMELHO_INC"}%s\t{"COR_AMARELO_INC"}%d\t{"COR_VERDE_INC"}%0.1f{"COR_BRANCO_INC"}\n", str, i, GetUserName(i), GetPlayerWantedLevel(i), GetPlayerDistanceFromPoint(playerid, x, y, z));
			count++;
		}
	}
	if(count)
		return ShowPlayerDialog(playerid, 0, DIALOG_STYLE_TABLIST_HEADERS, "LISTA DE PROCURADOS", str, "OK", "");

	SendClientMessage(playerid, COR_ERRO, "A Cidade está limpa! Não há nenhum criminoso a solta.");
	return true;
}

CMD:localizar(playerid, params[])
{
	if( call::PLAYER->GetPlayerVarInt(playerid, Profissao) != POLICIA_MILITAR )
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);
		
	if ( isnull(params) || !IsNumeric(params))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/localizar [playerid]");
	
	new id = strval(params);
	
	if ( !IsPlayerConnected(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador não está conectado.");

	if( call::ADMIN->IsAdminInJob(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não pode localizar um Administrador em modo trabalho.");

	if ( GetPlayerWantedLevel(id) < 1)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Esse playerid não está sendo procurado pela policia.");

	if ( Localizando[playerid] )
		RemovePlayerMapIcon(playerid, 15), KillTimer(Localizando[playerid]);

	static Float:x, Float:y, Float:z;
	GetPlayerPos(id, x, y, z);
	SetPlayerMapIcon(playerid, 15, x, y, z, 0, COR_VERMELHO);
	Localizando[playerid] = SetTimerEx("AtualizarLocalizacao", 500, true, "dd", playerid, id);
	return true;
}

CMD:deslocalizar(playerid, params[])
{
	if( call::PLAYER->GetPlayerVarInt(playerid, Profissao) != POLICIA_MILITAR )
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

	if ( !Localizando[playerid] )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está localizando ninguém");

	RemovePlayerMapIcon(playerid, 15);
	KillTimer(Localizando[playerid]);
	Localizando[playerid]=0;

	SendClientMessage(playerid, COR_AZUL, "* Você desativou o localizador.");
	return true;
}

forward AtualizarLocalizacao(playerid, suspeitoid);
public AtualizarLocalizacao(playerid, suspeitoid)
{
	if ( !IsPlayerConnected(suspeitoid) )
	{
		RemovePlayerMapIcon(playerid, 15);
		SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) que você estava localizando saiu do servidor.");
		return KillTimer(Localizando[playerid]), Localizando[playerid]=0;
	}

	if ( !IsPlayerConnected(playerid) )
		return KillTimer(Localizando[playerid]), Localizando[playerid]=0;

	static Float:x, Float:y, Float:z;
	
	if ( call::PLAYER->GetPlayerVarInt(playerid, Entrou) != ENTROU_NONE )
	{
		new interiorid = GetPlayerVirtualWorld(playerid);
		switch(call::PLAYER->GetPlayerVarInt(playerid, Entrou))
		{
			case ENTROU_EMPRESA:
				call::BUSINESS->GetBusinessPos(interiorid, x, y, z);

			case ENTROU_CASA:
				call::HOUSE->GetHousePos(interiorid, x, y, z);

			case ENTROU_PROPRIEDADE:
				call::PROPERTY->GetPropertyPos(interiorid, x, y, z);
		}
	}
	else
	{
		GetPlayerPos(suspeitoid, x, y, z);
	}

	RemovePlayerMapIcon(playerid, 15);
	SetPlayerMapIcon(playerid, 15, x, y, z, 0, COR_VERMELHO);
	return true;
}

// ============================== [ FUNCTIONs ] ============================== //
stock EnviarCela(playerid)
{
	if(IsPlayerInRangeOfPoint(playerid, 50.0, 1525.9398, -1677.3005, 5.8906)) // Los Santos
	{
		new rand = random(sizeof(CelasLosSantos));
		Teleport(playerid, CelasLosSantos[rand][0], CelasLosSantos[rand][1], CelasLosSantos[rand][2], 0.0, 6, 3, ENTROU_PROPRIEDADE);
	}
	else if(IsPlayerInRangeOfPoint(playerid, 50.0, -1606.1379, 673.2057, -5.2422)) // San Fierro
	{
		new rand = random(sizeof(CelasSF));
		Teleport(playerid, CelasSF[rand][0], CelasSF[rand][1], CelasSF[rand][2], 0.0, 10, 5, ENTROU_PROPRIEDADE);
	}
	else if(IsPlayerInRangeOfPoint(playerid, 50.0, 2282.2183, 2424.3428, 3.4766)) // Las Venturas
	{
		new rand = random(sizeof(CelasLV));
		Teleport(playerid, CelasLV[rand][0], CelasLV[rand][1], CelasLV[rand][2], 0.0, 3, 11, ENTROU_PROPRIEDADE);
	}
	else
	{
		new rand = random(sizeof(CelasLosSantos));
		Teleport(playerid, CelasLosSantos[rand][0], CelasLosSantos[rand][1], CelasLosSantos[rand][2], 0.0, 6, 3, ENTROU_PROPRIEDADE);
	}
	return true;
}


stock function PM::PrenderJogador(pmid, playerid)
{
	new level = GetPlayerWantedLevel(playerid), valor = (level * 150), tempo = (level * 220);
	
	valor = ( valor > 15000 ? 15000 : valor);

	if(call::ADMIN->GetPlayerAdminLevel(pmid) >= AJUDANTE)
	{
		call::PLAYER->SetPlayerVarInt(playerid, Preso, PRESO_ADM);
	}
	else
	{
		call::PLAYER->SetPlayerVarInt(playerid, Preso, PRESO_POLICIA);
	}

	call::PLAYER->SetPlayerVarInt(playerid, TempoPreso, tempo);

	EnviarCela(playerid);

	GivePlayerMoney(pmid, valor);
	SendClientMessage(pmid, COR_SISTEMA, "» {"COR_BRANCO_INC"}Você prendeu um(a) fugitivo(a) e recebeu {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"}.", RealStr(valor));
	call::PLAYER->SetPlayerVarBool(pmid, Perseguindo, false);

	SendClientMessageToAll(COR_SISTEMA, "» O(A) fugitivo(a) %s foi preso pelo policial %s.", GetUserName(playerid), GetUserName(pmid));

	call::PM->ShowPlayerTextDrawPrisonTime(playerid);
	SetPlayerHealth(playerid, 1000);
	TogglePlayerControllable(playerid, true);

	SetPlayerSpecialAction(playerid, SPECIAL_ACTION_NONE), call::PLAYER->SetPlayerVarInt(playerid, Algemado, 0), call::PLAYER->SetPlayerVarBool(playerid, Perseguido, false);
	SendClientMessage(playerid, COR_SISTEMA, "» {"COR_BRANCO_INC"}Você foi preso pelo Policial Militar {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"}.", GetUserName(pmid));

	if(TimerPerseguicao[pmid] != -1)
		KillTimer(TimerPerseguicao[pmid]), TimerPerseguicao[pmid]=-1;

	if(TimerPerseguicao[playerid] != -1)
		KillTimer(TimerPerseguicao[playerid]), TimerPerseguicao[playerid]=-1;

	if ( Localizando[pmid] != 0)
		KillTimer(Localizando[pmid]), Localizando[pmid]=0, RemovePlayerMapIcon(pmid, 15);

	/**
	*
	*	resetar estrelas e armas.
	*
	**/
	ResetPlayerWeapons(playerid);
	if( Jogador[playerid][Preso] == PRESO_POLICIA )
	{
		SetPlayerWantedLevel(playerid, 0);
	}
	return true;
}

stock function PM::SoltarPrisioneiro(playerid)
{
	PlayerTextDrawDestroy(playerid, TimePrison[playerid]);
	TimePrison[playerid] = PlayerText:INVALID_TEXT_DRAW;

	Jogador[playerid][TempoPreso] = 0;
	Jogador[playerid][Preso] = SOLTO;

	if( IsPlayerInPlace(playerid, 261.6254, 75.6872, 266.7498, 90.1865) ) // Delegacia de Los Santos
	{
		Teleport(playerid, 269.0433, 91.4717, 1001.0391, 93.7280, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid), Jogador[playerid][Entrou]);
	}

	new query[128];
	mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `tempo_preso`='0',  `preso`='0' WHERE `id`='%d' LIMIT 1;", Jogador[playerid][PlayerID]);
	mysql_tquery(getConexao(), query);

	GameTextForPlayer(playerid, "~y~LIBERADO!!!", 2000, 4);
	SetPlayerHealth(playerid, 100);
	return true;
}

stock function PM::ShowPlayerTextDrawPrisonTime(playerid)
{
	if ( INVALID_TEXT_DRAW == _:TimePrison[playerid])
	{
		TimePrison[playerid] = CreatePlayerTextDraw(playerid, 142.500000, 351.583251, "tempo~n~00:00:00");
		PlayerTextDrawLetterSize(playerid, TimePrison[playerid], 0.239500, 1.034166);
		PlayerTextDrawAlignment(playerid, TimePrison[playerid], 1);
		PlayerTextDrawColor(playerid, TimePrison[playerid], -1);
		PlayerTextDrawSetShadow(playerid, TimePrison[playerid], 1);
		PlayerTextDrawSetOutline(playerid, TimePrison[playerid], 0);
		PlayerTextDrawBackgroundColor(playerid, TimePrison[playerid], 255);
		PlayerTextDrawFont(playerid, TimePrison[playerid], 2);
		PlayerTextDrawSetProportional(playerid, TimePrison[playerid], 1);
		PlayerTextDrawSetShadow(playerid, TimePrison[playerid], 1);
	}
	PlayerTextDrawShow(playerid, TimePrison[playerid]);
	return true;
}


stock function PM::FinalizarPerseguicao(playerid, reason)
{
	if ( call::PLAYER->GetPlayerVarInt(playerid, Perseguido) )
	{

		switch(reason)
		{
			case 1: // disconnect
			{
				/**
				*
				*	resetar variáveis do policial.
				*
				**/
				KillTimer(TimerPerseguicao[PmID[playerid]]);
				TimerPerseguicao[PmID[playerid]] = -1;
				call::PLAYER->SetPlayerVarInt(PmID[playerid], Perseguindo, false);


				call::PM->PrenderJogador(PmID[playerid], playerid);

				/**
				*
				*	resetar variáveis do fugitivo.
				*
				**/
				KillTimer(TimerPerseguicao[playerid]);
				TimerPerseguicao[playerid] = -1;
				PmID[playerid] = INVALID_PLAYER_ID;
				call::PLAYER->SetPlayerVarInt(playerid, Perseguido, false);
			}
		}
	}
	return true;
}

hook OnTimeOneSecondsPlayer(playerid)
{
	if ( Jogador[playerid][Preso] >= 1 )
	{
		if(GetPlayerAFK(playerid))
		{
			PlayerTextDrawSetString(playerid, TimePrison[playerid], "tempo~n~~h~~r~paused");
			return Y_HOOKS_CONTINUE_RETURN_1;
		}
		
		new str[30], hours, minutes, seconds;
		Jogador[playerid][TempoPreso] -= 1;
		formatSeconds(Jogador[playerid][TempoPreso], hours, minutes, seconds);

		format(str, sizeof(str), "tempo~n~%02d:%02d:%02d", hours, minutes, seconds);
		PlayerTextDrawSetString(playerid, TimePrison[playerid], str);
		if ( Jogador[playerid][TempoPreso] <= 0 )
		{
			call::PM->SoltarPrisioneiro(playerid);
			SetPlayerHealth(playerid, 100);
		}
		else
		{
			/**
			*
			*	Vida infitina enquanto esteja preso.
			*
			**/
			SetPlayerHealth(playerid, 1000);
		}
	}
	return Y_HOOKS_CONTINUE_RETURN_1;
}


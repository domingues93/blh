enum e_PESCADOR
{
	Object,
	bool:Working,
	WorkVehicle,
	Result,
	bool:Permission
}
static Pescador[MAX_PLAYERS][e_PESCADOR];

new gzmar;

function PESCADOR::CancelarServico(playerid)
{
	DisablePlayerCheckpoint(playerid), DestroyObject(Pescador[playerid][Object]);
	RemovePlayerMapIcon(playerid, 99), GangZoneHideForPlayer(playerid, gzmar);
	new r[e_PESCADOR];
	Pescador[playerid] = r;
	return true;
}

#include <YSI\y_hooks>

hook OnGameModeInit()
{
	gzmar = GangZoneCreate(2676.1719, -2929.1147, 2775.1821, -3027.1753);
	CreateDynamic3DTextLabel("Use {"COR_AMARELO_INC"}/pegarpesca{"COR_BRANCO_INC"} para pegar o local da pesca.", COR_BRANCO, 2739.3804, -2550.5669, 13.6461, 10.0, .streamdistance=10.0, .testlos=1);

	// veiculos da profissão
	call::JOB->SetVehicleJob(CreateVehicle(453, 2722.5901, -2586.4531, -0.4271, 179.5034,   0, 0, TIME_VEHICLE_SPAWN), PESCADOR); // 
	call::JOB->SetVehicleJob(CreateVehicle(453, 2728.9075,-2586.8345,-0.7405,182.5293, 0, 0, TIME_VEHICLE_SPAWN), PESCADOR); // 
	call::JOB->SetVehicleJob(CreateVehicle(453, 2736.0645, -2587.4424, 0.1054, 184.8322, 0, 0, TIME_VEHICLE_SPAWN), PESCADOR); // 
	call::JOB->SetVehicleJob(CreateVehicle(453, 2743.9844, -2586.7209, -0.1946, 178.9491, 0, 0, TIME_VEHICLE_SPAWN), PESCADOR); //
	call::JOB->SetVehicleJob(CreateVehicle(453, 2750.3582, -2587.1455, -0.7487, 178.4601, 0, 0, TIME_VEHICLE_SPAWN), PESCADOR); //

}

hook OnPlayerEnterCheckpoint(playerid)
{
	if(IsPlayerInRangeOfPoint(playerid, 6.0, 2778.9761, -2570.8940, -0.8988))
	{
		new vehicleid = Pescador[playerid][WorkVehicle];

		if(call::JOB->GetVehicleJob(vehicleid) == PESCADOR)
		{
			SetVehicleToRespawn(vehicleid);

			SendClientMessage(playerid, COR_BRANCO, "* Você vendeu um(a) {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"} e recebeu: {"COR_VERDE_INC"}%s{"COR_BRANCO_INC"}.", GivePeixeName(playerid), RealStr(GivePeixeValue(playerid)));
			call::PESCADOR->CancelarServico(playerid);
		}
	}
	return Y_HOOKS_CONTINUE_RETURN_1;
}

CMD:pegarpesca(playerid, params[])
{
	if ( Jogador[playerid][Profissao] != PESCADOR )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não faz parte da profissão Pescador");

	if(Pescador[playerid][Permission] != false)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você já tem permissão para a pesca.");

	if(!call::JOB->IsPlayerInWorking(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está trabalhando para usar este comando.");

	if(!IsPlayerInRangeOfPoint(playerid, 5.0, 2739.3804, -2550.5669, 13.6461)) 
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está no local de solicitar a pesca.");

	SetPlayerMapIcon(playerid, 99, 2718.5708, -2978.9954, -0.2439, 9, 0, MAPICON_GLOBAL);
	GangZoneShowForPlayer(playerid, gzmar, COR_AZUL);
	SendClientMessage(playerid, COR_BRANCO, "* Foi marcado o ponto de pesca em seu mapa, siga para lá com um barco de pesca.");
	Pescador[playerid][Permission] = true;
	return true;
}

CMD:arpao(playerid, params[])
{
	new vehicleid = GetPlayerVehicleProx(playerid), Float:pos[3];

	GetPlayerPos(playerid, pos[0],pos[1],pos[2]);

	if ( Jogador[playerid][Profissao] != PESCADOR )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não faz parte da profissão Pescador");

	if(Pescador[playerid][Permission] != true)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não tem permissão para a pesca.");

	if(!call::JOB->IsPlayerInWorking(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está trabalhando para usar este comando.");

	if ( call::JOB->GetVehicleJob(vehicleid) != PESCADOR )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está em um caminhão de Pescador.");

	if(Pescador[playerid][Working] == true)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você já lançou o arpão recentemente.");

	if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER || GetPlayerState(playerid) == PLAYER_STATE_PASSENGER) 
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você precisa estar no convés do barco para lançar o arpão.");

	if(!IsPlayerInRangeOfPoint(playerid, 50.0, 2718.5708, -2978.9954, -0.2439))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está no local de pesca.");

	if(GetVehicleDistanceFromPoint(vehicleid, pos[0], pos[1], pos[2]) < 4.0)
	{
		SetPlayerAttachedObject(playerid, 30, 2036, 2, -0.003002, 0.819994,  0.012999, -176.2, -4.00001, -74.7001, 0.777, 0.771999, 1);
		ApplyAnimation(playerid,"ped", "ARRESTgun", 4.0,0,1,1,1,1);
		SetTimerEx("Pescando", 10000, false, "d", playerid);
		Pescador[playerid][Working] = true, Pescador[playerid][WorkVehicle] = vehicleid;
	}
	else
	{
		SendClientMessage(playerid, COR_ERRO, "Erro: Você precisa estar no convés do barco para lançar o arpão.");
	}
	return true;
}

forward Pescando(playerid);
public Pescando(playerid)
{
	new peixeid = random(6, 1), vehicleid = GetPlayerVehicleProx(playerid);

	switch(peixeid)
	{
		case 1:
		{
			ClearAnimations(playerid);
			ApplyAnimation(playerid, "CARRY", "crry_prtial", 1.0, 0, 0, 0, 0, 0);
			RemovePlayerAttachedObject(playerid, 30);
			SetPlayerCheckpoint(playerid, 2778.9761, -2570.8940, -0.8988, 6.0);
			Pescador[playerid][Object] = CreateObject(1609, 0, 0, -1000, 0, 0, 0, 100);
			AttachObjectToVehicle(Pescador[playerid][Object], vehicleid, -0.075000, -6.524995, 2.175000, 80.999992, 0.000000, 0.000000);
			SendClientMessage(playerid, COR_BRANCO, "* Você pescou uma Tartaruga, leve a para Ocean Docs para vendê-la.");
			Pescador[playerid][Result] = 1, Pescador[playerid][Working] = true;
		}
		case 2:
		{
			ClearAnimations(playerid);
			ApplyAnimation(playerid, "CARRY", "crry_prtial", 1.0, 0, 0, 0, 0, 0);
			RemovePlayerAttachedObject(playerid, 30);
			SetPlayerCheckpoint(playerid, 2778.9761, -2570.8940, -0.8988, 6.0);
			Pescador[playerid][Object] = CreateObject(1608, 0, 0, -1000, 0, 0, 0, 100);
			AttachObjectToVehicle(Pescador[playerid][Object], vehicleid, -0.075000, -6.524995, 2.175000, 80.999992, 0.000000, 0.000000);
			SendClientMessage(playerid, COR_BRANCO, "* Você pescou um Tubarão, leve o para Ocean Docs para vendê-lo.");
			Pescador[playerid][Result] = 2, Pescador[playerid][Working] = true;
		}
		case 3:
		{
			ClearAnimations(playerid);
			ApplyAnimation(playerid, "CARRY", "crry_prtial", 1.0, 0, 0, 0, 0, 0);
			RemovePlayerAttachedObject(playerid, 30);
			SetPlayerCheckpoint(playerid, 2778.9761, -2570.8940, -0.8988, 6.0);
			Pescador[playerid][Object] = CreateObject(902, 0, 0, -1000, 0, 0, 0, 100);
			AttachObjectToVehicle(Pescador[playerid][Object], vehicleid, -0.075000, -6.524995, 2.175000, 80.999992, 0.000000, 0.000000);
			SendClientMessage(playerid, COR_BRANCO, "* Você pescou uma Estrela, leve a para Ocean Docs para vendê-lo.");
			Pescador[playerid][Result] = 3, Pescador[playerid][Working] = true;
		}
		case 4:
		{
			ClearAnimations(playerid);
			ApplyAnimation(playerid, "CARRY", "crry_prtial", 1.0, 0, 0, 0, 0, 0);
			RemovePlayerAttachedObject(playerid, 30);
			SetPlayerCheckpoint(playerid, 2778.9761, -2570.8940, -0.8988, 6.0);
			Pescador[playerid][Object] = CreateObject(1604, 0, 0, -1000, 0, 0, 0, 100);
			AttachObjectToVehicle(Pescador[playerid][Object], vehicleid, -0.075000, -6.524995, 2.175000, 80.999992, 0.000000, 0.000000);
			SendClientMessage(playerid, COR_BRANCO, "* Você pescou um Pacu, leve o para Ocean Docs para vendê-lo.");
			Pescador[playerid][Result] = 4, Pescador[playerid][Working] = true;
		}
		case 5:
		{
			ClearAnimations(playerid);
			ApplyAnimation(playerid, "CARRY", "crry_prtial", 1.0, 0, 0, 0, 0, 0);
			RemovePlayerAttachedObject(playerid, 30);
			SetPlayerCheckpoint(playerid, 2778.9761, -2570.8940, -0.8988, 6.0);
			Pescador[playerid][Object] = CreateObject(1599, 0, 0, -1000, 0, 0, 0, 100);
			AttachObjectToVehicle(Pescador[playerid][Object], vehicleid, -0.075000, -6.524995, 2.175000, 80.999992, 0.000000, 0.000000);
			SendClientMessage(playerid, COR_BRANCO, "* Você pescou uma Tilápia, leve a para Ocean Docs para vendê-lo.");
			Pescador[playerid][Result] = 5, Pescador[playerid][Working] = true;
		}
		case 6:
		{
			ClearAnimations(playerid);
			ApplyAnimation(playerid, "CARRY", "crry_prtial", 1.0, 0, 0, 0, 0, 0);
			RemovePlayerAttachedObject(playerid, 30);
			SetPlayerCheckpoint(playerid, 2778.9761, -2570.8940, -0.8988, 6.0);
			Pescador[playerid][Object] = CreateObject(19630, 0, 0, -1000, 0, 0, 0, 100);
			AttachObjectToVehicle(Pescador[playerid][Object], vehicleid, -0.075000, -6.524995, 2.175000, 80.999992, 0.000000, 0.000000);
			SendClientMessage(playerid, COR_BRANCO, "* Você pescou um Tucunaré, leve o para Ocean Docs para vendê-lo.");
			Pescador[playerid][Result] = 6, Pescador[playerid][Working] = true;
		}
		default:
		{
			ClearAnimations(playerid);
			ApplyAnimation(playerid, "CARRY", "crry_prtial", 1.0, 0, 0, 0, 0, 0);
			RemovePlayerAttachedObject(playerid, 30);
			SendClientMessage(playerid, COR_ERRO, "Erro: Você não teve êxito em sua pesca, jogue o arpão novamente.");
			Pescador[playerid][Working] = false;
		}
	}
	return true;
}

stock GivePeixeValue(playerid)
{
	new peixeid = Pescador[playerid][Result], valor;

	switch(peixeid)
	{
		case 1: return valor = random(600, 500);
		case 2: return valor = random(800, 700);
		case 3: return valor = random(500, 300);
		case 4: return valor = random(400, 250);
		case 5: return valor = random(350, 150);
		case 6: return valor = random(250, 100);
	}
	return valor;
}

stock GivePeixeName(playerid)
{
	new peixeid = Pescador[playerid][Result], name[24];

	switch(peixeid)
	{
		case 1: name = "Tartatura";
		case 2: name = "Tubarão";
		case 3: name = "Estrela";
		case 4: name = "Pacu";
		case 5: name = "Tilápia";
		case 6: name = "Tucunaré";
	}
	return name;
}
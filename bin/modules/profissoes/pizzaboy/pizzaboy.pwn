
// ============================== [ VARIAVEIS ] ============================== //

#define MAXIMO_ENTREGAS 			6

#define LoopEntregasPizza(%0) 		for(new %0; %0 <= MAXIMO_ENTREGAS; %0++)

static 
	CP_PEGAR_PIZZA
;

enum e_PIZZAS_INFO
{
	EmpresaID,
	Pizzas,
	Endereco[MAXIMO_ENTREGAS + 1],
	ValorEntrega[MAXIMO_ENTREGAS + 1],
	Select[MAXIMO_ENTREGAS + 1]
}
static 
	Pizza[MAX_PLAYERS][e_PIZZAS_INFO];


stock function PIZZA::CancelarEntrega(playerid)
{
	new reset[e_PIZZAS_INFO];
	Pizza[playerid] = reset;

	if(IsPlayerAttachedObjectSlotUsed(playerid, 1))
			RemovePlayerAttachedObject(playerid, 1);

	if ( call::PLAYER->GetPlayerRequestGPS(playerid) )
		call::PLAYER->DisablePlayerGPS(playerid);
	
	LoopEntregasPizza(i){
		Pizza[playerid][Endereco][i] = INVALID_HOUSE_ID;
	}
	return true;
}
// ============================== [ HOOKS ] ============================== //

#include <YSI_Coding\y_hooks>
hook OnGameModeInit()
{
	call::JOB->SetVehicleJob(CreateVehicle(448, 2097.5735, -1792.2543, 13.0780, 93.6600, -1, -1, TIME_VEHICLE_SPAWN), PIZZA_BOY);
	call::JOB->SetVehicleJob(CreateVehicle(448, 2097.4282, -1793.7966, 13.0780, 88.2625, -1, -1, TIME_VEHICLE_SPAWN), PIZZA_BOY);
	call::JOB->SetVehicleJob(CreateVehicle(448, 2097.1970, -1795.4265, 13.0780, 88.6988, -1, -1, TIME_VEHICLE_SPAWN), PIZZA_BOY);
	call::JOB->SetVehicleJob(CreateVehicle(448, 2097.1338, -1796.7592, 13.0780, 89.3693, -1, -1, TIME_VEHICLE_SPAWN), PIZZA_BOY);
	call::JOB->SetVehicleJob(CreateVehicle(448, 2096.9536, -1798.3271, 13.0780, 89.6760, -1, -1, TIME_VEHICLE_SPAWN), PIZZA_BOY);
	call::JOB->SetVehicleJob(CreateVehicle(448, 2096.7627, -1799.9895, 13.0780, 91.2128, -1, -1, TIME_VEHICLE_SPAWN), PIZZA_BOY);
	call::JOB->SetVehicleJob(CreateVehicle(448, 2096.6785, -1801.5847, 13.0780, 91.3361, -1, -1, TIME_VEHICLE_SPAWN), PIZZA_BOY);

	call::JOB->SetVehicleJob(CreateVehicle(448, 2097.4998, -1813.3033, 12.8771, 451.9853, -1, -1, TIME_VEHICLE_SPAWN), PIZZA_BOY);
	call::JOB->SetVehicleJob(CreateVehicle(448, 2097.4663, -1812.2346, 12.8771, 451.9853, -1, -1, TIME_VEHICLE_SPAWN), PIZZA_BOY);
	call::JOB->SetVehicleJob(CreateVehicle(448, 2097.4561, -1814.6576, 12.8771, 451.9853, -1, -1, TIME_VEHICLE_SPAWN), PIZZA_BOY);
	call::JOB->SetVehicleJob(CreateVehicle(448, 2097.4211, -1815.9635, 12.8771, 451.9853, -1, -1, TIME_VEHICLE_SPAWN), PIZZA_BOY);
	call::JOB->SetVehicleJob(CreateVehicle(448, 2097.3442, -1817.2083, 12.8771, 451.9853, -1, -1, TIME_VEHICLE_SPAWN), PIZZA_BOY);
	call::JOB->SetVehicleJob(CreateVehicle(448, 2097.4160, -1818.2175, 12.8771, 451.9853, -1, -1, TIME_VEHICLE_SPAWN), PIZZA_BOY);
	call::JOB->SetVehicleJob(CreateVehicle(448, 2097.2913, -1819.3678, 12.8771, 451.9853, -1, -1, TIME_VEHICLE_SPAWN), PIZZA_BOY);
	call::JOB->SetVehicleJob(CreateVehicle(448, 2097.3279, -1820.4163, 12.8771, 451.9853, -1, -1, TIME_VEHICLE_SPAWN), PIZZA_BOY);
	call::JOB->SetVehicleJob(CreateVehicle(448, 2097.2915, -1821.5560, 12.8771, 451.9853, -1, -1, TIME_VEHICLE_SPAWN), PIZZA_BOY);


	CreateDynamic3DTextLabel("digite {"COR_AMARELO_INC"}/pegarpizzas{"COR_BRANCO_INC"} para pegar as pizzas a ser entregues.", COR_BRANCO, 379.1031, -114.2407, 1001.4922, 20.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, .interiorid=5, .streamdistance=2.0);
	CP_PEGAR_PIZZA = CreateDynamicCP(379.1031, -114.2407, 1001.4922, 1.1, .interiorid=5, .streamdistance=10.0);
}

hook OnPlayerConnect(playerid)
{
	call::PIZZA->CancelarEntrega(playerid);
	return true;
}

hook OnPlayerDeath(playerid, killerid, reason)
{
	if(Pizza[playerid][Pizzas])
		SendClientMessage(playerid, COR_AVISO, "Suas entregas de Pizza foram canceladas.");
	
	call::PIZZA->CancelarEntrega(playerid);
	return true;
}

hook OnPlayerStateChange(playerid, newstate, oldstate)
{
	if(call::PLAYER->GetPlayerVarInt(playerid, Profissao) != PIZZA_BOY || Pizza[playerid][Pizzas] <= 0)
		return Y_HOOKS_CONTINUE_RETURN_1;

	if(newstate == PLAYER_STATE_DRIVER)
	{
		if(GetVehicleModel(GetPlayerVehicleID(playerid)) != 448)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� tem que pegar uma moto dos PizzaBoy's para efetuar a entrega."), RemovePlayerFromVehicle(playerid), Y_HOOKS_BREAK_RETURN_1;

		if(IsPlayerAttachedObjectSlotUsed(playerid, 1))
			RemovePlayerAttachedObject(playerid, 1);
	}
	else if(newstate == PLAYER_STATE_ONFOOT)
	{
		if(!IsPlayerAttachedObjectSlotUsed(playerid, 1))
			SetPlayerAttachedObject(playerid, 1, 19571, 6, 0.057000, -0.132000, 0.094999, -20.900054, -6.800011, -92.699958, 1.000000, 1.000000, 1.000000, 0, 0);
	}
	return Y_HOOKS_CONTINUE_RETURN_1;
}

// ============================== [ COMANDOS ] ============================== //

CMD:pegarpizzas(playerid)
{
	if(!call::JOB->IsPlayerInWorking(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� trabalhando.");

	if(call::PLAYER->GetPlayerVarInt(playerid, Profissao) != PIZZA_BOY)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o faz parte da profiss�o de Pizzaboy");

	if(!IsPlayerInDynamicCP(playerid, CP_PEGAR_PIZZA))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� no local de pegar a pizza.");

	if(Pizza[playerid][Pizzas] > 0)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� j� pegou suas pizzas.");

	new entregas = Pizza[playerid][Pizzas] = random(MAXIMO_ENTREGAS, 2), casaid,
		Float:hX, Float:hY, Float:hZ, Float:dist, Float:bX, Float:bY, Float:bZ;

	Pizza[playerid][EmpresaID] = GetPlayerVirtualWorld(playerid);
	
	LoopEntregasPizza(i)
	{
		Pizza[playerid][Endereco][i] = INVALID_HOUSE_ID;
	}

	for(new i; i < entregas; i++)
	{
		casaid = random(Iter_Count(Casas));
		call::HOUSE->GetHousePos(casaid, hX, hY, hZ);

		Pizza[playerid][Endereco][i] = casaid;

		call::BUSINESS->GetBusinessPos(Pizza[playerid][EmpresaID], bX, bY, bZ);

		dist = GetDistanceBetweenPoints(bX, bY, bZ, hX, hY, hZ);
		Pizza[playerid][ValorEntrega][i] = (floatround(dist) * 20 / 100);
	}
	SetPlayerAttachedObject(playerid, 1, 19571, 6, 0.057000, -0.132000, 0.094999, -20.900054, -6.800011, -92.699958, 1.000000, 1.000000, 1.000000, 0, 0);

	ShowPlayerDialog(playerid, 0, DIALOG_STYLE_MSGBOX, "ENTREGAS DE PIZZA", 
		"{"COR_BRANCO_INC"}\tVoc� pegou {"COR_AMARELO_INC"}%d{"COR_BRANCO_INC"} pizzas para entregar.\
		\n\nDigite {"COR_AMARELO_INC"}/entregas{"COR_BRANCO_INC"} para ver seu romaneio de entregas.\
		\n\nDigite {"COR_AMARELO_INC"}/entregarpizza{"COR_BRANCO_INC"} quando estiver na resid�ncia para entregar a pizza.",
		"OK", "", entregas);
	return true;
}


CMD:entregas(playerid)
{
	if(!call::JOB->IsPlayerInWorking(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� trabalhando.");

	if(call::PLAYER->GetPlayerVarInt(playerid, Profissao) != PIZZA_BOY)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o faz parte da profiss�o de Pizzaboy");

	if(Pizza[playerid][Pizzas] < 1)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem pizzas para entregar.");

	if ( !IsPlayerInAnyVehicle(playerid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� no veiculo da profiss�o.");

	new vehicleid = GetPlayerVehicleID(playerid);
	if ( call::JOB->GetVehicleJob(vehicleid) != PIZZA_BOY)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� no veiculo da profiss�o.");

	new str[800], Float:x, Float:y, Float:z, count;

	str = "Endere�o\tDist�ncia\tValor\n";
	LoopEntregasPizza(i)
	{
		if(Pizza[playerid][Endereco][i] != INVALID_HOUSE_ID)
		{
			call::HOUSE->GetHousePos(Pizza[playerid][Endereco][i], x, y, z);
			format(str, sizeof(str), "%s%s N�%d\tMetro(s) %d\t{"COR_VERDE_INC"}R$%s\n", str, call::HOUSE->GetHouseLocalName(Pizza[playerid][Endereco][i]), Pizza[playerid][Endereco][i], floatround(GetPlayerDistanceFromPoint(playerid, x, y, z)), RealStr(Pizza[playerid][ValorEntrega][i]));
			
			Pizza[playerid][Select][count] = i;
			++count;
		}
	}
	if(count)
		ShowPlayerDialog(playerid, ENTREGAS_DE_PIZZA, DIALOG_STYLE_TABLIST_HEADERS, "ENTREGAS PIZZA", str, "Selecionar", "Fechar");
	else
		ShowPlayerDialog(playerid, 0, DIALOG_STYLE_LIST, "ENTREGAS PIZZA", "{"COR_ERRO_INC"}Voc� n�o tem pizzas a entregar.", "OK", "");
	return true;
}

CMD:entregarpizza(playerid)
{
	if(!call::JOB->IsPlayerInWorking(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� trabalhando.");
		
	if(call::PLAYER->GetPlayerVarInt(playerid, Profissao) != PIZZA_BOY)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o faz parte da profiss�o de Pizzaboy");

	if(Pizza[playerid][Pizzas] < 1)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem pizzas para entregar.");

	if(!IsPlayerAttachedObjectSlotUsed(playerid, 1))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� com a pizza.");

	LoopEntregasPizza(i)
	{
		if ( Pizza[playerid][Endereco][i] == INVALID_HOUSE_ID )continue;
		
		if( Pizza[playerid][Endereco][i] == call::HOUSE->GetHouseIdInRange(playerid, 2.0) )
		{
			--Pizza[playerid][Pizzas];

			
			RemovePlayerAttachedObject(playerid, 1);

			GivePlayerMoney(playerid, Pizza[playerid][ValorEntrega][i]);
			SendClientMessage(playerid, COR_AMARELO, "PIZZA: {"COR_BRANCO_INC"}Voc� recebeu {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"} por entregar essa pizza.", RealStr(Pizza[playerid][ValorEntrega][i]));
	
			Pizza[playerid][ValorEntrega][i] = 0;
			Pizza[playerid][Endereco][i] = INVALID_HOUSE_ID;


			if(Pizza[playerid][Pizzas] <= 0)
			{
				call::PIZZA->CancelarEntrega(playerid);
				SendClientMessage(playerid, COR_BRANCO, "� Voc� entregou todas as Pizzas com {"COR_VERDE_INC"}�xito{"COR_BRANCO_INC"}, volte at� a pizzaria pegar pegar mais pizzas.");
			}
			return true;
		}	
	}
	SendClientMessage(playerid, COR_ERRO, "Erro: Voc� precisa est� proximo a resid�ncia para efetuar a entrega.");
	return true;
}
// ============================== [ DIALOGS ] ============================== //

Dialog:ENTREGAS_DE_PIZZA(playerid, response, listitem, inpttext[])
{
	if(response)
	{
		if(call::PLAYER->GetPlayerVarInt(playerid, Profissao) != PIZZA_BOY)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o faz parte da profiss�o de Pizzaboy");

		if(Pizza[playerid][Pizzas] < 1)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem pizzas para entregar.");

		new i = Pizza[playerid][Select][listitem], Float:x, Float:y, Float:z;

		call::HOUSE->GetHousePos(Pizza[playerid][Endereco][i], x, y, z);
		call::PLAYER->SetPlayerMarkGPS(playerid, x, y, z);

		GameTextForPlayer(playerid, "~y~Local da entrega marcado no radar.", 3000, 3);
		return true;
	}
	return true;
}
#include <YSI_Coding\y_hooks>

static 
	PlayerText:FirePTD[MAX_PLAYERS];



enum e_BOMBEIRO
{
	CompanheiroId,
	bool:Working,
	FireObject,
	FireLife,
	Time,
	WorkVehicle,
	WorkHouse,
	Pay
}
static Bombeiro[MAX_PLAYERS][e_BOMBEIRO];

forward ApagandoFogo(playerid);

hook OnGameModeInit()
{
	call::JOB->SetVehicleJob(CreateVehicle(407, -2064.6946, 51.4032, 28.7446, 272.2909, 3, 3, TIME_VEHICLE_SPAWN), BOMBEIRO);
	call::JOB->SetVehicleJob(CreateVehicle(407, -2064.8955, 56.9623, 28.7467, 271.3896, 3, 3, TIME_VEHICLE_SPAWN), BOMBEIRO);
	call::JOB->SetVehicleJob(CreateVehicle(407, -2064.9619, 69.2078, 28.6338, 271.5610, 3, 3, TIME_VEHICLE_SPAWN), BOMBEIRO);
	call::JOB->SetVehicleJob(CreateVehicle(407, -2064.9824, 75.0724, 28.6269, 269.8289, 3, 3, TIME_VEHICLE_SPAWN), BOMBEIRO);
	call::JOB->SetVehicleJob(CreateVehicle(407, -2064.9470, 80.2984, 28.6256, 270.4295, 3, 3, TIME_VEHICLE_SPAWN), BOMBEIRO);
	call::JOB->SetVehicleJob(CreateVehicle(407, -2064.9607, 86.1980, 28.6250, 269.9244, 3, 3, TIME_VEHICLE_SPAWN), BOMBEIRO);
	call::JOB->SetVehicleJob(CreateVehicle(407, -2064.7776, 91.6433, 28.6270, 269.0984, 3, 3, TIME_VEHICLE_SPAWN), BOMBEIRO);
	call::JOB->SetVehicleJob(CreateVehicle(407, -2064.7151, 97.4238, 28.6248, 264.1002, 3, 3, TIME_VEHICLE_SPAWN), BOMBEIRO);
	call::JOB->SetVehicleJob(CreateVehicle(407, -2064.8779, 63.5667, 28.6859, 269.1982, 3, 3, TIME_VEHICLE_SPAWN), BOMBEIRO);
	return Y_HOOKS_CONTINUE_RETURN_1;
}

hook OnPlayerConnect(playerid)
{
	Bombeiro[playerid][CompanheiroId] = INVALID_PLAYER_ID;
}

hook OnPlayerDisconnect(playerid, reason)
{
	if ( !Bombeiro[playerid][Working] )return Y_HOOKS_CONTINUE_RETURN_1;
	
	if ( Bombeiro[playerid][CompanheiroId] != INVALID_PLAYER_ID )
	{
		SendClientMessage(Bombeiro[playerid][CompanheiroId], COR_ERRO, "Erro: Seu companheiro de trabalho sai do servidor e o serviço foi cancelado.");
		call::BOMBEIRO->CancelarServico(Bombeiro[playerid][CompanheiroId]);
	}
	call::BOMBEIRO->CancelarServico(playerid);
	return Y_HOOKS_CONTINUE_RETURN_1;
}

hook OnPlayerDeath(playerid, killerid, reason)
{
	if ( !Bombeiro[playerid][Working] )return Y_HOOKS_CONTINUE_RETURN_1;

	if ( playerid != INVALID_PLAYER_ID )
	{
		if ( Bombeiro[playerid][CompanheiroId] != INVALID_PLAYER_ID )
		{
			SendClientMessage(Bombeiro[playerid][CompanheiroId], COR_ERRO, "Erro: Seu companheiro de trabalho foi morto e o serviço foi cancelado.");
			call::BOMBEIRO->CancelarServico(Bombeiro[playerid][CompanheiroId]);
		}
		call::BOMBEIRO->CancelarServico(playerid);
	}

	return Y_HOOKS_CONTINUE_RETURN_1;	
}


hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	if( PRESSING(newkeys, KEY_FIRE))
	{
		if ( GetPlayerWeapon(playerid) != 42 || Jogador[playerid][Profissao] != BOMBEIRO || !call::JOB->IsPlayerInWorking(playerid) )
		{
			return Y_HOOKS_CONTINUE_RETURN_1;
		}

		new casaid = Bombeiro[playerid][WorkHouse];
		if ( Bombeiro[playerid][Working] && IsPlayerInRangeOfPoint(playerid, 5.0, Casa[casaid][Entrada][X], Casa[casaid][Entrada][Y], Casa[casaid][Entrada][Z]) )
		{
			if ( Bombeiro[playerid][FireLife] == -1 )
			{
				SendClientMessage(playerid, COR_ERRO, "Erro: O Seu companheiro de profissão que deve apagar o fogo");
				return Y_HOOKS_BREAK_RETURN_1;
			}

			KillTimer(Bombeiro[playerid][Time]);
			Bombeiro[playerid][Time] = SetTimerEx("ApagandoFogo", 1000, true, "i", playerid);
			return Y_HOOKS_BREAK_RETURN_1;
		}
	}
	if ( RELEASED(KEY_FIRE) )
	{
		if ( Bombeiro[playerid][Time] != 0 )
		{
			KillTimer(Bombeiro[playerid][Time]);
			Bombeiro[playerid][Time] = 0;
			return Y_HOOKS_BREAK_RETURN_1;
		}
	}
	return Y_HOOKS_CONTINUE_RETURN_1;
}

public ApagandoFogo(playerid)
{
	if ( Bombeiro[playerid][Working] )
	{
		new id = Bombeiro[playerid][CompanheiroId];
		if ( GetPlayerState(id) != PLAYER_STATE_DRIVER || !IsPlayerInVehicle(id, Bombeiro[id][WorkVehicle]) )
		{
			KillTimer(Bombeiro[playerid][Time]);
			Bombeiro[playerid][Time] = 0;

			SendClientMessage(playerid, COR_ERRO, "Erro: Seu companheiro de trabalho deve ficar no caminhão dos bombeiros enquanto você apaga o fogo.");
			return 1;
		}


		new string[128];
		Bombeiro[playerid][FireLife] -= random(3, 1);
		format(string, sizeof(string), "Fogo: %d%", Bombeiro[playerid][FireLife]);
		
		UpdateFireTD(playerid, string);
		UpdateFireTD(id, string);

		if ( Bombeiro[playerid][FireLife] < 1 )
		{
			Bombeiro[playerid][FireLife] = 0;
			KillTimer(Bombeiro[playerid][Time]);

			GivePlayerMoney(playerid, Bombeiro[playerid][Pay], true);
			GivePlayerMoney(id, Bombeiro[playerid][Pay], true);

			SendClientMessage(id, COR_SISTEMA, "» Volte ao caminhão para iniciar um novo trabalho.");
			SendClientMessage(id, COR_SISTEMA, "» Vocês receberam {"COR_VERDE_INC"}R$%s{"COR_SISTEMA_INC"} cada", RealStr(Bombeiro[playerid][Pay]));

			SendClientMessage(playerid, COR_SISTEMA, "» Fogo apagado, use {"COR_BRANCO_INC"}/pegarincendio{"COR_SISTEMA_INC"} para ir ao próximo incendio.");
			SendClientMessage(playerid, COR_SISTEMA, "» Vocês receberam {"COR_VERDE_INC"}R$%s{"COR_SISTEMA_INC"} cada", RealStr(Bombeiro[playerid][Pay]));

			call::BOMBEIRO->CancelarServico(playerid);
			call::BOMBEIRO->CancelarServico(id);
		}
		return 1;
	}
	return 1;
}

function BOMBEIRO::CancelarServico(playerid)
{
	if ( Bombeiro[playerid][Time] != 0 )
	{
		KillTimer(Bombeiro[playerid][Time]);
	}

	DestroyFireTD(playerid);
	if ( Bombeiro[playerid][FireObject] )
	{
		DestroyObject(Bombeiro[playerid][FireObject]);
	}
	DisablePlayerCheckpoint(playerid);

	new r[e_BOMBEIRO];
	Bombeiro[playerid] = r;
	Bombeiro[playerid][CompanheiroId] = INVALID_PLAYER_ID;
	return true;
}

stock CreateFireTD(playerid)
{
	if ( GetPVarInt(playerid, "CreateFireTD") )return 0;
	
	SetPVarInt(playerid, "CreateFireTD", 1);

	FirePTD[playerid] = CreatePlayerTextDraw(playerid, 119.000000, 378.000000, "FOGO: 0");
	PlayerTextDrawBackgroundColor(playerid, FirePTD[playerid], 253310497);
	PlayerTextDrawFont(playerid, FirePTD[playerid], 1);
	PlayerTextDrawLetterSize(playerid, FirePTD[playerid], 0.210000, 0.599999);
	PlayerTextDrawColor(playerid, FirePTD[playerid], 0xFF6A6AFF);
	PlayerTextDrawSetOutline(playerid, FirePTD[playerid], 0);
	PlayerTextDrawSetProportional(playerid, FirePTD[playerid], 1);
	PlayerTextDrawSetShadow(playerid, FirePTD[playerid], 1);
	PlayerTextDrawUseBox(playerid, FirePTD[playerid], 1);
	PlayerTextDrawBoxColor(playerid, FirePTD[playerid], 219027789);
	PlayerTextDrawTextSize(playerid, FirePTD[playerid], 177.000000, -98.000000);

	PlayerTextDrawShow(playerid, FirePTD[playerid]);
	return 1;
}

stock UpdateFireTD(playerid, string[])
{
	if ( GetPVarInt(playerid, "CreateFireTD") )
	{
		PlayerTextDrawSetString(playerid, FirePTD[playerid], string);
		return 1;
	}
	return 1;
}

stock DestroyFireTD(playerid)
{
	if ( GetPVarInt(playerid, "CreateFireTD") )
	{
		DeletePVar(playerid, "CreateFireTD");
		PlayerTextDrawDestroy(playerid, FirePTD[playerid]);
		FirePTD[playerid] = PlayerText:INVALID_TEXT_DRAW;
		return 1;
	}
	return 0;
}

CMD:pegarincendio(playerid, params[])
{
	if ( !IsPlayerInAnyVehicle(playerid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está em um veiculo.");

	if ( Jogador[playerid][Profissao] != BOMBEIRO )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não faz parte da profissão de Bombeiro");

	if(!call::JOB->IsPlayerInWorking(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está trabalhando para usar este comando.");

	if( Bombeiro[playerid][Working] )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você já tem um incêndio para ser concluido.");

	if ( GetPlayerState(playerid) != PLAYER_STATE_DRIVER )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não é o motorista.");

	new vehicleid = GetPlayerVehicleID(playerid);

	if ( call::JOB->GetVehicleJob(vehicleid) != BOMBEIRO )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está em um caminhão de bombeiro.");

	if ( !IsVehicleSeatOccupied(vehicleid, 1) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você precisa de um parceiro de profissão sentado no banco do carona.");

	foreach(new i: Player)
	{
		if( IsPlayerInVehicle(i, vehicleid) && GetPlayerVehicleSeat(i) == 1 )
		{
			new rand = Iter_Random(Casas), zone[MAX_ZONE_NAME];
			GetLocalName(Casa[rand][Entrada][X], Casa[rand][Entrada][Y], zone, MAX_ZONE_NAME);

			new dist = floatround(GetPlayerDistanceFromPoint(playerid, Casa[rand][Entrada][X], Casa[rand][Entrada][Y], Casa[rand][Entrada][Z]));
			 
			Bombeiro[playerid][WorkHouse] = rand;
			Bombeiro[playerid][CompanheiroId] = i;
			Bombeiro[playerid][Working] = true;
			Bombeiro[playerid][WorkVehicle] = vehicleid;
			Bombeiro[playerid][FireLife] = -1;
			Bombeiro[playerid][Pay] = (dist * 30 / 100);
			CreateFireTD(playerid);
			UpdateFireTD(playerid, "Fogo: 100%");

			Bombeiro[i][WorkHouse] = rand;
			Bombeiro[i][Working] = true;
			Bombeiro[i][CompanheiroId] = playerid;
			Bombeiro[i][WorkVehicle] = vehicleid;
			Bombeiro[i][FireLife] = 100;
			Bombeiro[i][Pay] = (dist * 30 / 100);
			CreateFireTD(i);
			UpdateFireTD(i, "Fogo: 100%");

			GivePlayerWeapon(i, 42, 999);

			Bombeiro[i][FireObject] = CreateObject(18691, Casa[rand][Entrada][X], Casa[rand][Entrada][Y], Casa[rand][Entrada][Z]-2.5, 0.0, 0.0, 0.0);

			SetPlayerCheckpoint(i, Casa[rand][Entrada][X], Casa[rand][Entrada][Y], Casa[rand][Entrada][Z]-2.5, 3.0);
			SetPlayerCheckpoint(playerid, Casa[rand][Entrada][X], Casa[rand][Entrada][Y], Casa[rand][Entrada][Z]-2.5, 3.0);
			return 1;
		}
	}
	return 1;
}

static 
	SendoAssaltado[MAX_PROPERTY];

//
forward ExplosaoCofre(playerid, id, type);

#include <YSI_Coding\y_hooks>

hook OnGameModeInit()
{
	call::JOB->SetVehicleJob(CreateVehicle(468, 2401.8301, -2016.3604, 13.2411, 18.3000, -1, -1, TIME_VEHICLE_SPAWN), ASSALTANTE);
	call::JOB->SetVehicleJob(CreateVehicle(468, 2400.4980, -2016.4412, 13.2411, 18.1200, -1, -1, TIME_VEHICLE_SPAWN), ASSALTANTE);
	call::JOB->SetVehicleJob(CreateVehicle(468, 2398.8918, -2016.5074, 13.2411, 19.0800, -1, -1, TIME_VEHICLE_SPAWN), ASSALTANTE);
	call::JOB->SetVehicleJob(CreateVehicle(468, 2397.3918, -2016.5150, 13.2411, 18.3000, -1, -1, TIME_VEHICLE_SPAWN), ASSALTANTE);
	call::JOB->SetVehicleJob(CreateVehicle(468, 2395.8240, -2016.5044, 13.2411, 22.8600, -1, -1, TIME_VEHICLE_SPAWN), ASSALTANTE);
	call::JOB->SetVehicleJob(CreateVehicle(468, 2394.3574, -2016.6595, 13.2411, 20.4000, -1, -1, TIME_VEHICLE_SPAWN), ASSALTANTE);
	call::JOB->SetVehicleJob(CreateVehicle(468, 2393.0251, -2016.6829, 13.2411, 18.4800, -1, -1, TIME_VEHICLE_SPAWN), ASSALTANTE);
	call::JOB->SetVehicleJob(CreateVehicle(468, 2391.5808, -2016.6262, 13.2411, 19.0200, -1, -1, TIME_VEHICLE_SPAWN), ASSALTANTE);
	call::JOB->SetVehicleJob(CreateVehicle(468, 2390.0981, -2016.7483, 13.2411, 19.6800, -1, -1, TIME_VEHICLE_SPAWN), ASSALTANTE);

	return Y_HOOKS_CONTINUE_RETURN_1;
}

CMD:arrombar(playerid, params[])
{
	if ( call::PLAYER->GetPlayerVarInt(playerid, Profissao) != ASSALTANTE )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o � um assaltante.");

	if ( !IsPlayerInRangeOfPoint(playerid, 2.2, 1413.4065, -308.3211, 4576.0054) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� pr�ximo ao cofre.");
	
	new id = GetPlayerVirtualWorld(playerid);
	
	if ( SendoAssaltado[id] > gettime())
		return SendClientMessage(playerid, COR_ERRO, "Erro: Esse banco j� foi assaltado recentemente, aguarde {"COR_BRANCO_INC"}%d{"COR_ERRO_INC"} segundos", SendoAssaltado[id] - gettime() );


	SendoAssaltado[id] = gettime() + (10 * 60);

	SetTimerEx("ExplosaoCofre", 10000, false, "ddd", playerid, id, 0);
	ApplyAnimation(playerid, "BOMBER", "BOM_PLANT_LOOP", 4.1, 1, 0, 0, 0, 0, 0);
	SetPlayerWantedLevel(playerid, GetPlayerWantedLevel(playerid) + 1);

	SendClientMessageToAll(COR_BRANCO, "O Assaltante {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"} est� arrombando o cofre do {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"}", GetUserName(playerid), Propriedade[id][Nome]);
	return true;
}

public ExplosaoCofre(playerid, id, type)
{
	if ( type == 0) // Plantando
	{
		ClearAnimations(playerid);
		SendClientMessage(playerid, GetPlayerColor(playerid), "Voc� ativou a bomba para explodir o cofre, saia de perto para n�o se ferir.");
		SetTimerEx("ExplosaoCofre", 7000, false, "ddd", playerid, id, 1);
		return true;
	}
	else
	{
		CreateExplosion(1413.4065, -308.3211, 4576.0054, 0, 5.0);
		SendClientMessageToAll(COR_ERRO, "O Assaltante {"COR_BRANCO_INC"}%s{"COR_ERRO_INC"} conseguiu roubar o cofre do {"COR_BRANCO_INC"}%s{"COR_ERRO_INC"}.", GetUserName(playerid), Propriedade[id][Nome]);

		new money = random(8000, 4000);
		GivePlayerMoney(playerid, money);
		SendClientMessage(playerid, COR_BRANCO, "Voc� roubou o banco com {"COR_VERDE_INC"}sucesso{"COR_BRANCO_INC"}, voc� pegou {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"} nesse assalto.", RealStr(money));
		SetPlayerWantedLevel(playerid, GetPlayerWantedLevel(playerid) + 5);
	}
	return true;
}
#include <YSI_Coding\y_hooks>
#include <VCF>

#define	VALOR_CADA_PECA 30

enum RepararVehicle
{
	Grana,
	Pecas
}
new 
	RVehicle[MAX_PLAYERS][RepararVehicle]
;

hook OnGameModeInit()
{
	call::JOB->SetVehicleJob(CreateVehicle(525, 1595.1973, -2159.0459, 13.4312, 89.4450, -1, -1, TIME_VEHICLE_SPAWN), MECANICO);
	call::JOB->SetVehicleJob(CreateVehicle(525, 1595.2180, -2163.7651, 13.4347, 90.1318, -1, -1, TIME_VEHICLE_SPAWN), MECANICO);
	call::JOB->SetVehicleJob(CreateVehicle(525, 1595.2214, -2168.4360, 13.4281, 89.9248, -1, -1, TIME_VEHICLE_SPAWN), MECANICO);
	call::JOB->SetVehicleJob(CreateVehicle(525, 1583.0228, -2158.6980, 13.3923, 268.5335, -1, -1, TIME_VEHICLE_SPAWN), MECANICO);
	call::JOB->SetVehicleJob(CreateVehicle(525, 1582.9304, -2163.4866, 13.4312, 268.9530, -1, -1, TIME_VEHICLE_SPAWN), MECANICO);

	CreateDynamic3DTextLabel("Menu do Mecanico\nAperte a tecla: {"COR_AMARELO_INC"}'Y'", COR_BRANCO, 1566.5634, -2161.7891, 13.5547, 8.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, -1, -1, -1, 8.0);
	return Y_HOOKS_CONTINUE_RETURN_1;
}

hook OnPlayerDisconnect(playerid, reason)
{
	call::MECANICO->CancelarServico(playerid);
	return Y_HOOKS_CONTINUE_RETURN_1;
}

hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	if(PRESSED(KEY_YES))
	{
		if(IsPlayerInRangeOfPoint(playerid, 1.0, 1566.5634, -2161.7891, 13.5547) && (Jogador[playerid][Profissao] == MECANICO))
		{
			if(RVehicle[playerid][Pecas] >= 21)
				return SendClientMessage(playerid, COR_ERRO, "Erro: Você já tem o limite de peças.");

			ShowPlayerDialog(playerid, Menu_Mecanico, DIALOG_STYLE_INPUT, "* Loja de Peças | preço: {088A08}R$"#VALOR_CADA_PECA" cada", "Digite o número de peças a serem compradas.", "Comprar", "Fechar");
		}
		return true;
	}
	return Y_HOOKS_CONTINUE_RETURN_1;
}

function MECANICO::CancelarServico(playerid)
{
	new id = Jogador[playerid][ReparandoVeiculo];
	new r[RepararVehicle], t[StateVehicle];
	RVehicle[playerid] = r, State[id] = t;
	return true;
}

/*CMD:reparar(playerid) 
{
    
    if ( Jogador[playerid][Profissao] != MECANICO)
        return SendClientMessage(playerid, COR_ERRO, "Erro: Você não faz parte da profissão Mecânico.");

    if ( !call::JOB->IsPlayerInWorking(playerid) )
        return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está trabalhando.");

    if(IsPlayerInAnyVehicle(playerid))
        return SendClientMessage(playerid, COR_ERRO, "Erro: Você não pode usar este comando dentro de um veiculo.");

    if(!IsPlayerInRangeOfPoint(playerid, 3.0, 1569.34424, -2156.86060, 12.96720))
        return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está no local de reparação.");
    
    new id = InRangeOfAnyCar(playerid);
    if(id == INVALID_VEHICLE_ID )
        return SendClientMessage(playerid, COR_ERRO, "Você não esta perto de nenhum veiculo.");

    if ( call::CONCE->IsValidVehicleConce(id))
        return SendClientMessage(playerid, COR_ERRO, "Erro: Você não pode reparar esse veiculo."); 

    if(IsVehicleInRangeOfPoint(id, 5.0, 1569.34424, -2156.86060, 12.96720))
    {
        Jogador[playerid][ReparandoVeiculo] = id;
        ShowDialogMenuMechanic(playerid, id);
        
        GetVehicleDoorsDamageStatus(id, State[id][Doors][0], State[id][Doors][1], State[id][Doors][2], State[id][Doors][3]);
        GetVehicleLightsDamageStatus(id, State[id][Lights][0], State[id][Lights][1], State[id][Lights][2], State[id][Lights][3]);
        GetVehicleTiresDamageStatus(id, State[id][Tires][0], State[id][Tires][1], State[id][Tires][2], State[id][Tires][3]);
        return true;
    }

    return true;
}*/

CMD:mecanicos(playerid, params[])
{
	new str[1045], count;
	foreach(new i: Player)
	{
		if ( Jogador[i][Profissao] == MECANICO )
		{
			format(str, sizeof(str), "%s[%d] - {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"}\n", str, i, GetUserName(i));
			count++;
		}
	}
	if(count)
		return ShowPlayerDialog(playerid, 0, DIALOG_STYLE_MSGBOX, "LISTA DE MECÂNICOS", str, "OK", "");

	SendClientMessage(playerid, COR_ERRO, "Não há nenhum Mecânico online.");
	return true;
}

ShowDialogMenuMechanic(playerid, vehicleid) 
{
	new DialogStr[100] = "Componente\tEstado\nPortas\nRodas\nFarois\nCapo\t";

	if(GetDoorState(vehicleid, VCF_DOOR_HOOD) == VCF_DOOR_FIXED)
		strcat(DialogStr, "Fixado\n");
	else if(GetDoorState(vehicleid, VCF_DOOR_HOOD) == VCF_DOOR_OPENED)
		strcat(DialogStr, "Aberto\n");
	else if(GetDoorState(vehicleid, VCF_DOOR_HOOD) == VCF_DOOR_DAMAGED)
		strcat(DialogStr, "Quebrado\n");
	else if(GetDoorState(vehicleid, VCF_DOOR_HOOD) == VCF_DOOR_REMOVED)
		strcat(DialogStr, "Faltando\n");
	else if(GetDoorState(vehicleid, VCF_DOOR_HOOD) == VCF_DOOR_OPENED_DAMAGED)
		strcat(DialogStr, "Quebrado e Aberto\n");

	strcat(DialogStr, "Porta-Malas\t");

	if(GetDoorState(vehicleid, VCF_DOOR_TRUNK) == VCF_DOOR_FIXED)
		strcat(DialogStr, "Fixado\n");
	else if(GetDoorState(vehicleid, VCF_DOOR_TRUNK) == VCF_DOOR_OPENED)
		strcat(DialogStr, "Aberto\n");
	else if(GetDoorState(vehicleid, VCF_DOOR_TRUNK) == VCF_DOOR_DAMAGED)
		strcat(DialogStr, "Quebrado\n");
	else if(GetDoorState(vehicleid, VCF_DOOR_TRUNK) == VCF_DOOR_REMOVED)
		strcat(DialogStr, "Faltando\n");

	strcat(DialogStr, "Concluir\t");

	ShowPlayerDialog(playerid, MENU_MECHANIC_COMPONENTS, DIALOG_STYLE_TABLIST_HEADERS, "Menu do mecanico", DialogStr, "Selecionar", "Sair");

	return true;
}

ShowMenuMechanicDoors(playerid, vehicleid) 
{
	new DialogStr[100] = "Componente\tEstado\nPorta Esquerda\t";

	if(GetDoorState(vehicleid, VCF_DOOR_DRIVER) == VCF_DOOR_FIXED)
		strcat(DialogStr, "Fixado\n");
	else if(GetDoorState(vehicleid, VCF_DOOR_DRIVER) == VCF_DOOR_OPENED)
		strcat(DialogStr, "Aberto\n");
	else if(GetDoorState(vehicleid, VCF_DOOR_DRIVER) == VCF_DOOR_DAMAGED)
		strcat(DialogStr, "Quebrado\n");
	else if(GetDoorState(vehicleid, VCF_DOOR_DRIVER) == VCF_DOOR_REMOVED)
		strcat(DialogStr, "Faltando\n");
	else if(GetDoorState(vehicleid, VCF_DOOR_DRIVER) == VCF_DOOR_OPENED_DAMAGED)
		strcat(DialogStr, "Aberta e Quebrada\n");

	strcat(DialogStr, "Porta Direita\t");

	if(GetDoorState(vehicleid, VCF_DOOR_PASSENGER) == VCF_DOOR_FIXED)
		strcat(DialogStr, "Fixado\n");
	else if(GetDoorState(vehicleid, VCF_DOOR_PASSENGER) == VCF_DOOR_OPENED)
		strcat(DialogStr, "Aberto\n");
	else if(GetDoorState(vehicleid, VCF_DOOR_PASSENGER) == VCF_DOOR_DAMAGED)
		strcat(DialogStr, "Quebrado\n");
	else if(GetDoorState(vehicleid, VCF_DOOR_PASSENGER) == VCF_DOOR_REMOVED)
		strcat(DialogStr, "Faltando\n");
	else if(GetDoorState(vehicleid, VCF_DOOR_PASSENGER) == VCF_DOOR_OPENED_DAMAGED)
		strcat(DialogStr, "Aberta e Quebrada\n");

	ShowPlayerDialog(playerid, MENU_MECHANIC_DOORS, DIALOG_STYLE_TABLIST_HEADERS, "Menu do Mecanico", DialogStr, "Selecionar", "Voltar");
	
	return true;
}

ShowMenuMechanicTires(playerid, vehicleid) 
{
	new DialogStr[150] = "Pneu\tEstado\nDianteiro Esquerda\t";

	if( IsTireInflated(vehicleid, VCF_TIRE_FRONT_LEFT) )
		strcat(DialogStr, "Inflado\n");
	else
		strcat(DialogStr, "Furado\n");

	strcat(DialogStr, "Dianteiro Direito\t");

	if( IsTireInflated(vehicleid, VCF_TIRE_FRONT_RIGHT) )
		strcat(DialogStr, "Inflado\n");
	else
		strcat(DialogStr, "Furado\n");

	strcat(DialogStr, "Traseiro Esquerdo\t");

	if( IsTireInflated(vehicleid, VCF_TIRE_REAR_LEFT) )
		strcat(DialogStr, "Inflado\n");
	else
		strcat(DialogStr, "Furado\n");

	strcat(DialogStr, "Traseiro Direito\t");

	if( IsTireInflated(vehicleid, VCF_TIRE_REAR_RIGHT) )
		strcat(DialogStr, "Inflado\n");
	else
		strcat(DialogStr, "Furado\n");

	ShowPlayerDialog(playerid, MENU_MECHANIC_TIRES, DIALOG_STYLE_TABLIST_HEADERS, "Menu do Mecanico", DialogStr, "Selecionar", "Voltar");

	return true;
}

ShowMenuMechanicLights(playerid, vehicleid)
{
	new DialogStr[150] = "Pneu\tEstado\nDianteiro Esquerdo\t";

	if(IsLightEnabled(vehicleid, VCF_LIGHT_FRONT, VCF_LIGHT_LEFT))
		strcat(DialogStr, "Fixado\n");
	else
		strcat(DialogStr, "Quebrado\n");

	strcat(DialogStr, "Dianteiro Direito\t");

	if(IsLightEnabled(vehicleid, VCF_LIGHT_FRONT, VCF_LIGHT_RIGHT))
		strcat(DialogStr, "Fixado\n");
	else
		strcat(DialogStr, "Quebrado\n");

	strcat(DialogStr, "Traseiro Esquerdo\t");

	if(IsLightEnabled(vehicleid, VCF_LIGHT_BACK, VCF_LIGHT_LEFT))
		strcat(DialogStr, "Fixado\n");
	else
		strcat(DialogStr, "Quebrado\n");

	strcat(DialogStr, "Traseiro Direito\t");

	if(IsLightEnabled(vehicleid, VCF_LIGHT_BACK, VCF_LIGHT_RIGHT))
		strcat(DialogStr, "Fixado\n");
	else
		strcat(DialogStr, "Qubrado\n");

	ShowPlayerDialog(playerid, MENU_MECHANIC_LIGHTS, DIALOG_STYLE_TABLIST_HEADERS, "Menu do Mecnaido", DialogStr, "Selecionar", "Voltar");
	return true;
}

Dialog:MENU_MECHANIC_TIRES(playerid, response, listitem, inputtext[]) 
{
	if(!response)
		return ShowDialogMenuMechanic(playerid, Jogador[playerid][ReparandoVeiculo]);

	switch(listitem) 
	{
		case 0: 
		{
			if(RVehicle[playerid][Pecas] <= 0)
				return SendClientMessage(playerid, COR_ERRO, "Erro: Você não tem peças suficientes.");

			new id = Jogador[playerid][ReparandoVeiculo];
			if( IsTireInflated(id, VCF_TIRE_FRONT_LEFT) ) 
				return SendClientMessage(playerid, COR_ERRO, "Você não pode reparar uma peça já reparada.");

			ToogleTireState(id, VCF_TIRE_FRONT_LEFT);
			SendClientMessage(playerid, COR_AVISO, "O pneu dianteiro esquerdo do veiculo %d foi reparado.", id);
			RVehicle[playerid][Pecas] -= 1, RVehicle[playerid][Grana] += random(280, 150);
			ShowDialogMenuMechanic(playerid, id);
		}
		case 1: 
		{
			if(RVehicle[playerid][Pecas] <= 0)
				return SendClientMessage(playerid, COR_ERRO, "Erro: Você não tem peças suficientes.");

			new id = Jogador[playerid][ReparandoVeiculo];
			if( IsTireInflated(id, VCF_TIRE_FRONT_RIGHT) ) 
				return SendClientMessage(playerid, COR_ERRO, "Você não pode reparar uma peça já reparada.");

			ToogleTireState(id, VCF_TIRE_FRONT_RIGHT);
			SendClientMessage(playerid, COR_AVISO, "O pneu dianteiro direito do veiculo %d foi reparado.", id);
			RVehicle[playerid][Pecas] -= 1, RVehicle[playerid][Grana] += random(280, 150);
			ShowDialogMenuMechanic(playerid, id);

		}
		case 2: 
		{
			if(RVehicle[playerid][Pecas] <= 0)
				return SendClientMessage(playerid, COR_ERRO, "Erro: Você não tem peças suficientes.");

			new id = Jogador[playerid][ReparandoVeiculo];
			if( IsTireInflated(id, VCF_TIRE_REAR_LEFT) ) 
				return SendClientMessage(playerid, COR_ERRO, "Você não pode reparar uma peça já reparada.");

			ToogleTireState(id, VCF_TIRE_REAR_LEFT);
			SendClientMessage(playerid, COR_AVISO, "O pneu traseiro esquerdo do veiculo %d foi reparado.", id);
			RVehicle[playerid][Pecas] -= 1, RVehicle[playerid][Grana] += random(280, 150);
			ShowDialogMenuMechanic(playerid, id);

		}
		case 3: 
		{
			if(RVehicle[playerid][Pecas] <= 0)
				return SendClientMessage(playerid, COR_ERRO, "Erro: Você não tem peças suficientes.");

			new id = Jogador[playerid][ReparandoVeiculo];
			if( IsTireInflated(id, VCF_TIRE_REAR_RIGHT) ) 
				return SendClientMessage(playerid, COR_ERRO, "Você não pode reparar uma peça já reparada.");

			ToogleTireState(id, VCF_TIRE_REAR_RIGHT);
			SendClientMessage(playerid, COR_AVISO, "O pneu traseiro direito do veiculo %d foi reparado.", id);
			RVehicle[playerid][Pecas] -= 1, RVehicle[playerid][Grana] += random(280, 150);
			ShowDialogMenuMechanic(playerid, id);

		}
	}
	return true;

}

Dialog:MENU_MECHANIC_DOORS(playerid, response, listitem, inputtext[]) 
{
	if(!response)
		return ShowDialogMenuMechanic(playerid, Jogador[playerid][ReparandoVeiculo]);

	switch(listitem)
	{
		case 0:
		{
			if(RVehicle[playerid][Pecas] <= 0)
				return SendClientMessage(playerid, COR_ERRO, "Erro: Você não tem peças suficientes.");

			new id = Jogador[playerid][ReparandoVeiculo];
			if(GetDoorState(id, VCF_DOOR_DRIVER) != VCF_DOOR_FIXED) 
			{
				SetDoorState(id, VCF_DOOR_DRIVER, VCF_DOOR_FIXED);
				SendClientMessage(playerid, COR_AVISO, "A porta direita do veiculo %d foi reparada.", id);
				RVehicle[playerid][Pecas] -= 1, RVehicle[playerid][Grana] += random(450, 250);
				ShowDialogMenuMechanic(playerid, id);
			} else 
				SendClientMessage(playerid, COR_ERRO, "Você não pode reparar uma peça já reparada.");
		}
		case 1: 
		{
			if(RVehicle[playerid][Pecas] <= 0)
				return SendClientMessage(playerid, COR_ERRO, "Erro: Você não tem peças suficientes.");

			new id = Jogador[playerid][ReparandoVeiculo];
			if(GetDoorState(id, VCF_DOOR_PASSENGER) != VCF_DOOR_FIXED) 
			{
				SetDoorState(id, VCF_DOOR_PASSENGER, VCF_DOOR_FIXED);
				SendClientMessage(playerid, COR_AVISO, "A porta direita do veiculo %d foi reparada.", id);
				RVehicle[playerid][Pecas] -= 1, RVehicle[playerid][Grana] += random(450, 250);
				ShowDialogMenuMechanic(playerid, id);
			} else 
				SendClientMessage(playerid, COR_ERRO, "Você não pode reparar uma peça já reparada.");
		}
	}
	return true;
}

Dialog:MENU_MECHANIC_LIGHTS(playerid, response, listitem, inputtext[])
{
	if(!response)
		return ShowDialogMenuMechanic(playerid, Jogador[playerid][ReparandoVeiculo]);

	switch(listitem)
	{
		case 0:
		{
			if(RVehicle[playerid][Pecas] <= 0)
				return SendClientMessage(playerid, COR_ERRO, "Erro: Você não tem peças suficientes.");

			new id = Jogador[playerid][ReparandoVeiculo];
			if(IsLightEnabled(id, VCF_LIGHT_FRONT, VCF_LIGHT_LEFT))
				return SendClientMessage(playerid, COR_ERRO, "Você não pode reparar uma peça já reparada.");

			ToogleLightState(id, VCF_LIGHT_FRONT, VCF_LIGHT_LEFT);
			SendClientMessage(playerid, COR_AVISO, "O farol dianteiro esquerdo do veiculo %d foi reparado.", id);
			RVehicle[playerid][Pecas] -= 1, RVehicle[playerid][Grana] += random(150, 50);
			ShowDialogMenuMechanic(playerid, id);
		}
		case 1:
		{
			if(RVehicle[playerid][Pecas] <= 0)
				return SendClientMessage(playerid, COR_ERRO, "Erro: Você não tem peças suficientes.");

			new id = Jogador[playerid][ReparandoVeiculo];
			if(IsLightEnabled(id, VCF_LIGHT_FRONT, VCF_LIGHT_RIGHT))
				return SendClientMessage(playerid, COR_ERRO, "Você não pode reparar uma peça já reparada.");

			ToogleLightState(id, VCF_LIGHT_FRONT, VCF_LIGHT_RIGHT);
			SendClientMessage(playerid, COR_AVISO, "O farol dianteiro direito do veiculo %d foi reparado.", id);
			RVehicle[playerid][Pecas] -= 1, RVehicle[playerid][Grana] += random(150, 50);
			ShowDialogMenuMechanic(playerid, id);
		}
		case 2:
		{
			if(RVehicle[playerid][Pecas] <= 0)
				return SendClientMessage(playerid, COR_ERRO, "Erro: Você não tem peças suficientes.");

			new id = Jogador[playerid][ReparandoVeiculo];
			if(IsLightEnabled(id, VCF_LIGHT_BACK, VCF_LIGHT_LEFT))
				return SendClientMessage(playerid, COR_ERRO, "Você não pode reparar uma peça já reparada.");

			ToogleLightState(id, VCF_LIGHT_BACK, VCF_LIGHT_LEFT);
			SendClientMessage(playerid, COR_AVISO, "O farol traseiro esquerdo do veiculo %d foi reparado.", id);
			RVehicle[playerid][Pecas] -= 1, RVehicle[playerid][Grana] += random(150, 50);
			ShowDialogMenuMechanic(playerid, id);
		}
		case 3:
		{
			if(RVehicle[playerid][Pecas] <= 0)
				return SendClientMessage(playerid, COR_ERRO, "Erro: Você não tem peças suficientes.");

			new id = Jogador[playerid][ReparandoVeiculo];
			if(IsLightEnabled(id, VCF_LIGHT_BACK, VCF_LIGHT_RIGHT))
				return SendClientMessage(playerid, COR_ERRO, "Você não pode reparar uma peça já reparada.");

			ToogleLightState(id, VCF_LIGHT_BACK, VCF_LIGHT_RIGHT);
			SendClientMessage(playerid, COR_AVISO, "O farol traseiro direito do veiculo %d foi reparado.", id);
			RVehicle[playerid][Pecas] -= 1, RVehicle[playerid][Grana] += random(150, 50);
			ShowDialogMenuMechanic(playerid, id);
		}
	}
	return true;
}

Dialog:MENU_MECHANIC_COMPONENTS(playerid, response, listitem, inputtext[]) {

	if(response)
	{
		switch(listitem)
		{
			case 0: // mostrar dialog portas
			{
				ShowMenuMechanicDoors(playerid, Jogador[playerid][ReparandoVeiculo]);
			}
			case 1: // mostrar dialog rodas
			{
				ShowMenuMechanicTires(playerid, Jogador[playerid][ReparandoVeiculo]);
			}
			case 2: // mostrar dialog farois
			{
				ShowMenuMechanicLights(playerid, Jogador[playerid][ReparandoVeiculo]);
			}
			case 3: // capo
			{
				if(RVehicle[playerid][Pecas] <= 0)
					return SendClientMessage(playerid, COR_ERRO, "Erro: Você não tem peças suficientes.");

				new id = Jogador[playerid][ReparandoVeiculo];
				if(GetDoorState(id, VCF_DOOR_HOOD) != VCF_DOOR_FIXED) 
				{
					SetDoorState(id, VCF_DOOR_HOOD, VCF_DOOR_FIXED);
					SendClientMessage(playerid, COR_AVISO, "O capô do veiculo %d foi reparado.", id);
					RVehicle[playerid][Pecas] -= 2, RVehicle[playerid][Grana] += random(380, 180);
					ShowDialogMenuMechanic(playerid, id);
				} else 
					SendClientMessage(playerid, COR_ERRO, "Você não pode reparar uma peça já reparada.");
			}
			case 4: // porta-malas
			{
				if(RVehicle[playerid][Pecas] <= 0)
					return SendClientMessage(playerid, COR_ERRO, "Erro: Você não tem peças suficientes.");

				new id = Jogador[playerid][ReparandoVeiculo];
				if(GetDoorState(id, VCF_DOOR_TRUNK) != VCF_DOOR_FIXED) 
				{
					SetDoorState(id, VCF_DOOR_TRUNK, VCF_DOOR_FIXED);
					SendClientMessage(playerid, COR_AVISO, "O porta-malas do veiculo %d foi reparado.", id);
					RVehicle[playerid][Pecas] -= 2, RVehicle[playerid][Grana] += random(500, 150);
					ShowDialogMenuMechanic(playerid, id);
				} else 
					SendClientMessage(playerid, COR_ERRO, "Você não pode reparar uma peça já reparada.");
			}
			case 5: // conclusão
			{
				if(RVehicle[playerid][Pecas] <= 4)
					return SendClientMessage(playerid, COR_ERRO, "Erro: Você não tem peças suficientes.");

				if(RVehicle[playerid][Grana] <= 0)
					return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está reparando o veiculo para concluir sua reparação.");

				new vehicleid = Jogador[playerid][ReparandoVeiculo], id = GetPlayerIDByName(Vehicle[vehicleid][Owner][Name]);

				SetPVarInt(id, "MecanicoID", playerid), SetPVarInt(id, "MecSolicitado", 1), SetPVarInt(id, "DinheiroReparo", RVehicle[playerid][Grana]);


				SendClientMessage(playerid, COR_AVISO, "* Você está reparando o veiculo de: %s aguarde-o a aceitar o conserto.", GetUserName(id));
				SendClientMessage(id, COR_BRANCO, "* O(A) Mecânico {"COR_VERDE_INC"}%s{"COR_BRANCO_INC"} consertou seu veiculo e ficou no valor de: {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"}.", GetUserName(playerid), RealStr(RVehicle[playerid][Grana]));
				SendClientMessage(id, COR_BRANCO, "* Use: /aceitar conserto ou /recusar conserto.");
			}
			default: SendClientMessage(playerid, COR_ERRO, "Você cancelou a reparação do veículo."), call::MECANICO->CancelarServico(playerid);
		}
	}
	return true;
}

Dialog:Menu_Mecanico(playerid, response, listitem, inputtext[])
{
	if(response)
	{
		if ( isnull(inputtext) || !IsNumeric(inputtext))
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você tem que digitar um número válido.");

		new pecas = strval(inputtext);

		if(pecas <= 0 || pecas >= 21)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você só pode digitar valores entre 1 e 20.");

		if( (VALOR_CADA_PECA * pecas) > GetPlayerMoney(playerid))
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não tem dinheiro suficiente.");

		GivePlayerMoney(playerid, -(VALOR_CADA_PECA * pecas) );
		RVehicle[playerid][Pecas] += strval(inputtext);

		SendClientMessage(playerid, COR_BRANCO, "* Você comprou {"COR_AMARELO_INC"}%d{"COR_BRANCO_INC"} Peças no valor de {088A08}R$%s{"COR_BRANCO_INC"}.", pecas, RealStr(VALOR_CADA_PECA * pecas));
	}
	return true;
}



/**
*
*	forwards
*
**/
forward OnPlayerPay();



enum
{
	HONESTAS = 1,
	CRIMINOSA,
	TRANSPORTE,
	GOVERNAMENTAL
}

enum e_JobInfo
{
	Float:MaletaX,
	Float:MaletaY,
	Float:MaletaZ,
	Float:SkinX,
	Float:SkinY,
	Float:SkinZ,
	sInterior,
	Salario,
	Color,
	Level,
	SkinID,
	Nome[50],
	Categoria
}
new Job[MAX_PROFISSOES][e_JobInfo];


static VehicleJob[MAX_VEHICLES], bool:IsVehicleJob[MAX_VEHICLES], bool:IsPlayerInWorking[MAX_PLAYERS], pSelectCategory[MAX_PLAYERS], SelectedJob[MAX_PLAYERS][25];


#define TIME_VEHICLE_SPAWN 				(300)

// ============================== [ LOAD PROFS ] ============================== //

#include "../bin/modules/profissoes/pizzaboy/pizzaboy.pwn"
#include "../bin/modules/profissoes/caminhoneiro/caminhoneiro.pwn"
#include "../bin/modules/profissoes/taxi/taxi.pwn"
#include "../bin/modules/profissoes/petroleiro/petroleiro.pwn"
#include "../bin/modules/profissoes/dnit/dnit.pwn"
#include "../bin/modules/profissoes/policia/policia.pwn"
#include "../bin/modules/profissoes/assaltante/assaltante.pwn"
#include "../bin/modules/profissoes/assasino/assasino.pwn"
#include "../bin/modules/profissoes/bombeiro/bombeiro.pwn"
#include "../bin/modules/profissoes/transportador de madeira/tdm.pwn"
#include "../bin/modules/profissoes/advogado/advogado.pwn"
#include "../bin/modules/profissoes/mecanico/mecanico.pwn"
#include "../bin/modules/profissoes/plantador/ptd.pwn"
#include "../bin/modules/profissoes/produtor/produtor.pwn"
#include "../bin/modules/profissoes/lenhador/lenhador.pwn"
#include "../bin/modules/profissoes/transportador de drogas/tpd.pwn"
#include "../bin/modules/profissoes/lixeiro/lixeiro.pwn"
#include "../bin/modules/profissoes/Transportardor de Valores/tdv.pwn"
#include "../bin/modules/profissoes/pescador/pescador.pwn"

#define LoopJob(%0)					foreach(new %0: Profissoes)


// ============================== [ CALLBACKs ] ============================== //

function JOB::CountJobs()
{
	return Iter_Count(Profissoes);
}

stock function JOB::ShowPlayerCategoryJobs(playerid)
{
	ShowPlayerDialog(playerid, CATEGORYS_JOBS, DIALOG_STYLE_LIST, "PROFISS�ES", "Profiss�es � Honestas\nProfiss�es � Criminosas\nProfiss�es � Transportes\nProfiss�es � Governamentais", "Selecionar", "Cancelar");
	return true;
}

function JOB::ShowPlayerJobsInfo(playerid, categoria)
{
	new str[512], query[128];
	str = "Emprego\tLevel\tSal�rio\n";

	mysql_format(getConexao(), query, sizeof(query), "SELECT id,nome,level,salario FROM "TABLE_JOBS" WHERE `categoria`='%d' ORDER BY level ASC", categoria);
	new Cache:cache = mysql_query(getConexao(), query, true), rows=cache_num_rows();
	if ( rows )
	{
		new nome[50], level, salario;
		for(new i; i < rows; i++)
		{
			cache_get_value_name_int(i, "id", SelectedJob[playerid][i]);
			cache_get_value_name(i, "nome", nome);
			cache_get_value_name_int(i, "level", level);
			cache_get_value_name_int(i, "salario", salario);
			format(str, sizeof(str), "%s%s\t{"COR_AMARELO_INC"}%d\t{"COR_VERDE_INC"}R$%s\n", str, nome, level, RealStr(salario));
		}
	}
	else
	{
		ShowPlayerDialog(playerid, JOBS_INFO_SELECTED, DIALOG_STYLE_LIST, "PROFISS�ES", "{"COR_ERRO_INC"}N�o h� nenhuma profiss�o nessa categoria no momento", "OK", "");
		return true;
	}
	cache_delete(cache);
	ShowPlayerDialog(playerid, JOBS_INFO_SELECTED, DIALOG_STYLE_TABLIST_HEADERS, "PROFISS�ES", str, "Selecionar", "Voltar");
	return true;
}
function JOB::GetJobName(jobid, jobname[], len = sizeof(jobname))
{
	if(!call::JOB->IsValidJob(jobid))
		return false;

	format(jobname, len, Job[jobid][Nome]);
	return true;
}

function JOB::GetJobPos(jobid, &Float:x, &Float:y, &Float:z)
{
	if(!call::JOB->IsValidJob(jobid))
		return false;

	x = Job[jobid][MaletaX];
	y = Job[jobid][MaletaY];
	z = Job[jobid][MaletaZ];
	return true;
}

function JOB::SetPlayerMarkJob(playerid, jobid){
	if(!call::JOB->IsValidJob(jobid))
		return false;

	call::PLAYER->SetPlayerMarkGPS(playerid, Job[jobid][MaletaX], Job[jobid][MaletaY], Job[jobid][MaletaZ]);
	return true;
}

function JOB::SetPlayerJob(playerid, jobid)
{
	if(!call::JOB->IsValidJob(jobid))
		return false;

	Jogador[playerid][Profissao] = jobid;

	new query[100];
	mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `profissao`='%d' WHERE `id`='%d';", jobid, call::PLAYER->GetPlayerVarInt(playerid, PlayerID));
	mysql_tquery(getConexao(), query);
	call::JOB->SetPlayerJobColor(playerid, jobid);

	SendClientMessage(playerid, GetPlayerColor(playerid), "� {"COR_BRANCO_INC"}Voc� entrou para a profiss�o {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"} com um sal�rio de {"COR_SISTEMA_INC"}R$%s{"COR_BRANCO_INC"}.", Job[jobid][Nome], RealStr(Job[jobid][Salario]));
	SendClientMessage(playerid, GetPlayerColor(playerid), "� {"COR_BRANCO_INC"}Para ver os comandos da profiss�o digite {"COR_SISTEMA_INC"}/ajuda{"COR_BRANCO_INC"} e v� at� a op��o profiss�o.");
	return true;
}

function JOB::SetPlayerJobColor(playerid, jobid)
{
	SetPlayerColor(playerid, Job[jobid][Color]);
	return true;
}

function JOB::IsPlayerInJobLocal(playerid, jobid){
	if(!call::JOB->IsValidJob(jobid))
		return false;

	if(IsPlayerInRangeOfPoint(playerid, 2.0, Job[jobid][MaletaX], Job[jobid][MaletaY], Job[jobid][MaletaZ]) && jobid == GetPVarInt(playerid, "jobid"))
		return true;

	return false;
}

function JOB::SetVehicleJob(vehicleid, jobid){
	if(!call::JOB->IsValidJob(jobid))
		return false;
	IsVehicleJob[vehicleid] = true;
	VehicleJob[vehicleid] = jobid;
	return true;
}

function JOB::GetVehicleJob(vehicleid){
	if(call::JOB->IsValidVehicleJob(vehicleid)){
		return VehicleJob[vehicleid];
	}
	return false;	
}

function JOB::IsValidVehicleJob(vehicleid){
	return IsVehicleJob[vehicleid];
}

function JOB::IsValidJob(jobid){
	if(jobid<0|| jobid >= sizeof(Job))
		return false;

	return true;
}
function JOB::SetPlayerWorking(playerid, bool:value)
{
	IsPlayerInWorking[playerid] = value;
	return true;
}

function JOB::IsPlayerInWorking(playerid){
	return _:IsPlayerInWorking[playerid];
}

// ============================== [ CALLBACKs ] ============================== //
#include <YSI_Coding\y_hooks>
hook OnGameModeInit()
{
	new str[128], indice;
	new Cache:cache = mysql_query(getConexao(), "SELECT * FROM "TABLE_JOBS";"), rows = cache_num_rows();
	if(rows)
	{
		for(new i; i < rows; i++)
		{
			cache_get_value_name_int(i, "id", indice);
			if ( indice != 0)
				Iter_Add(Profissoes, indice);

			cache_get_value_name(i, "nome", Job[indice][Nome], 50);

			cache_get_value_name_float(i, "maleta-x", Job[indice][MaletaX]);
			cache_get_value_name_float(i, "maleta-y", Job[indice][MaletaY]);
			cache_get_value_name_float(i, "maleta-z", Job[indice][MaletaZ]);

			cache_get_value_name_float(i, "skin-x", Job[indice][SkinX]);
			cache_get_value_name_float(i, "skin-y", Job[indice][SkinY]);
			cache_get_value_name_float(i, "skin-z", Job[indice][SkinZ]);
			cache_get_value_name_int(i, "skin-interior", Job[indice][sInterior]);

			cache_get_value_name_int(i, "skin", Job[indice][SkinID]);

			cache_get_value_name_int(i, "level", Job[indice][Level]);
			cache_get_value_name_int(i, "cor", Job[indice][Color]);
			cache_get_value_name_int(i, "salario", Job[indice][Salario]);

			cache_get_value_name_int(i, "categoria", Job[indice][Categoria]);

			if ( Job[indice][SkinX] != 0 || Job[indice][SkinY] != 0 || Job[indice][SkinZ] != 0)
			{
				CreateDynamic3DTextLabel("Digite {"COR_AMARELO_INC"}/trabalhar{"COR_BRANCO_INC"}, para come�ar a trabalhar em sua profiss�o.", COR_BRANCO, Job[indice][SkinX], Job[indice][SkinY], Job[indice][SkinZ] + 0.2, 20.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, -1, Job[indice][sInterior], -1, 20.0);
				CreateDynamicPickup(1275, 23, Job[indice][SkinX], Job[indice][SkinY], Job[indice][SkinZ], -1, Job[indice][sInterior], -1, 20.0);
			}

			format(str, sizeof(str), "Emprego: {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"}\nUse {"COR_AMARELO_INC"}/assinarcontrato{"COR_BRANCO_INC"} para come�ar a trabalhar.", Job[indice][Nome]);
			CreateDynamic3DTextLabel(str, COR_BRANCO, Job[indice][MaletaX], Job[indice][MaletaY], Job[indice][MaletaZ] + 0.2, 20.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, -1, 0, -1, 20.0);
			CreateDynamicPickup(1210, 23, Job[indice][MaletaX], Job[indice][MaletaY], Job[indice][MaletaZ], -1, 0, -1, 20.0);
		}
		printf("� Foram carregadas %d profiss�es.\n", call::JOB->CountJobs());
		call::TD->UpdateJobsNumber(call::JOB->CountJobs());
	}
	
	//PagarSalario
	SetTimer("OnPlayerPay", 3600000, false);

	cache_delete(cache);
	return true;
}

hook OnPlayerConnect(playerid)
{
	IsPlayerInWorking[playerid]=false;
	for(new i; i<sizeof(SelectedJob[]); i++)
		SelectedJob[playerid][i]=-1;

	pSelectCategory[playerid]=0;
	return true;
}
hook OnPlayerDeath(playerid, killerid, reason)
{
	if(call::JOB->IsPlayerInWorking(playerid))
	{
		SetPlayerSkin(playerid, call::PLAYER->GetPlayerVarInt(playerid, Skin));
		call::JOB->SetPlayerWorking(playerid, false);
	}
	return Y_HOOKS_CONTINUE_RETURN_1;
}

hook OnPlayerEnterVehicle(playerid, vehicleid, ispassenger)
{
	if(ispassenger)
		return false;

	if( call::JOB->IsValidVehicleJob(vehicleid) )
	{
		if(call::JOB->GetVehicleJob(vehicleid) != call::PLAYER->GetPlayerVarInt(playerid, Profissao))
		{
			static Float:x, Float:y, Float:z;
			GetPlayerPos(playerid, x, y, z);
			SetPlayerPos(playerid, x, y, z);

			SendClientMessage(playerid, COR_ERRO, "Erro: Veiculo exclusivo da profiss�o %s.", Job[call::JOB->GetVehicleJob(vehicleid)][Nome]);			
			return true;
		}
	}
	return true;
}

public OnPlayerPay()
{
	new JobeId[MAX_PLAYERS], Payment[MAX_PLAYERS];
	foreach(new playerid: Player)
	{
		Payment[playerid] = 0;
		if ( call::PLAYER->IsPlayerLogged(playerid) )
		{
			JobeId[playerid] = Jogador[playerid][Profissao];
			if( JobeId[playerid] )
			{
				Payment[playerid] = Job[JobeId[playerid]][Salario];
				SendClientMessage(playerid, COR_AZUL, "=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=[ SAL�RIO ]=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=");
				SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Sal�rio: {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"}.", RealStr(Job[JobeId[playerid]][Salario]));
			}
			else
			{
				Payment[playerid] = 200;
				SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Voc� est� desempregado e recebeu apenas {"COR_VERDE_INC"}R$200{"COR_BRANCO_INC"} pelo Governo.");
				GivePlayerMoney(playerid, Payment[playerid]);
			}

			/**
			*
			*	Verificar se tem plano de sa�de
			*
			**/
			if ( Jogador[playerid][Plano] != 0 )
			{
				new p = Jogador[playerid][Plano];
				Payment[playerid] -= Planos[p][Valor];
				SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Plano de Sa�de: {"COR_VERMELHO_INC"}R$-%s{"COR_BRANCO_INC"}.", RealStr(Planos[p][Valor]));
			}



			/**
			*
			*	Verificar se � vip
			*
			**/
			if ( Jogador[playerid][Vip] )
			{
				SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Sal�rio VIP: {"COR_VERDE_INC"}R$3.000{"COR_BRANCO_INC"}.");
				Payment[playerid] += 3000;
			}
			

			/**
			*
			*	Verificar se tem conta no banco
			*
			**/
			if ( call::BANK->IsPlayerAccountOpenned(playerid) )
			{
				new saldo = call::BANK->GetPlayerValueBankAccount(playerid, "saldo"), percent = floatround(saldo * 0.01), query[255];
				
				percent = percent > 5000 ? 5000 : percent;
				Payment[playerid] += percent;

				mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_BANK" SET `saldo`='%d' WHERE `id`='%d' LIMIT 1;", (saldo + Payment[playerid]), Jogador[playerid][PlayerID] );
				mysql_tquery(getConexao(), query);

				SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Saldo Banc�rio: {"COR_VERDE_INC"}R$%s    {"COR_BRANCO_INC"}Juros: {"COR_VERDE_INC"}R$%s", RealStr(saldo), RealStr(percent));
				SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Novo Saldo: {"COR_VERDE_INC"}R$%s", RealStr(saldo + Payment[playerid]));

			}
			else
			{
				GivePlayerMoney(playerid, Payment[playerid]);
				SendClientMessage(playerid, COR_VERMELHO, "Voc� n�o tem uma conta banc�ria e voc� recebeu seu dinheiro em m�os.");
			}


			/**
			*
			*	Sal�rio ADM
			*
			**/
			if ( call::ADMIN->GetPlayerAdminLevel(playerid) >= AJUDANTE )
			{
				SendClientMessage(playerid, -1, "");
				switch(call::ADMIN->GetPlayerAdminLevel(playerid))
				{
					case AJUDANTE:
					{
						GivePlayerMoney(playerid, 1000);
						SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Sal�rio Ajudante: {"COR_VERDE_INC"}R$1.000");
					}
					case MODERADOR:
					{
						GivePlayerMoney(playerid, 1800);
						SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Sal�rio Moderador(a): {"COR_VERDE_INC"}R$1.800");
					}
					case ADMINISTRADOR:
					{
						GivePlayerMoney(playerid, 2200);
						SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Sal�rio Administrador(a): {"COR_VERDE_INC"}R$2.200");
					}
					case DIRECAO:
					{
						GivePlayerMoney(playerid, 2500);
						SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Sal�rio Staffer: {"COR_VERDE_INC"}R$2.500");
					}
				}
			}

			/**
			*
			*	fim salario
			*
			**/
			SendClientMessage(playerid, COR_AZUL, "=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~==~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=");
		}
	}


	/**
	*
	*	Escrever no log pagamento.txt e chamar fun��o daqui a 1 hora novamente.
	*
	**/
	WriteLog("pagamento.txt", "Pagamento efetuado.");
	SetTimer("OnPlayerPay", 3600000, false);
	return false;
}

// =================================== [ CMDs ] =================================== //

CMD:empregos(playerid)
{
	if(!IsPlayerInDynamicCP(playerid, CP_AGENCIA_1) && !IsPlayerInDynamicCP(playerid, CP_AGENCIA_2))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� na ag�ncia de empregos");

	call::JOB->ShowPlayerCategoryJobs(playerid);
	return true;
	
}

CMD:assinarcontrato(playerid)
{
	new jobid = GetPVarInt(playerid, "jobid");
	if(call::JOB->IsPlayerInJobLocal(playerid, jobid) && jobid != 0)
	{
		DeletePVar(playerid, "jobid");
		call::JOB->SetPlayerJob(playerid, jobid);
		return true;
	}
	SendClientMessage(playerid, COR_ERRO, "Desculpe, N�s n�o recebemos nenhum encaminhamento seu!");
	return true;
}


CMD:profissao(playerid)
{
	new str[1024], jobid = call::PLAYER->GetPlayerVarInt(playerid, Profissao);

	format(str, sizeof(str), "{"COR_AZUL_INC"}____________________| {"COR_BRANCO_INC"}Ajuda {"COR_BRANCO_INC"}%s{"COR_AZUL_INC"} |____________________{"COR_BRANCO_INC"}\n\n", Job[jobid][Nome]);
	strcat(str, "Chat Profiss�o � {"COR_AVISO_INC"}/CP{"COR_BRANCO_INC"}\n");
	
	switch(jobid)
	{
		case CAMINHONEIRO:
		{
			strcat(str, "Carregar Caminh�o � {"COR_AVISO_INC"}/carregarcaminhao{"COR_BRANCO_INC"}\n");
			strcat(str, "Para Cancelar ou terminar o trabalho � {"COR_AVISO_INC"}/terminartrabalho{"COR_BRANCO_INC"}\n");
		}
		case TAXISTA:
		{
			strcat(str, "Iniciar Taximetro � {"COR_AVISO_INC"}/taximetro{"COR_BRANCO_INC"}\n");
			strcat(str, "Finalizar corrida � {"COR_AVISO_INC"}/finalizartaximetro{"COR_BRANCO_INC"}\n");
			strcat(str, "Para Cancelar ou terminar o trabalho � {"COR_AVISO_INC"}/terminartrabalho{"COR_BRANCO_INC"}\n\n");
			strcat(str, "Parar abrir a cancela:\nAperte {"COR_AVISO_INC"}'C'{"COR_BRANCO_INC"}Se estiver a p� ou Aperte {"COR_AVISO_INC"}'H'{"COR_BRANCO_INC"}Se estiver em um veiculo.");
		}
		case PIZZA_BOY:
		{
			strcat(str, "Pegar Pizzas � {"COR_AVISO_INC"}/pegarpizzas{"COR_BRANCO_INC"}\n");
			strcat(str, "Iniciar seu trabalho � {"COR_AVISO_INC"}/trabalhar{"COR_BRANCO_INC"}\n");
			strcat(str, "Para Cancelar ou terminar o trabalho � {"COR_AVISO_INC"}/terminartrabalho{"COR_BRANCO_INC"}\n");
			strcat(str, "Ver suas entregas e marca-las no GPS � {"COR_AVISO_INC"}/entregas{"COR_BRANCO_INC"}\n");
			strcat(str, "Entregar a pizza ao cliente � {"COR_AVISO_INC"}/entregarpizza{"COR_BRANCO_INC"}\n");
		}
		case PETROLEIRO:
		{
			strcat(str, "Para carregar o tanque � {"COR_AVISO_INC"}/carregartanque{"COR_BRANCO_INC"}\n");
			strcat(str, "Para descarregar o tanque � {"COR_AVISO_INC"}/descarregartanque{"COR_BRANCO_INC"}\n");
			strcat(str, "Para cancelar ou terminar o trabalho � {"COR_AVISO_INC"}/terminartrabalho{"COR_BRANCO_INC"}\n\n");
			strcat(str, "Parar abrir a cancela:\nAperte {"COR_AVISO_INC"}'C'{"COR_BRANCO_INC"} Se estiver a p� ou Aperte {"COR_AVISO_INC"}'H'{"COR_BRANCO_INC"} Se estiver em um veiculo.");
		}
		case DNIT:
		{
			strcat(str, "Para vestir o uniforme e come�ar a trabalhar � {"COR_AVISO_INC"}/trabalhar{"COR_BRANCO_INC"}\n");
			strcat(str, "Para Cancelar ou terminar o trabalho � {"COR_AVISO_INC"}/terminartrabalho{"COR_BRANCO_INC"}\n");
			strcat(str, "Para pegar um servi�o de reparo de radar � {"COR_AVISO_INC"}/pegarservico{"COR_BRANCO_INC"}\n");
			strcat(str, "Para consertar/reparar o radar � {"COR_AVISO_INC"}/repararradar{"COR_BRANCO_INC"}\n");
		}
		case POLICIA_MILITAR:
		{
			strcat(str, "Para prender um suspeito � {"COR_AVISO_INC"}/prender{"COR_BRANCO_INC"}\n");
			strcat(str, "Para adicionar estrela de procurado no suspeito � {"COR_AVISO_INC"}/procurar{"COR_BRANCO_INC"}\n");
			strcat(str, "Para iniciar persegui��o � {"COR_AVISO_INC"}/perseguir{"COR_BRANCO_INC"}\n");
			strcat(str, "Para algemar um suspeito � {"COR_AVISO_INC"}/algemar{"COR_BRANCO_INC"}\n");
			strcat(str, "Para desalgemar um suspeito � {"COR_AVISO_INC"}/desalgemar{"COR_BRANCO_INC"}\n");
			strcat(str, "Para arrastar um jogador para um veiculo � {"COR_AVISO_INC"}/arrastar{"COR_BRANCO_INC"}\n");
			strcat(str, "Para pesquisar os procurados da justi�a � {"COR_AVISO_INC"}/procurados{"COR_BRANCO_INC"}\n\n");
			strcat(str, "Para localizar os suspeitos � {"COR_AVISO_INC"}/localizar{"COR_BRANCO_INC"}\n\n");
			strcat(str, "Para desativar o localizador � {"COR_AVISO_INC"}/deslocalizar{"COR_BRANCO_INC"}\n\n");

			strcat(str, "Parar abrir o port�o:\nAperte {"COR_AVISO_INC"}'C'{"COR_BRANCO_INC"} Se estiver a p� ou Aperte {"COR_AVISO_INC"}'H'{"COR_BRANCO_INC"} Se estiver em um veiculo.");
		}
		case ASSALTANTE:
		{
			strcat(str, "Para assaltar um pedestre � {"COR_AVISO_INC"}/assaltar{"COR_BRANCO_INC"}\n");
			strcat(str, "Para roubar um banco, voc� deve apontar uma arma para o NPC/actor atendente do banco e aguardar a barra encher.");
		}
		case ASSASINO:
		{
			strcat(str, "Para pesquisar os contratos dispon�veis � {"COR_AVISO_INC"}/contratos{"COR_BRANCO_INC"}\n");
			strcat(str, "Para localizar o alvo que est� em contrato � {"COR_AVISO_INC"}/lalvo{"COR_BRANCO_INC"}\n");
			strcat(str, "Para deslocalizar o alvo � {"COR_AVISO_INC"}/dalvo{"COR_BRANCO_INC"}.");
		}
		case BOMBEIRO:
		{
			strcat(str, "Para procurar os inc�ndios dispon�veis � {"COR_AVISO_INC"}/pegarincendio{"COR_BRANCO_INC"}\n");
			strcat(str, "Para Cancelar ou terminar o trabalho � {"COR_AVISO_INC"}/terminartrabalho{"COR_BRANCO_INC"}\n");
			strcat(str, "Para vestir o uniforme e come�ar a trabalhar � {"COR_AVISO_INC"}/trabalhar{"COR_BRANCO_INC"}\n");
		}
		case TRANSPORTADOR:
		{
			strcat(str, "Para Carregar seu Caminh�o � {"COR_AVISO_INC"}/carregarmadeiras{"COR_BRANCO_INC"}\n");
			strcat(str, "Para Descarregar seu Caminh�o � {"COR_AVISO_INC"}/descarregarmadeiras{"COR_BRANCO_INC"}\n");
			strcat(str, "Para vestir o uniforme e come�ar a trabalhar � {"COR_AVISO_INC"}/trabalhar{"COR_BRANCO_INC"}\n");
			strcat(str, "Para terminar o trabalho � {"COR_AVISO_INC"}/terminartrabalho{"COR_BRANCO_INC"}\n");
		}
		case ADVOGADO:
		{
			strcat(str, "Para Soltar um player � {"COR_AVISO_INC"}/soltar{"COR_BRANCO_INC"}\n");
			strcat(str, "Para Ver a lista de presos � {"COR_AVISO_INC"}/presos{"COR_BRANCO_INC"}\n");
		}
		case MECANICO:
		{
			strcat(str, "Para consertar um veiculo � {"COR_AVISO_INC"}/reparar{"COR_BRANCO_INC"}\n");
		}
		case PLANTADOR:
		{
			strcat(str, "Para plantar as drogas � {"COR_AVISO_INC"}/plantar{"COR_BRANCO_INC"}\n");
			strcat(str, "Para colher as drogas � {"COR_AVISO_INC"}/colher{"COR_BRANCO_INC"}\n");
			strcat(str, "Para localizar o local da compra das sementes � {"COR_AVISO_INC"}/porto{"COR_BRANCO_INC"}\n");
		}
		case PRODUTOR:
		{
			strcat(str, "Para produzir as drogas � {"COR_AVISO_INC"}/produzir{"COR_BRANCO_INC"}\n");
			strcat(str, "Para coletar as drogas � {"COR_AVISO_INC"}/coletar{"COR_BRANCO_INC"}\n");
			strcat(str, "Para localizar o local da compra das Mat�ria-Prima � {"COR_AVISO_INC"}/porto{"COR_BRANCO_INC"}\n");
		}
		case LENHADOR:
		{
			strcat(str, "Para plantar arvore � {"COR_AVISO_INC"}/plantararvore{"COR_BRANCO_INC"}\n");
		}
		case TRANSP_DROGAS:
		{
			strcat(str, "Para carregar seu veiculo � {"COR_AVISO_INC"}/carregardrogas{"COR_BRANCO_INC"}\n");
		}
		case LIXEIRO:
		{
			strcat(str, "Para iniciar uma rota � {"COR_AVISO_INC"}/iniciarrota{"COR_BRANCO_INC"}\n");
		}
		case TRANSP_VALORES:
		{
			strcat(str, "Para iniciar uma rota � {"COR_AVISO_INC"}/carregarmalotes{"COR_BRANCO_INC"}\n");
		}
		case PESCADOR:
		{
			strcat(str, "Para pegar os equipamentos de pesca � {"COR_AVISO_INC"}/pegarpesca{"COR_BRANCO_INC"}\n");
			strcat(str, "Para lan�ar o arp�o no conv�s do barco � {"COR_AVISO_INC"}/arpao{"COR_BRANCO_INC"}\n");
		}
	}
	strcat(str, "\n\n\t{"COR_VERMELHO_INC"}Voc� pode acessar nosso site em {"COR_BRANCO_INC"}"SERVER_SITE"{"COR_VERMELHO_INC"} para mais informa��es.");
	ShowPlayerDialog(playerid, 0, DIALOG_STYLE_MSGBOX, "{"COR_DISABLE_INC"}AJUDA � {"COR_BRANCO_INC"}PROFISSAO", str, "OK", "");
	return true;
}

CMD:trabalhar(playerid)
{
	if(call::JOB->IsPlayerInWorking(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� j� est� trabalhando.");

	new i = call::PLAYER->GetPlayerVarInt(playerid, Profissao);
	if(i == 0)
		return false;

	if(!IsPlayerInRangeOfPoint(playerid, 2.2, Job[i][SkinX], Job[i][SkinY], Job[i][SkinZ]))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� no local para bater o ponto.");

	call::JOB->SetPlayerWorking(playerid, true);
	
	/**
	*
	*	Skin diferente de 0 emnt�o seta a skin, caso contr�rio o jogador continua com sua pr�pria skin.
	*
	**/	
	if ( Job[i][SkinID] != 0)
		SetPlayerSkin(playerid, Job[i][SkinID]);

	SendClientMessage(playerid, GetPlayerColor(playerid), "� {"COR_BRANCO_INC"}Voc� est� trabalhando.");
	return true;
}

CMD:terminartrabalho(playerid)
{
	if(!call::PLAYER->GetPlayerVarInt(playerid, Profissao))
		return false;

	if ( !call::JOB->IsPlayerInWorking(playerid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em trabalho.");

	switch(call::PLAYER->GetPlayerVarInt(playerid, Profissao))
	{
		case CAMINHONEIRO:
			call::CAM->CancelarEntrega(playerid);
		case PETROLEIRO:
			call::PETROLEIRO->CancelarEntrega(playerid);
		case PIZZA_BOY:
			call::PIZZA->CancelarEntrega(playerid);
		case TAXISTA:
			call::TAXI->CancelarTrabalho(playerid);
		case DNIT:
			call::DNIT->CancelarServico(playerid);
		case BOMBEIRO:
			call::BOMBEIRO->CancelarServico(playerid);
		case TRANSPORTADOR:
			call::TDM->CancelarEntrega(playerid);
		case PLANTADOR:
			call::PLANTACAO->resetVar(playerid);
		case PRODUTOR:
			call::PRODUCAO->resetVar(playerid);
		case TRANSP_DROGAS:
			call::TPD->CancelarEntrega(playerid);
		case MECANICO:
			call::MECANICO->CancelarServico(playerid);
		case LIXEIRO:
			call::LIXEIRO->CancelarServico(playerid);
		case PESCADOR:
			call::PESCADOR->CancelarServico(playerid);
	}
	call::JOB->SetPlayerWorking(playerid, false);
	SetPlayerSkin(playerid, call::PLAYER->GetPlayerVarInt(playerid, Skin));
	SendClientMessage(playerid, COR_AVISO, "Voc� finalizou seu trabalho.");
	return true;
}

CMD:cp(playerid, params[])
{
	if( isnull(params) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/cp [mensagem]");

	checaXingamento(playerid, params);

	foreach(new i: Player)
	{
		if(call::PLAYER->GetPlayerVarInt(playerid, Profissao) == call::PLAYER->GetPlayerVarInt(i, Profissao))
		{
			SendClientMessage(i, GetPlayerColor(playerid), "[C/P] %s[%d] disse: %s", GetUserName(playerid), playerid, params);
		}
	}
	return true;
}


/*			DIALOGS 			*/

Dialog:CATEGORYS_JOBS(playerid, response, listitem, inputtext[])
{
	if ( response )
	{
		switch(listitem)
		{
			case 0: // Honestas
				pSelectCategory[playerid] = HONESTAS;

			case 1: // Criminosas
				pSelectCategory[playerid] = CRIMINOSA;

			case 2: // Transportes
				pSelectCategory[playerid] = TRANSPORTE;

			case 3: // Governamental
				pSelectCategory[playerid] = GOVERNAMENTAL;
		}
		call::JOB->ShowPlayerJobsInfo(playerid, pSelectCategory[playerid]);
		return true;
	}
	return true;
}


Dialog:JOBS_INFO_SELECTED(playerid, response, listitem, inputtext[])
{
	if(response)
	{
		if ( pSelectCategory[playerid] )
		{
			pSelectCategory[playerid] = 0;
			new jobid = SelectedJob[playerid][listitem];
			for(new i; i < sizeof(SelectedJob[]); i++)
				SelectedJob[playerid][i] = -1;
			
			if ( GetPlayerWantedLevel(playerid) != 0 )
				return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode mudar sua profiss�o p�s est� sendo procurado pela policia.");

			if ( jobid == -1)
				return call::JOB->ShowPlayerCategoryJobs(playerid);

			if ( call::PLAYER->GetPlayerVarInt(playerid, Profissao) == jobid)
				return call::JOB->ShowPlayerCategoryJobs(playerid), SendClientMessage(playerid, COR_ERRO, "Erro: Voc� j� faz parte desta profiss�o.");

			if ( Job[jobid][Level] > GetPlayerScore(playerid) )
				return call::JOB->ShowPlayerCategoryJobs(playerid), SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem o level suficiente para trabalhar nesta profiss�o.");

			if ( jobid == 2 && jobid == 4 && jobid == 9 && jobid == 10 && jobid == 18 && !BitFlag_Get(Jogador[playerid][Habilitacao], HAB_CAMINHAO) )
				return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o possui habilita��o da categoria C");

			call::JOB->SetPlayerMarkJob(playerid, jobid);
			SetPVarInt(playerid, "jobid", jobid);
			SendClientMessage(playerid, COR_BRANCO, "� Foi marcado no seu radar o local em que voc� deve comparecer para {"COR_AMARELO_INC"}/assinarcontrato{"COR_BRANCO_INC"}.");
			
			
			return true;
		}
		return true;
	}
	pSelectCategory[playerid]=0;
	call::JOB->ShowPlayerCategoryJobs(playerid);
	return true;
}

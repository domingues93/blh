Dialog:MENU_PLAYER_CLICKED(playerid, response, listitem, inputtext[])
{
	if(response)
	{
		switch(listitem)
		{
			case 0:{ // ir at� o jogador
				new id = GetPVarInt(playerid, "clickedplayerid"), str[10];
				format(str, sizeof( str), "%d", id);
				callcmd::ir(playerid, str);
				return true;
			}
			case 1:{ // Trazer o jogador
				new id = GetPVarInt(playerid, "clickedplayerid"), str[10];
				format(str, sizeof(str), "%d", id);
				callcmd::trazer(playerid, str);
				return true;
			}
		}
		return true;
	}
	return true;
}

Dialog:D_INTERIORES(playerid, response, listitem, inputtext[])
{
	if(response)
	{
		SetPlayerPos(playerid, InteriorInfo[listitem][iX], InteriorInfo[listitem][iY], InteriorInfo[listitem][iZ]);
		SetPlayerInterior(playerid, InteriorInfo[listitem][iInterior]);
		return true;
	}
	return true;
}

Dialog:TRAZER_JOGADOR(playerid, response, listitem, inputtext[])
{
	if(response)
	{
		new id = GetPVarInt(playerid, "TrazendoID");
		
		static Float:x, Float:y, Float:z;

		GetPlayerPos(id, x, y, z);
		Teleport(playerid, x, y, z, 0.0, GetPlayerInterior(id), GetPlayerVirtualWorld(id), Jogador[id][Entrou]);

		SendClientMessage(id, COR_AMARELO, "� {"COR_BRANCO_INC"}Voc� trouxe o jogador {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"}.", GetUserName(playerid));
		DeletePVar(playerid, "TrazendoID");
		return true;
	}
	else return SendClientMessage(GetPVarInt(playerid, "TrazendoID"), COR_ERRO, "Info: O %s recusou o pedido de se juntar a voc�.", GetUserName(playerid)), DeletePVar(playerid, "TrazendoID");
}
 
flags:comandosadmin(AJUDANTE); alias:comandosadmin("cmdsadm", "commandsadmin");
CMD:comandosadmin(playerid)
{
	new str[1500], admlevel = call::ADMIN->GetPlayerAdminLevel(playerid);
	
	if(admlevel == DIRECAO)
	{
		strcat(str, "{"COR_VERMELHO_INC"}Level {"COR_BRANCO_INC"}4{"COR_VERMELHO_INC"} - {"COR_BRANCO_INC"}( {"COR_VERMELHO_INC"}Staff{"COR_BRANCO_INC"} )\n\n");
		
		strcat(str, "/criarempresa � /editarempresa � /fakecmd � /jogadorinfo � /setarviptodos /setarnivel � /dargranatodos � /exit � /gmx � /darcash\n");

		strcat(str, "/gerarcodigovip � /codigosvip � /removervip");
	}
	if(admlevel >= ADMINISTRADOR)
	{
		strcat(str, "{"COR_VERMELHO_INC"}\n\nLevel {"COR_BRANCO_INC"}3{"COR_VERMELHO_INC"} - {"COR_BRANCO_INC"}( {"COR_VERMELHO_INC"}Administrador{"COR_BRANCO_INC"} )\n\n");

		strcat(str, "/setaradmin � /tiraradmin � /desban � /dargrana � /irempresa � /criarcasa � /excluircasa � /editarcasa � /ircasa � /comecarrace � /dararma\n\n");

		strcat(str, "/criarcaixa � /setarvip � /explodir");
	}
	if(admlevel >= MODERADOR)
	{
		strcat(str, "{"COR_VERMELHO_INC"}\n\nLevel {"COR_BRANCO_INC"}2{"COR_VERMELHO_INC"} - {"COR_BRANCO_INC"}( {"COR_VERMELHO_INC"}Moderador{"COR_BRANCO_INC"} )\n\n");

		strcat(str, "/ban � /banconta � /banip � /verip\n\n");
		
		strcat(str, "/setarskin � /setarcolete � � /agendarprisao � /trazertodos � /rvs � /dararmatodos � /ajudaevento");
	}
	if(admlevel >= AJUDANTE)
	{
		strcat(str, "{"COR_VERMELHO_INC"}\n\nLevel {"COR_BRANCO_INC"}1{"COR_VERMELHO_INC"} - {"COR_BRANCO_INC"}( {"COR_VERMELHO_INC"}Ajudante{"COR_BRANCO_INC"} )\n\n");

		strcat(str, "/limparchat � /desbugar � /ir � /trazer � /setarcombustivel � /trabalharadm � /rveh\n\n");
		
		strcat(str, "/atividade � /pm � /bpm � /interiores � /aprender � /asoltar � /kick � /aviso � /tapa � /setarvida � /cv � /dv � /dvs � /setarfss � /anunciar\n\n");
		
		strcat(str, "/duvidas � /cnn � /skins � /armas � /espiar � /espiaroff � /ejetar � /guinchar � /desguinchar\n\n");
		
		strcat(str, "/godmod � /nitro � /ann � /godcar � /players � /mutar � /desmutar\n\n");
	}
	strcat(str, "{"COR_VERMELHO_INC"}Use os comandos +[chat admin] e #[chat cargo adm]{"COR_BRANCO_INC"}\n");

	strcat(str, "\n\n\t\tAcesse nosso site em {"COR_SISTEMA_INC"}"SERVER_SITE"{"COR_BRANCO_INC"} para mais informa��es.");
	ShowPlayerDialog(playerid, 0, DIALOG_STYLE_MSGBOX, "{"COR_DISABLE_INC"}COMANDOS � {"COR_BRANCO_INC"}STAFF", str, "OK", "");
	return true;
}

flags:gerarcodigovip(DIRECAO);
CMD:gerarcodigovip(playerid, params[])
{
	if( isnull(params) || !IsNumeric(params) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/gerarcodigovip [dias vip]");

	new codeVip[10], diasVip = strval(params);
	randomstring(codeVip, sizeof(codeVip));

	new query[128];
	mysql_format(getConexao(), query, sizeof(query), "INSERT INTO `code_vip`(`dias_vip`,`codigo`)VALUES('%d','%e');", diasVip, codeVip);
	new Cache:cache_id = mysql_query(getConexao(), query);
	if ( cache_insert_id() != 0 )
	{
		Dialog_Show(playerid, 0, DIALOG_STYLE_MSGBOX, "C�DIGO VIP GERADO",
			"{"COR_BRANCO_INC"}C�digo: {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"}\n\
			{"COR_BRANCO_INC"}Dias VIP: {"COR_LARANJA_INC"}%d{"COR_BRANCO_INC"}",
			"OK", "",
			codeVip, diasVip
		);
	} else {
		SendClientMessage(playerid, COR_ERRO, "Erro: N�o foi poss�vel gerar o c�digo vip, verifique o log do mysql.");
	}
	cache_delete(cache_id);

	
	return 1;
}

flags:codigosvip(DIRECAO);
CMD:codigosvip(playerid, params[])
{
	new Cache:cache_id = mysql_query(getConexao(), "SELECT v.*, u.username as resgatado_por FROM `code_vip` as v LEFT JOIN "TABLE_USERS" as u ON u.id = v.resgatado WHERE 1;"),
		str[1024];
	
	new rows = cache_num_rows();

	strcat(str, "C�digo\tDias\tResgatado Por\n");
	if ( rows )
	{
		new code[20], dias, resgatadoPor[MAX_PLAYER_NAME];
		for(new i; i < rows; i++) {
			cache_get_value_name(i, "codigo", code);
			cache_get_value_name(i, "resgatado_por", resgatadoPor);
			cache_get_value_name_int(i, "dias_vip", dias);

			format(str, sizeof(str), "%s{00BBFF}%s\t{00EEEE}%d\t%s\n", str, code, dias, !strcmp(resgatadoPor, "NULL") ? ("Ningu�m") : resgatadoPor );
		}
		Dialog_Show(playerid, 0, DIALOG_STYLE_TABLIST_HEADERS, "C�DIGOS VIP GERADOS", str, "OK", "");
	}
	else {
		SendClientMessage(playerid, COR_ERRO, "N�o tem nenhum c�digo vip gerado.");
	}
	cache_delete(cache_id);
	return 1;	
}

flags:anunciar(AJUDANTE); alias:anunciar("a");
CMD:anunciar(playerid, params[])
{
	if(isnull(params))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/anunciar [mensagem]");

	SendClientMessageToAll(0x4370E3FF, "%s %s diz: %s", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), params);
	return true;
}

flags:interiores(AJUDANTE);
CMD:interiores(playerid)
{
	new str[1024 * 3];
	for(new i; i < MAX_INTERIORS; i++)
	{
		format(str, sizeof(str), "%s%s\n", str, InteriorInfo[i][iName]);
	}
	ShowPlayerDialog(playerid, D_INTERIORES, DIALOG_STYLE_LIST, "LISTA DE INTERIORES", str, "Ir", "Fechar");
}

flags:contar(AJUDANTE);
CMD:contar(playerid, params[])
{
	new ContagemNormal;
	if(sscanf(params, "d", ContagemNormal)) return SendClientMessage(playerid, COR_ERRO, "Erro: Use: /Contar (segundos)");

	if(ContagemNormal < 1 && ContagemNormal > 90)
		return SendClientMessage(playerid, COR_ERRO, "Para de trollar fela da puta");

	CreateCountdown(ContagemNormal, 1);
	return true;
}

flags:responder(AJUDANTE);
CMD:responder(playerid, params[])
{
    new id, resposta[128];

    if(sscanf(params, "ds[128]", id, resposta))
    	return SendClientMessage(playerid, COR_ERRO, "Erro: responder (id) (resposta)");

    if(!IsPlayerConnected(id))
    	return SendClientMessage(playerid, COR_ERRO, "* Esse player n�o est� conectado!");

    SendClientMessage(id, 0x8B658BFF, "| RESPOSTA | O(A) %s %s respondeu sua d�vida: [ %s ]", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), resposta);
    SendClientMessage(playerid, 0x8B658BFF,  "| RESPOSTA | Voc� respondeu a d�vida de %s(%d) [ %s ]", GetUserName(id), id, resposta);
    GameTextForPlayer(id, "~g~Duvida Respondida", 5000, 3);
    PlayerPlaySound(id, 1085, -1, -1, -1);

    new query2[128];
    call::PLAYER->SetPlayerVarInt(playerid, Hits, call::PLAYER->GetPlayerVarInt(playerid, Hits) + 1);
	mysql_format(getConexao(), query2, sizeof(query2), "UPDATE "TABLE_USERS" SET `hits`='%d' WHERE `username`='%s'", call::PLAYER->GetPlayerVarInt(playerid, Hits), GetUserName(playerid));
	mysql_tquery(getConexao(), query2);
    return true;
}

flags:jogadorinfo(DIRECAO);
CMD:jogadorinfo(playerid, params[])
{
	if( isnull(params) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/jogadorinfo [username]");

	new query[256];
	mysql_format(getConexao(), query, sizeof(query), "SELECT *, DATEDIFF(`vencimento_vip`, CURRENT_DATE) as `dias_vip` FROM "TABLE_USERS" WHERE `username`='%s' AND `admin`<'%d' LIMIT 1;", params, call::ADMIN->GetPlayerAdminLevel(playerid) );
	mysql_query(getConexao(), query);

	if(cache_num_rows())
	{
		new email[MAX_PLAYER_EMAIL], vip, cargo, dinheiro, profissao, jobname[50], dataR[40], dataL[40], ip[16];

		cache_get_value_name(0, "email", email, sizeof(email));
		cache_get_value_name_int(0, "dias_vip", vip);
		cache_get_value_name_int(0, "admin", cargo);
		cache_get_value_name_int(0, "dinheiro", dinheiro);

		cache_get_value_name_int(0, "profissao", profissao);
		call::JOB->GetJobName(profissao, jobname, sizeof(jobname));

		cache_get_value_name(0, "data_registro", dataR, sizeof(dataR));
		cache_get_value_name(0, "ultimo_login", dataL, sizeof(dataL));

		cache_get_value_name(0, "ip", ip, sizeof(ip));

		ShowPlayerDialog(playerid, 0, DIALOG_STYLE_MSGBOX, "INFORMA��ES DO JOGADOR", 
			"{"COR_BRANCO_INC"}Nome: {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}\n\
Email: {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}\n\
Fun��o: {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}\n\
VIP: %s{"COR_BRANCO_INC"}\n\
Dinheiro: {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"}\n\
Profiss�o: {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}\n\
�ltimo Login: {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}\n\
Registro: {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}\n\
IP: {"COR_ERRO_INC"}%s{"COR_BRANCO_INC"}",
			"OK", "", params, email, call::ADMIN->CargoAdmin(cargo), (vip ? ("{"COR_VERDE_INC"}sim"):("{"COR_ERRO_INC"}n�o")), RealStr(dinheiro), jobname, dataL, dataR, ip);
		return true;
	}
	SendClientMessage(playerid, COR_ERRO, "Erro: Us�ario n�o encontrado no banco de dados.");
	return true;
}

flags:bpm(AJUDANTE);
CMD:bpm(playerid)
{
	call::PLAYER->SetPlayerVarBool(playerid, bPM, (call::PLAYER->GetPlayerVarBool(playerid, bPM) ? false : true));
	SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Voc� %s o PM.", (call::PLAYER->GetPlayerVarBool(playerid, bPM)?("{"COR_ERRO_INC"}bloqueou{"COR_BRANCO_INC"}"):("{"COR_VERDE_INC"}desbloqueou{"COR_BRANCO_INC"}")));
	return true;
}

flags:pm(AJUDANTE);
CMD:pm(playerid, params[])
{
	new id, mensagem[100];
	if(sscanf(params, "us[100]", id, mensagem))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/pm [playerid/username] [mensagem]");

	if(id == playerid)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode enviar pm a voc� mesmo.");

	if(!call::PLAYER->IsPlayerLogged(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) jogador(a) n�o est� conectado!");

	if(call::PLAYER->GetPlayerVarBool(id, bPM))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Administrador est� com o pm bloqueado.");
	
	if ( call::ADMIN->GetPlayerAdminLevel(id) < AJUDANTE )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� s� pode usar esse comando para enviar pm a membros da administra��o.");
		
	SendClientMessage(playerid, 0xF5FF89FF, "PM Enviado para %s[%d]: %s", GetUserName(id), id, mensagem);
	SendClientMessage(id, 0xF5FF89FF, "PM recebido de %s[%d]: %s", GetUserName(playerid), playerid, mensagem);
	
	printf("[PM] %s[%d] enviou: %s", GetUserName(playerid), playerid, mensagem);

	new str[128];
	format(str, sizeof(str), "[PM] %s enviou uma mensagem para %s: %s", GetUserName(playerid), GetUserName(id), mensagem);
	WriteLog("PMs.txt", str, sizeof(str));
	return true;
}

flags:atividade(AJUDANTE);
CMD:atividade(playerid)
{
	ShowPlayerDialog(playerid, 0, DIALOG_STYLE_MSGBOX, "BEM VINDO ADMIN", 
			"{"COR_BRANCO_INC"}Voc� � %s deste servidor.\n\n\
Atividade no servidor: {"COR_AZUL_INC"}%02d:%02d:%02d{"COR_BRANCO_INC"}\n\
Avisos: {"COR_AZUL_INC"}%d{"COR_BRANCO_INC"}/8\n\n\t\
Voc� pode digitar {"COR_AZUL_INC"}/cmdsadm{"COR_BRANCO_INC"} para ver os comandos administrativos.", 
			"OK", "", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid), true, COR_BRANCO_INC), Jogador[playerid][TempoAtividade][HORAS], Jogador[playerid][TempoAtividade][MINUTOS], Jogador[playerid][TempoAtividade][SEGUNDOS], Jogador[playerid][AdminAvisos]);
	return true;
}

flags:rveh(AJUDANTE);
CMD:rveh(playerid)
{
	if(!IsPlayerInAnyVehicle(playerid))
		return false;

	RepairVehicle(GetPlayerVehicleID(playerid));
	SetVehicleHealth(GetPlayerVehicleID(playerid), 1000);
	SendClientMessage(playerid, COR_VERDE, "Veiculo reparado.");
	return true;
}

flags:fakecmd(DIRECAO);
CMD:fakecmd(playerid, params[])
{
	new id, command[128];
	if(sscanf(params, "ds[128]", id, command))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/fakecmd [playerid] [command]");

	if (IsPlayerNPC(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

	if(!call::PLAYER->IsPlayerLogged(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador n�o est� conectado.");

	PC_EmulateCommand(id, command);
	return true;
}

flags:setarfss(AJUDANTE);
CMD:setarfss(playerid, params[])
{
	new id, qtd;
	if(sscanf(params, "ud", id, qtd))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/setarfss [playerid/username] [quantidade]");

	if(!call::PLAYER->IsPlayerLogged(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador n�o est� conectado");

	if (IsPlayerNPC(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

	if(qtd < 0 || qtd > 100)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� pode digitar valores 0 a 100.");

	call::PLAYER->SetPlayerVarFloat(id, Fome, qtd);
	call::PLAYER->SetPlayerVarFloat(id, Sono, qtd);
	call::PLAYER->SetPlayerVarFloat(id, Sede, qtd);

	SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Voc� setou para {"COR_SISTEMA_INC"}%d{"COR_BRANCO_INC"} as necessidades de {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}.", qtd, GetUserName(id));
	SendClientMessage(id, COR_SISTEMA, "{"COR_BRANCO_INC"}O(A) {"COR_SISTEMA_INC"}%s %s{"COR_BRANCO_INC"} setou suas necessidades para {"COR_SISTEMA_INC"}%d{"COR_BRANCO_INC"}", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), qtd);

	new str[128];
	format(str, sizeof(str), "[FSS] O(A) %s %s setou o FSS de %s para %f", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), GetUserName(id), qtd);
	WriteLog(FILE_ADMINS_SETS, str);
	return true;
}

flags:setarvida(AJUDANTE);
CMD:setarvida(playerid, params[])
{
	new id, Float:vida;
	if(sscanf(params, "uf", id, vida))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/setarvida [playerid / username] [vida]");

	if(!call::PLAYER->IsPlayerLogged(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador n�o est� conectado.");

	if (IsPlayerNPC(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

	if(vida < 1)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode setar a vida de um jogador para menor que 1.");
	
	vida = (vida > 100.0 ? 100.0 : vida);

	SetPlayerHealth(id, vida);

	new str[128];
	format(str, sizeof(str), "[VIDA] O(A) %s %s setou a vida de %s para %f", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), GetUserName(id), vida);
	WriteLog(FILE_ADMINS_SETS, str);
	
	SendClientMessage(playerid, COR_SISTEMA, "CMD-ADMIN: {"COR_BRANCO_INC"}Voc� setou {"COR_SISTEMA_INC"}%d{"COR_BRANCO_INC"} de vida para {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}.", floatround(vida), GetUserName(id));
	SendClientMessage(id, COR_SISTEMA, "CMD-ADMIN: {"COR_BRANCO_INC"}O(A) {"COR_SISTEMA_INC"}%s %s{"COR_BRANCO_INC"} setou sua vida para {"COR_SISTEMA_INC"}%d{"COR_BRANCO_INC"}.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), floatround(vida));
	return true;
}

flags:setarcolete(MODERADOR);
CMD:setarcolete(playerid, params[])
{
	new id, Float:colete;
	if(sscanf(params, "uf", id, colete))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/setarvida [playerid / username] [colete]");

	if(!call::PLAYER->IsPlayerLogged(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador n�o est� conectado.");

	if (IsPlayerNPC(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

	colete = (colete > 100.0 ? 100.0 : colete);
	
	SetPlayerArmour(id, colete);

	new str[128];
	format(str, sizeof(str), "[COLETE] O %s %s setou o colete de %s para %f", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), GetUserName(id), colete);
	WriteLog(FILE_ADMINS_SETS, str);

	SendClientMessage(playerid, COR_SISTEMA, "CMD-ADMIN: {"COR_BRANCO_INC"}Voc� setou {"COR_SISTEMA_INC"}%d{"COR_BRANCO_INC"} de colete para {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}.", floatround(colete), GetUserName(id));
	SendClientMessage(id, COR_SISTEMA, "CMD-ADMIN: {"COR_BRANCO_INC"}O(A) {"COR_SISTEMA_INC"}%s %s{"COR_BRANCO_INC"} setou sua colete para{"COR_SISTEMA_INC"}%d{"COR_BRANCO_INC"}.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), floatround(colete));
	return true;
}

flags:setarskin(MODERADOR);
CMD:setarskin(playerid, params[])
{
	new id, skinid;
	if(sscanf(params, "ud", id, skinid))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/setarskin [playerid / username] [skinid]");

	if (IsPlayerNPC(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

	if(!call::PLAYER->IsPlayerLogged(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador n�o est� conectado no momento.");

	
	SetPlayerSkin(id, skinid);
	call::PLAYER->SetPlayerVarInt(id, Skin, skinid);

	new str[128];
	format(str, sizeof(str), "[SKIN] O(A) %s %s setou a skin id %d para %s", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), skinid, GetUserName(id));
	WriteLog(FILE_ADMINS_SETS, str);

	SendClientMessage(playerid, COR_SISTEMA, "CMD-ADMIN: {"COR_BRANCO_INC"}Voc� setou a skinid {"COR_SISTEMA_INC"}%d{"COR_BRANCO_INC"} para {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}.", skinid, GetUserName(id));
	SendClientMessage(id, COR_SISTEMA, "CMD-ADMIN: {"COR_BRANCO_INC"}O(A) %s {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"} setou a skin {"COR_SISTEMA_INC"}%d{"COR_BRANCO_INC"} para voc�.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), skinid);
	return true;
}

alias:trabalharadm("trabalharadmin");
CMD:trabalharadm(playerid)
{
	if(call::ADMIN->GetPlayerAdminLevel(playerid) < AJUDANTE)
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);
	
	switch(call::ADMIN->IsAdminInJob(playerid))
	{
		case true:
		{
			SendClientMessageToAll(COR_SISTEMA, "CMD-ADMIN: {"COR_BRANCO_INC"}O(A) %s {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"} n�o est� mais trabalhando.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid));
			call::ADMIN->ToggleAdminInJob(playerid, false);
		}
		case false:
		{
			SendClientMessageToAll(COR_SISTEMA, "CMD-ADMIN: {"COR_BRANCO_INC"}O(A) %s {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"} est� trabalhando no momento.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid));
			call::ADMIN->ToggleAdminInJob(playerid, true);
		}
	}
	return true;
}

flags:irempresa(ADMINISTRADOR);
CMD:irempresa(playerid, params[])
{
	if(isnull(params) || !IsNumeric(params))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/irempresa [empresaid]");

	new businessid = strval(params);
	
	if(!call::BUSINESS->IsValidBusiness(businessid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Est� empresa n�o existe.");

	if( IsPlayerInAnyVehicle(playerid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando dentro de um veiculo.");
		
	new Float:x, Float:y, Float:z;
	call::BUSINESS->GetBusinessPos(businessid, x, y, z);
	
	Teleport(playerid, x, y, z, 0.0, 0, 0, ENTROU_NONE);
	SendClientMessage(playerid, COR_SISTEMA, "CMD-ADMIN: {"COR_BRANCO_INC"}Voc� foi at� a empresa {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}", call::BUSINESS->GetBusinessName(businessid));
	return true;
}

flags:ircasa(ADMINISTRADOR);
CMD:ircasa(playerid, params[])
{
	if(isnull(params) || !IsNumeric(params))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/ircasa [casaid]");

	new casaid = strval(params);

	if ( !call::HOUSE->IsValidHouse(casaid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Est� casa n�o existe.");
	
	Teleport(
		playerid,
		Casa[casaid][Entrada][X],
		Casa[casaid][Entrada][Y],
		Casa[casaid][Entrada][Z],
		Casa[casaid][Entrada][A],
		0,
		0,
		ENTROU_NONE
	);

	SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Voc� foi at� a casa id {"COR_AZUL_INC"}%d{"COR_BRANCO_INC"}.", casaid);
	return true;
}

flags:irgaragem(ADMINISTRADOR);
CMD:irgaragem(playerid, params[])
{
	if( isnull(params) || !IsNumeric(params) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/irgaragem [casaid]");

	new casaid = strval(params);

	if ( !call::HOUSE->IsValidHouse(casaid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Est� casa n�o existe.");

	if ( !Casa[casaid][Garagem][Id] )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Essa casa n�o tem garagem");

	Teleport(
		playerid,
		Casa[casaid][Garagem][pEntrada][X],
		Casa[casaid][Garagem][pEntrada][Y],
		Casa[casaid][Garagem][pEntrada][Z],
		Casa[casaid][Garagem][pEntrada][A],
		0,
		0,
		ENTROU_NONE
	);
	SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Voc� foi at� a garagem da casa {"COR_AZUL_INC"}%d{"COR_BRANCO_INC"}.", casaid);
	return true;
}
flags:dargrana(ADMINISTRADOR); alias:dargrana("givemoney");
CMD:dargrana(playerid, params[])
{
	new id, valor;
	if(sscanf(params, "ud", id, valor))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/dargrana [playerid/username] [valor]");

	if (IsPlayerNPC(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

	if(!call::PLAYER->IsPlayerLogged(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador n�o est� conectado.");
	
	SendClientMessage(playerid, COR_SISTEMA, "CMD-ADMIN: {"COR_BRANCO_INC"}Voc� deu {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"} para o jogador {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}", RealStr(valor), GetUserName(id));
	SendClientMessage(id, COR_SISTEMA, "CMD-ADMIN: {"COR_BRANCO_INC"}O(A) {"COR_SISTEMA_INC"}%s %s{"COR_BRANCO_INC"} deu {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"} para voc�.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), RealStr(valor));
	GivePlayerMoney(id, valor);

	new fileLog[128 + (MAX_PLAYER_NAME * 2)];
	format(fileLog, sizeof(fileLog), "[ MONEY ] O(A) %s %s deu R$%s de dinheiro para %s", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), RealStr(valor), GetUserName(id));
	WriteLog(FILE_GIVE, fileLog);
	return true;
}

flags:dararma(ADMINISTRADOR); alias:dararma("giveweapon");
CMD:dararma(playerid, params[])
{
	new id, weaponid, ammo;
	if(sscanf(params, "udd", id, weaponid, ammo))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/dararma [playerid/username] [weaponid] [ammo]");

	if (IsPlayerNPC(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

	if ( !call::PLAYER->IsPlayerLogged(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) n�o est� conectado(a).");

	SendClientMessage(playerid, COR_SISTEMA, "CMD-ADMIN: {"COR_BRANCO_INC"}Voc� deu a arma id {"COR_SISTEMA_INC"}%d{"COR_BRANCO_INC"} para o jogador(a) {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}", weaponid, GetUserName(id));
	SendClientMessage(id, COR_SISTEMA, "CMD-ADMIN: {"COR_BRANCO_INC"}O(A) {"COR_SISTEMA_INC"}%s %s{"COR_BRANCO_INC"} deu a arma id {"COR_SISTEMA_INC"}%d{"COR_BRANCO_INC"} com {"COR_SISTEMA_INC"}%d{"COR_BRANCO_INC"} balas para voc�.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), weaponid, ammo);
	
	GivePlayerWeapon(id, weaponid, ammo);

	new fileLog[128 + (MAX_PLAYER_NAME * 2)];
	format(fileLog, sizeof(fileLog), "[ WEAPON ] O(A) %s %s deu a arma id %d com %d muni��es para %s", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), weaponid, ammo, GetUserName(id));
	WriteLog(FILE_GIVE, fileLog);
	return true;
}

flags:removergrana(ADMINISTRADOR); alias:removergrana("removemoney");
CMD:removergrana(playerid, params[])
{
	new id;
	if(sscanf(params, "d", id))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/dargrana [playerid]");

	if (IsPlayerNPC(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

	if(!call::PLAYER->IsPlayerLogged(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador n�o est� conectado.");

	if(call::ADMIN->GetPlayerAdminLevel(id) >= call::ADMIN->GetPlayerAdminLevel(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode espiar esse administrador.");
	
	GivePlayerMoney(playerid, -GetPlayerMoney(playerid));
	ResetPlayerMoney(playerid);

	new query[80];
	mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `dinheiro`='0' WHERE `id`='%d' LIMIT 1;", call::PLAYER->GetPlayerVarInt(playerid, PlayerID));
	mysql_tquery(getConexao(), query);

	new fileLog[130];
	format(fileLog, sizeof(fileLog), "O %s %s resetou o dinheiro de %s", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid));
	WriteLog(FILE_REMOVE_MONEY, fileLog);
	return true;
}

flags:setarcombustivel(AJUDANTE); alias:setarcombustivel("setcomb", "setfuel");
CMD:setarcombustivel(playerid, params[])
{
	new id, Float:fuel;
	if(sscanf(params, "uf", id, fuel))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/setarcombustivel [playerid/username] [quantidade]");

	if ( IsPlayerNPC(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");
 
	if ( !call::PLAYER->IsPlayerLogged(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) n�o est� conectado(a).");

	if( !IsPlayerInAnyVehicle(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador n�o est� no veiculo");

	if( fuel < 1 )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode setar uma valor menor que 1.");

	new Float:max_fuel = GetVehicleMaxFuel(GetVehicleModel(GetPlayerVehicleID(id)));
	
	fuel = ( fuel >  max_fuel ? max_fuel : fuel );
	call::VH->SetVehicleFuel(GetPlayerVehicleID(id), fuel);

	new str[128];
	format(str, sizeof(str), "[COMBUSTIVEL] O %s %s setou %f de combustivel para %s", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), fuel, GetUserName(id));
	WriteLog(FILE_ADMINS_SETS, str);

	SendClientMessage(playerid, COR_SISTEMA, "Voc� setou {"COR_BRANCO_INC"}%d{"COR_SISTEMA_INC"} de combustivel para {"COR_BRANCO_INC"}%s{"COR_SISTEMA_INC"}.", floatround(fuel), GetUserName(id));
	SendClientMessage(id, COR_SISTEMA, "O(A) %s {"COR_BRANCO_INC"}%s{"COR_SISTEMA_INC"} setou o combustivel do seu veiculo para {"COR_BRANCO_INC"}%d{"COR_SISTEMA_INC"}.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), floatround(fuel));
	
	return true;
}

flags:desbugar(AJUDANTE);
CMD:desbugar(playerid, params[])
{
	new id;
	if(sscanf(params, "u", id))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/desbugar [playerid/username]");
	
	if ( IsPlayerNPC(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

	if ( !call::PLAYER->IsPlayerLogged(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) n�o est� conectado(a).");

	Teleport(id, 1481.2660, -1771.1407, 18.7958, 0.0, 0, 0, ENTROU_NONE);
	
	TogglePlayerControllable(id, true);
	ClearAnimations(id);

	SendClientMessage(id, COR_SISTEMA, "O(A) %s {"COR_BRANCO_INC"}%s{"COR_SISTEMA_INC"} desbugou voc�.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid));
	SendClientMessage(playerid, COR_SISTEMA, "Voc� desbugou o jogador %s", GetUserName(id) );
	return true;
}

flags:setaradmin(ADMINISTRADOR); alias:setaradmin("setadmin");
CMD:setaradmin(playerid, params[])
{
	new username[MAX_PLAYER_NAME], level;
	if(sscanf(params, "s[25]d", username, level))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/setaradmin [playerid / username] [level]");


	if(level < 1 || level > DIRECAO)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� tem que digitar um valor entre 1 e %d", DIRECAO);

	if (level >= call::ADMIN->GetPlayerAdminLevel(playerid) )
		return SendClientMessage(playerid, COR_ERRO, "Voc� n�o tem permiss�o para setar esse level de administra��o.");

	new query[255], str[MAX_PLAYER_NAME * 2 + 128], id;

	if(IsNumeric(username))
		id = strval(username);
	else
		id = GetPlayerIDByName(username);

	if(id != INVALID_PLAYER_ID)
	{
		if ( IsPlayerNPC(id) )
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

		if( call::PLAYER->IsPlayerLogged(id) )
		{
			if(call::ADMIN->SetPlayerAdminLevel(id, level))
			{
				mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `admin`='%d' WHERE `id`='%d' LIMIT 1;", level, call::PLAYER->GetPlayerVarInt(id, PlayerID));
				mysql_tquery(getConexao(), query);

				SendClientMessage(id, COR_SISTEMA, "� {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"} adicionou voc� na administra��o do servidor.", GetUserName(playerid));
				SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Voc� setou o(a) jogador(a) {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"} de {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"} do servidor.", GetUserName(id), call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(id)));

				format(str, sizeof(str), "O %s %s setou o jogador %s de %s", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)),  GetUserName(playerid), GetUserName(id), call::ADMIN->CargoAdmin(level) );
				WriteLog(FILE_SET_ADMIN, str);
				return true;
			}
			SendClientMessage(playerid, COR_ERRO, "N�o foi possivel setar o jogador de administrador, o mesmo ainda n�o logou no servidor.");
			return true;
		}
		SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) n�o logou no servidor.");
		return true;
	}
	else
	{
		mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `admin`='%d' WHERE `username`='%e' AND `admin`<'%d' LIMIT 1;", level, username, call::ADMIN->GetPlayerAdminLevel(playerid));
		new Cache:cache = mysql_query(getConexao(), query), affected_rows = cache_affected_rows();
		cache_delete(cache);

		if( affected_rows > 0 )
			SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Voc� setou o jogador {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"} de {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"} do servidor.", username, call::ADMIN->CargoAdmin(level));
		else
			return SendClientMessage(playerid, COR_ERRO, "Erro: N�o foi possivel setar o cargo para o(a) jogador(a).");

		format(str, sizeof(str), "O %s %s setou o jogador %s de %s", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)),  GetUserName(playerid), username, call::ADMIN->CargoAdmin(level));
		WriteLog(FILE_SET_ADMIN, str);
		return true;
	}
}

flags:tiraradmin(ADMINISTRADOR); alias:tiraradmin("removeadmin", "tiraradm");
CMD:tiraradmin(playerid, params[])
{
	new username[MAX_PLAYER_NAME];
	if(sscanf(params, "s[25]", username))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/tiraradmin [playerid / username]");

	
	new query[255], str[MAX_PLAYER_NAME * 2 + 128], id;

	if( IsNumeric(username) )
		id = strval(username);
	else
		id = GetPlayerIDByName(username);

	//
	if( id != INVALID_PLAYER_ID )
	{
		if ( IsPlayerNPC(id) )
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

		if(call::PLAYER->IsPlayerLogged(id))
		{ 
			mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `admin`='0' WHERE `username`='%e' AND `admin`<'%d' LIMIT 1;", username, call::ADMIN->GetPlayerAdminLevel(playerid));
			new Cache:cache = mysql_query(getConexao(), query), affected_rows = cache_num_rows();
			cache_delete(cache);

			if ( affected_rows <= 0)
				return SendClientMessage(playerid, COR_ERRO, "Erro: N�o foi possivel remover o membro da administra��o.");

			SendClientMessage(id, COR_SISTEMA, "� {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"} removeu voc� da administra��o do servidor.", GetUserName(playerid));
			SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Voc� removeu o(a) jogador(a) {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"} da administra��o do servidor.", GetUserName(id));
			
			format(str, sizeof(str), "O(A) %s %s removeu o jogador %s de %s", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)),  GetUserName(playerid), GetUserName(id), call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(id)) );
			WriteLog(FILE_REMOVE_ADMIN, str);

			call::ADMIN->RemovePlayerAdmin(id);

		}
		else
			return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador n�o est� conectado.");
	}	
	else
	{
		mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `admin`='0' WHERE `username`='%e' AND `admin`<'%d' LIMIT 1;", username, call::ADMIN->GetPlayerAdminLevel(playerid) );
		new Cache:cache = mysql_query(getConexao(), query, true), affected_rows = cache_affected_rows();
		cache_delete(cache);
		
		if( affected_rows > 0)
			SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Voc� removeu o jogador {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"} da administra��o do servidor.", username);
		else
			return SendClientMessage(playerid, COR_ERRO, "Erro: N�o foi possivel retirar o(a) jogador(a) da administra��o.");

		format(str, sizeof(str), "O(A) %s %s removeu o jogador %s da administra��o do servidor.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)),  GetUserName(playerid), username);
		WriteLog(FILE_REMOVE_ADMIN, str);
	}
	
	return true;
}

flags:espiar(AJUDANTE); 		alias:espiar("spectate");
CMD:espiar(playerid, params[])
{
	new id;
	if(sscanf(params, "u", id))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/espiar [username/playerid]");

	if(!call::PLAYER->IsPlayerLogged(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador n�o est� conectado.");

	if (IsPlayerNPC(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

	if(call::ADMIN->GetPlayerAdminLevel(id) >= call::ADMIN->GetPlayerAdminLevel(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode espiar esse administrador.");

	if(playerid == id)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode espiar voc� mesmo.");

	if ( id == INVALID_PLAYER_ID )
		return SendClientMessage(playerid, COR_ERRO, "Erro: ID Inv�lido.");

	if(GetPlayerState(playerid) != PLAYER_STATE_SPECTATING)
	{
		static
			Float:x, Float:y, Float:z;
		
		/**
		 *
		 *	@Pegar posi��o atual do administrador.
		 *
		 */
		GetPlayerPos(playerid, x, y, z);
		SetPVarFloat(playerid, "spectating-x", x);
		SetPVarFloat(playerid, "spectating-y", y);
		SetPVarFloat(playerid, "spectating-z", z);
		SetPVarInt(playerid, "spectating-interior", GetPlayerInterior(playerid));
		SetPVarInt(playerid, "spectating-world", GetPlayerVirtualWorld(playerid));
		SetPVarInt(playerid, "spectating-skin", GetPlayerSkin(playerid));

		/**
		 *
		 *	@Setar o interior do jogador(a) a ser monitorado(a).
		 *
		 */

		TogglePlayerSpectating(playerid, true);
	}
	
	SetPlayerVirtualWorld(playerid, GetPlayerInterior(id));
	SetPlayerInterior(playerid, GetPlayerInterior(id));

	if( IsPlayerInAnyVehicle(id) )
		PlayerSpectateVehicle(playerid, GetPlayerVehicleID(id), SPECTATE_MODE_NORMAL);
	else
		PlayerSpectatePlayer(playerid, id, SPECTATE_MODE_NORMAL);
	
	call::TD->ShowPlayerMonitoramento(playerid, id);
	SendClientMessage(playerid, COR_AZUL, "Voc� est� espionando o(a) jogador(a) %s", GetUserName(id));
	return true;
}

flags:espiaroff(AJUDANTE); 		alias:espiaroff("spectateoff");
CMD:espiaroff(playerid, params[])
{
	if(GetPlayerState(playerid) != PLAYER_STATE_SPECTATING)
		return SendClientMessage(playerid, COR_ERRO, "Voc� n�o est� espiando ningu�m no momento.");
	
	TogglePlayerSpectating(playerid, false);

	SetPlayerSkin(playerid, GetPVarInt(playerid, "spectating-skin"));
	SetPlayerPos(playerid, GetPVarFloat(playerid, "spectating-x"), GetPVarFloat(playerid, "spectating-y"), GetPVarFloat(playerid, "spectating-z"));
	SetPlayerInterior(playerid, GetPVarInt(playerid, "spectating-interior"));
	SetPlayerVirtualWorld(playerid, GetPVarInt(playerid, "spectating-world"));
	
	DeletePVar(playerid, "spectating-x");
	DeletePVar(playerid, "spectating-y");
	DeletePVar(playerid, "spectating-z");
	DeletePVar(playerid, "spectating-interior");
	DeletePVar(playerid, "spectating-world");
	DeletePVar(playerid, "spectating-skin");

	call::TD->HidePlayerMonitoramento(playerid);
	SendClientMessage(playerid, COR_AZUL, "Voc� n�o est� mais espionando.");
	return true;
}

flags:criarveiculo(AJUDANTE); 		alias:criarveiculo("cv", "createvehicle");
CMD:criarveiculo(playerid, params[])
{
	if ( isnull(params) || !IsNumeric(params) )
		ShowModelSelectionMenu(playerid, mS_CarAdmins, "Veiculos Admin", 150, 0xA4A4A4FF, 0x00FFCCAA);
	else
	{
		new modelid =  strval(params);
		if ( modelid < 400 || modelid > 611 )
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� digitou um modelo inv�lido.");

		if(call::ADMIN->CreateVehicleToAdmin(playerid, modelid)){
				SendClientMessage(playerid, COR_SISTEMA, "CMD-ADMIN: {"COR_BRANCO_INC"}Voc� criou o veiculo {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}(Modelid: {"COR_SISTEMA_INC"}%d{"COR_BRANCO_INC"}).", GetVehicleModelName(modelid), modelid);
			}	
		}
	return true;
}


flags:skins(AJUDANTE);
CMD:skins(playerid, params[])
{
	ShowModelSelectionMenu(playerid, mS_AllSkins, "Skins Admin", 150, 0xA4A4A4FF, 0x00FFCCAA);
	return true;
}


flags:armas(AJUDANTE);
CMD:armas(playerid, params[])
{
	ShowModelSelectionMenu(playerid, mS_AllWeapons, "Armas Admin", 150, 0xA4A4A4FF, 0x00FFCCAA);
	return true;
}



flags:destruirveiculos(AJUDANTE); alias:destruirveiculos("dvs", "destroyvehicles");
CMD:destruirveiculos(playerid)
{
	if(call::ADMIN->DestroyAllVehicleToAdmin(playerid)){
		SendClientMessage(playerid, COR_SISTEMA, "Voc� destruiu todos os seus veiculos criados.");
	}
	return true;
}

flags:destruirveiculo(AJUDANTE); alias:destruirveiculo("dv", "destroyvehicle");
CMD:destruirveiculo(playerid)
{
	if(!IsPlayerInAnyVehicle(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� deve entrar no veiculo que criou para destrui-lo.");

	if(call::ADMIN->DestroyVehicleToAdmin(playerid, GetPlayerVehicleID(playerid))){
		SendClientMessage(playerid, COR_SISTEMA, "Voc� destruiu o veiculo com sucesso.");
	}
	return true;
}

flags:tempban(MODERADOR);
CMD:tempban(playerid, params[])
{
	new id, reason[100], tempo, gip[16];
	if(sscanf(params, "us[100]d", id, reason, tempo))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/tempban [playerid / username] [ras�o] [tempo]");

	if (!IsPlayerConnected(id) || IsPlayerNPC(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador n�o est� conectado! Voc� pode usar o /banip ou /banconta");

	if ( call::ADMIN->GetPlayerAdminLevel(playerid) < call::ADMIN->GetPlayerAdminLevel(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode banir um administrador.");

	GetPlayerIp(id, gip, sizeof(gip));

	CleanChat(id);

	if(call::ADMIN->Ban(GetUserName(id), gip, GetUserName(playerid), reason, tempo))
	{
		new banLog[128 + (MAX_PLAYER_NAME * 2) + sizeof(reason)];
		format(banLog, sizeof(banLog), "%s baniu o jogador %s pelo motivo %s", GetUserName(playerid), GetUserName(id), reason);
		WriteLog(FILE_BAN, banLog);

		SendClientMessage(id, COR_VERMELHO, "O(A) %s %s baniu voc� por %d dia(s) motivo: ( %s ).", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), tempo, reason);
		SendClientMessage(id, COR_VERMELHO, "CONTA-BAN: Voc� foi banido(a) do Servidor por %d dia(s) motivo de (%s) se achar que foi banido(a) injustamente, fa�a uma Revis�o de Ban - IP mas tire uma print usando o bot�o F8 do seu computador e poste em: F�RUM-REVIS�O: "SERVER_SITE"", tempo, reason);
		SendClientMessageToAll(COR_VERMELHO, "CMD-ADMIN: O(A) %s %s baniu o(a) jogador(a) %s por %d dia(s) motivo: ( %s ).", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), GetUserName(id), tempo, reason);
		return Kick(id);
	}
	SendClientMessage(playerid, COR_ERRO, "Erro: N�o foi possivel banir o jogador.");
	return true;
}

flags:ban(MODERADOR);
CMD:ban(playerid, params[])
{
	new id, reason[100];
	if(sscanf(params, "us[100]", id, reason))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/ban [playerid / username] [ras�o]");


	if (!IsPlayerConnected(id) || IsPlayerNPC(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador n�o est� conectado! Voc� pode usar o /banip ou /banconta");

	if ( call::ADMIN->GetPlayerAdminLevel(playerid) < call::ADMIN->GetPlayerAdminLevel(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode banir um administrador.");

	new gip[16];
	GetPlayerIp(id, gip, sizeof(gip));

	CleanChat(id);

	if(call::ADMIN->Ban(GetUserName(id), gip, GetUserName(playerid), reason, 0))
	{
		new banLog[128 + (MAX_PLAYER_NAME * 2) + sizeof(reason)];
		format(banLog, sizeof(banLog), "%s baniu o jogador %s pelo motivo %s", GetUserName(playerid), GetUserName(id), reason);
		WriteLog(FILE_BAN, banLog);

		SendClientMessage(id, COR_AMARELO, "O(A) %s %s baniu voc� pelo motivo: %s", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), reason);
		SendClientMessageToAll(COR_SISTEMA, "CMD-ADMIN: {"COR_BRANCO_INC"}O(A) Jogador(a) {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"} foi banido {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"} (motivo: {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}).", GetUserName(id), GetUserName(playerid), reason);
		return Kick(id);
	}
	SendClientMessage(playerid, COR_ERRO, "Erro: N�o foi possivel banir o jogador.");
	return true;
}

flags:banip(MODERADOR);
CMD:banip(playerid, params[])
{
	new ip[16], reason[100];
	if(sscanf(params, "s[16]s[100]", ip, reason))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/banip [ip] [motivo]");

	if( strcmp(ip, "127.0.0.1", true) == 0 && !isnull(ip))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode banir este IP.");

	if(call::ADMIN->Ban("", ip, GetUserName(playerid), reason, 0))
	{
		new banLog[128 + MAX_PLAYER_NAME + sizeof(reason)];
		format(banLog, sizeof(banLog), "%s baniu o ip %s pelo motivo %s", GetUserName(playerid), ip, reason);
		WriteLog(FILE_BAN, banLog);

		SendClientMessage(playerid, COR_SISTEMA, "CMD-ADMIN: {"COR_BRANCO_INC"}O IP {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"} foi banido.", ip);
		return true;
	}
	SendClientMessage(playerid, COR_ERRO, "Erro: N�o foi possivel banir o ip.");
	return true;
}

flags:banconta(MODERADOR);
CMD:banconta(playerid, params[])
{
	new username[MAX_PLAYER_NAME], reason[100];
	if(sscanf(params, "s[25]s[100]" , username, reason))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/banconta [nick] [motivo]");

	if( call::ADMIN->Ban(username, "", GetUserName(playerid), reason, 0) )
	{
		new banLog[255], id = GetPlayerIDByName(username);

		if ( IsPlayerConnected(id) )
			Kick(id);
			
		format(banLog, sizeof(banLog), "%s baniu o jogador %s pelo motivo %s", GetUserName(playerid), username, reason);
		WriteLog(FILE_BAN, banLog);

		SendClientMessage(playerid, COR_SISTEMA, "CMD-ADMIN: {"COR_BRANCO_INC"}O(A) Jogador(a) {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"} foi banido.", username);
		return true;
	}
	SendClientMessage(playerid, COR_ERRO, "Erro: N�o foi possivel banir o jogador %s.", username);
	return true;
}

flags:desban(ADMINISTRADOR); alias:desban("desbanir", "unban");
CMD:desban(playerid, params[])
{
	new username[MAX_PLAYER_NAME];
	if(sscanf(params, "s[25]", username))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/desban [username / ip]");

	if(call::ADMIN->Unban(username, username))
	{
		new banLog[14 + MAX_PLAYER_NAME];
		format(banLog, sizeof(banLog), "%s desbaniu o IP/Jogador(a) %s", GetUserName(playerid), username);
		WriteLog(FILE_DESBAN, banLog);

		SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Voc� desbaniu o ip/jogador(a) %s", username);
		return true;
	}
	SendClientMessage(playerid, COR_ERRO, "Erro: N�o foi possivel desbanir, verifique se a conta est� realmente banida.");
	return true;
}

flags:kick(AJUDANTE); alias:kick("kickar", "kickplayer");
CMD:kick(playerid, params[])
{
	new pid, motivo[64];
	if(sscanf(params, "ds[64]", pid, motivo))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/kick [playerid] [motivo]");

	if (IsPlayerNPC(pid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

	if(!call::PLAYER->IsPlayerLogged(pid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador n�o est� conectado.");

	CleanChat(pid);
	SendClientMessage(pid, COR_BRANCO, "� {"COR_AVISO_INC"}Voc� foi kickado do servidor pelo %s %s.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid));
	SendClientMessage(pid, COR_BRANCO, "� {"COR_AVISO_INC"}Motivo: {"COR_BRANCO_INC"}%s.", motivo);
	Kick(pid);
	
	SendClientMessage(playerid, COR_SISTEMA, "� Voc� kickou o jogador %s pelo motivo %s", GetUserName(pid), motivo);

	SendClientMessageToAll(COR_VERMELHO, "CMD-ADMIN: O(A) %s %s kickou o(a) jogador(a) %s ( Motivo: %s )", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), GetUserName(pid), motivo);

	new str[128];
	format(str, sizeof(str), "O(A) %s %s kickou o(a) jogador(a) %s pelo motivo %s", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), GetUserName(pid), motivo);
	WriteLog(FILE_KICK, str, sizeof(str));
	return true;
}

flags:limparchat(MODERADOR); alias:limparchat("lc", "cleanchat");
CMD:limparchat(playerid, params[])
{
	if ( IsUsedCommandTime() )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Aguarde o comando ser executado no tempo.");

	if ( isnull(params) ){
		CleanChat();
		SendClientMessageToAll(COR_AMARELO, " O(A) {"COR_BRANCO_INC"}%s %s{"COR_AMARELO_INC"} Limpou o chat log..", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid));
	}
	else
	{
		if ( !IsNumeric(params) )
			return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/limparchat [segundos]");

		if ( strval(params) > 60 )
			return SendClientMessage(playerid, COR_ERRO, "Erro: N�o pode usar mais que 1 minuto.");

		ExecuteToTime(playerid, CMD_TYPE_LIMPARCHAT, strval(params));
	}
	return true;
}

flags:ir(AJUDANTE);
CMD:ir(playerid, params[])
{
	
	if(isnull(params))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/ir [playerid/username]");

	new id;
	if(!IsNumeric(params))
		id = GetPlayerIDByName(params);
	else 
		id = strval(params);

	if ( IsPlayerNPC(id) && call::ADMIN->GetPlayerAdminLevel(playerid) < DIRECAO)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

	if(!call::PLAYER->IsPlayerLogged(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador n�o est� mais conectado ou logado.");

	if(id == playerid)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o precisa ir at� voc� mesmo n�o � verdade!");
	
	if( call::PLAYER->GetPlayerVarInt(id, Perseguido) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode ir esse jogador(a) est� em persegui��o.");


	if ( GetPlayerState(id) == PLAYER_STATE_SPECTATING )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode ir at� esse jogador no momento.");

	static Float:x, Float:y, Float:z;

	GetPlayerPos(id, x, y, z);
	Teleport(playerid, x, y, z, 0.0, GetPlayerInterior(id), GetPlayerVirtualWorld(id), Jogador[id][Entrou]);

	SendClientMessage(id, COR_AMARELO, "� {"COR_BRANCO_INC"}O(A) {"COR_AMARELO_INC"}%s %s{"COR_BRANCO_INC"} foi at� voc�!", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid));
	SendClientMessage(playerid, COR_AMARELO, "� {"COR_BRANCO_INC"}Voc� foi at� o jogador {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"}.", GetUserName(id));
	return true;
}

flags:trazer(AJUDANTE);
CMD:trazer(playerid, params[])
{
	if(isnull(params))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/trazer [playerid/username]");

	new id;
	if(!IsNumeric(params))
		id = GetPlayerIDByName(params);
	else 
		id = strval(params);

	if ( IsPlayerNPC(id) && call::ADMIN->GetPlayerAdminLevel(playerid) < DIRECAO )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

	if(!call::PLAYER->IsPlayerLogged(id))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador n�o est� conectado no servidor.");

	if(id == playerid)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o precisa trazer voc� mesmo n�o � verdade!");
	
	if(Jogador[id][Perseguido] == true)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode trazer esse jogador(a) em persegui��o.");

	if ( GetPlayerState(id) == PLAYER_STATE_SPECTATING )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode trazer esse jogador no momento.");

	SetPVarInt(id, "TrazendoID", playerid);
	ShowPlayerDialog(id, TRAZER_JOGADOR, DIALOG_STYLE_MSGBOX, "{"COR_DISABLE_INC"}Trazer Jogador", "� {"COR_BRANCO_INC"}O(A) %s %s deseja levar voc� at� ele.", "Aceitar", "Recusar", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid));
	return true;
}

flags:agendarprisao(MODERADOR);
CMD:agendarprisao(playerid, params[])
{
	new nick[MAX_PLAYER_NAME], tempo, motivo[64];

	if(sscanf(params, "s[70]is[50]", nick, tempo, motivo))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/agendarprisao [username] [tempo] [motivo]");

	if ( tempo <= 0)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� tem que digitar um valor maior que 0.");

	if ( !call::PLAYER->IsNickRegistered(nick) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: N�o encontramos a conta {"COR_BRANCO_INC"}%s{"COR_ERRO_INC"} no banco de dados.", nick);

	SendClientMessageToAll(COR_AMARELO, "CMD-ADMIN: O(A) %s %s agendou a pris�o do(a) jogador(a) %s por %d minutos ( Motivo: %s ).", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), nick, tempo, motivo);

	tempo = ( tempo * 60);

	new query[255];
    mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `tempo_preso`='%d', `preso`='%d' WHERE `username`='%s'", tempo, PRESO_ADM, nick);
	mysql_tquery(getConexao(), query);
	return true;
}

flags:aprender(AJUDANTE);
CMD:aprender(playerid, params[])
{
	new pid, tempo, motivo[50];

	if(sscanf(params, "uis[50]", pid, tempo, motivo))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/aprender [playerid / username] [tempo] [motivo]");

	if ( tempo <= 0)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� tem que digitar um valor maior que 0.");

	if ( IsPlayerNPC(pid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

	if ( !call::PLAYER->IsPlayerLogged(pid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) n�o est� conectado.");

	if(call::ADMIN->GetPlayerAdminLevel(pid) >= call::ADMIN->GetPlayerAdminLevel(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode prender esse administrador.");

	EnviarCela(pid);
	call::PM->ShowPlayerTextDrawPrisonTime(pid);

	SendClientMessageToAll(COR_AMARELO, "CMD-ADMIN: O(A) %s %s prendeu o(a) jogador(a) %s por %d minutos ( Motivo: %s. )", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), GetUserName(pid), tempo, motivo);

	SendClientMessage(pid, COR_VERMELHO, "PRIS�O-CADEIA: Voc� foi preso(a) por %d minutos por raz�o de ( Motivo: %s. )", tempo, motivo);
	SendClientMessage(playerid, COR_AVISO, "Voc� prendeu o(a) jogador(a) {"COR_BRANCO_INC"}%s{"COR_AVISO_INC"} por {"COR_BRANCO_INC"}%d{"COR_AVISO_INC"} minutos.", GetUserName(pid), tempo);

	tempo = ( tempo * 60);
	Jogador[pid][Preso] = PRESO_ADM;
	Jogador[pid][TempoPreso] = tempo;

	new query[128];
    mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `tempo_preso`='%d', `preso`='%d' WHERE `id`=%d", tempo, PRESO_ADM, call::PLAYER->GetPlayerVarInt(pid, PlayerID));
	mysql_tquery(getConexao(), query);

	new str[128];
	format(str, sizeof(str), "O %s %s prendeu o(a) %s por %d minutos pelo motivo %s", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), GetUserName(pid), tempo, motivo);
	WriteLog(FILE_APRENDER_PRISAO, str, sizeof(str));
	return 1;
}

flags:asoltar(AJUDANTE);
CMD:asoltar(playerid, params[])
{
	new pid, motivo[50];
	if(sscanf(params, "ds[50]", pid, motivo))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/asoltar [playerid] [motivo]");

	if ( IsPlayerNPC(pid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

	if(!call::PLAYER->IsPlayerLogged(pid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) jogador(a) n�o est� conectado.");

	if(Jogador[pid][TempoPreso] < 1)
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) jogador(a) n�o est� preso.");

	call::PM->SoltarPrisioneiro(pid);
	SendClientMessage(pid, COR_SISTEMA, "O(A) {"COR_BRANCO_INC"}%s %s{"COR_SISTEMA_INC"} soltou voc� da priss�o", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid));

	SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Voc� soltou o prisioneiro {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}.", GetUserName(pid));

	new str[128];
	format(str, sizeof(str), "O(A) %s %s soltou o(a) prisioneiro(a) %s pelo motivo %s", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), GetUserName(pid), motivo);
	WriteLog(FILE_SOLTAR_PRISAO, str, sizeof(str));
	return true;
}

flags:tapa(MODERADOR);
CMD:tapa(playerid, params[])
{
	new pid, Float:x, Float:y, Float:z;
	
	if(sscanf(params, "u", pid))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/tapa [playerid / username]");
	
	if ( IsPlayerNPC(pid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

	if( !call::PLAYER->IsPlayerLogged(pid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) n�o est� conectado.");

	if(call::ADMIN->GetPlayerAdminLevel(pid) >= call::ADMIN->GetPlayerAdminLevel(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode tapear esse administrador.");

	GetPlayerPos(pid, x, y, z);
	SetPlayerPos(pid, x, y, z + 20);
	SendClientMessage(pid, COR_SISTEMA, "Voc� levou um tapa do {"COR_BRANCO_INC"}%s %s{"COR_SISTEMA_INC"}.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid));

	SendClientMessage(playerid, COR_SISTEMA, "Voc� tapeou o(a) jogador(a) {"COR_BRANCO_INC"}%s{"COR_SISTEMA_INC"}.", GetUserName(pid));
	return 1;
}

flags:aviso(AJUDANTE);
CMD:aviso(playerid, params[])
{
	new pid, motivo[50];

	if ( sscanf(params, "us[50]", pid, motivo) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/aviso [playerid / username] [motivo]");

	if ( IsPlayerNPC(pid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em NPCs do servidor.");

	if ( !call::PLAYER->IsPlayerLogged(pid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) jogador(a) n�o est� conectado.");

	if ( call::ADMIN->GetPlayerAdminLevel(playerid) < call::ADMIN->GetPlayerAdminLevel(pid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode avisar esse jogador.");

	call::PLAYER->SetPlayerVarInt(pid, Avisos, call::PLAYER->GetPlayerVarInt(pid, Avisos) + 1);

	SendClientMessage(pid, COR_VERMELHO, "AVISO-RECEBIDO: Voc� recebeu [%d/3] aviso(s) motivo: %s, ao completar [3/3] ser� preso por 30 minutos.",call::PLAYER->GetPlayerVarInt(pid, Avisos), motivo);
	SendClientMessage(playerid, COR_SISTEMA, "Voc� avisou o jogador {"COR_BRANCO_INC"}%s{"COR_SISTEMA_INC"} pelo motivo {"COR_BRANCO_INC"}%s{"COR_SISTEMA_INC"}", GetUserName(pid), motivo);
	
	if ( call::PLAYER->GetPlayerVarInt(pid, Avisos) >= 3 )
	{
		call::PLAYER->SetPlayerVarInt(pid, Preso, PRESO_ADM);
		call::PLAYER->SetPlayerVarInt(pid, TempoPreso, (30 * 60));
		EnviarCela(pid);
		call::PM->ShowPlayerTextDrawPrisonTime(pid);
		call::PLAYER->SetPlayerVarInt(pid, Avisos, 0);

		SendClientMessage(pid, COR_ERRO, "Erro: Voc� foi preso por 30 minutos por chegar ao limite de avisos.");	
		call::PM->resetVarsPerseguicao(pid);
	}

	new query[128];
    mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `avisos`='%d' WHERE `id`=%d", call::PLAYER->GetPlayerVarInt(pid, Avisos), call::PLAYER->GetPlayerVarInt(pid, PlayerID));
	mysql_tquery(getConexao(), query);
	return true;
}

flags:rv(MODERADOR);
CMD:rv(playerid)
{
	if ( !IsPlayerInAnyVehicle(playerid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em um veiculo.");

	new vehicleid = GetPlayerVehicleID(playerid);
	if ( !IsValidVehicle(vehicleid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em um veiculo.");

	new trailer = GetVehicleTrailer(vehicleid);
	if ( IsValidVehicle(trailer) )
		SetVehicleToRespawn(trailer);

	SetVehicleToRespawn(vehicleid);
    SendClientMessage(playerid, 0x33FF00FF, "� Voc� respawnou o veiculo");
    return true;
}

flags:rvs(MODERADOR);
CMD:rvs(playerid, params[])
{
	if ( IsUsedCommandTime() )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Aguarde o comando ser executado no tempo.");

	if ( isnull(params) )
	{
		new bool:IsTrailerAttached[MAX_VEHICLES];

		/* verificar trailer attachado */
		foreach(new i: Vehicles)
		{
			if ( IsTrailerAttachedToVehicle(i) ){
				new trailer = GetVehicleTrailer(i);
				IsTrailerAttached[trailer] = true;
			}
		}
		/*		 verificar veiculos desocupados */
	    foreach(new vehicleid: Vehicles)
	    {
	        if ( !IsVehicleOccupied(vehicleid) && !IsTrailerAttached[vehicleid] )
				SetVehicleToRespawn(vehicleid);
	    }
	    SendClientMessageToAll(COR_VERDE, " O(A) {"COR_BRANCO_INC"}%s %s{"COR_VERDE_INC"} deu respawn em todos os veiculos desocupados.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid));
	    return true;
	}
	else
	{
		if ( !IsNumeric(params))
			return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/rvs [segundos]");

		if ( strval(params) > 60 )
			return SendClientMessage(playerid, COR_ERRO, "Erro: N�o pode usar mais que 1 minuto.");
			
		ExecuteToTime(playerid, CMD_TYPE_RVS, strval(params));

	}
    return true;
}

flags:cnn(AJUDANTE);
CMD:cnn(playerid, params[])
{
	if ( isnull(params) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/cnn [mensage]");

	call::TD->ShowCnnMessage(playerid, params);
	return true;
}

flags:ann(AJUDANTE);
CMD:ann(playerid, params[])
{
	new string[128];

	if ( isnull(params) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/cnn [mensage]");

	format(string, sizeof(string), "~w~%s", params);
    GameTextForAll(string, 2000, 3);
	return true;
}

flags:cornick(AJUDANTE);
CMD:cornick(playerid, params[])
{
 	if( isnull(params) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/cornick [C�digo-Html]");
	
	if ( !IsValidHex(params, VERIFY_HEX_8) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voce n�o digitou um c�digo html v�lido, use o tipo RRGGBBAA.");

	Jogador[playerid][CorNick] = HexToInt(params);
	SetPlayerColor(playerid, Jogador[playerid][CorNick]);
	SendClientMessage(playerid, COR_SISTEMA, "CMD-ADMIN:{"COR_BRANCO_INC"} Voc� alterou a cor do seu nick para ( {%06x}Cor{"COR_BRANCO_INC"} ).", (Jogador[playerid][CorNick] >>> 8) );
	return 1;
}

flags:setarvip(ADMINISTRADOR); alias:setarvip("setvip");
CMD:setarvip(playerid, params[])
{
	new username[MAX_PLAYER_NAME], dias;

	if ( sscanf(params, "s[25]d", username, dias) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/setarvip [username] [dias]");

	if ( dias < 1)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode setar vip com menos de 1 dia.");

	if ( dias > 365)
		return SendClientMessage(playerid, COR_ERRO, "Erro: N�o � possivel setar vip com mais de um ano de dura��o.");

	new id = GetPlayerIDByName(username);
	if ( id == INVALID_PLAYER_ID || !IsPlayerConnected(id))
	{
		SendClientMessage(playerid, COR_ERRO, "Erro: Jogador(a) n�o est� conectado.");
		return 1;
	}

	if ( !Jogador[id][Vip] )
	{
		if ( SetPlayerVip(username, dias, false) )
		{
			Jogador[id][Vip] = true;
			SendClientMessage(id, COR_AZUL, "� {"COR_BRANCO_INC"}O(A) {"COR_AZUL_INC"}%s %s{"COR_BRANCO_INC"} setou {"COR_AZUL_INC"}%d{"COR_BRANCO_INC"} dias de vip para voc�.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), dias);
			SendClientMessage(playerid, COR_AZUL, "� {"COR_BRANCO_INC"}Voc� setou {"COR_AZUL_INC"}%d{"COR_BRANCO_INC"} dia(s) vip para o(a) jogador(a) {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"} com {"COR_VERDE_INC"}sucesso{"COR_BRANCO_INC"}!", dias, username );

			new str[128];
			format(str, sizeof(str), "O %s %s setou vip para o jogador %s de %d dias.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), username, dias);
			WriteLog(FILE_SET_VIP, str);
			return true;
		}
		SendClientMessage(playerid, COR_ERRO, "Erro: N�o foi poss�vel setar a vip do jogador.");
		return true;
	}
	else
	{
		if ( SetPlayerVip(username, dias, true) )
		{
			Jogador[playerid][Vip] = true;
			
			SendClientMessage(id, COR_AZUL, "� {"COR_BRANCO_INC"}O(A) {"COR_AZUL_INC"}%s %s{"COR_BRANCO_INC"} renovou sua vip com mais {"COR_AZUL_INC"}%d{"COR_BRANCO_INC"} dias.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), dias);
			SendClientMessage(playerid, COR_AZUL, "� {"COR_BRANCO_INC"}Voc� renovou a vip do jogador {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"} com {"COR_VERDE_INC"}sucesso{"COR_BRANCO_INC"}!", username );

			new str[128];
			format(str, sizeof(str), "O %s %s setou vip para o jogador %s de %d dias.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), username, dias);
			WriteLog(FILE_SET_VIP, str);
			return true;
		}
	}
	SendClientMessage(playerid, COR_ERRO, "Erro: N�o foi possivel setar a vip do jogador, verifique o nome digitado.");
	return true;
}

flags:removervip(DIRECAO);
CMD:removervip(playerid, params[])
{
	new username[MAX_PLAYER_NAME];
	if ( sscanf(params, "s[25]", username) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/removervip [username]");

	new id = GetPlayerIDByName(username), query[128];
	
	if ( IsPlayerConnected(id) )
	{
		Jogador[id][Vip] = false;
		Jogador[id][TempoVip] = 0;
		
		mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `vip`='0',`vencimento_vip`='0' WHERE `id`='%d';", Jogador[id][PlayerID]);
		mysql_tquery(getConexao(), query);

		SendClientMessage(id, COR_LARANJA, "O(A) Diretor(a) %s removeu seu plano VIP.", GetUserName(playerid));
	}
	else
	{
		mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `vip`='0',`vencimento_vip`='0' WHERE `id`='%d';", Jogador[id][PlayerID]);
		mysql_tquery(getConexao(), query);
	}
	SendClientMessage(playerid, COR_LARANJA, "Voc� removeu a vip do(a) jogador(a) %s", GetUserName(id));
	return 1;
}

flags:setarviptodos(DIRECAO);
CMD:setarviptodos(playerid, params[])
{
	new dias;
	if ( sscanf(params, "d", dias) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/setarviptodos [dias]");

	foreach(new i: Player)
	{
		if ( !Jogador[i][Vip] && call::ADMIN->GetPlayerAdminLevel(i) < AJUDANTE)
		{
			SetPlayerVip(GetUserName(i), dias, false);
			Jogador[i][Vip] = true;
			SendClientMessage(i, COR_VERDE, "� {"COR_AZUL_INC"}O(A) %s %s setou vip para voc� de {"COR_VERDE_INC"}%d{"COR_AZUL_INC"} dia(s).", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), dias);
		}
	}
	SendClientMessage(playerid, COR_VERDE, "� Vip setadas para todos.");
	return true;
}

flags:dargranatodos(DIRECAO);
CMD:dargranatodos(playerid, params[])
{
	new money;
	if ( sscanf(params, "d", money) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/dargranatodos [valor]");

	foreach(new i: Player)
	{
		if ( call::ADMIN->GetPlayerAdminLevel(i) < AJUDANTE)
		{
			GivePlayerMoney(playerid, money);
			SendClientMessage(i, COR_VERDE, "� {"COR_AZUL_INC"}O(A) %s %s deu R$%s para voc�.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), RealStr(money));
		}
	}
	SendClientMessage(playerid, COR_VERDE, "� Todos receberam sua grana.");
	return true;
}

flags:setarnivel(DIRECAO); alias:setarnivel("setscore");
CMD:setarnivel(playerid, params[])
{
	new id, score;
	if (sscanf(params, "dd", id, score))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/setarnivel [playerid] [score]");

	if ( !call::PLAYER->IsPlayerLogged(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) n�o est� logado(a).");

	SetPlayerScore(id, score);
	
	new query[128];
	mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `level`='%d' WHERE `id`='%d' LIMIT 1;", GetPlayerScore(id), call::PLAYER->GetPlayerVarInt(id, PlayerID));
	mysql_tquery(getConexao(), query);
	call::TD->UpdateTextDrawHudLevel(playerid);
	call::TD->UpdateTextDrawHudExp(playerid);
	
	SendClientMessage(playerid, COR_AZUL, "* {"COR_BRANCO_INC"}Voc� setou o level do jogador {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"} para {"COR_VERMELHO_INC"}%d{"COR_BRANCO_INC"}.", GetUserName(id), score);
	SendClientMessage(id, COR_AZUL, "* {"COR_BRANCO_INC"}O(A) %s {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"} setou seu level para {"COR_VERMELHO_INC"}%d{"COR_BRANCO_INC"}.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), score);
	return true;
}

flags:desmutar(AJUDANTE); alias:desmutar("descalar");
CMD:desmutar(playerid, params[])
{
	new id, str[128], motivo[64];
	if (sscanf(params, "ds[64]", id, motivo))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/desmutar [playerid] [motivo]");

	if ( !call::PLAYER->IsPlayerLogged(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) n�o est� logado(a).");

	if ( call::ADMIN->GetPlayerAdminLevel(playerid) < call::ADMIN->GetPlayerAdminLevel(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode desmutar esse administrador.");

	if(Jogador[id][Calado] != true)
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) n�o est� mutado(a).");

	Jogador[id][TempoCalado] = 0, Jogador[id][Calado] = false;

	SendClientMessageToAll(COR_AZUL, "* O(A) {"COR_BRANCO_INC"}%s %s{"COR_AZUL_INC"} desmutado o jogador {"COR_BRANCO_INC"}%s{"COR_AZUL_INC"} Motivo: {"COR_BRANCO_INC"}%s.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), GetUserName(id), motivo);
	SendClientMessage(id, COR_VERMELHO, "* Voc� foi desmutado Motivo: {"COR_BRANCO_INC"}%s.", motivo);

	format(str, sizeof(str), "O(A) %s desmutou o jogador %s Motivo (( %s ))", GetUserName(playerid), GetUserName(id), motivo);
	WriteLog("admins/calar.txt", str);
	return true;
}

flags:mutar(AJUDANTE); alias:mutar("calar");
CMD:mutar(playerid, params[])
{
	new id, motivo[64], tempo;
	if (sscanf(params, "ds[64]d", id, motivo, tempo))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/mutar [playerid] [motivo] [tempo]");

	if ( tempo <= 0)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� tem que digitar um valor maior que 0.");

	if ( !call::PLAYER->IsPlayerLogged(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) n�o est� logado(a).");

	if ( call::ADMIN->GetPlayerAdminLevel(playerid) < call::ADMIN->GetPlayerAdminLevel(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode mutar esse administrador.");

	if(Jogador[id][Calado] != false)
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) j� est� calado(a).");

	SendClientMessageToAll(COR_AZUL, "* O(A) {"COR_BRANCO_INC"}%s %s{"COR_AZUL_INC"} mutou o jogador {"COR_BRANCO_INC"}%s{"COR_AZUL_INC"} Motivo: {"COR_BRANCO_INC"}%s{"COR_AZUL_INC"} Tempo: {"COR_BRANCO_INC"}%d minutos.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), GetUserName(id), motivo, tempo);
	SendClientMessage(id, COR_VERMELHO, "* Voc� foi mutado Motivo: {"COR_BRANCO_INC"}%s{"COR_VERMELHO_INC"} Tempo{"COR_BRANCO_INC"}: %d minutos.", motivo, tempo);
	
	new str[128];
	format(str, sizeof(str), "O(A) %s mutou o jogador %s pelo motivo (( %s )) tempo (( %d ))", GetUserName(playerid), GetUserName(id), motivo, tempo);
	WriteLog("admins/calar.txt", str);

	SetTimerEx("TimerMute", 1000, false, "i", id);

	tempo = ( tempo * 60);
	Jogador[id][TempoCalado] = tempo, Jogador[id][Calado] = true;
	return true;
}

flags:mudarclima(AJUDANTE);
CMD:mudarclima(playerid, params[])
{
	new climaid;

	if (sscanf(params, "d", climaid))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/mudarclima [climaid]");

	if(climaid < 0 || climaid > 23)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Clima inexistente.");

	foreach(new i: Player)
	{
		SetPlayerWeather(i, climaid);
	}
	SendClientMessageToAll(COR_VERDE, "� {"COR_AZUL_INC"}O(A) %s %s mudou o clima do servidor para: %d.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid), climaid);
	return true;
}

flags:guinchar(AJUDANTE);
CMD:guinchar(playerid, params[])
{
	new vehicleid = GetPlayerVehicleID(playerid);
	if ( GetVehicleModel(vehicleid) != 525 )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em um caminh�o guincho");

	if ( IsTrailerAttachedToVehicle(vehicleid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O veiculo j� est� com um veiculo no guincho.");

	if((IsPlayerInAnyVehicle(playerid)) && (GetPlayerState(playerid) == PLAYER_STATE_DRIVER))
	{
		new vehid = GetVehicleIdToRadiu(vehicleid, 15.0);
		if ( vehid != INVALID_VEHICLE_ID )
		{
			AttachTrailerToVehicle(vehid, vehicleid);
			SendClientMessage(playerid, COR_AVISO, "* Veiculo colocado no guincho.");
		}
		return true;
	}
	return true;
}

flags:verip(MODERADOR);
CMD:verip(playerid, params[])
{
	new pid, gip[16];

	if (sscanf(params, "d", pid))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/verip [playerid]");

	if ( call::ADMIN->GetPlayerAdminLevel(playerid) < call::ADMIN->GetPlayerAdminLevel(pid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode ver ip de um administrador.");

	GetPlayerIp(pid, gip, sizeof(gip));
	SendClientMessage(playerid, COR_AVISO, "Nick: %s IP: %s", GetUserName(pid), gip);
	return true;
}

flags:godmod(AJUDANTE); alias:godmod("gm");
CMD:godmod(playerid)
{
	if(GodMod[playerid] == false)
	{
		GodMod[playerid] = true;
		SetPlayerHealth(playerid, 0x24FF0AB9);
		GameTextForPlayer(playerid,"~n~~g~GodMod Ativado!", 3000, 5);
	}
	else
	{
		GodMod[playerid] = false;
		SetPlayerHealth(playerid, 100);
		GameTextForPlayer(playerid, "~n~~g~GodMod Desativado!", 3000, 5);
	}
	return true;
}

flags:godcar(AJUDANTE); alias:godcar("gc");
CMD:godcar(playerid)
{
	if(GodCar[playerid] == false)
	{
		if((IsPlayerInAnyVehicle(playerid)) && (GetPlayerState(playerid) == PLAYER_STATE_DRIVER))
		{
			GodCar[playerid] = true;
			RepairVehicle(GetPlayerVehicleID(playerid));
			SetVehicleHealth(GetPlayerVehicleID(playerid), 1000.0);
			GameTextForPlayer(playerid,"~n~~g~GodCar Ativado!", 3000, 5);
			PlayerPlaySound(playerid, 1057, 0,0,0);
		}
	}
	else
	{
		GodCar[playerid] = false;
		SetVehicleHealth(GetPlayerVehicleID(playerid), 1000);
		GameTextForPlayer(playerid, "~n~~g~GodCar Desativado!", 3000, 5);
		PlayerPlaySound(playerid, 1057, 0,0,0);
	}
	return true;
}

flags:explodir(ADMINISTRADOR);
CMD:explodir(playerid, params[])
{
	new pid, Float: pos[3];

	if (sscanf(params, "d", pid))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/explodir [id]");

	if ( call::ADMIN->GetPlayerAdminLevel(playerid) < call::ADMIN->GetPlayerAdminLevel(pid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode explodir um administrador.");

	GetPlayerPos(pid, pos[0], pos[1], pos[2]);
    CreateExplosion(pos[0], pos[1], pos[2], 12, 25.0);
    CreateExplosion(pos[0], pos[1], pos[2], 12, 25.0);

    SendClientMessage(playerid, COR_AVISO, "CMD-ADMIN: Voc� explodiu o(a) jogador(a) %s.", GetUserName(pid));
    SendClientMessage(pid, COR_AVISO, "CMD-ADMIN: O(A) %s %s explodiu voc�.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid));
    return true;
}

flags:players(AJUDANTE);
CMD:players(playerid)
{
	new str[1024 * 2];
	foreach(new i: Player)
	{
		format(str, sizeof(str), "%s{"COR_VERMELHO_INC"}%d\t{"COR_AZUL_INC"}%s\t%s\n", str, i, GetUserName(i), call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(i)));
	}
	ShowPlayerDialog(playerid, 0, DIALOG_STYLE_TABLIST, "PLAYERS ONLINE", str, "OK", "");
	return true;
}

flags:exit(DIRECAO);
CMD:exit(playerid)
{
	SendClientMessage(playerid, -1, "Fechando o servidor.");
	SendRconCommand(#exit);
	return true;
}


flags:gmx(DIRECAO);
CMD:gmx(playerid)
{
	SendClientMessage(playerid, -1, "Reiniciando o servidor.");
	SendRconCommand(#gmx);
	return true;
}

flags:darcash(DIRECAO);
CMD:darcash(playerid, params[])
{
	new pid, cash;
	if ( sscanf(params, "ud", pid, cash) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/darcash [id] [quantidade]");


	Jogador[pid][Cash] += cash;

	new query[128];
	mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `cash`='%d' WHERE `id`='%d' LIMIT 1;", Jogador[pid][Cash], Jogador[pid][PlayerID]);
	mysql_tquery(getConexao(), query);

	SendClientMessage(playerid, COR_BRANCO, "Voc� setou {"COR_AZUL_INC"}%d{"COR_BRANCO_INC"} de BitCoins para o jogador {"COR_LARANJA_INC"}%s{"COR_BRANCO_INC"}.", cash, GetUserName(pid));
	SendClientMessage(pid, COR_BRANCO, "Voc� ganhou {"COR_LARANJA_INC"}%d{"COR_BRANCO_INC"} BitCoins pelo %s %s", Jogador[pid][Cash], call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid));
	return true;
}
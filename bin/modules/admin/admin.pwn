//A TESTANDO PORRA

#define ConvertDays(%0) (gettime() + (86400 * (%0)))

enum e_ADMIN_TIME
{
	HORAS,
	MINUTOS,
	SEGUNDOS
}

enum e_ADMIN_INFO
{
	AdminLevel,
	vAdmin[MAX_VEHICLES_PER_ADMIN],
	bool:Trabalhando
}
static 
	Admin[MAX_PLAYERS][e_ADMIN_INFO];

new bool: GodMod[MAX_PLAYERS], bool: GodCar[MAX_PLAYERS];

#include ../bin/modules/admin/cmds.pwn
#include ../bin/modules/admin/dialogs.pwn

// ============================== [ CALLBACKS ] ============================== //
#include <YSI_Coding\y_hooks>
hook OnPlayerConnect(playerid)
{
	new reset[e_ADMIN_INFO];
	Admin[playerid] = reset;
}

hook OnPlayerDisconnect(playerid, reason)
{
	call::ADMIN->DestroyAllVehicleToAdmin(playerid);
	GodMod[playerid] = false, GodCar[playerid] = false;
	return Y_HOOKS_CONTINUE_RETURN_1;
}

hook OnPlayerClickMap(playerid, Float:fX, Float:fY, Float:fZ)
{
	if(call::ADMIN->GetPlayerAdminLevel(playerid) >= AJUDANTE)
	{
		if(!call::ADMIN->IsAdminInJob(playerid))
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em modo trabalho para usar teleport map.");

		if ( call::JOB->IsPlayerInWorking(playerid) )
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar o teleport enquanto trabalha como player.");
		
		Teleport(playerid, fX, fY, fZ, 0.0, 0, 0, ENTROU_NONE);

		new str[128];
		format(str, sizeof(str), "O %s %s se teleportou para as coordenadas x=%0.3f y=%0.3f z=%0.3f", call::ADMIN->CargoAdmin(Admin[playerid][AdminLevel]), GetUserName(playerid), fX, fY, fZ);
		WriteLog(FILE_ADMIN_TELEPORT, str);
		return true;
	}
	return false;
}

hook OnPlayerClickPlayer(playerid, clickedplayerid, source)
{
	if(call::ADMIN->GetPlayerAdminLevel(playerid) >= AJUDANTE)
	{
		if( !call::ADMIN->IsAdminInJob(playerid) )
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em modo trabalho para usar esta fun��o.");

		if ( call::JOB->IsPlayerInWorking(playerid) )
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar est� op��o enquanto trabalha como player.");

		if ( clickedplayerid == playerid )
			return false;

		SetPVarInt(playerid, "clickedplayerid", clickedplayerid);
		ShowPlayerDialog(playerid, MENU_PLAYER_CLICKED, DIALOG_STYLE_LIST, "OP��ES", "Ir\nTrazer", "Selecionar", "Cancelar");
		return true;
	}
	return false;
}

hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	if(PRESSED(KEY_FIRE))
	{
		if(NitroInfi[playerid] == true)
		{
			if (IsPlayerInAnyVehicle(playerid))
        	{
            	AddVehicleComponent(GetPlayerVehicleID(playerid), 1010);
        	}
		}
	}
	return Y_HOOKS_CONTINUE_RETURN_1;
}

hook OnVehicleDamageStatusU(vehicleid, playerid)
{
	if(call::ADMIN->GetPlayerAdminLevel(playerid) >= AJUDANTE)
	{
		if(GodCar[playerid] == true)
		{
			RepairVehicle(vehicleid), SetVehicleHealth(vehicleid, 1000.0);
			return true;
		}
	}
	return Y_HOOKS_CONTINUE_RETURN_1;
}
// ============================== [ FUNCTIONS ] ============================== //
function ADMIN::CargoAdmin(level, bool:usecolor=false, resetcolor[]="")
{
	new cargo[35];

	switch(level)
	{
		case DIRECAO:
			format(cargo, sizeof(cargo), "%s%s", (usecolor ? ("{418628}Diretor(a)") : ("Diretor(a)")));
		case ADMINISTRADOR:
			format(cargo, sizeof(cargo), "%s%s", (usecolor ? ("{cf3b3b}Administrador(a)") : ("Administrador(a)")));
		case MODERADOR:
			format(cargo, sizeof(cargo), "%s%s", (usecolor ? ("{378bf8}Moderador(a)") : ("Moderador(a)")));
		case AJUDANTE:
			format(cargo, sizeof(cargo), "%s%s", (usecolor ? ("{ffd700}Ajudante") : ("Ajudante")));
		default:
			format(cargo, sizeof(cargo), "Jogador(a)");
	}
	if(!isnull(resetcolor))
		format(cargo, sizeof(cargo), "%s{%s}", cargo, resetcolor);

	return cargo; 
}

function ADMIN::GetPlayerAdminLevel(playerid){
	return Admin[playerid][AdminLevel];
}

function ADMIN::SetPlayerAdminLevel(playerid, level){

	if(level < AJUDANTE || level > DIRECAO)
		return false;

	Admin[playerid][AdminLevel] = level;

	if(!Iter_Contains(PlayersAdmin, playerid))
		Iter_Add(PlayersAdmin, playerid);
		
	return true;
}
function ADMIN::ToggleAdminInJob(playerid, bool:toggle)
{
	if(!IsPlayerConnected(playerid) || !call::PLAYER->IsPlayerLogged(playerid))
		return false;

	Admin[playerid][Trabalhando] = toggle;
	return true;
}

function ADMIN::IsAdminInJob(playerid)
{
	if ( !IsPlayerConnected(playerid))
		return false;

	return Admin[playerid][Trabalhando];
}

function ADMIN::RemovePlayerAdmin(playerid){

	if(Iter_Contains(PlayersAdmin, playerid)){
		Iter_Remove(PlayersAdmin, playerid);
		Admin[playerid][AdminLevel] = 0;
		return true;
	}
	return false;
}

function ADMIN::CreateVehicleToAdmin(playerid, model)
{
	static Float:x, Float:y, Float:z, Float:a;
	GetPlayerPos(playerid, x, y, z);
	GetPlayerFacingAngle(playerid, a);

	for(new i; i < MAX_VEHICLES_PER_ADMIN; i++)
	{
		if(IsValidVehicle(Admin[playerid][vAdmin][i]))
			continue;

		Admin[playerid][vAdmin][i] = CreateVehicle(model, x, y, z, a, random(200), random(200), -1);
		LinkVehicleToInterior(Admin[playerid][vAdmin][i], GetPlayerInterior(playerid));
		SetVehicleVirtualWorld(Admin[playerid][vAdmin][i], GetPlayerVirtualWorld(playerid));
		PutPlayerInVehicle(playerid, Admin[playerid][vAdmin][i], 0);
		return true;
	}
	return false;
}

function ADMIN::DestroyAllVehicleToAdmin(playerid)
{
	new count=0;
	for(new i; i < MAX_VEHICLES_PER_ADMIN; i++)
	{
		if(IsValidVehicle(Admin[playerid][vAdmin][i]))
		{
			DestroyVehicle(Admin[playerid][vAdmin][i]);
			Admin[playerid][vAdmin][i] = 0;
			count++;
		}
	}
	if(count)return true;
	return false;
}


function ADMIN::DestroyVehicleToAdmin(playerid, vehicleid)
{
	for(new i; i < MAX_VEHICLES_PER_ADMIN; i++)
	{
		if(Admin[playerid][vAdmin][i] == vehicleid)
		{
			DestroyVehicle(Admin[playerid][vAdmin][i]);
			Admin[playerid][vAdmin][i] = 0;
			return true;
		}
	}
	return false;
}

function ADMIN::Ban(name[], ip[], admin[], reason[], tempo)
{
	if( !call::ADMIN->IsBanned(name, ip) )
	{
		if(tempo >= 1)
		{
			tempo = ConvertDays(tempo);
			new query[MAX_PLAYER_NAME + 255];
			mysql_format(getConexao(), query, sizeof(query), "INSERT INTO "TABLE_BANNEDS" (`username`,`admin`,`ip`,`data`,`motivo`,`temp-ban`)VALUES('%s','%s','%s',NOW(),'%s','%d')", name, admin, ip, reason, tempo);
			mysql_tquery(getConexao(), query);
		}
		if(tempo == 0)
		{
			new query[MAX_PLAYER_NAME + 255];
			mysql_format(getConexao(), query, sizeof(query), "INSERT INTO "TABLE_BANNEDS" (`username`,`admin`,`ip`,`data`,`motivo`,`temp-ban`)VALUES('%s','%s','%s',NOW(),'%s','0')", name, admin, ip, reason);
			mysql_tquery(getConexao(), query);
		}

		new gip[16];
		foreach(new i: Player)
		{
			GetPlayerIp(i, gip, sizeof(gip));
			
			if( (strcmp(name, GetUserName(i), true) == 0 && !isnull(name)) || (strcmp(ip, gip, true) == 0 && !isnull(ip)) )
				call::PLAYER->ShowPlayerInfoBanned(i), Kick(i);
		}
		return true;
	}
	return false;
}

function ADMIN::Unban(name[], ip[])
{
	if(call::ADMIN->IsBanned(name, ip))
	{
		new query[MAX_PLAYER_NAME + 255];
		
		if(!isnull(name) && isnull(ip))
			mysql_format(getConexao(), query, sizeof(query), "DELETE FROM "TABLE_BANNEDS" WHERE `username`='%s' LIMIT 1;", name);
		else if(isnull(name))
			mysql_format(getConexao(), query, sizeof(query), "DELETE FROM "TABLE_BANNEDS" WHERE `ip`='%s' LIMIT 1;", ip);
		else 
			mysql_format(getConexao(), query, sizeof(query), "DELETE FROM "TABLE_BANNEDS" WHERE `username`='%s' OR `ip`='%s' LIMIT 1;", name, ip);

		return mysql_tquery(getConexao(), query);
	}
	return false;
}

function ADMIN::ReadBan(username[], ip[], info[], len = sizeof(info))
{
	if(call::ADMIN->IsBanned(username, ip))
	{
		new query[255], tempban;
		mysql_format(getConexao(), query, sizeof(query), "SELECT `admin`,`motivo`,`temp-ban`,DATE_FORMAT(`data`, '%%d/%%m/%%Y as %%H:%%i:%%s') as `dataf` FROM "TABLE_BANNEDS" WHERE `username`='%s' OR `ip`='%s' LIMIT 1;", username, ip);
		new Cache:cache = mysql_query(getConexao(), query, true), admin[MAX_PLAYER_NAME], reason[100], data[30];
		
		cache_get_value_name(0, "admin", admin, sizeof(admin));
		cache_get_value_name(0, "motivo", reason, sizeof(reason));
		cache_get_value_name(0, "dataf", data, sizeof(data));
		cache_get_value_name_int(0, "temp-ban", tempban);

		if(tempban >= 1)
		{
			format(info, len, "{"COR_BRANCO_INC"}\tVoc� foi banido deste servidor.\n\nAdmin: {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"}\n\
			Motivo: {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"}\n\
			Tempo: {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"}\n\
			Data: {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"}.\n\n\tAcesse nosso f�rum em {"COR_SISTEMA_INC"}"SERVER_SITE"", admin, reason, ConvertToDays(tempban), data);
		}
		if(tempban == 0)
		{
			format(info, len, "{"COR_BRANCO_INC"}\tVoc� foi banido deste servidor.\n\nAdmin: {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"}\n\
			Motivo: {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"}\n\
			Tempo: {"COR_VERMELHO_INC"}Permanente{"COR_BRANCO_INC"}\n\
			Data: {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"}.\n\n\tAcesse nosso f�rum em {"COR_SISTEMA_INC"}"SERVER_SITE"", admin, reason, data);
		}
		cache_delete(cache);
	}
	return true;
}

function ADMIN::IsBanned(username[], ip[])
{
	new query[MAX_PLAYER_NAME + 100];
	
	if( isnull(ip) )
		mysql_format(getConexao(), query, sizeof(query), "SELECT `username` FROM "TABLE_BANNEDS" WHERE `username`='%s'LIMIT 1;", username);
	else
		mysql_format(getConexao(), query, sizeof(query), "SELECT `username` FROM "TABLE_BANNEDS" WHERE `ip`='%s' LIMIT 1;", ip);
	
	
	new Cache:cache = mysql_query(getConexao(), query, true), rows = cache_num_rows();
	cache_delete(cache);
	return rows;
}

stock ConvertToDays(n)
{
    new t[5], DString[128];
    t[4] = n-gettime();
    t[0] = t[4] / 3600;
    t[1] = ((t[4] / 60) - (t[0] * 60));
    t[2] = (t[4] - ((t[0] * 3600) + (t[1] * 60)));
    t[3] = (t[0]/24);
 
    if(t[3] > 0)
        t[0] = t[0] % 24,
        format(DString, sizeof(DString), "%ddias, %02dh %02dm e %02ds", t[3], t[0], t[1], t[2]);
    else if(t[0] > 0)
        format(DString, sizeof(DString), "%02dh %02dm e %02ds", t[0], t[1], t[2]);
    else
        format(DString, sizeof(DString), "%02dm e %02ds", t[1], t[2]);
    return DString;
}

forward ExecuteToTime(playerid, type, time);
public ExecuteToTime(playerid, type, time)
{
	SetSVarInt("ExecuteToTime_Var", 1);
	if(time <= 0)
	{
		DeleteSVar("ExecuteToTime_Var");
		switch(type)
		{
			case CMD_TYPE_LIMPARCHAT:
			{
				PC_EmulateCommand(playerid, "/limparchat");
			}
			case CMD_TYPE_RVS:
			{
				PC_EmulateCommand(playerid, "/rvs");
			}
		}
		time = 0;
	    return false;
	}
	new str[5];
	--time;
	format(str, sizeof(str),"%d", time);
	GameTextForAll(str, 1000, 3);
	SetTimerEx("ExecuteToTime", 1000, false, "ddd", playerid, type, time);
	return true;
}

stock IsUsedCommandTime()
	return GetSVarInt("ExecuteToTime_Var");

forward TimerMute(playerid);
public TimerMute(playerid)
{
	if(IsPlayerConnected(playerid))
	{
		if(Jogador[playerid][Calado] != true)
			return false;

		if(Jogador[playerid][TempoCalado] > 0)
		{
			Jogador[playerid][TempoCalado]--;
			SetTimerEx("TimerMute", 1000, false, "i", playerid);
		}
		else
		{
			Jogador[playerid][Calado] = false, Jogador[playerid][TempoCalado] = 0;
			SendClientMessage(playerid, COR_AMARELO, "[BLH]Betinho desmutou voc�.");
		}
	}
	return true;
}

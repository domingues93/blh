

/*
*
*
*			INCLUDE HOOK
*
*
*/
#include <YSI_Coding\y_hooks>


/*
*
*
*			CALLBACK HOOK
*
*
*/

hook OnPlayerDisconnect(playerid, reason)
{
	if( IsPlayerNPC(playerid) )
		return Y_HOOKS_CONTINUE_RETURN_1;

	if( call::PLAYER->IsPlayerLogged(playerid) )
	{
		Jogador[playerid][Calado] = false, Jogador[playerid][TempoCalado] = 0;
		
		static query[1036 * 4], Float:vida, Float:colete, Float:x, Float:y, Float:z, Float:a;

		GetPlayerPos(playerid, x, y, z), GetPlayerFacingAngle(playerid, a);
		GetPlayerHealth(playerid, vida), GetPlayerArmour(playerid, colete);
		
		query[0] = EOS;

		if( call::PLAYER->GetPlayerVarInt(playerid, Perseguido) )
		{
			SendClientMessageToAll(COR_AVISO, "O(A) jogador(a) {"COR_BRANCO_INC"}%s{"COR_AVISO_INC"} foi preso por desconectar do servidor em perseguio.", GetUserName(playerid));
			call::PLAYER->SetPlayerVarInt(playerid, TempoPreso, (GetPlayerWantedLevel(playerid) * 300));
			call::PLAYER->SetPlayerVarInt(playerid, Preso, PRESO_POLICIA);
			call::PM->FinalizarPerseguicao(playerid, 1);
		}
		
		mysql_format(getConexao(), query, sizeof(query),
			"UPDATE "TABLE_USERS" SET `vida`='%0.3f',`colete`='%0.3f',`upm`='%d',`ups`='%d',`spawn_x`='%0.3f',`spawn_y`='%0.3f',`spawn_z`='%0.3f',`spawn_a`='%0.3f',`world`='%d',`interior`='%d',`dinheiro`='%d',`fome`='%0.3f',`sede`='%0.3f',`sono`='%0.3f',`bloqueado_carga`='%d',`ultimo_login`=NOW(),`mercadoria`='%d',`skin`='%d',`admin_trabalhando_horas`='%d',`admin_trabalhando_minutos`='%d',`admin_trabalhando_segundos`='%d',`multas`='%d',`tempo_preso`='%d',`estrelas`='%d',`saldo`='%d',`preso`='%d',`entrou`='%d',`sementes`='%d',`crack`='%d',`maconha`='%d',`cocaina`='%d',`vip`='%d',`skill_pistol`='%d',`skill_mp5`='%d',`skill_fuzil`='%d',`skill_rifle`='%d',`skill_shotgun`='%d' WHERE `id`='%d';",
			vida, 
			colete,
			Jogador[playerid][UPm], Jogador[playerid][UPs],
			x, y, z, a, GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid),
			GetPlayerMoney(playerid),
			Jogador[playerid][Fome],
			Jogador[playerid][Sede],
			Jogador[playerid][Sono],
			(Jogador[playerid][bCarga] > gettime() ? Jogador[playerid][bCarga] : 0),
			Jogador[playerid][Mercadoria],
			Jogador[playerid][Skin],
			Jogador[playerid][TempoAtividade][HORAS],
			Jogador[playerid][TempoAtividade][MINUTOS],
			Jogador[playerid][TempoAtividade][SEGUNDOS],
			Jogador[playerid][Multas],
			Jogador[playerid][TempoPreso],
			GetPlayerWantedLevel(playerid),
			Jogador[playerid][SaldoCelular],
			Jogador[playerid][Preso],
			Jogador[playerid][Entrou],
			Jogador[playerid][Sementes],
			Jogador[playerid][Crack],
			Jogador[playerid][Maconha],
			Jogador[playerid][Cocaina],
			Jogador[playerid][Vip],
			Jogador[playerid][SkillPistol],
			Jogador[playerid][SkillMP5],
			Jogador[playerid][SkillFuzil],
			Jogador[playerid][SkillRifle],
			Jogador[playerid][SkillShotgun],
			Jogador[playerid][PlayerID]);
		mysql_tquery(getConexao(), query);

		// salvar armas
		query[0] = EOS;
		new str[512], weapon, ammo;
		for(new slot; slot <= MAX_PLAYER_WEAPONS; slot++)
		{
			GetPlayerWeaponData(playerid, slot, weapon, ammo);
			format(str, sizeof(str), "%s,`slot%d`=%d,`slot%d_ammo`=%d", str, slot, weapon, slot, ammo);
		}
		str[0] = ' ';
		mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET %s WHERE `id`='%d'", str, Jogador[playerid][PlayerID]);
		mysql_tquery(getConexao(), query);

		// salvar veiculos do jogador.
		SavePlayerVehicles(playerid);
	}

	/* Finalizar a chamada, se o player estiver em uma. */
	if ( call::PLAYER->GetPlayerVarInt(playerid, StateCell) != 0)
		call::CELL->FinalizarChamada(playerid, call::PLAYER->GetPlayerVarInt(playerid, CallerID) );

	/* "Matar" timer o tratamento */
	if ( Jogador[playerid][EmTratamento] != 0 )
		KillTimer(Jogador[playerid][EmTratamento]), Jogador[playerid][EmTratamento]=0;
	
	if ( call::PLAYER->GetPlayerVarInt(playerid, PrisioneiroID) != INVALID_PLAYER_ID ) // Prisioneiro saindo
	{
		new id = call::PLAYER->GetPlayerVarInt(playerid, PrisioneiroID);
		call::PLAYER->SetPlayerVarInt(id, AdvogadoID, INVALID_PLAYER_ID);
		call::PLAYER->SetPlayerVarInt(id, AdvogadoValor, 0);
		call::PLAYER->SetPlayerVarInt(playerid, PrisioneiroID, INVALID_PLAYER_ID);

		SendClientMessage(id, COR_ERRO, "Erro: O(A) Prisioneiro {"COR_BRANCO_INC"}%s{"COR_ERRO_INC"} saiu do servidor e o serviço foi cancelado.", GetUserName(playerid));
	}

	if ( call::PLAYER->GetPlayerVarInt(playerid, AdvogadoID) != INVALID_PLAYER_ID ) // Advogado saindo
	{
		new id = call::PLAYER->GetPlayerVarInt(playerid, AdvogadoID);
		call::PLAYER->SetPlayerVarInt(playerid, AdvogadoID, INVALID_PLAYER_ID);
		call::PLAYER->SetPlayerVarInt(playerid, AdvogadoValor, 0);
		call::PLAYER->SetPlayerVarInt(id, PrisioneiroID, INVALID_PLAYER_ID);

		SendClientMessage(id, COR_ERRO, "Erro: O(A) Advogado {"COR_BRANCO_INC"}%s{"COR_ERRO_INC"} saiu do servidor e o serviço foi cancelado.", GetUserName(playerid));
	}
	
	call::PLAYER->TogglePlayerLogged(playerid, false);
	DeletePVar(playerid, "OferecedorMID"), DeletePVar(playerid, "DinheiroMaconha"), DeletePVar(playerid, "MaconhaOferecida"), DeletePVar(playerid, "MQuantidade");
	DeletePVar(playerid, "OferecedorCID"), DeletePVar(playerid, "DinheiroCocaina"), DeletePVar(playerid, "CocainaOferecida"), DeletePVar(playerid, "CQuantidade");
	DeletePVar(playerid, "OferecedorCraID"), DeletePVar(playerid, "DinheiroCrack"), DeletePVar(playerid, "CrackOferecida"), DeletePVar(playerid, "CraQuantidade");
	DeletePVar(playerid, "AddNeon");
	DestroyDynamicObject(GetPVarInt(playerid, "NeonVermelho")), DestroyDynamicObject(GetPVarInt(playerid, "NeonVermelho2")), DestroyDynamicObject(GetPVarInt(playerid, "NeonAzul"));
	DestroyDynamicObject(GetPVarInt(playerid, "NeonAzul2")), DestroyDynamicObject(GetPVarInt(playerid, "NeonVerde")), DestroyDynamicObject(GetPVarInt(playerid, "NeonVerde2"));
	DestroyDynamicObject(GetPVarInt(playerid, "NeonAmarelo")), DestroyDynamicObject(GetPVarInt(playerid, "NeonAmarelo2")), DestroyDynamicObject(GetPVarInt(playerid, "NeonRosa"));
	DestroyDynamicObject(GetPVarInt(playerid, "NeonRosa2")), DestroyDynamicObject(GetPVarInt(playerid, "NeonBranco")), DestroyDynamicObject(GetPVarInt(playerid, "NeonBranco2"));


	// destruir player progressbar
	DestroyAllPlayerProgressBars(playerid);

	/*		resetar as variaveis 		*/
	new reset[e_PLAYER_INFO];
	Jogador[playerid] = reset;
	return true;
}

hook OnPlayerConnect(playerid)
{

	//if ( IsPlayerAndroid(playerid) )
  	//	SendClientMessage(playerid, COR_AVISO, "Você se conectou por um Celular e foi liberado do Anti-S0beit");

	// Pegar o nome do jogador.
	new loadingPlayerName[MAX_PLAYER_NAME];
	format(loadingPlayerName, MAX_PLAYER_NAME, "connecting_%d", playerid);
	SetPlayerName(playerid, loadingPlayerName);

	SetPlayerColor(playerid, 0x00000000);

	/*if(IsPlayerSobeit(playerid))
	{
		SendClientMessage(playerid, COR_ERRO, "* Você tentou entrar com s0beit e foi kickado.");
		Kick(playerid);
	}*/
	
	Jogador[playerid][Vip] = true;

	return Y_HOOKS_CONTINUE_RETURN_1;
}

hook OnPlayerRequestClass(playerid, classid)
{
	// iniciar váriaveis.
	call::PLAYER->SetPlayerVarInt(playerid, PrisioneiroID, INVALID_PLAYER_ID);

	new pName[MAX_PLAYER_NAME], tempban;
	new Cache:c = mysql_query(getConexao(), "SELECT `username`,`temp-ban` FROM "TABLE_BANNEDS" LIMIT 1;");
	for(new i, r=cache_num_rows(); i < r; i++)
	{
		cache_get_value_name(i, "username", pName, MAX_PLAYER_NAME);
		cache_get_value_name_int(i, "temp-ban", tempban);
	}
	if(tempban >= 1)
	{
		call::PLAYER->ShowPlayerInfoBanned(playerid), Kick(playerid);
	}
	cache_delete(c);

	new gip[16];
	GetPlayerIp(playerid, gip, sizeof(gip));

	if(call::ADMIN->IsBanned(GetUserName(playerid), gip))
	{
		call::PLAYER->ShowPlayerInfoBanned(playerid), Kick(playerid);
		return true;
	}

	if( !call::PLAYER->IsPlayerLoggin(playerid) )
	{
		call::PLAYER->SetPlayerVarInt(playerid, AdvogadoID, INVALID_PLAYER_ID);
		PlayAudioStreamForPlayer(playerid, "https://perfectcityrpg.tk/sounds/open.mp3");
		
		SetPlayerColor(playerid, 0x000000AA);

		TogglePlayerSpectating(playerid, true);
		
		CleanChat(playerid);
		call::PLAYER->TogglePlayerLoggin(playerid, true);

		SetPlayerCameraLogin(playerid, 0);
		SendClientMessage(playerid, COR_SISTEMA, "» {"COR_BRANCO_INC"}Seja Bem vindo(a) á {"COR_SISTEMA_INC"}"SERVER_NAME".");
		call::TD->ShowPlayerLogin(playerid);
		
		return Y_HOOKS_CONTINUE_RETURN_1;
	}
	return Y_HOOKS_CONTINUE_RETURN_0;
}

hook OnPlayerRequestSpawn(playerid)
{
	return call::PLAYER->IsPlayerLogged(playerid);
}

hook OnPlayerSpawn(playerid)
{
	if( !call::PLAYER->IsPlayerLogged(playerid) )
	{
		call::PLAYER->TogglePlayerLogged(playerid, true);

		call::TD->ShowPlayerHud(playerid);
		call::JOB->SetPlayerJobColor(playerid, Jogador[playerid][Profissao]);
		
		StopAudioStreamForPlayer(playerid);		

		//	Criar arquivo de configuração
		new file[MAX_FILE_CONFIG_LEN];
		format(file, sizeof(file), CONFIG_PLAYER_PATH, GetUserName(playerid));

		if( !fexist(file) )
		{
			DOF2::CreateFile(file);
			
			DOF2::SetInt(file, TAG_MOSTRAR_STATUS, 1);
			DOF2::SetInt(file, TAG_MOSTRAR_DATA, 1);
			DOF2::SetInt(file, TAG_CORTAGVIP, -1);
			DOF2::SaveFile();
		}

		if ( DOF2::GetInt(file, TAG_MOSTRAR_STATUS) )
			call::TD->ShowPlayerHudStatus(playerid);
		else
			call::TD->HidePlayerHudStatus(playerid);

		if ( DOF2::GetInt(file, TAG_MOSTRAR_DATA) )
			call::TD->ShowPlayerHudData(playerid);
		else
			call::TD->HidePlayerHudData(playerid);

		Jogador[playerid][CorTag] = DOF2::GetInt(file, TAG_CORTAGVIP);
		return true;
	}

	if ( call::PLAYER->GetPlayerVarInt(playerid, Preso) == SOLTO)
	{
		if(call::PLAYER->GetPlayerVarInt(playerid, pHospital) != 0 )
		{
			if(call::PLAYER->GetPlayerVarInt(playerid, pHospital) == 1){
				SetPlayerPos(playerid, 1173.1145, -1323.7125, 15.3962);
				SetPlayerFacingAngle(playerid, 268.1490);
			}
			else if(call::PLAYER->GetPlayerVarInt(playerid, pHospital) == 2){
				SetPlayerPos(playerid, 2036.5521,-1412.5513,16.9922);
				SetPlayerFacingAngle(playerid, 135.5749);
			}
			else if(call::PLAYER->GetPlayerVarInt(playerid, pHospital) == 3){
				SetPlayerPos(playerid, 1607.4396,1819.1462,10.8280);
				SetPlayerFacingAngle(playerid, 359.6572);
			}
			else if(call::PLAYER->GetPlayerVarInt(playerid, pHospital) == 4){
				SetPlayerPos(playerid, -2665.0503,636.5155,14.4531);
				SetPlayerFacingAngle(playerid, 177.0288);
			}
		    else if(call::PLAYER->GetPlayerVarInt(playerid, pHospital) == 5){
	            SetPlayerPos(playerid, -315.0838,1050.7095,20.3403);
	            SetPlayerFacingAngle(playerid, 359.3905);
			}
			else if(call::PLAYER->GetPlayerVarInt(playerid, pHospital) == 6){
	            SetPlayerPos(playerid, -1514.6455,2523.4263,55.8138);
	            SetPlayerFacingAngle(playerid, 359.6804);
			}
			else if(call::PLAYER->GetPlayerVarInt(playerid, pHospital) == 7){
	            SetPlayerPos(playerid, -2208.2485,-2286.8516,30.6250);
	            SetPlayerFacingAngle(playerid, 319.4752);
			}
			SetCameraBehindPlayer(playerid);

			call::PLAYER->SetPlayerVarInt(playerid, pHospital, 0);

			if ( call::PLAYER->GetPlayerVarInt(playerid, Plano) == 0 )
			{
				call::PLAYER->SetPlayerVarFloat(playerid, Fome, call::PLAYER->GetPlayerVarFloat(playerid, Fome) + Planos[0][FSS]);
				call::PLAYER->SetPlayerVarFloat(playerid, Sede, call::PLAYER->GetPlayerVarFloat(playerid, Sede) + Planos[0][FSS]);
				call::PLAYER->SetPlayerVarFloat(playerid, Sono, call::PLAYER->GetPlayerVarFloat(playerid, Sono) + Planos[0][FSS]);

				SetPlayerArmour(playerid, Planos[0][Vida]);
				
				SendClientMessage(playerid, COR_VERDE, "» {"COR_BRANCO_INC"}Você não possui um plano de saúde, por isso está pagando {"COR_VERDE_INC"}R$350{"COR_BRANCO_INC"} pelo tratamento.");
				GivePlayerMoney(playerid, -350, true);
			}	
			else
			{
				new p = call::PLAYER->GetPlayerVarInt(playerid, Plano);
				SetPlayerArmour(playerid, Planos[p][Colete]);
				SetPlayerHealth(playerid, Planos[p][Vida]);
				Jogador[playerid][Fome] += float(Planos[p][FSS]);
				Jogador[playerid][Sede] += float(Planos[p][FSS]);
				Jogador[playerid][Sono] += float(Planos[p][FSS]);

				SendClientMessage(playerid, COR_AZUL, "* {"COR_BRANCO_INC"}Você é assinante do plano de saúde {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"} e está recebendo os benefícios do plano.", Planos[p][Nome]);
			}
		}
	}
	else
	{
		if ( call::PLAYER->GetPlayerVarInt(playerid, Preso) == PRESO_POLICIA )
		{
			EnviarCela(playerid);
		}
		
	}
	// Anti Cheats
	SetTimerEx("AntiFly", 1000, true, "i", playerid);
	SetTimerEx("AntiAnim", 1000, true, "i", playerid);
	SetTimerEx("AntiInvSlap", 500, true, "i", playerid);
	return true;
}


hook OnPlayerDeath(playerid, killerid, reason)
{
	ResetPlayerWeapons(playerid);
	if ( IsPlayerInEvent(playerid) )
	{
		Teleport(
			playerid,
			1481.2660,
			-1771.1407,
			18.7958,
			0.0,
			0,
			0,
			ENTROU_NONE
		);
		
		TogglePlayerControllable(playerid, true);
		ClearAnimations(playerid);

		SendDeathMessage(killerid, playerid, reason);

		SetPlayerHealth(playerid, 100);

		SendClientMessage(playerid, 0xAF2A2AFF, "» Obrigado por participar do nosso evento.");
		TogglePlayerEvent(playerid, false);
	}
	else
	{
		
		if ( call::PLAYER->GetPlayerVarBool(playerid, Perseguido) && killerid != INVALID_PLAYER_ID && call::PLAYER->GetPlayerVarInt(killerid, Profissao) == POLICIA_MILITAR)
		{
			call::PM->PrenderJogador(killerid, playerid);
		}
		else
		{
			if ( call::PLAYER->GetPlayerVarInt(playerid, Preso) == SOLTO )
			{
				call::PLAYER->SetPlayerVarInt(playerid, pHospital, GetPlayerHospital(playerid));
				SendClientMessage(playerid, COR_AVISO, "Você foi levado para o hospital.");
			}		
			else
			{
				EnviarCela(playerid);
				call::PM->ShowPlayerTextDrawPrisonTime(playerid);
				SendClientMessage(playerid, 0xB1DD00FF, "* Você ainda não cumpriu sua pena.");
			}
		}
		SendDeathMessage(killerid, playerid, reason);
	}
	return Y_HOOKS_CONTINUE_RETURN_1;	
}

hook OnPlayerWeaponShot(playerid, weaponid, hittype, hitid, Float:fX, Float:fY, Float:fZ)
{
	if ( IsPlayerInEvent(playerid) )
		return Y_HOOKS_CONTINUE_RETURN_1;

	if ( BULLET_HIT_TYPE_PLAYER == hittype )
	{
		if ( call::PLAYER->GetPlayerVarInt(hitid, Perseguido) && call::PLAYER->GetPlayerVarInt(playerid, Profissao) == POLICIA_MILITAR)
			return Y_HOOKS_CONTINUE_RETURN_1;

		return Y_HOOKS_CONTINUE_RETURN_1;
	}
	return Y_HOOKS_CONTINUE_RETURN_1;
}

hook OnPlayerText(playerid, text[])
{
	if(call::PLAYER->IsPlayerLogged(playerid))
	{
		if(call::ADMIN->GetPlayerAdminLevel(playerid) >= AJUDANTE)
		{
			if(text[0] == '+' && text[1] == ' ') // chat admins
			{
				foreach(new i: Player)
				{
					if(call::ADMIN->GetPlayerAdminLevel(i))
						SendClientMessage(i, 0x69958FAA, "[ + ] %s [%s] disse: %s", GetUserName(playerid), call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), text[2]);
				}
				
				//SendDiscord(CHANNEL_ADM, "```[ + ] %s [%s] disse: %s```", GetUserName(playerid), call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), text[2]);
				return false;
			}
			else if(text[0] == '#' && text[1] == ' ') // chat admins
			{
				foreach(new i: Player)
				{
					if(call::ADMIN->GetPlayerAdminLevel(playerid) == call::ADMIN->GetPlayerAdminLevel(i))
						SendClientMessage(i, 0x26F5FDFF, "[ # ] %s [%s] disse: %s", GetUserName(playerid), call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), text[2]);
				}
				return false;
			}
		}

		static Float:x, Float:y, Float:z, cargo[20];
		GetPlayerPos(playerid, x, y, z);

		if(Jogador[playerid][Calado] != false) 
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você está mutado e não pode falar no chat ou usar comandos."), false;

		// =========================== [ IN CHAMADA ] =========================== //
		if ( call::PLAYER->GetPlayerVarInt(playerid, StateCell) == EM_CHAMADA )
		{
			new caller = call::PLAYER->GetPlayerVarInt(playerid, CallerID);
			SendClientMessage(caller, 0xFFEC8BFF, "%s diz [celular]: %s", GetUserName(playerid), text);
			SendClientMessage(playerid, 0xFFEC8BFF, "%s diz [celular]: %s", GetUserName(playerid), text);
			return Y_HOOKS_CONTINUE_RETURN_0;
		}
		// ===================================================================== //

		if ( call::ADMIN->GetPlayerAdminLevel(playerid) >= AJUDANTE && call::ADMIN->IsAdminInJob(playerid) )
		{
			switch(call::ADMIN->GetPlayerAdminLevel(playerid))
			{
				case AJUDANTE:cargo="{FFD700}Ajud";
				case MODERADOR:cargo="{FF8000}Mod";
				case ADMINISTRADOR:cargo="{0080FF}Admin";
				case DIRECAO:cargo="{088A08}Diretor(a)";
			}

			foreach(new i: Player)
			{
				if(IsPlayerInRangeOfPoint(i, 5.0, x, y, z) && GetPlayerVirtualWorld(playerid) == GetPlayerVirtualWorld(i)){
					SendClientMessage(i, COR_BRANCO, "%s[%d][%s{"COR_BRANCO_INC"}] disse: %s", GetUserName(playerid), playerid, cargo, text);
				}else if(IsPlayerInRangeOfPoint(i, 10.0, x, y, z) && GetPlayerVirtualWorld(playerid) == GetPlayerVirtualWorld(i)){
					SendClientMessage(i, 0xD2D2D2FF, "%s[%d][%s{D2D2D2}] disse: %s", GetUserName(playerid), playerid, cargo, text);
				}
				else if(IsPlayerInRangeOfPoint(i, 15.0, x, y, z) && GetPlayerVirtualWorld(playerid) == GetPlayerVirtualWorld(i)){
					SendClientMessage(i, 0x898989FF, "%s[%d][%s{898989}] disse: %s", GetUserName(playerid), playerid, cargo, text);
				}
				else if(IsPlayerInRangeOfPoint(i, 20.0, x, y, z)  && GetPlayerVirtualWorld(playerid) == GetPlayerVirtualWorld(i)){
					SendClientMessage(i, 0x666666FF, "%s[%d][%s{666666}] disse: %s", GetUserName(playerid), playerid, cargo, text);
				}
			}
		}
		else
		{
			if ( Jogador[playerid][Vip] )
			{
				foreach(new i: Player)
				{
					if(IsPlayerInRangeOfPoint(i, 5.0, x, y, z) && GetPlayerVirtualWorld(playerid) == GetPlayerVirtualWorld(i)){
						SendClientMessage(i, GetPlayerColor(playerid), "%s[%d][{%06x}VIP{%06x}] {"COR_BRANCO_INC"}disse: %s", GetUserName(playerid), playerid, (Jogador[playerid][CorTag] >>> 8), (GetPlayerColor(playerid) >>> 8), text);
					}
					else if(IsPlayerInRangeOfPoint(i, 10.0, x, y, z) && GetPlayerVirtualWorld(playerid) == GetPlayerVirtualWorld(i)){
						SendClientMessage(i, GetPlayerColor(playerid), "%s[%d][{%06x}VIP{%06x}] {D2D2D2}disse: %s", GetUserName(playerid), playerid, (Jogador[playerid][CorTag] >>> 8), (GetPlayerColor(playerid) >>> 8), text);
					}
					else if(IsPlayerInRangeOfPoint(i, 15.0, x, y, z) && GetPlayerVirtualWorld(playerid) == GetPlayerVirtualWorld(i)){
						SendClientMessage(i, GetPlayerColor(playerid), "%s[%d][{%06x}VIP{%06x}] {898989}disse: %s", GetUserName(playerid), playerid, (Jogador[playerid][CorTag] >>> 8), (GetPlayerColor(playerid) >>> 8), text);
					}
					else if(IsPlayerInRangeOfPoint(i, 20.0, x, y, z)  && GetPlayerVirtualWorld(playerid) == GetPlayerVirtualWorld(i)){
						SendClientMessage(i, GetPlayerColor(playerid), "%s[%d][{%06x}VIP{%06x}] {666666}disse: %s", GetUserName(playerid), playerid, (Jogador[playerid][CorTag] >>> 8), (GetPlayerColor(playerid) >>> 8), text);
					}
				}
			}
			else
			{
				foreach(new i: Player)
				{
					if(IsPlayerInRangeOfPoint(i, 5.0, x, y, z) && GetPlayerVirtualWorld(playerid) == GetPlayerVirtualWorld(i)){
						SendClientMessage(i, GetPlayerColor(playerid), "%s[%d] {"COR_BRANCO_INC"}disse: %s", GetUserName(playerid), playerid, text);
					}
					else if(IsPlayerInRangeOfPoint(i, 10.0, x, y, z) && GetPlayerVirtualWorld(playerid) == GetPlayerVirtualWorld(i)){
						SendClientMessage(i, GetPlayerColor(playerid), "%s[%d] {D2D2D2}disse: %s", GetUserName(playerid), playerid, text);
					}
					else if(IsPlayerInRangeOfPoint(i, 15.0, x, y, z) && GetPlayerVirtualWorld(playerid) == GetPlayerVirtualWorld(i)){
						SendClientMessage(i, GetPlayerColor(playerid), "%s[%d] {898989}disse: %s", GetUserName(playerid), playerid, text);
					}
					else if(IsPlayerInRangeOfPoint(i, 20.0, x, y, z)  && GetPlayerVirtualWorld(playerid) == GetPlayerVirtualWorld(i)){
						SendClientMessage(i, GetPlayerColor(playerid), "%s[%d] {666666}disse: %s", GetUserName(playerid), playerid, text);
					}
				}
			}
		}
		return Y_HOOKS_CONTINUE_RETURN_0;
	}
	return Y_HOOKS_CONTINUE_RETURN_0;
}

stock Nitro2(vehicleid)
{
    new nos = GetVehicleModel(vehicleid);
    switch(nos)
    {
        case 444: return 0; case 581: return 0; case 586: return 0; case 481: return 0; case 509: return 0;
        case 446: return 0; case 556: return 0; case 443: return 0; case 452: return 0; case 453: return 0;
        case 454: return 0; case 472: return 0; case 473: return 0; case 484: return 0; case 493: return 0;
        case 595: return 0; case 462: return 0; case 463: return 0; case 468: return 0; case 521: return 0;case 522:
        return 0;case 417:return 0;case 425:return 0;case 447:return 0;case 487:return 0;case 488:return 0;case 497:
        return 0;case 501:return 0;case 548:return 0;case 563:return 0;case 406:return 0;case 520:return 0;
        case 539:return 0;case 553:return 0;case 557:return 0;case 573:return 0;case 460:return 0;case 593:
        return 0;case 464:return 0;case 476:return 0;case 511:return 0;case 512:return 0;case 577:return 0;
        case 592:return 0;case 471:return 0;case 448:return 0;case 461:return 0; case 523:return 0;case 510:
        return 0;case 430:return 0;case 465:return 0;case 469:return 0; case 513:return 0;case 519:return 0;
    }
    return true;
}

hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	if(IsPlayerInAnyVehicle(playerid)) {
        if(call::ADMIN->GetPlayerAdminLevel(playerid) >= AJUDANTE || Jogador[playerid][Vip] == true)  {
        	new nos = GetPlayerVehicleID(playerid);
            if(Nitro2(nos) && (oldkeys & 1 || oldkeys & 4)) {
                if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER) {
                	if(Autotunning[playerid] == 1) 
                	{
                		new var5 = 0; 
		                var5 = GetPlayerVehicleID(playerid); 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 562) { 
		                    AddVehicleComponent(var5, 1046); 
		                    AddVehicleComponent(var5, 1171); 
		                    AddVehicleComponent(var5, 1149); 
		                    AddVehicleComponent(var5, 1035); 
		                    AddVehicleComponent(var5, 1147); 
		                    AddVehicleComponent(var5, 1036); 
		                    AddVehicleComponent(var5, 1040); 
		                    ChangeVehiclePaintjob(var5, 2); 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1079); 
		                }
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 560) { 
		                    AddVehicleComponent(var5, 1028); 
		                    AddVehicleComponent(var5, 1169); 
		                    AddVehicleComponent(var5, 1141); 
		                    AddVehicleComponent(var5, 1032); 
		                    AddVehicleComponent(var5, 1138); 
		                    AddVehicleComponent(var5, 1026); 
		                    AddVehicleComponent(var5, 1027); 
		                    ChangeVehiclePaintjob(var5, 2); 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1079); 
		                } 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 565) { 
		                    AddVehicleComponent(var5, 1046); 
		                    AddVehicleComponent(var5, 1153); 
		                    AddVehicleComponent(var5, 1150); 
		                    AddVehicleComponent(var5, 1054); 
		                    AddVehicleComponent(var5, 1049); 
		                    AddVehicleComponent(var5, 1047); 
		                    AddVehicleComponent(var5, 1051); 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1079); 
		                    ChangeVehiclePaintjob(var5, 2); 
		                } 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 559) { 
		                    AddVehicleComponent(var5, 1065); 
		                    AddVehicleComponent(var5, 1160); 
		                    AddVehicleComponent(var5, 1159); 
		                    AddVehicleComponent(var5, 1067); 
		                    AddVehicleComponent(var5, 1162); 
		                    AddVehicleComponent(var5, 1069); 
		                    AddVehicleComponent(var5, 1071); 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1079); 
		                    ChangeVehiclePaintjob(var5, 2); 
		                } 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 561) { 
		                    AddVehicleComponent(var5, 1064); 
		                    AddVehicleComponent(var5, 1155); 
		                    AddVehicleComponent(var5, 1154); 
		                    AddVehicleComponent(var5, 1055); 
		                    AddVehicleComponent(var5, 1158); 
		                    AddVehicleComponent(var5, 1056); 
		                    AddVehicleComponent(var5, 1062); 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1079); 
		                    ChangeVehiclePaintjob(var5, 2); 
		                } 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 558) { 
		                    AddVehicleComponent(var5, 1089); 
		                    AddVehicleComponent(var5, 1166); 
		                    AddVehicleComponent(var5, 1168); 
		                    AddVehicleComponent(var5, 1088); 
		                    AddVehicleComponent(var5, 1164); 
		                    AddVehicleComponent(var5, 1090); 
		                    AddVehicleComponent(var5, 1094); 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1079); 
		                    ChangeVehiclePaintjob(var5, 2); 
		                } 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 575) { 
		                    AddVehicleComponent(var5, 1044); 
		                    AddVehicleComponent(var5, 1174); 
		                    AddVehicleComponent(var5, 1176); 
		                    AddVehicleComponent(var5, 1042); 
		                    AddVehicleComponent(var5, 1099); 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1079); 
		                    ChangeVehiclePaintjob(var5, 2); 
		                } 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 534) { 
		                    AddVehicleComponent(var5, 1126); 
		                    AddVehicleComponent(var5, 1179); 
		                    AddVehicleComponent(var5, 1180); 
		                    AddVehicleComponent(var5, 1122); 
		                    AddVehicleComponent(var5, 1101); 
		                    AddVehicleComponent(var5, 1125); 
		                    AddVehicleComponent(var5, 1123); 
		                    AddVehicleComponent(var5, 1100); 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1079); 
		                    ChangeVehiclePaintjob(var5, 2); 
		                } 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 536) { 
		                    AddVehicleComponent(var5, 1104); 
		                    AddVehicleComponent(var5, 1182); 
		                    AddVehicleComponent(var5, 1184); 
		                    AddVehicleComponent(var5, 1108); 
		                    AddVehicleComponent(var5, 1107); 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1079); 
		                    ChangeVehiclePaintjob(var5, 2); 
		                } 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 535) { 
		                    AddVehicleComponent(var5, 1104); 
		                    AddVehicleComponent(var5, 1182); 
		                    AddVehicleComponent(var5, 1184); 
		                    AddVehicleComponent(var5, 1108); 
		                    AddVehicleComponent(var5, 1107); 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1079); 
		                    ChangeVehiclePaintjob(var5, 2); 
		                } 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 567) { 
		                    AddVehicleComponent(var5, 1129); 
		                    AddVehicleComponent(var5, 1189); 
		                    AddVehicleComponent(var5, 1187); 
		                    AddVehicleComponent(var5, 1102); 
		                    AddVehicleComponent(var5, 1133); 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1079); 
		                    ChangeVehiclePaintjob(var5, 2); 
		                } 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 420) { 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1079); 
		                    AddVehicleComponent(var5, 1139); 
		                    AddVehicleComponent(var5, 1147); 
		                } 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 400) { 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1018); 
		                    AddVehicleComponent(var5, 1013); 
		                    AddVehicleComponent(var5, 1079); 
		                    AddVehicleComponent(var5, 1086); 
		                } 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 444) { 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1079); 
		                } 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 556) { 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1079); 
		                } 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 557) { 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1079); 
		                } 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 401) { 
		                    AddVehicleComponent(var5, 1086); 
		                    AddVehicleComponent(var5, 1139); 
		                    AddVehicleComponent(var5, 1079); 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1012); 
		                    AddVehicleComponent(var5, 1013); 
		                    AddVehicleComponent(var5, 1042); 
		                    AddVehicleComponent(var5, 1043); 
		                    AddVehicleComponent(var5, 1018); 
		                    AddVehicleComponent(var5, 1006); 
		                    AddVehicleComponent(var5, 1007); 
		                    AddVehicleComponent(var5, 1017); 
		                } 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 483) { 
		                    AddVehicleComponent(var5, 1028); 
		                    AddVehicleComponent(var5, 1169); 
		                    AddVehicleComponent(var5, 1141); 
		                    AddVehicleComponent(var5, 1032); 
		                    AddVehicleComponent(var5, 1138); 
		                    AddVehicleComponent(var5, 1026); 
		                    AddVehicleComponent(var5, 1027); 
		                    ChangeVehiclePaintjob(var5, 0); 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1079); 
		                } 
		                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 576) { 
		                    ChangeVehiclePaintjob(var5, 2); 
		                    AddVehicleComponent(var5, 1191); 
		                    AddVehicleComponent(var5, 1193); 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1018); 
		                    AddVehicleComponent(var5, 1079); 
		                    AddVehicleComponent(var5, 1134); 
		                    AddVehicleComponent(var5, 1137); 
		                } 
		                else{ 
		                    AddVehicleComponent(var5, 1010); 
		                    AddVehicleComponent(var5, 1079); 
		                } 
		                RepairVehicle(var5);
                	}
                }
            }
        }
    }
	return Y_HOOKS_CONTINUE_RETURN_1;
}

/*hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	if(IsPlayerInAnyVehicle(playerid))
	{
		new nos = GetPlayerVehicleID(playerid);
		if(Nitro(nos) && (oldkeys & 1 || oldkeys & 4)) 
	    { 
	        if(Autotunning[playerid] == 1) 
	        { 
	            if(call::ADMIN->GetPlayerAdminLevel(playerid) >= AJUDANTE || Jogador[playerid][Vip] == 1) 
	            { 
	                new var5 = 0; 
	                var5 = GetPlayerVehicleID(playerid); 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 562) { 
	                    AddVehicleComponent(var5, 1046); 
	                    AddVehicleComponent(var5, 1171); 
	                    AddVehicleComponent(var5, 1149); 
	                    AddVehicleComponent(var5, 1035); 
	                    AddVehicleComponent(var5, 1147); 
	                    AddVehicleComponent(var5, 1036); 
	                    AddVehicleComponent(var5, 1040); 
	                    ChangeVehiclePaintjob(var5, 2); 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1079); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 560) { 
	                    AddVehicleComponent(var5, 1028); 
	                    AddVehicleComponent(var5, 1169); 
	                    AddVehicleComponent(var5, 1141); 
	                    AddVehicleComponent(var5, 1032); 
	                    AddVehicleComponent(var5, 1138); 
	                    AddVehicleComponent(var5, 1026); 
	                    AddVehicleComponent(var5, 1027); 
	                    ChangeVehiclePaintjob(var5, 2); 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1079); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 565) { 
	                    AddVehicleComponent(var5, 1046); 
	                    AddVehicleComponent(var5, 1153); 
	                    AddVehicleComponent(var5, 1150); 
	                    AddVehicleComponent(var5, 1054); 
	                    AddVehicleComponent(var5, 1049); 
	                    AddVehicleComponent(var5, 1047); 
	                    AddVehicleComponent(var5, 1051); 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1079); 
	                    ChangeVehiclePaintjob(var5, 2); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 559) { 
	                    AddVehicleComponent(var5, 1065); 
	                    AddVehicleComponent(var5, 1160); 
	                    AddVehicleComponent(var5, 1159); 
	                    AddVehicleComponent(var5, 1067); 
	                    AddVehicleComponent(var5, 1162); 
	                    AddVehicleComponent(var5, 1069); 
	                    AddVehicleComponent(var5, 1071); 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1079); 
	                    ChangeVehiclePaintjob(var5, 2); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 561) { 
	                    AddVehicleComponent(var5, 1064); 
	                    AddVehicleComponent(var5, 1155); 
	                    AddVehicleComponent(var5, 1154); 
	                    AddVehicleComponent(var5, 1055); 
	                    AddVehicleComponent(var5, 1158); 
	                    AddVehicleComponent(var5, 1056); 
	                    AddVehicleComponent(var5, 1062); 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1079); 
	                    ChangeVehiclePaintjob(var5, 2); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 558) { 
	                    AddVehicleComponent(var5, 1089); 
	                    AddVehicleComponent(var5, 1166); 
	                    AddVehicleComponent(var5, 1168); 
	                    AddVehicleComponent(var5, 1088); 
	                    AddVehicleComponent(var5, 1164); 
	                    AddVehicleComponent(var5, 1090); 
	                    AddVehicleComponent(var5, 1094); 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1079); 
	                    ChangeVehiclePaintjob(var5, 2); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 575) { 
	                    AddVehicleComponent(var5, 1044); 
	                    AddVehicleComponent(var5, 1174); 
	                    AddVehicleComponent(var5, 1176); 
	                    AddVehicleComponent(var5, 1042); 
	                    AddVehicleComponent(var5, 1099); 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1079); 
	                    ChangeVehiclePaintjob(var5, 2); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 534) { 
	                    AddVehicleComponent(var5, 1126); 
	                    AddVehicleComponent(var5, 1179); 
	                    AddVehicleComponent(var5, 1180); 
	                    AddVehicleComponent(var5, 1122); 
	                    AddVehicleComponent(var5, 1101); 
	                    AddVehicleComponent(var5, 1125); 
	                    AddVehicleComponent(var5, 1123); 
	                    AddVehicleComponent(var5, 1100); 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1079); 
	                    ChangeVehiclePaintjob(var5, 2); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 536) { 
	                    AddVehicleComponent(var5, 1104); 
	                    AddVehicleComponent(var5, 1182); 
	                    AddVehicleComponent(var5, 1184); 
	                    AddVehicleComponent(var5, 1108); 
	                    AddVehicleComponent(var5, 1107); 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1079); 
	                    ChangeVehiclePaintjob(var5, 2); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 535) { 
	                    AddVehicleComponent(var5, 1104); 
	                    AddVehicleComponent(var5, 1182); 
	                    AddVehicleComponent(var5, 1184); 
	                    AddVehicleComponent(var5, 1108); 
	                    AddVehicleComponent(var5, 1107); 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1079); 
	                    ChangeVehiclePaintjob(var5, 2); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 567) { 
	                    AddVehicleComponent(var5, 1129); 
	                    AddVehicleComponent(var5, 1189); 
	                    AddVehicleComponent(var5, 1187); 
	                    AddVehicleComponent(var5, 1102); 
	                    AddVehicleComponent(var5, 1133); 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1079); 
	                    ChangeVehiclePaintjob(var5, 2); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 420) { 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1079); 
	                    AddVehicleComponent(var5, 1139); 
	                    AddVehicleComponent(var5, 1147); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 400) { 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1018); 
	                    AddVehicleComponent(var5, 1013); 
	                    AddVehicleComponent(var5, 1079); 
	                    AddVehicleComponent(var5, 1086); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 444) { 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1079); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 556) { 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1079); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 557) { 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1079); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 401) { 
	                    AddVehicleComponent(var5, 1086); 
	                    AddVehicleComponent(var5, 1139); 
	                    AddVehicleComponent(var5, 1079); 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1012); 
	                    AddVehicleComponent(var5, 1013); 
	                    AddVehicleComponent(var5, 1042); 
	                    AddVehicleComponent(var5, 1043); 
	                    AddVehicleComponent(var5, 1018); 
	                    AddVehicleComponent(var5, 1006); 
	                    AddVehicleComponent(var5, 1007); 
	                    AddVehicleComponent(var5, 1017); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 483) { 
	                    AddVehicleComponent(var5, 1028); 
	                    AddVehicleComponent(var5, 1169); 
	                    AddVehicleComponent(var5, 1141); 
	                    AddVehicleComponent(var5, 1032); 
	                    AddVehicleComponent(var5, 1138); 
	                    AddVehicleComponent(var5, 1026); 
	                    AddVehicleComponent(var5, 1027); 
	                    ChangeVehiclePaintjob(var5, 0); 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1079); 
	                } 
	                if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 576) { 
	                    ChangeVehiclePaintjob(var5, 2); 
	                    AddVehicleComponent(var5, 1191); 
	                    AddVehicleComponent(var5, 1193); 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1018); 
	                    AddVehicleComponent(var5, 1079); 
	                    AddVehicleComponent(var5, 1134); 
	                    AddVehicleComponent(var5, 1137); 
	                } 
	                else{ 
	                    AddVehicleComponent(var5, 1010); 
	                    AddVehicleComponent(var5, 1079); 
	                } 
	                RepairVehicle(var5);
	            } 
	        } 
	    }
    }
	return 1;
}*/

public OnPlayerAuthLogin(playerid)
{
	if(call::PLAYER->IsPlayerLogged(playerid) || !call::PLAYER->IsPlayerLoggin(playerid))
		return false;

	if( !cache_num_rows() ) {
		SendClientMessage(playerid, COR_ERRO, "Erro: Os dados digitados não confeerem, verifique o Nick é Senha.");
		return 1;
	}

	call::PLAYER->SetPlayerVarInt(playerid, CallerID, INVALID_PLAYER_ID);

	CleanChat(playerid);
	SendClientMessage(playerid, COR_SISTEMA, "» {"COR_BRANCO_INC"}Carregando sua conta...");
	
	/**
	 *
	 * Enviar mensagem no chat principal do discord
	 *
	 */

	/**
	*
	*	Destruir textdraws de login
	*
	**/
	call::TD->DestroyPlayerConnect(playerid);
	call::TD->HidePlayerLogin(playerid);


	call::PLAYER->TogglePlayerLoggin(playerid, false);

	new value_int, Float:value_float, lastLogin[30], weaponid, ammo;

	cache_get_value_name_int(0, "id", Jogador[playerid][PlayerID]);

	cache_get_value_name(0, "lastLogin", lastLogin, sizeof(lastLogin));

	cache_get_value_name(0, "username", Jogador[playerid][Nome], MAX_PLAYER_NAME);
	SetPlayerName(playerid, Jogador[playerid][Nome]);
	
	cache_get_value_name(0, "email", Jogador[playerid][Email], MAX_PLAYER_EMAIL);

	cache_get_value_name_float(0, "spawn_x", Jogador[playerid][Spawn][X]);
	cache_get_value_name_float(0, "spawn_y", Jogador[playerid][Spawn][Y]);
	cache_get_value_name_float(0, "spawn_z", Jogador[playerid][Spawn][Z]);
	cache_get_value_name_float(0, "spawn_a", Jogador[playerid][Spawn][A]);
	cache_get_value_name_int(0, "skin", Jogador[playerid][Skin]);
	cache_get_value_name_int(0, "profissao", Jogador[playerid][Profissao]);
	cache_get_value_name_int(0, "hits", Jogador[playerid][Hits]);

	cache_get_value_name_int(0, "vip", value_int);
	if ( value_int ){

		cache_get_value_name_int(0, "dias_vip", value_int);
		if ( value_int > 0 ){
			Jogador[playerid][Vip] = true;
			Jogador[playerid][TempoVip] = value_int;

			SendClientMessage(playerid, COR_AZUL, "~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=");
			SendClientMessage(playerid, COR_BRANCO, "[!] Uau! Você é um jogador {"COR_ERRO_INC"}VIP{"COR_BRANCO_INC"} obrigado por ajudar a manter nosso servidor online.");
			SendClientMessage(playerid, COR_AZUL, "~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=");
		}
		else
		{
			Jogador[playerid][Vip] = false;
			Jogador[playerid][TempoVip] = 0;
			SendClientMessage(playerid, COR_AZUL, "~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=");
			SendClientMessage(playerid, COR_AVISO, "[!] Seus dias vips expiraram, nos ajudate a manter o servidor online e receba seus beneficios novamente.");
			SendClientMessage(playerid, COR_AVISO, "[!] Para mais informações acesse: {"COR_BRANCO_INC"}"SERVER_SITE"/forum");
			SendClientMessage(playerid, COR_AZUL, "~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=");
		}
	}
	
	SetSpawnInfo(playerid, NO_TEAM, Jogador[playerid][Skin], Jogador[playerid][Spawn][X], Jogador[playerid][Spawn][Y], Jogador[playerid][Spawn][Z], Jogador[playerid][Spawn][A], 0, 0, 0, 0, 0, 0);

	cache_get_value_name_int(0, "world", Jogador[playerid][World]);
	SetPlayerVirtualWorld(playerid, Jogador[playerid][World]);

	cache_get_value_name_int(0, "interior", Jogador[playerid][Interior]);
	SetPlayerInterior(playerid, Jogador[playerid][Interior]);

	cache_get_value_name_int(0, "level", value_int);
	SetPlayerScore(playerid, value_int);

	cache_get_value_name_int(0, "dinheiro", value_int);
	GivePlayerMoney(playerid, value_int);

	cache_get_value_name_float(0, "vida", value_float);
	SetPlayerHealth(playerid, (value_float > 5.0 ? value_float : 5.0));

	cache_get_value_name_float(0, "colete", value_float);
	SetPlayerArmour(playerid, value_float);

	cache_get_value_name_int(0, "exp", Jogador[playerid][EXP]);

	cache_get_value_name_int(0, "upm", Jogador[playerid][UPm]);
	cache_get_value_name_int(0, "ups", Jogador[playerid][UPs]);

	cache_get_value_name_float(0, "fome", Jogador[playerid][Fome]);
	cache_get_value_name_float(0, "sede", Jogador[playerid][Sede]);
	cache_get_value_name_float(0, "sono", Jogador[playerid][Sono]);

	cache_get_value_name_int(0, "multas", Jogador[playerid][Multas]);

	cache_get_value_name_int(0, "mercadoria", Jogador[playerid][Mercadoria]);

	cache_get_value_name_int(0, "bloqueado_carga", Jogador[playerid][bCarga]);
	
	cache_get_value_name_int(0, "celular", Jogador[playerid][Celular]);
	cache_get_value_name_int(0, "saldo", Jogador[playerid][SaldoCelular]);
	cache_get_value_name_bool(0, "gps", Jogador[playerid][GPS]);
	cache_get_value_name_bool(0, "agenda", Jogador[playerid][Agenda]);
	cache_get_value_name_int(0, "mp3", Jogador[playerid][MP3]);
	cache_get_value_name_int(0, "galao", Jogador[playerid][Galao]);
	cache_get_value_name_int(0, "kills", Jogador[playerid][Matou]);
	cache_get_value_name_int(0, "deaths", Jogador[playerid][Morreu]);

	// player skills
	cache_get_value_name_int(0, "skill_pistol", Jogador[playerid][SkillPistol]);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_PISTOL, Jogador[playerid][SkillPistol]);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_PISTOL_SILENCED, Jogador[playerid][SkillPistol]);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_DESERT_EAGLE, Jogador[playerid][SkillPistol]);

	cache_get_value_name_int(0, "skill_fuzil", Jogador[playerid][SkillFuzil]);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_AK47, Jogador[playerid][SkillFuzil]);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_M4, Jogador[playerid][SkillFuzil]);

	cache_get_value_name_int(0, "skill_mp5", Jogador[playerid][SkillMP5]);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_MP5, Jogador[playerid][SkillMP5]);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_MICRO_UZI, Jogador[playerid][SkillMP5]);

	cache_get_value_name_int(0, "skill_shotgun", Jogador[playerid][SkillShotgun]);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_SHOTGUN, Jogador[playerid][SkillShotgun]);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_SAWNOFF_SHOTGUN, Jogador[playerid][SkillShotgun]);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_SPAS12_SHOTGUN, Jogador[playerid][SkillShotgun]);

	cache_get_value_name_int(0, "skill_rifle", Jogador[playerid][SkillRifle]);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_SNIPERRIFLE, Jogador[playerid][SkillRifle]);
	

	cache_get_value_name_int(0, "estrelas", value_int);
	SetPlayerWantedLevel(playerid, value_int);
	
	cache_get_value_name_int(0, "avisos", Jogador[playerid][Avisos]);

	cache_get_value_name_int(0, "preso", Jogador[playerid][Preso]);
	cache_get_value_name_int(0, "tempo_preso", Jogador[playerid][TempoPreso]);

	cache_get_value_name_int(0, "cash", Jogador[playerid][Cash]);

	cache_get_value_name_int(0, "plano_saude", Jogador[playerid][Plano]);

	cache_get_value_name_int(0, "habilitacao_a", value_int);
	if ( value_int )
		BitFlag_Set(Jogador[playerid][Habilitacao], HAB_MOTO);

	cache_get_value_name_int(0, "habilitacao_b", value_int);
	if ( value_int )
		BitFlag_Set(Jogador[playerid][Habilitacao], HAB_CARRO);

	cache_get_value_name_int(0, "habilitacao_c", value_int);
	if ( value_int )
		BitFlag_Set(Jogador[playerid][Habilitacao], HAB_CAMINHAO);

	cache_get_value_name_int(0, "habilitacao_d", value_int);
	if ( value_int )
		BitFlag_Set(Jogador[playerid][Habilitacao], HAB_BARCO);

	cache_get_value_name_int(0, "habilitacao_e", value_int);
	if ( value_int )
		BitFlag_Set(Jogador[playerid][Habilitacao], HAB_AVIAO);

	cache_get_value_name_int(0, "sementes", Jogador[playerid][Sementes]);
	cache_get_value_name_int(0, "sementesl", Jogador[playerid][SementesL]);
	
	cache_get_value_name_int(0, "maconha", Jogador[playerid][Maconha]);
	cache_get_value_name_int(0, "cocaina", Jogador[playerid][Cocaina]);
	cache_get_value_name_int(0, "crack", Jogador[playerid][Crack]);
	cache_get_value_name_int(0, "materias", Jogador[playerid][Materias]);
	cache_get_value_name_int(0, "morador_casa_id", Jogador[playerid][pMorador]); // retorna o id da casa na tabela do banco de dados.
	
	// carregar casa
	cache_get_value_name_int(0, "entrou", Jogador[playerid][Entrou]);
	new casaid = GetPlayerHouse(playerid);
	if ( casaid != INVALID_HOUSE_ID )
	{
		Jogador[playerid][TimeContinuar] = gettime() + 20;

		SetSpawnInfo(playerid, NO_TEAM, Jogador[playerid][Skin], Casa[casaid][Saida][X], Casa[casaid][Saida][Y], Casa[casaid][Saida][Z], Casa[casaid][Saida][A], 0, 0, 0, 0, 0, 0);
		SetPlayerInterior(playerid, Casa[casaid][Interior]);
		SetPlayerVirtualWorld(playerid, casaid);
		Jogador[playerid][Entrou] = ENTROU_CASA;
	}

	cache_get_value_name_int(0, "empresaid", value_int);
	Jogador[playerid][EmpresaID] = GetBusinessIdByRow(value_int);

	cache_get_value_name_int(0, "tutorial", value_int);
	if(!value_int){
		call::TUTO->InitPlayerTutorial(playerid);
		return true;
	}

	cache_get_value_name_int(0, "admin", value_int);
	call::ADMIN->SetPlayerAdminLevel(playerid, value_int);

	if(value_int >= AJUDANTE)
	{
		cache_get_value_name_int(0, "admin_trabalhando_horas", Jogador[playerid][TempoAtividade][HORAS]);
		cache_get_value_name_int(0, "admin_trabalhando_minutos", Jogador[playerid][TempoAtividade][MINUTOS]);
		cache_get_value_name_int(0, "admin_trabalhando_segundos", Jogador[playerid][TempoAtividade][SEGUNDOS]);
		cache_get_value_name_int(0, "admin_avisos", Jogador[playerid][AdminAvisos]);
	}
	TogglePlayerSpectating(playerid, false);

	new str[20];
	for(new i=1; i <= 7; i++)
	{
		format(str, sizeof(str), "slot%d", i);
		cache_get_value_name_int(0, str, weaponid);

		format(str, sizeof(str), "slot%d_ammo", i);
		cache_get_value_name_int(0, str, ammo);

		GivePlayerWeapon(playerid, weaponid, ammo);
	}

	SendClientMessage(playerid, COR_SISTEMA, "» {"COR_BRANCO_INC"}Olá Sr(a) {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}, nos vimos pela ultima vez em %s", GetUserName(playerid), lastLogin);
	
	if ( Jogador[playerid][TimeContinuar] != 0)
		SendClientMessage(playerid, COR_AZUL, "» {"COR_BRANCO_INC"}Para voltar a sua última posição use '{"COR_AMARELO_INC"}/continuar{"COR_BRANCO_INC"}', você tem 20 segundos.");


	/**
	*
	*	Prender jogador.
	*
	**/
	if ( Jogador[playerid][Preso] != SOLTO )
	{
		EnviarCela(playerid);
		call::PM->ShowPlayerTextDrawPrisonTime(playerid);
		SendClientMessage(playerid, 0xB1DD00FF, "* Você ainda não cumpriu sua pena.");
	}
	
	// carregar veiculo do jogador
	LoadPlayerVehicles(playerid);
	return true;
}


public OnPlayerRegister(playerid)
{
	CleanChat(playerid);

	new id = cache_insert_id();
	if( cache_affected_rows() != 1 )
	{
		SendClientMessage(playerid, COR_ERRO, "Erro: Não foi possivel registrar sua conta em nosso banco de dados.");
		SendClientMessage(playerid, COR_ERRO, "Erro: Por favor tire print apertando a tecla F8 e poste a no fórum.");
		return true;
	}
	
	new _log[128];
	format(_log, sizeof(_log), "A conta de %s foi criada, a inserção demorou %d ms", GetUserName(playerid), cache_get_query_exec_time());
	print(_log);
	
	call::TD->ShowPlayerLogin(playerid);
	call::TD->UpdateTextDrawLogin();
	
	new query[200], codigo[11];

	randomstring(codigo, sizeof(codigo)-1);

	mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `codigo_ativacao`='%s' WHERE `id`='%d' LIMIT 1;", codigo, id);
	mysql_tquery(getConexao(), query);
	/*
	new str[200];
	format(str, sizeof(str), "Nick: <b>%s</b><br>Senha: <b>%s</b><br>código: <b>%s</b>", GetUserName(playerid), Jogador[playerid][Senha], codigo);
	SendEmail(SERVER_EMAIL, Jogador[playerid][Email], "Perfect City RPG - Ativar conta", str, "registro.php");
	*/
	SendClientMessage(playerid, COR_SISTEMA, "» {"COR_BRANCO_INC"}Sua conta foi registrada com {"COR_VERDE_INC"}sucesso{"COR_BRANCO_INC"}.");
	// SendClientMessage(playerid, COR_SISTEMA, "» {"COR_BRANCO_INC"}Foi enviado para você um email com o código de ativação.");
	// SendClientMessage(playerid, COR_SISTEMA, "» {"COR_BRANCO_INC"}Verifique na sua caixa de entrada e Spam.");
	return true;
}


public GetPlayerHospital(playerid)
{
    if(!IsPlayerConnected(playerid))
		return -1;

    new
		Float:dist, prevhosp, Float:distancia = 100000.000;
	for(new hospid = 1; hospid < 7; hospid++)
	{
        if(hospid == 1)
	        dist = GetPlayerDistanceToPoint(playerid, 1173.1145, -1323.7125); // Hospital de Los Santos Perto da Ammu-Nation
        else if(hospid == 2)
			dist = GetPlayerDistanceToPoint(playerid, 2036.5521, -1412.5513); // Hospital de Los Santos Perto da Favela
        else if(hospid == 3)
			dist = GetPlayerDistanceToPoint(playerid, 1607.4396, 1819.1462); // Hspital de Las Venturas
        else if(hospid == 4)
			dist = GetPlayerDistanceToPoint(playerid, -2665.0503, 636.5155); // Hospital de San Fierro
        else if(hospid == 5)
			dist = GetPlayerDistanceToPoint(playerid, -315.0838, 1050.7095); // Hospital de Fort Carson
        else if(hospid == 6)
			dist = GetPlayerDistanceToPoint(playerid, -1514.6455, 2523.4263); // Hospital de El-Quebrados
        else if(hospid == 7)
			dist = GetPlayerDistanceToPoint(playerid, -2208.2485, -2286.8516); // Hospital de Angel Pine

        if((dist < distancia)){
            distancia = dist, prevhosp = hospid;
		}
	}
	return prevhosp;
}

new static TocandoTimer[MAX_PLAYERS][2], TimerCobranca[MAX_PLAYERS] = -1;
forward TocarCelular(playerid);
public TocarCelular(playerid)
{
	if ( call::PLAYER->GetPlayerVarInt(playerid, StateCell) == CHAMANDO )
	{
		PlayerPlaySound(playerid, 3401, 0.0, 0.0, 0.0);
		TocandoTimer[playerid][0] = SetTimerEx("TocarCelular2", 950, false, "i", playerid);
	}
	return 1;
}
forward TocarCelular2(playerid);
public TocarCelular2(playerid)
{
	if ( call::PLAYER->GetPlayerVarInt(playerid, StateCell) == CHAMANDO )
	{
		PlayerPlaySound(playerid, 3402, 0.0, 0.0, 0.0);
		TocandoTimer[playerid][1] = SetTimerEx("TocarCelular", 850, false, "i", playerid);
	}
	return 1;
}

forward ExpirouMaconha(playerid);
public ExpirouMaconha(playerid)
{
	DeletePVar(playerid, "OferecedorMID"), DeletePVar(playerid, "DinheiroMaconha"), DeletePVar(playerid, "MaconhaOferecida"), DeletePVar(playerid, "MQuantidade");
	return true;
}
forward ExpirouCocaina(playerid);
public ExpirouCocaina(playerid)
{
	DeletePVar(playerid, "OferecedorCID"), DeletePVar(playerid, "DinheiroCocaina"), DeletePVar(playerid, "CocainaOferecida"), DeletePVar(playerid, "CQuantidade");
	return true;
}

forward ExpirouCrack(playerid);
public ExpirouCrack(playerid)
{
	DeletePVar(playerid, "OferecedorCraID"), DeletePVar(playerid, "DinheiroCrack"), DeletePVar(playerid, "CrackOferecida"), DeletePVar(playerid, "CraQuantidade");
	return true;
}

stock function CELL::FinalizarChamada(playerid, id)
{
	SendClientMessage(id, COR_SISTEMA, "* Desligaram no outro lado da linha.");
	call::PLAYER->SetPlayerVarInt(id, StateCell, 0);
	SetPlayerSpecialAction(id, SPECIAL_ACTION_NONE);
	RemovePlayerAttachedObject(id, ATTACH_CELL);
	call::PLAYER->SetPlayerVarInt(id, CallerID, INVALID_PLAYER_ID);
	KillTimer(TocandoTimer[id][0]);
	KillTimer(TocandoTimer[id][1]);
	PlayerPlaySound(id, 3402, 0.0, 0.0, 0.0);
	call::CELL->PararCobranca(id);

	call::PLAYER->SetPlayerVarInt(playerid, StateCell, 0);
	SendClientMessage(playerid, COR_SISTEMA, "* Telefone foi desligado");
	SetPlayerSpecialAction(playerid, SPECIAL_ACTION_NONE);
	RemovePlayerAttachedObject(playerid, ATTACH_CELL);
	call::PLAYER->SetPlayerVarInt(playerid, CallerID, INVALID_PLAYER_ID);
	KillTimer(TocandoTimer[playerid][0]);
	KillTimer(TocandoTimer[playerid][1]);
	PlayerPlaySound(playerid, 3402, 0.0, 0.0, 0.0);
	call::CELL->PararCobranca(playerid);
	return true;
}

forward CobrarTarifaCelular(playerid);
public CobrarTarifaCelular(playerid)
{
	if ( call::PLAYER->GetPlayerVarInt(playerid, TypeCall) != EMERGENCIA)
	{
		if ( call::PLAYER->GetPlayerVarInt(playerid, SaldoCelular) < TARIFA_LIGACAO )
			return SendClientMessage(playerid, COR_BRANCO, "Você não tem mais créditos para continuar a ligação."), call::CELL->FinalizarChamada(playerid, call::PLAYER->GetPlayerVarInt(playerid, CallerID) );
		
		new str[70];
		format(str, sizeof(str), "~r~Créditos: R$%s", RealStr(call::PLAYER->GetPlayerVarInt(playerid, SaldoCelular)) );
		GameTextForPlayer(playerid, str, 3000, 1);

		call::PLAYER->SetPlayerVarInt(playerid, SaldoCelular, call::PLAYER->GetPlayerVarInt(playerid, SaldoCelular) - TARIFA_LIGACAO);
		TimerCobranca[playerid] = SetTimerEx("CobrarTarifaCelular", 60000, false, "i", playerid);
	}
	return true;
}

stock function CELL::PararCobranca(playerid)
{
	if ( TimerCobranca[playerid] != -1 )
	{
		KillTimer(TimerCobranca[playerid]);
		TimerCobranca[playerid]=-1;
	}
	return true;
}
#pragma unused TocandoTimer, TimerCobranca
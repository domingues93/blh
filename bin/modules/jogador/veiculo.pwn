
#include <YSI_Coding\y_hooks>
forward OnLoadVehiclesPlayer(playerid);
// ============================== [ VARS ] ============================== //
static 
	BuyVehicle_pID[MAX_PLAYERS] = INVALID_PLAYER_ID, BuyVehicle_Value[MAX_PLAYERS], BuyVehicle_vID[MAX_PLAYERS] = INVALID_VEHICLE_ID;



// ============================== [ FORWARDS ] ============================== //
forward OnPlayerVendingVehiclePlayer(playerid, id, avisos);

// ============================== [ CALLBACKs ] ============================== //

hook OnPlayerDisconnect(playerid, reason)
{
	if ( BuyVehicle_pID[playerid] != INVALID_PLAYER_ID )
	{
		if ( BuyVehicle_vID[playerid] != INVALID_VEHICLE_ID )
		{
			SendClientMessage(BuyVehicle_pID[playerid], COR_AVISO, "* O(A) Jogador(a) saiu do servidor e a venda do veiculo foi cancelada.");
			BuyVehicle_Value[playerid] = 0;
			BuyVehicle_vID[playerid] = INVALID_VEHICLE_ID;
			BuyVehicle_pID[playerid] = INVALID_PLAYER_ID;
		}
		else
		{
			new id = BuyVehicle_pID[playerid];
			if ( IsPlayerConnected(id) )
				SendClientMessage(id, COR_AVISO, "* O(A) Jogador(a) saiu do servidor e a venda do veiculo foi cancelada.");

			BuyVehicle_Value[id] = 0;
			BuyVehicle_vID[id] = INVALID_VEHICLE_ID;
			BuyVehicle_pID[id] = INVALID_PLAYER_ID;
			BuyVehicle_pID[playerid] = INVALID_PLAYER_ID;
		}
	}
	foreach(new i: VehiclePlayers)
	{
		if ( Jogador[playerid][PlayerID] == Vehicle[i][Owner][Id] )
		{
			DestroyVehicle(Vehicle[i][Id]);
			new r[e_VEHICLE_INFO];
			Vehicle[i] = r;
		}
	}
	return Y_HOOKS_CONTINUE_RETURN_1;
}
hook OnPlayerExitVehicle(playerid, vehicleid)
{
	if ( vehicleid != INVALID_VEHICLE_ID )
	{
		if ( Vehicle[vehicleid][RadioOn] )
		{
			StopAudioStreamForPlayer(playerid);
		}
	}
	return Y_HOOKS_CONTINUE_RETURN_1;
}
hook OnPlayerConnect(playerid)
{
	BuyVehicle_vID[playerid] = INVALID_PLAYER_ID;
}

hook OnPlayerStateChange(playerid, newstate, oldstate)
{
	
	if ( IsPlayerInAnyVehicle(playerid) )
	{
		new vehicleid = GetPlayerVehicleID(playerid);
		if ( newstate == PLAYER_STATE_DRIVER || newstate == PLAYER_STATE_PASSENGER)
		{
			if ( Vehicle[vehicleid][RadioOn] )
			{
				new name[MAX_STATION_NAME], url[MAX_STATION_URL];
				GetStationName(Vehicle[vehicleid][RadioID], name), GetStationUrl(Vehicle[vehicleid][RadioID], url);
				PlayAudioStreamForPlayer(playerid, url);
				SendClientMessage(playerid, COR_BRANCO, "%s{"COR_SISTEMA_INC"} sintonizada.", name);
			}
		}

		if(newstate == PLAYER_STATE_DRIVER){
			if(IsBike(GetVehicleModel(vehicleid)) && GetVehicleParams(vehicleid, VEHICLE_TYPE_ENGINE) != VEHICLE_PARAMS_ON){
				SetVehicleParams(vehicleid, VEHICLE_TYPE_ENGINE, VEHICLE_PARAMS_ON);
			}
		}

		if(Iter_Contains(VehiclePlayers, vehicleid))
		{
			SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Esse veiculo pertence a {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}.", Vehicle[vehicleid][Owner][Name]);
			return Y_HOOKS_CONTINUE_RETURN_1;
		}
	}
	return Y_HOOKS_CONTINUE_RETURN_1;
}

public OnPlayerVendingVehiclePlayer(playerid, id, avisos)
{
	if ( BuyVehicle_pID[id] == playerid )
	{
		--avisos;
		if ( !avisos )
		{
			SendClientMessage(playerid, COR_AVISO, "O(A) Jogador(a) demorou para responder a proposta de venda do veiculo e a mesma foi cancelada.");
			SendClientMessage(id, COR_AVISO, "Voc� demorou para responder e a proposta de venda do veiculo foi cancelada.");
			
			BuyVehicle_pID[id] = INVALID_PLAYER_ID;
			BuyVehicle_vID[id] = INVALID_VEHICLE_ID;
			BuyVehicle_Value[id] = 0;
			return true;
		}
		SendClientMessage(id, COR_AZUL, "� {"COR_BRANCO_INC"}O(A) Jogador(a) est� te oferecendo o veiculo {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"} no valor de {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"}, use /aceitarveiculo.", GetVehicleName(BuyVehicle_vID[id]), RealStr(BuyVehicle_Value[id]));
		SetTimerEx("OnPlayerVendingVehiclePlayer", 15000, false, "ddd", playerid, id, avisos);
		return true;
	}
	return true;
}

// ============================== [ CMDs ] ============================== //

alias:ligar("ligarmotor");
CMD:ligar(playerid)
{
	if(!IsPlayerInAnyVehicle(playerid) || GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
		return SendClientMessage(playerid, COR_ERRO, "Voc� n�o pode ligar o veiculo se n�o estiver dirigindo.");

	new vehicleid = GetPlayerVehicleID(playerid), model = GetVehicleModel(vehicleid);
	if(GetVehicleParams(vehicleid, VEHICLE_TYPE_ENGINE) == VEHICLE_PARAMS_ON || call::CONCE->IsValidVehicleConce(vehicleid))
		return true;
	
	if ( IsBike(model) )
		return true;

	if(call::VH->GetVehicleFuel(vehicleid) <= 0)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Esse veiculo est� sem combust�vel.");

	new Float:health;
	GetVehicleHealth(vehicleid, health);
	if ( health <= 300 )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Veiculo est� muito danificado, chame um Mec�nico.");

	SendClientMessage(playerid, COR_SISTEMA, "Motor do veiculo ligado");
	SetVehicleParams(vehicleid, VEHICLE_TYPE_ENGINE, VEHICLE_PARAMS_ON);
	call::TD->ShowPlayerVelocimetro(playerid);
	return true;
}

alias:desligar("desligarmotor");
CMD:desligar(playerid)
{
	if(!IsPlayerInAnyVehicle(playerid) || GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
		return SendClientMessage(playerid, COR_ERRO, "Voc� n�o pode desligar o veiculo se n�o estiver dirigindo.");

	new vehicleid = GetPlayerVehicleID(playerid), model = GetVehicleModel(vehicleid);
	if(GetVehicleParams(vehicleid, VEHICLE_TYPE_ENGINE) <= VEHICLE_PARAMS_OFF)
		return true;
	
	if ( IsBike(model) )
		return true;

	SendClientMessage(playerid, COR_SISTEMA, "Motor do veiculo desligado");
	SetVehicleParams(vehicleid, VEHICLE_TYPE_ENGINE, VEHICLE_PARAMS_OFF);
	call::TD->ShowPlayerVelocimetro(playerid);
	return true;
}

CMD:ligarfarois(playerid)
{
	if(!IsPlayerInAnyVehicle(playerid) || GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
		return SendClientMessage(playerid, COR_ERRO, "Voc� n�o pode ligar o farol se n�o estiver dirigindo.");

	new vehicleid = GetPlayerVehicleID(playerid), model = GetVehicleModel(vehicleid);
	
	if ( IsBike(model) )
		return true;

	if(GetVehicleParams(vehicleid, VEHICLE_TYPE_LIGHTS) == VEHICLE_PARAMS_ON)
		return true;

	SendClientMessage(playerid, COR_SISTEMA, "Farois do veiculo ligados");
	SetVehicleParams(vehicleid, VEHICLE_TYPE_LIGHTS, VEHICLE_PARAMS_ON);
	call::TD->ShowPlayerVelocimetro(playerid);
	return true;
}

CMD:desligarfarois(playerid)
{
	if(!IsPlayerInAnyVehicle(playerid) || GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
		return SendClientMessage(playerid, COR_ERRO, "Voc� n�o pode desligar o farol se n�o estiver dirigindo.");

	new vehicleid = GetPlayerVehicleID(playerid), model = GetVehicleModel(vehicleid);
	if(GetVehicleParams(vehicleid, VEHICLE_TYPE_LIGHTS) <= VEHICLE_PARAMS_OFF)
		return false;

	if ( IsBike(model) )
		return true;

	SendClientMessage(playerid, COR_SISTEMA, "Farois do veiculo desligados");
	SetVehicleParams(vehicleid, VEHICLE_TYPE_LIGHTS, VEHICLE_PARAMS_OFF);
	call::TD->ShowPlayerVelocimetro(playerid);
	return true;
}

CMD:abrircapo(playerid)
{
	if(!IsPlayerInAnyVehicle(playerid) || GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
		return SendClientMessage(playerid, COR_ERRO, "Voc� n�o pode abrir o capo se n�o estiver dirigindo.");

	new vehicleid = GetPlayerVehicleID(playerid), model = GetVehicleModel(vehicleid);
	if(GetVehicleParams(vehicleid, VEHICLE_TYPE_BONNET) == VEHICLE_PARAMS_ON)
		return false;

	if ( IsBike(model) )
		return true;

	SetVehicleParams(vehicleid, VEHICLE_TYPE_BONNET, VEHICLE_PARAMS_ON);
	return true;
}

CMD:fecharcapo(playerid)
{
	if(!IsPlayerInAnyVehicle(playerid) || GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
		return SendClientMessage(playerid, COR_ERRO, "Voc� n�o pode fechar o capo se n�o estiver dirigindo.");

	new vehicleid = GetPlayerVehicleID(playerid), model = GetVehicleModel(vehicleid);
	if(GetVehicleParams(vehicleid, VEHICLE_TYPE_BONNET) <= VEHICLE_PARAMS_OFF)
		return false;
	
	if ( IsBike(model) )
		return true;

	SetVehicleParams(vehicleid, VEHICLE_TYPE_BONNET, VEHICLE_PARAMS_OFF);
	return true;
}

CMD:abrirportamalas(playerid)
{
	if(!IsPlayerInAnyVehicle(playerid) || GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
		return SendClientMessage(playerid, COR_ERRO, "Voc� n�o pode abrir o portamalas se n�o estiver dirigindo.");

	new vehicleid = GetPlayerVehicleID(playerid), model = GetVehicleModel(vehicleid);
	if(GetVehicleParams(vehicleid, VEHICLE_TYPE_BOOT) == VEHICLE_PARAMS_ON)
		return false;

	if ( IsBike(model) )
		return true;

	SetVehicleParams(vehicleid, VEHICLE_TYPE_BOOT, VEHICLE_PARAMS_ON);
	return true;
}

CMD:fecharportamalas(playerid)
{
	if(!IsPlayerInAnyVehicle(playerid) || GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
		return SendClientMessage(playerid, COR_ERRO, "Voc� n�o pode fechar o portamalas se n�o estiver dirigindo.");

	new vehicleid = GetPlayerVehicleID(playerid), model = GetVehicleModel(vehicleid);
	if(GetVehicleParams(vehicleid, VEHICLE_TYPE_BOOT) <= VEHICLE_PARAMS_OFF)
		return false;

	if ( IsBike(model) )
		return true;

	SetVehicleParams(vehicleid, VEHICLE_TYPE_BOOT, VEHICLE_PARAMS_OFF);
	return true;
}

CMD:trancarveiculo(playerid)
{
	if(IsPlayerInAnyVehicle(playerid))
	{
		if(GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
			return false;

		new vehicleid = GetPlayerVehicleID(playerid), model = GetVehicleModel(vehicleid);
		
		if ( IsBike(model) )
			return true;

		if( Iter_Contains(VehiclePlayers, vehicleid) )
		{
			if(GetVehicleParams(vehicleid, VEHICLE_TYPE_DOORS) == VEHICLE_PARAMS_ON)
				return false;
				
			SetVehicleParams(vehicleid, VEHICLE_TYPE_DOORS, VEHICLE_PARAMS_ON);
			SendClientMessage(playerid, COR_VERDE, "� Veiculo trancado.");

			new query[128];
			mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_VEHICLES_PLAYER" SET `porta`='%d' WHERE `id`='%d'", VEHICLE_PARAMS_ON, Vehicle[vehicleid][Id]);
			mysql_tquery(getConexao(), query);
		}
		else
		{
			foreach(new i: Player)
			{
				if(i != playerid)
				{
					SetVehicleParamsForPlayer(vehicleid, i, 0, VEHICLE_PARAMS_ON);
				}
			}
			SendClientMessage(playerid, COR_VERDE, "� Veiculo trancado.");
		}
		return true;
	}
	else
	{
		new Float:vx, Float:vy, Float:vz;
		foreach(new vehicleid: VehiclePlayers)
		{
			GetVehiclePos(vehicleid, vx, vy, vz);
			if(GetPlayerDistanceFromPoint(playerid, vx, vy, vz) <= 2.0)
			{
				if(GetVehicleParams(vehicleid, VEHICLE_TYPE_DOORS) == VEHICLE_PARAMS_ON)
					return true;

				if ( IsBike(GetVehicleModel(vehicleid)) )
					return true;

				SetVehicleParams(vehicleid, VEHICLE_TYPE_DOORS, VEHICLE_PARAMS_ON);
				SendClientMessage(playerid, COR_VERDE, "� Veiculo trancado.");

				new query[128];
				mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_VEHICLES_PLAYER" SET `porta`='%d' WHERE `id`='%d'", VEHICLE_PARAMS_ON, Vehicle[vehicleid][Id]);
				mysql_tquery(getConexao(), query);
				return true;
			}
		}
	}
	return true;
}

CMD:destrancarveiculo(playerid)
{
	if(IsPlayerInAnyVehicle(playerid))
	{
		if(GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
			return true;

		new vehicleid = GetPlayerVehicleID(playerid);

		if(Iter_Contains(VehiclePlayers, vehicleid))
		{
			if(GetVehicleParams(vehicleid, VEHICLE_TYPE_DOORS) == VEHICLE_PARAMS_OFF)
				return true;

			if ( IsBike(GetVehicleModel(vehicleid)) )
				return true;

			SetVehicleParams(vehicleid, VEHICLE_TYPE_DOORS, VEHICLE_PARAMS_OFF);
			SendClientMessage(playerid, COR_VERDE, "� Veiculo destrancado.");
			
			new query[128];
			mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_VEHICLES_PLAYER" SET `porta`='%d' WHERE `id`='%d'", VEHICLE_PARAMS_OFF, Vehicle[vehicleid][Id]);
			mysql_tquery(getConexao(), query);
			return true;
		}
		else
		{
			foreach(new i: Player)
			{
				if(i != playerid)
				{
					SetVehicleParamsForPlayer(vehicleid, i, 0, VEHICLE_PARAMS_OFF);
				}
			}
			SendClientMessage(playerid, COR_VERDE, "� Veiculo destrancado.");
			return true;
		}
	}
	else
	{
		new Float:vx, Float:vy, Float:vz;
		foreach(new vehicleid: VehiclePlayers)
		{
			GetVehiclePos(vehicleid, vx, vy, vz);
			if(GetPlayerDistanceFromPoint(playerid, vx, vy, vz) <= 2.0)
			{
				if(GetVehicleParams(vehicleid, VEHICLE_TYPE_DOORS) == VEHICLE_PARAMS_OFF)
					return true;
				
				if ( IsBike(GetVehicleModel(vehicleid)) )
					return true;

				SetVehicleParams(vehicleid, VEHICLE_TYPE_DOORS, VEHICLE_PARAMS_OFF);
				SendClientMessage(playerid, COR_VERDE, "� Veiculo destrancado.");

				new query[128];
				mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_VEHICLES_PLAYER" SET `porta`='%d' WHERE `id`='%d'", VEHICLE_PARAMS_OFF, Vehicle[vehicleid][Id]);
				mysql_tquery(getConexao(), query);
				return true;
			}
		}
		return true;
	}
}

CMD:estacionar(playerid, params[])
{
	if ( !IsPlayerInAnyVehicle(playerid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em um veiculo.");

	new vehicleid = GetPlayerVehicleID(playerid);

	if ( !Iter_Contains(VehiclePlayers, vehicleid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode fazer isso.");

	new query[512];
	GetVehiclePos(vehicleid, Vehicle[vehicleid][Position][X], Vehicle[vehicleid][Position][Y], Vehicle[vehicleid][Position][Z]), 
	GetVehicleZAngle(vehicleid, Vehicle[vehicleid][Position][A]);

	EVF::SetVehicleSpawnInfo(vehicleid, Vehicle[vehicleid][Position][X], Vehicle[vehicleid][Position][Y], Vehicle[vehicleid][Position][Z], Vehicle[vehicleid][Position][A], GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid));

	mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_VEHICLES_PLAYER" SET `posicao_x`='%.3f',`posicao_y`='%.3f',`posicao_z`='%.3f',`posicao_a`='%.3f',`world`='%d',`interior`='%d' WHERE `id`='%d' LIMIT 1;",
		Vehicle[vehicleid][Position][X], Vehicle[vehicleid][Position][Y], Vehicle[vehicleid][Position][Z], Vehicle[vehicleid][Position][A],
		GetVehicleVirtualWorld(vehicleid),
		GetVehicleInterior(vehicleid),
		Vehicle[vehicleid][Id]
	);
	mysql_tquery(getConexao(), query);

	SendClientMessage(playerid, COR_VERDE, "� Veiculo estacionado com sucesso!");
	return true;
}

CMD:localizarveiculos(playerid)
{
	new count, str[255], zone[MAX_ZONE_NAME], Float:x, Float:y, Float:z;
	
	str = "Veiculo\tLocaliza��o\n";
	foreach(new vehicleid: VehiclePlayers)
	{
		if( Vehicle[vehicleid][Owner][Id] == Jogador[playerid][PlayerID] )
		{
			new casaid = GetVehicleVirtualWorld(vehicleid);
			if ( casaid != 0 )
			{
				if ( !call::HOUSE->IsValidHouse(casaid) )return 1;
				GetLocalName(Casa[casaid][Entrada][X], Casa[casaid][Entrada][Y], zone, MAX_ZONE_NAME);
			}
			else
			{
				GetVehiclePos(vehicleid, x, y, z);
				GetLocalName(x, y, zone, sizeof(zone));
			}
			
			format(str, sizeof(str), "%s{"COR_AMARELO_INC"}%s\t{"COR_SISTEMA_INC"}%s\n", str, GetVehicleName(vehicleid), zone);
			count++;
		}
	}
	if ( count )
		return ShowPlayerDialog(playerid, LOCALIZAR_VEICULO, DIALOG_STYLE_TABLIST_HEADERS, "LOCALIZAR VEICULOS", str, "Localizar", "Fechar");
	
	SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o possui nenhum veiculo.");
	return true;
}

CMD:venderveiculo(playerid, params[])
{
	if ( !IsPlayerInAnyVehicle(playerid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� precisa est� dentro do veiculo que quer vender.");

	new vehicleid = GetPlayerVehicleID(playerid);

	if ( !Iter_Contains(VehiclePlayers, vehicleid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em um veiculo de concession�ria.");

	if ( Vehicle[vehicleid][Owner][Id] != Jogador[playerid][PlayerID] )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o � propriet�rio deste veiculo.");

	new id, valor;
	if (sscanf(params, "dd", id, valor))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/venderveiculo [id] [valor]");

	if ( id == playerid )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode oferecer um veiculo a s� mesmo.");

	if ( valor > call::VH->GetVehicleModelValor(GetVehicleModel(vehicleid)) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode vender o veiculo mais caro do que na concession�ria.");

	if ( !call::PLAYER->IsPlayerLogged(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) n�o est� conectado(a).");

	new Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x, y, z);
	if ( GetPlayerDistanceToPoint(id, x, y) > 15)
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) n�o est� pr�ximo de voc�.");
	
	BuyVehicle_pID[playerid] = id;

	BuyVehicle_pID[id] = playerid;
	BuyVehicle_vID[id] = vehicleid;
	BuyVehicle_Value[id] = valor;
	
	SendClientMessage(id, COR_AZUL, "� {"COR_BRANCO_INC"}O(A) Jogador(a) {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"} est� te oferecendo o veiculo {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"}...", GetUserName(playerid), GetVehicleName(vehicleid));
	SendClientMessage(id, COR_BRANCO, "no valor de {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"}, use {"COR_AMARELO_INC"}/aceitarveiculo{"COR_BRANCO_INC"}.", RealStr(valor));

	SendClientMessage(playerid, COR_BRANCO, "Voc� ofereceu o seu veiculo {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"} ao jogador {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"}  por {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"}, aguarde a resposta.", GetVehicleName(vehicleid), GetUserName(id), RealStr(valor));
	SetTimerEx("OnPlayerVendingVehiclePlayer", 15000, false, "ddd", playerid, id, 5);
	return true;
}

CMD:aceitarveiculo(playerid)
{
	if ( BuyVehicle_vID[playerid] != INVALID_VEHICLE_ID )
	{
		if ( BuyVehicle_Value[playerid] > GetPlayerMoney(playerid) )
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem a quantia necess�ria para comprar o veiculo.");

		new vehicleid = BuyVehicle_vID[playerid];

		Vehicle[vehicleid][Owner][Id] = Jogador[playerid][PlayerID];
		format(Vehicle[vehicleid][Owner][Name], MAX_PLAYER_NAME, GetUserName(playerid) );

		SendClientMessage(playerid, COR_BRANCO, "� Voc� comprou o veiculo {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"} por {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"} do jogador {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"}.", GetVehicleName(vehicleid), RealStr(BuyVehicle_Value[playerid]), GetUserName(BuyVehicle_pID[playerid]) );
		SendClientMessage(BuyVehicle_pID[playerid], COR_BRANCO, "� O(A) Jogador(a) {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"} comprou o veiculo {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"}, e voc� recebeu {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"}.", GetUserName(playerid), GetVehicleName(vehicleid), RealStr(BuyVehicle_Value[playerid]));

		new query[128];
		mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_VEHICLES_PLAYER" SET `proprietario_id`='%d' WHERE `id`='%d' LIMIT 1;", Vehicle[vehicleid][Owner][Id], Vehicle[vehicleid][Id]);
		mysql_tquery(getConexao(), query);

		GivePlayerMoney(playerid, -BuyVehicle_Value[playerid]);
		GivePlayerMoney(BuyVehicle_pID[playerid], BuyVehicle_Value[playerid]);

		BuyVehicle_vID[playerid] = INVALID_VEHICLE_ID;
		BuyVehicle_pID[playerid] = INVALID_PLAYER_ID;
		BuyVehicle_Value[playerid] = 0;

		BuyVehicle_pID[BuyVehicle_pID[playerid]] = INVALID_PLAYER_ID;
		return true;
	}
	SendClientMessage(playerid, COR_ERRO, "Erro: N�o lhe foi oferecido nenhum veiculo.");
	return true;
}

// ============================== [ DIALOGs ] ============================== //
Dialog:LOCALIZAR_VEICULO(playerid, response, listitem, inputtext[])
{
	if(response)
	{
		new count;
		foreach(new vehicleid: VehiclePlayers)
		{
			if( Vehicle[vehicleid][Owner][Id] == Jogador[playerid][PlayerID] )
			{
				if(listitem == count)
				{
					new casaid = GetVehicleVirtualWorld(vehicleid);
					if ( casaid )
					{
						if ( !call::HOUSE->IsValidHouse(casaid) )return 1;

						new zone[MAX_ZONE_NAME];
						GetLocalName(Casa[casaid][Entrada][X], Casa[casaid][Entrada][Y], zone, MAX_ZONE_NAME);
						call::PLAYER->SetPlayerMarkGPS(playerid, Casa[casaid][Garagem][pEntrada][X], Casa[casaid][Garagem][pEntrada][Y], Casa[casaid][Garagem][pEntrada][Z]);
						SendClientMessage(playerid, COR_BRANCO, "� Seu veiculo {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"} est� localizado em uma garagem na cidade de {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"}.", GetVehicleName(vehicleid), zone);
						return 1;
					}
					else
					{
						new zone[MAX_ZONE_NAME], Float:x, Float:y, Float:z;
						GetLocalName(x, y, zone, MAX_ZONE_NAME);
						GetVehiclePos(vehicleid, x, y, z);
						call::PLAYER->SetPlayerMarkGPS(playerid, x, y, z);
						SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Seu veiculo {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"} est� localizado em {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"}.", GetVehicleName(vehicleid), zone);
						return 1;
					}
				}
				count++;
			}
		}
		return 1;
	}
	return 1;
}


stock LoadPlayerVehicles(playerid)
{
	new query[255];
	mysql_format(getConexao(), query, sizeof(query), "SELECT *, u.username as proprietario FROM "TABLE_VEHICLES_PLAYER" as v LEFT JOIN "TABLE_USERS" as u ON u.id = v.proprietario_id WHERE proprietario_id = %d;", Jogador[playerid][PlayerID]);
	mysql_tquery(getConexao(), query, "OnLoadVehiclesPlayer", "i", playerid);
	return 1;
}

public OnLoadVehiclesPlayer(playerid)
{
	new rows = cache_num_rows();
	if ( rows )
	{
		new model, color1, color2, Float:x, Float:y, Float:z, Float:a, Float:km, Float:health;
		for(new i; i < rows; i++)
		{
			cache_get_value_name_int(i, "modelo", model);
			cache_get_value_name_int(i, "cor1", color1);
			cache_get_value_name_int(i, "cor2", color2);

			cache_get_value_name_float(i, "posicao_x", x);
			cache_get_value_name_float(i, "posicao_y", y);
			cache_get_value_name_float(i, "posicao_z", z);
			cache_get_value_name_float(i, "posicao_a", a);
			
			new vehicleid = CreateVehicle(model, x, y, z, a, color1, color2, -1);
			
			Vehicle[vehicleid][Position][X] = x;
			Vehicle[vehicleid][Position][Y] = y;
			Vehicle[vehicleid][Position][Z] = z;
			Vehicle[vehicleid][Position][A] = a;
			cache_get_value_name_int(i, "interior", Vehicle[vehicleid][Interior]);
			LinkVehicleToInterior(vehicleid, Vehicle[vehicleid][Interior]);

			cache_get_value_name_int(i, "world", Vehicle[vehicleid][World]);
			SetVehicleVirtualWorld(vehicleid, Vehicle[vehicleid][World]);

			Iter_Add(VehiclePlayers, vehicleid);

			cache_get_value_name_int(i, "id", Vehicle[vehicleid][Id]);
			cache_get_value_name(i, "proprietario", Vehicle[vehicleid][Owner][Name], MAX_PLAYER_NAME);
			cache_get_value_name_int(i, "proprietario_id", Vehicle[vehicleid][Owner][Id]);
			
			cache_get_value_name(i, "placa", Vehicle[vehicleid][Placa], MAX_VEHICLE_PLACA_LEN);
			SetVehicleNumberPlate(vehicleid, Vehicle[vehicleid][Placa]);
			
			cache_get_value_name_int(i, "nitro", Vehicle[vehicleid][Nitro]);
			AddVehicleComponent(vehicleid, Vehicle[vehicleid][Nitro]);

			cache_get_value_name_int(i, "roda", Vehicle[vehicleid][Rodas]);
			AddVehicleComponent(vehicleid, Vehicle[vehicleid][Rodas]);

			cache_get_value_name_int(i, "hidraulica", Vehicle[vehicleid][Hidraulica]);
			AddVehicleComponent(vehicleid, Vehicle[vehicleid][Hidraulica]);

			cache_get_value_name_int(i, "paintjob", Vehicle[vehicleid][PaintJob]);
			ChangeVehiclePaintjob(vehicleid, Vehicle[vehicleid][PaintJob]);

			cache_get_value_name_int(i, "porta", Vehicle[vehicleid][Porta]);
			SetVehicleParams(vehicleid, VEHICLE_TYPE_DOORS, Vehicle[vehicleid][Porta]);

			cache_get_value_name_float(i, "km", km);
			SetVehicleDistanceTravaled(vehicleid, km);

			cache_get_value_name_float(i, "vida", health);
			SetVehicleHealth(vehicleid, (health > 300.0 ? health : 300.0));

			cache_get_value_name_int(i, "multas", Vehicle[vehicleid][Multas]);

			cache_get_value_name_float(i, "combustivel", Vehicle[vehicleid][Fuel]);
			
			if(Vehicle[vehicleid][Fuel] > GetVehicleMaxFuel(model) )
				Vehicle[vehicleid][Fuel] = GetVehicleMaxFuel(model);

			cache_get_value_name_int(i, "ipva", Vehicle[vehicleid][IPVA]);
			new interior, world;
			cache_get_value_name_int(i, "interior", interior);
			LinkVehicleToInterior(vehicleid, interior);

			cache_get_value_name_int(i, "world", world);
			SetVehicleVirtualWorld(vehicleid, world);
		}
	}
}

stock SavePlayerVehicles(playerid)
{
	new query[1024 * 2], Float:health, bool:is_verified = false;
	for(new i; i < MAX_VEHICLES; i++)
	{
		if ( !IsValidVehicle(i) )continue;

		if ( Jogador[playerid][PlayerID] == Vehicle[i][Owner][Id] )
		{
			is_verified = true;
			GetVehicleHealth(i, health);
			format(query, sizeof(query), "%sUPDATE "TABLE_VEHICLES_PLAYER" SET `combustivel`='%.3f',`vida`='%.3f',`km`='%.3f' WHERE `id`='%d';", query, GetVehicleFuel(i), health, GetDistanceTraveled(i), Vehicle[i][Id]);
			
			DestroyVehicle(i);

			// resetar variaveis do veiculo do jogador.
			new r[e_VEHICLE_INFO];
			Vehicle[i] = r;

			Iter_SafeRemove(VehiclePlayers, i, i);
		}
	}
	if ( !is_verified )return 0;

	mysql_tquery(getConexao(), query);
	return 1;
}
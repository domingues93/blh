
alias:ajuda("help");
CMD:ajuda(playerid, params[])
{
	new tmp[20];
	if(!sscanf(params, "s[20]", tmp))
	{
		static str[1024 * 3];
		if(strcmp(tmp, "geral", true) == 0)
		{
			str[0] = EOS;
			ReadFile("textos/help/geral.txt", str, sizeof(str));
			ShowPlayerDialog(playerid, PLAYER_MENU_AJUDA_R, DIALOG_STYLE_MSGBOX, "AJUDA � {00BBFF}GERAL", str, "OK", "Voltar");
			return true;
		}
		else if(strcmp(params ,"profissao", true) == 0)
		{
			callcmd::profissao(playerid);
			return true;
		}
		else if(strcmp(tmp, "veiculo", true) == 0)
		{
			str[0] = EOS;
			ReadFile("textos/help/veiculo.txt", str, sizeof(str));
			ShowPlayerDialog(playerid, PLAYER_MENU_AJUDA_R, DIALOG_STYLE_MSGBOX, "AJUDA � {00BBFF}VEICULO", str, "OK", "Voltar");
			return true;
		}
		else if(strcmp(tmp, "casa", true) == 0)
		{
			str[0] = EOS;
			ReadFile("textos/help/casa.txt", str, sizeof(str));
			ShowPlayerDialog(playerid, PLAYER_MENU_AJUDA_R, DIALOG_STYLE_MSGBOX, "AJUDA � {00BBFF}CASA", str, "OK", "Voltar");
			return true;
		}
		else if ( strcmp(tmp, "empresa", true) == 0 )
		{
			str[0] = EOS;
			ReadFile("textos/help/empresa.txt", str, sizeof(str));
			ShowPlayerDialog(playerid, PLAYER_MENU_AJUDA_R, DIALOG_STYLE_MSGBOX, "AJUDA � {00BBFF}EMPRESA", str, "OK", "Voltar");
			return true;
		}
		else if ( strcmp(tmp, "celular", true) == 0 )
		{
			str[0] = EOS;
			ReadFile("textos/help/celular.txt", str, sizeof(str));
			ShowPlayerDialog(playerid, PLAYER_MENU_AJUDA_R, DIALOG_STYLE_MSGBOX, "AJUDA � {00BBFF}CELULAR", str, "OK", "Voltar");
			return true;
		}
		else if ( strcmp(tmp, "vip", true) == 0 )
		{
			str[0] = EOS;
			ReadFile("textos/help/vip.txt", str, sizeof(str));
			ShowPlayerDialog(playerid, PLAYER_MENU_AJUDA_R, DIALOG_STYLE_MSGBOX, "AJUDA � {00BBFF}VIP", str, "OK", "Voltar");
			return true;
		}
		else if ( strcmp(tmp, "contato", true) == 0 )
		{
			str[0] = EOS;
			ReadFile("textos/help/contato.txt", str, sizeof(str));
			ShowPlayerDialog(playerid, PLAYER_MENU_AJUDA_R, DIALOG_STYLE_MSGBOX, "AJUDA � {00BBFF}CONTATO", str, "OK", "Voltar");
			return true;
		}
		SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/ajuda");
		return true;
	}
	ShowPlayerDialog(playerid, PLAYER_MENU_AJUDA, DIALOG_STYLE_LIST, "AJUDA", "Ajuda Geral\nAjuda Profiss�o\nAjuda Veiculo\nAjuda Casa\nAjuda Empresa\nAjuda Celular\nAjuda VIP\nAjuda Contato", "Selecionar", "Cancelar");
	return true;
}

CMD:von(playerid)
{
	if ( !Jogador[playerid][Vip] )
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

	if ( Jogador[playerid][ChatVip] ) {
		SendClientMessage(playerid, COR_ERRO, "Erro: O chat VIP j� esta ativado.");
		return 0;
	}

	Jogador[playerid][ChatVip] = true;
	SendClientMessage(playerid, COR_LARANJA, "INFO: Voc� ativou o chat vip.");
	return 1;
}

CMD:voff(playerid)
{
	if ( !Jogador[playerid][Vip] )
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

	if ( !Jogador[playerid][ChatVip] ) {
		SendClientMessage(playerid, COR_ERRO, "Erro: O chat VIP j� esta desativado.");
		return 0;
	}

	Jogador[playerid][ChatVip] = false;
	SendClientMessage(playerid, COR_LARANJA, "INFO: Voc� desativou o chat vip, ningu�m mais vai te incomodar.");
	return 1;
}


CMD:ativarcodigo(playerid, params[])
{
	if( isnull(params) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/ativarcodigo [c�digo]");

	new query[128];

	mysql_format(getConexao(), query, sizeof(query), "SELECT * FROM `code_vip` WHERE `resgatado`='%d' LIMIT 1;", Jogador[playerid][PlayerID]);
	new Cache:cache_id = mysql_query(getConexao(), query, true);

	if ( cache_num_rows() )
	{
		cache_delete(cache_id);

		SendClientMessage(playerid, COR_ERRO, "Erro: Voc� j� fez um resgate e n�o pode mais resgatar c�digos de vip.");
		return 0;
	}
	cache_delete(cache_id);


	mysql_format(getConexao(), query, sizeof(query), "SELECT * FROM `code_vip` WHERE `codigo`='%e' LIMIT 1;", params);
	cache_id = mysql_query(getConexao(), query, true);
	
	if ( cache_num_rows() )
	{
		new bool:resgatado, dias_vip;
		cache_is_value_name_null(0, "resgatado", resgatado);
		if ( !resgatado )
		{
			cache_delete(cache_id);
			SendClientMessage(playerid, COR_ERRO, "Erro: O c�digo digitado j� foi resgatado.");
			return 0;
		}

		cache_get_value_name_int(0, "dias_vip", dias_vip);
		cache_delete(cache_id);

		new name[MAX_PLAYER_NAME];
		GetPlayerName(playerid, name, MAX_PLAYER_NAME);

		new response = SetPlayerVip(name, dias_vip, Jogador[playerid][Vip]);
		
		if ( !response )
		{
			SendClientMessage(playerid, COR_ERRO, "Erro: N�o foi poss�vel setar seus dias vip");
			SendClientMessage(playerid, COR_ERRO, "Erro: Tire print apertando F8 e informe a algum Administrador.");
			return 0;
		}

		mysql_format(getConexao(), query, sizeof(query), "UPDATE `code_vip` SET `resgatado`='%d' WHERE `codigo`='%e';", Jogador[playerid][PlayerID], params);
		mysql_tquery(getConexao(), query);
		
		Jogador[playerid][Vip] = true;
		SendClientMessage(playerid, COR_AZUL, "� {"COR_BRANCO_INC"}Parab�ns voc� resgatou seus {"COR_AZUL_INC"}%d{"COR_BRANCO_INC"} dias vip com {"COR_VERDE_INC"}sucesso{"COR_BRANCO_INC"}.", dias_vip);
		return 1;
	}
	SendClientMessage(playerid, COR_ERRO, "Erro: N�o encontramos esse c�digo, verifique o c�digo digitado e tente novamente.");
	cache_delete(cache_id);
	return 1;
}

CMD:duvida(playerid, params[])
{
    new
    	duvida[128];

    if(call::PLAYER->GetPlayerVarInt(playerid, Preso) > 0)
    	return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode mandar uma d�vida estando preso!");

    if(sscanf(params, "s[128]", duvida))
    	return SendClientMessage(playerid, COR_ERRO, "Erro: duvida (mensagem)");

    if ( call::PLAYER->GetPlayerVarInt(playerid, Duvida) > gettime() )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� deve esperar {"COR_BRANCO_INC"}%d{"COR_ERRO_INC"} segundos para fazer uma nova pergunta.", call::PLAYER->GetPlayerVarInt(playerid, Duvida) - gettime());

    new AdminOn;
    foreach(new i: Player)
    {
        if(call::ADMIN->GetPlayerAdminLevel(i) >= AJUDANTE)
        {
            GameTextForPlayer(i, "~b~Duvida Recebida", 5000, 3);
            PlayerPlaySound(i, 1085, -1, -1, -1);
            //Jogador[playerid][EnviouDuvida] = gettime() + 60, AdminOn++;
            call::PLAYER->SetPlayerVarInt(playerid, Duvida, gettime() + 30), AdminOn++;
            SendClientMessage(i, 0xFF6A6AFF, "| DUVIDA | Duvida de %s(%d) [ %s ]", GetUserName(playerid), playerid, duvida);
        }
    }
    if(AdminOn)
    	SendClientMessage(playerid, 0x00BBFFFF, "| DUVIDA | Sua d�vida foi enviada para a Administra��o, aguarde uma resposta!");
    else
    	SendClientMessage(playerid, COR_ERRO, "| DUVIDA | N�o h� administradores online. Procure ajuda no f�rum!");
    return true;
}

/*CMD:nitro(playerid)
{
	if ( !Jogador[playerid][Vip] || call::ADMIN->GetPlayerAdminLevel(playerid) < AJUDANTE )return SendClientMessage(playerid, COR_ERRO, "Erro: N�o n�o tem permiss�o para usar esse comando.");
	
	if(NitroInfi[playerid] == false)
	{
		NitroInfi[playerid] = true;
		GameTextForPlayer(playerid,"~n~~g~Nitro Infinito Ativado!", 3000, 5);
	}
	else
	{
		NitroInfi[playerid] = false;
		SetPlayerHealth(playerid, 100);
		GameTextForPlayer(playerid, "~n~~g~Nitro Infinito Desativado!", 3000, 5);
	}
	return true;
}*/

CMD:vips(playerid) {
	new ContarVips, __str[1024];
	format(__str, sizeof(__str), "Nome(id)");
	foreach(new i: Player) {
	    if(Jogador[i][Vip] == true) {
	        format(__str, sizeof(__str), "%s\n%s(%d)", __str, GetUserName(i), i);
	        ContarVips++;
	    }
	}
	if(ContarVips > 0) {
	    ShowPlayerDialog(playerid, DIALOG_VIPS, DIALOG_STYLE_TABLIST_HEADERS, "{FFFFFF}Vips Online", __str, "Fechar", #);
	} else {
	    SendClientMessage(playerid, COR_ERRO, "Erro:  N�o tem nenhum(a) VIP online no momento!");
	}
	return true;
}

CMD:mp3(playerid)
{
	if ( !call::PLAYER->GetPlayerVarBool(playerid, MP3) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem um mp3, voc� precisa comprar um em uma loja de utilit�rios.");

	new str[512], nome[MAX_STATION_NAME];
	for(new i, stations = CountStations(); i < stations; i++)
	{
		GetStationName(i, nome);
		format(str, sizeof(str), "%s%s\n", str, nome);
	}
	format(str, sizeof(str), "%s{F05555}Desligar", str);
	ShowPlayerDialog(playerid, PLAYER_MENU_RADIO, DIALOG_STYLE_LIST, "LISTA DE R�DIOS", str, "Ouvir", "Sair");
	return true;
}

CMD:usargalao(playerid)
{
	new Float:fuel = 15, vehicleid = GetPlayerVehicleID(playerid);
	if ( !call::PLAYER->GetPlayerVarBool(playerid, Galao) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem um Gal�o de Gasolina, voc� precisa comprar um em uma loja de utilit�rios.");

	if( !IsPlayerInAnyVehicle(playerid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em um veiculo.");

	if(call::VH->GetVehicleFuel(vehicleid) >= GetVehicleMaxFuel(GetVehicleModel(GetPlayerVehicleID(playerid))))
  		return SendClientMessage(playerid, COR_ERRO, "Erro: O Veiculo est� com o tanque cheio.");

  	if(call::VH->GetVehicleFuel(vehicleid) > 15)
  		return SendClientMessage(playerid, COR_ERRO, "Erro: Esse veiculo n�o est� na reserva.");

	new Float:max_fuel = GetVehicleMaxFuel(GetVehicleModel(GetPlayerVehicleID(playerid)));
	
	fuel = ( fuel >  max_fuel ? max_fuel : fuel );
	call::VH->SetVehicleFuel(GetPlayerVehicleID(playerid), fuel);

	SendClientMessage(playerid, COR_SISTEMA, "Voc� usou gal�o de gasolina e o combustivel do seu veiculo foi para {"COR_BRANCO_INC"}%d{"COR_SISTEMA_INC"}.", floatround(fuel));

	call::PLAYER->SetPlayerVarBool(playerid, Galao, false);

	new query[128];
	mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `galao`='0' WHERE `id`='%d' LIMIT 1;", call::PLAYER->GetPlayerVarInt(playerid, PlayerID));
	mysql_tquery(getConexao(), query);

	return true;
}

CMD:tops(playerid)
{
	ShowPlayerDialog(playerid, SERVER_STATICS, DIALOG_STYLE_LIST, "ESTAT�STICAS", "Top Grana\nTop Level\nTop Hits", "Selecionar", "Fechar");
	return true;
}

CMD:gps(playerid)
{
	if ( !call::PLAYER->GetPlayerVarBool(playerid, GPS) && GetPlayerScore(playerid) > 5 )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem um gps");

	if ( GetPlayerScore(playerid) < 5 && !call::PLAYER->GetPlayerVarBool(playerid, GPS))
		SendClientMessage(playerid, COR_ERRO, "Erro: Voc� poder� usar gps at� o level 5, ap�s isso dever� comprar o seu.");

	ShowPlayerDialog(playerid, PLAYER_GPS_MENU, DIALOG_STYLE_LIST, "GPS", "Propriedades P�blicas\nMinha Empresa\nMinha Casa\nProfiss�es\nConcession�rias\n{"COR_ERRO_INC"}Desligar", "Selecionar", "Sair");
	return true;
}

CMD:chamartaxi(playerid, params[])
{
	call::TAXI->CallTaxi(playerid);
	return true;
}

CMD:abastecer(playerid)
{
	if(!call::PLAYER->IsPlayerInPosto(playerid, 4.5))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� proximo a uma bomba de combust�vel.");

	if(!IsPlayerInAnyVehicle(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em um veiculo.");
	
	if(GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Somente o motorista pode abastecer o veiculo.");

	new vehicleid = GetPlayerVehicleID(playerid), model = GetVehicleModel(vehicleid);

	if(call::VH->GetVehicleFuel(vehicleid) >= GetVehicleMaxFuel(model))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Veiculo est� com o tanque cheio.");

	if(GetPVarInt(playerid, "abastecendo"))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� j� est� abastecendo um veiculo.");

	if(GetVehicleParams(vehicleid, VEHICLE_TYPE_ENGINE) == VEHICLE_PARAMS_ON)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Desligue o motor do veiculo para abastecer.");

	new valor, tipo[50];
	switch(call::VH->GetVehicleFuelType(model))
	{
		case TYPE_GASOLINA:
			valor = VALOR_GASOLINA, tipo = "da {"COR_AZUL_INC"}Gasolina{"COR_BRANCO_INC"}";
		case TYPE_DIESEL:
			valor = VALOR_DIESEL, tipo = "do {"COR_AZUL_INC"}Diesel{"COR_BRANCO_INC"}";
		case TYPE_AVG:
			valor =  VALOR_AVG, tipo = "do {"COR_AZUL_INC"}AVG�s{"COR_BRANCO_INC"}";
	}
	new str[200];
	format(str, sizeof(str), "{"COR_BRANCO_INC"}Voc� deseja abastecer seu veiculo?\n\nO Valor %s est� {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"} por litro.\n\n{"COR_ERRO_INC"}* digite a quantidade de litros que deseja abastecer.", tipo, RealStr(valor));
	ShowPlayerDialog(playerid, ABASTECENDO_VEICULO, DIALOG_STYLE_INPUT, "ABASTECIMENTO", str, "Abastecer", "Cancelar");
	return true;
}

alias:admins("staff", "adms", "equipe");
CMD:admins(playerid)
{
	new info[1036], admin, pName[MAX_PLAYER_NAME], id, hits;
	info = "Nome\tCargo\tStatus\tHits\n";

	new Cache:cache = mysql_query(getConexao(), "SELECT `username`,`admin`,`hits` FROM "TABLE_USERS" WHERE `admin`>'0' ORDER BY `admin` DESC;");
	for(new i, rows = cache_num_rows(); i < rows; i++)
	{
		cache_get_value_name(i, "username", pName, MAX_PLAYER_NAME), cache_get_value_name_int(i, "admin", admin), cache_get_value_name_int(i, "hits", hits);
		id = GetPlayerIDByName(pName);
		format(info, sizeof(info), "%s%s\t%s\t%s\t%d\n", info, pName, call::ADMIN->CargoAdmin(admin, true), (call::PLAYER->IsPlayerLogged(id) ?(call::ADMIN->IsAdminInJob(id) ? ("{0F7EC4}Administrando"):("{37DF62}Jogando") ):("{E23131}Offline") ), hits);
	}
	cache_delete(cache);
	ShowPlayerDialog(playerid, 0, DIALOG_STYLE_TABLIST_HEADERS, "{"COR_VERMELHO_INC"}"SERVER_NAME"� {"COR_BRANCO_INC"}ADMINS", info, "OK", "");
	return true;
}


CMD:dormir(playerid)
{
	if(call::PLAYER->GetPlayerVarFloat(playerid, Sono) >= 100.0)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� com sono.");
	
	if(call::PLAYER->GetPlayerVarInt(playerid, pDormindo) != 0)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� j� est� dormindo.");

	TogglePlayerControllable(playerid, false);
	call::PLAYER->SetPlayerVarInt(playerid, pDormindo, 1);

	call::PLAYER->InitPlayerFadeEffect(playerid);
	ApplyAnimation(playerid, "CRACK", "CRCKIDLE2", 4.1, 1, 1, 1, 1, 0);
	
	SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Voc� est� dormindo...");
	return true;
}

CMD:acordar(playerid)
{
	if(call::PLAYER->GetPlayerVarInt(playerid, pDormindo) == 0)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� dormindo.");

	call::PLAYER->DestroyPlayerFadeSono(playerid, 0);
	call::PLAYER->SetPlayerVarInt(playerid, pDormindo, 0);

	ClearAnimations(playerid);
	SetCameraBehindPlayer(playerid);
	TogglePlayerControllable(playerid, true);
	SendClientMessage(playerid, COR_SISTEMA, "� {"COR_BRANCO_INC"}Voc� est� acordando...");
	return true;
}

alias:reportar("relatar", "report");
CMD:reportar(playerid, params[])
{
	new id, motivo[140];
	if ( sscanf(params, "ds[140]", id, motivo) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/reportar [id] [motivo]");

	if ( !IsPlayerConnected(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) reportado n�o est� online.");

	if ( GetPVarInt(playerid, "PlayerUserReport") > gettime() )
	{
		SendClientMessage(playerid, COR_ERRO, "[X] Voc� fez um reporte recentemente, aguarde {"COR_BRANCO_INC"}%d{"COR_ERRO_INC"} segundos para fazer outro reporte.", gettime() - GetPVarInt(playerid, "PlayerUserReport"));
		return 0;
	}

	foreach(new i: PlayersAdmin)
	{
		if( call::ADMIN->IsAdminInJob(i) )
		{
			SendClientMessage(i, 0xFF6B6BFF, "RELATO: {"COR_BRANCO_INC"}%s[%d]{FF6B6} reportou o jogador {"COR_BRANCO_INC"}%s[%d]{FF6B6}:", GetUserName(playerid), playerid, GetUserName(id), id);
			SendClientMessage(i, 0xFF6B6BFF, "� {"COR_BRANCO_INC"}%s", motivo);
			GameTextForPlayer(i, "~h~~h~~h~~r~REPORT", 3000, 4);
			PlayerPlaySound(i, 1057, 0.0, 0.0, 0.0);
		}
	}
	SetPVarInt(playerid, "PlayerUserReport", gettime() + 180);
	SendClientMessage(playerid, COR_VERDE, "� {"COR_BRANCO_INC"}Reporte enviado, por favor aguarde.");
	return true;
}
CMD:ejetar(playerid, params[])
{
	if(isnull(params) || !IsNumeric(params))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/ejetar [playerid]");

	if(!IsPlayerInAnyVehicle(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em um veiculo");

	if(GetPlayerState(playerid) != PLAYER_STATE_DRIVER)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o � motorista deste veiculo");

	new pid = strval(params), vehicleid = GetPlayerVehicleID(playerid);

	if(!call::PLAYER->IsPlayerLogged(pid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador n�o est� conectado.");

	if(!IsPlayerInVehicle(pid, vehicleid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: O Jogador n�o est� no seu veiculo.");

	RemovePlayerFromVehicle(pid);
	SendClientMessage(pid, COR_AVISO, "Voc� foi expulso do veiculo.");

	SendClientMessage(playerid, COR_SISTEMA, "� {1FDA9A}Voc� expulsou o jogador do seu veiculo.");
	return true;
}

CMD:kill(playerid)
{
	if (call::PLAYER->GetPlayerVarInt(playerid, Perseguido) || call::PLAYER->GetPlayerVarInt(playerid, Algemado))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em persegui��o ou algemado.");

	SetPlayerHealth(playerid, 0);
	SendClientMessage(playerid, COR_ERRO, "* Voc� se suicidou.");
	return true;
}

CMD:config(playerid)
{
	ShowPlayerDialog(playerid, CONFIGURACOES_JOGADOR, DIALOG_STYLE_LIST, "CONFIGURAES DO USURIO", "Hud", "Selecionar", "Fechar");
	return true;
}

CMD:sms(playerid, params[])
{
	if ( !call::PLAYER->GetPlayerVarInt(playerid, Celular) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem um celular.");

	if ( call::PLAYER->GetPlayerVarInt(playerid, SaldoCelular) < 1 )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem cr�ditos para enviar um sms.");

	new number, mensagem[100];

	if ( GetPVarInt(playerid, "number_sms") == 0 )
	{
		if (sscanf(params, "ds[100]", number, mensagem) )
			return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/sms [numero] [mensagem]");
	}
	else
	{
		if (sscanf(params, "s[100]", mensagem) )
			return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/sms [mensagem]");

		number = GetPVarInt(playerid, "number_sms");
	}

	if ( isnull(mensagem) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/sms [numero] [mensagem]");

	if ( number == call::PLAYER->GetPlayerVarInt(playerid, Celular) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode enviar sms para s� mesmo.");

	foreach(new i: Player)
	{
		if ( number == call::PLAYER->GetPlayerVarInt(i, Celular))
		{
			GameTextForPlayer(i, "~b~Mensagem Recebida!", 3000, 1);
			SendClientMessage(i, 0xBBCC00FF, "SMS recebido de {"COR_BRANCO_INC"}%s %d{BBCC00}: (( {"COR_BRANCO_INC"}%s{BBCC00} ))", GetUserName(playerid), call::PLAYER->GetPlayerVarInt(playerid, Celular), mensagem);
			SendClientMessage(playerid, 0xBBCC00FF, "SMS enviado para {"COR_BRANCO_INC"}%s %d{BBCC00}: (( {"COR_BRANCO_INC"}%s{BBCC00} ))", GetUserName(i), number, mensagem);
			GameTextForPlayer(playerid, "~b~Mensagem Enviada!", 3000, 1);

			call::PLAYER->SetPlayerVarInt(playerid, SaldoCelular, call::PLAYER->GetPlayerVarInt(playerid, SaldoCelular) - 1);
			SetPVarInt(playerid, "number_sms", number);

			if ( GetPVarInt(playerid, "number_sms") == 0 )
				SendClientMessage(playerid, COR_AMARELO, "Se quiser enviar mensagem para esse n�mero novamente � só usar /sms [mensagem]");

			return true;
		}
	}
	SendClientMessage(playerid, COR_ERRO, "O Telefone est� desligado ou fora da area de cobertura.");
	return true;
}

CMD:discar(playerid, params[])
{
	if ( !call::PLAYER->GetPlayerVarInt(playerid, Celular) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem um celular.");

	if ( isnull(params) || !IsNumeric(params))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/discar [numero]");	

	new number = strval(params);
	if ( number == call::PLAYER->GetPlayerVarInt(playerid, Celular) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode ligar para s� mesmo.");
	

	switch(number)
	{
		case 190:
		{
			new count;

			foreach(new i: Player)
			{
				if ( call::PLAYER->GetPlayerVarInt(i, Profissao) == POLICIA_MILITAR )
				{
					SendClientMessage(i, 0x221296FF, "O Telefone de den�ncia est� tocando. Use /atender para respoder a den�ncia.");
					call::PLAYER->SetPlayerVarInt(i, StateCell, CHAMANDO);
					call::PLAYER->SetPlayerVarInt(i, CallerID, playerid);

					TocarCelular(i);
					++count;
				}
			}

			if ( count )
			{
				call::PLAYER->SetPlayerVarInt(playerid, StateCell, CHAMANDO);
				call::PLAYER->SetPlayerVarInt(playerid, TypeCall, EMERGENCIA);
				
				SetPlayerSpecialAction(playerid, SPECIAL_ACTION_USECELLPHONE);
				SetPlayerAttachedObject(playerid, 9, 18868, 6);
				SendClientMessage(playerid, COR_BRANCO, "Chamando... Use {"COR_AZUL_INC"}/finalizar{"COR_BRANCO_INC"} para cancelar a discagem.");
				return true;
			}

			SendClientMessage(playerid, COR_ERRO, "Erro: Desculpe mais n�o temos nenhum policial disponivel no momento.");
			return true;
		}
		default:
		{
			if ( call::PLAYER->GetPlayerVarInt(playerid, SaldoCelular) < TARIFA_LIGACAO )
				return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem cr�ditos para fazer uma liga��o.");


			foreach(new i: Player)
			{
				if ( number == call::PLAYER->GetPlayerVarInt(i, Celular) )
				{
					if ( call::PLAYER->GetPlayerVarInt(i, CallerID) != INVALID_PLAYER_ID )
						return SendClientMessage(playerid, COR_ERRO, "O N�mero chamado est� ocupado.");

					call::PLAYER->SetPlayerVarInt(playerid, StateCell, CHAMANDO);
					SetPlayerSpecialAction(playerid, SPECIAL_ACTION_USECELLPHONE);
					SetPlayerAttachedObject(playerid, ATTACH_CELL, 18868, 6);
					SendClientMessage(playerid, COR_BRANCO, "Chamando... Use {"COR_AZUL_INC"}/finalizar{"COR_BRANCO_INC"} para cancelar a discagem.");

					call::PLAYER->SetPlayerVarInt(i, CallerID, playerid);
					call::PLAYER->SetPlayerVarInt(i, StateCell, CHAMANDO);
					
					SendClientMessage(i, COR_AZUL, "Seu celular est� tocando, use /atender para pegar o telefone.");
					TocarCelular(i);			
					return true;
				}
			}
		}
	}
	return true;
}

CMD:atender(playerid)
{
	if ( call::PLAYER->GetPlayerVarInt(playerid, StateCell) != CHAMANDO )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� recebendo uma liga��o.");

	new id = call::PLAYER->GetPlayerVarInt(playerid, CallerID);
	
	if ( playerid == id)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode atender sua própria liga��o.");

	call::PLAYER->SetPlayerVarInt(playerid, CallerID, id);
	call::PLAYER->SetPlayerVarInt(playerid, StateCell, EM_CHAMADA);
	SendClientMessage(playerid, 0x4F0A9CFF, "Voc� atendeu o telefone.");
	SetPlayerSpecialAction(playerid, SPECIAL_ACTION_USECELLPHONE);
	SetPlayerAttachedObject(playerid, ATTACH_CELL, 18868, 6);
	PlayerPlaySound(playerid, 3402, 0.0, 0.0, 0.0);
	
	SendClientMessage(id, 0x4F0A9CFF, "* Atenderam o telefone no outro lado da linha.");
	call::PLAYER->SetPlayerVarInt(id, StateCell, EM_CHAMADA);
	CobrarTarifaCelular(playerid);
	return true;
}

CMD:finalizar(playerid)
{
	if ( !call::PLAYER->GetPlayerVarInt(playerid, StateCell) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em uma liga��o.");

	call::CELL->FinalizarChamada( playerid, call::PLAYER->GetPlayerVarInt(playerid, CallerID) );
	return true;
}

CMD:meucelular(playerid)
{
	if ( !call::PLAYER->GetPlayerVarInt(playerid, Celular) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem um telefone.");

	SendClientMessage(playerid, COR_SISTEMA, "Seu n�mero � {"COR_AMARELO_INC"}%d{"COR_SISTEMA_INC"} 	| 	Cr�ditos: {"COR_AMARELO_INC"}%d{"COR_SISTEMA_INC"}", call::PLAYER->GetPlayerVarInt(playerid, Celular), call::PLAYER->GetPlayerVarInt(playerid, SaldoCelular));
	return true;
}

CMD:agenda(playerid, params[])
{
	if ( !call::PLAYER->GetPlayerVarBool(playerid, Agenda) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem uma agenda telef�nica.");

	new id;
	if ( sscanf(params, "u", id))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/agenda [id/username]");

	if ( !call::PLAYER->IsPlayerLogged(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O jogador n�o est� conectado.");

	if ( !call::PLAYER->GetPlayerVarInt(id, Celular) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Esse jogador n�o est� na lista telef�nica.");

	SendClientMessage(playerid, COR_SISTEMA, "� {FFFFFF}O n�mero do telefone de {00CCFF}%s{FFFFFF} � {DBED15}%d", GetUserName(id), call::PLAYER->GetPlayerVarInt(id, Celular));
	return true;
}

CMD:continuar(playerid, params[])
{
	if ( Jogador[playerid][TimeContinuar] < gettime() )
		return SendClientMessage(playerid, COR_ERRO, "O seu tempo para usar o comando '{"COR_BRANCO_INC"}/continuar{"COR_ERRO_INC"}', expirou!");

	Jogador[playerid][TimeContinuar] = 0;
	
	Teleport(
		playerid,
		Jogador[playerid][Spawn][X],
		Jogador[playerid][Spawn][Y],
		Jogador[playerid][Spawn][Z],
		Jogador[playerid][Spawn][A],
		Jogador[playerid][Interior],
		Jogador[playerid][World],
		GetPlayerTableValueInt(playerid, "entrou")
	);
	SendClientMessage(playerid, COR_AZUL, "Voc� foi levado at� sua ultima posi��o salva.");
	return true;
}


alias:identidade("rg");
CMD:identidade(playerid)
{
	new str[1050 * 2];

	/**
	*
	*	formatar as informa��es.
	*
	**/
	format(str, sizeof(str), "{"COR_BRANCO_INC"}Registro Geral: {"COR_AZUL_INC"}%06d{"COR_BRANCO_INC"}\n", Jogador[playerid][PlayerID]);
	format(str, sizeof(str), "%sNome: {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"}\nSkin: {FF8A00}%d{"COR_BRANCO_INC"}\nBitCoins: {"COR_LARANJA_INC"}%d{"COR_BRANCO_INC"}\n", str, GetUserName(playerid), GetPlayerSkin(playerid), Jogador[playerid][Cash] );

	if ( Jogador[playerid][Vip] )
		format(str, sizeof(str), "%sVIP: {"COR_VERDE_INC"}Sim{"COR_BRANCO_INC"}\tVence em: {1BD45C}%d{"COR_BRANCO_INC"} dias\n", str, IsPlayerTimeVip(playerid));
	else 
		format(str, sizeof(str), "%sVIP: {"COR_VERMELHO_INC"}N�o{"COR_BRANCO_INC"}\n", str);

	format(str, sizeof(str), "%sHabilita��es: ", str);
	if ( BitFlag_Get(Jogador[playerid][Habilitacao], HAB_MOTO) ||
		BitFlag_Get(Jogador[playerid][Habilitacao], HAB_CARRO) ||
		BitFlag_Get(Jogador[playerid][Habilitacao], HAB_CAMINHAO) ||
		BitFlag_Get(Jogador[playerid][Habilitacao], HAB_BARCO) ||
		BitFlag_Get(Jogador[playerid][Habilitacao], HAB_AVIAO) )
	{
		if ( BitFlag_Get(Jogador[playerid][Habilitacao], HAB_MOTO) )
			format(str, sizeof(str), "%s{"COR_AMARELO_INC"}A{"COR_BRANCO_INC"} ", str);
		if ( BitFlag_Get(Jogador[playerid][Habilitacao], HAB_CARRO) )
			format(str, sizeof(str), "%s{"COR_AMARELO_INC"}B{"COR_BRANCO_INC"} ", str);
		if ( BitFlag_Get(Jogador[playerid][Habilitacao], HAB_CAMINHAO) )
			format(str, sizeof(str), "%s{"COR_AMARELO_INC"}C{"COR_BRANCO_INC"} ", str);
		if ( BitFlag_Get(Jogador[playerid][Habilitacao], HAB_BARCO) )
			format(str, sizeof(str), "%s{"COR_AMARELO_INC"}D{"COR_BRANCO_INC"} ", str);
		if ( BitFlag_Get(Jogador[playerid][Habilitacao], HAB_AVIAO) )
			format(str, sizeof(str), "%s{"COR_AMARELO_INC"}E{"COR_BRANCO_INC"}", str);
	}
	else
		format(str, sizeof(str), "%s{"COR_VERMELHO_INC"}N�o � habilitado.{"COR_BRANCO_INC"}", str);
	
	format(str, sizeof(str), "%s\n", str);


	format(str, sizeof(str), "%sLevel: {"COR_AZUL_INC"}%d{"COR_BRANCO_INC"}\tExperi�ncia: {"COR_AZUL_INC"}%d{"COR_BRANCO_INC"}\n", str, GetPlayerScore(playerid), Jogador[playerid][EXP]);
	
	if ( call::PLAYER->GetPlayerVarInt(playerid, GPS) || call::PLAYER->GetPlayerVarInt(playerid, MP3) || call::PLAYER->GetPlayerVarInt(playerid, Celular) || call::PLAYER->GetPlayerVarInt(playerid, Agenda) )
	{
		if ( call::PLAYER->GetPlayerVarInt(playerid, GPS) )
			format(str, sizeof(str), "%sGPS: {"COR_VERDE_INC"}Sim{"COR_BRANCO_INC"}\n", str);

		if ( call::PLAYER->GetPlayerVarInt(playerid, MP3) )
			format(str, sizeof(str), "%sMP3: {"COR_VERDE_INC"}Sim{"COR_BRANCO_INC"}\n", str);
		
		if (  call::PLAYER->GetPlayerVarInt(playerid, Celular) != 0 )
			format(str, sizeof(str), "%sCelular: {"COR_VERDE_INC"}Sim{"COR_BRANCO_INC"}\tN�mero: {"COR_AMARELO_INC"}%d{"COR_BRANCO_INC"}\tcr�ditos: {"COR_VERDE_INC"}%d{"COR_BRANCO_INC"}\n", str, call::PLAYER->GetPlayerVarInt(playerid, Celular), call::PLAYER->GetPlayerVarInt(playerid, SaldoCelular));
		
		if ( call::PLAYER->GetPlayerVarInt(playerid, Agenda) )
			format(str, sizeof(str), "%sAgenda: {"COR_VERDE_INC"}Sim{"COR_BRANCO_INC"}\n", str);
	}

	if ( call::PLAYER->GetPlayerVarInt(playerid, Sementes) )
	{
		format(str, sizeof(str), "%sSementes: {"COR_VERDE_INC"}%d{"COR_BRANCO_INC"}\n", str, call::PLAYER->GetPlayerVarInt(playerid, Sementes));
	}

	if ( call::PLAYER->GetPlayerVarInt(playerid, Maconha) )
	{
		format(str, sizeof(str), "%sMaconha: {"COR_VERDE_INC"}%d{"COR_BRANCO_INC"}\n", str, call::PLAYER->GetPlayerVarInt(playerid, Maconha));
	}

	if ( call::PLAYER->GetPlayerVarInt(playerid, Cocaina) )
	{
		format(str, sizeof(str), "%sCocaina: {"COR_VERDE_INC"}%d{"COR_BRANCO_INC"}\n", str, call::PLAYER->GetPlayerVarInt(playerid, Cocaina));
	}

	if ( call::PLAYER->GetPlayerVarInt(playerid, Crack) )
	{
		format(str, sizeof(str), "%sCrack: {"COR_VERDE_INC"}%d{"COR_BRANCO_INC"}\n", str, call::PLAYER->GetPlayerVarInt(playerid, Crack));
	}

	/**
	*
	*	mostrar o dialog
	*
	**/
	ShowPlayerDialog(playerid, 0, DIALOG_STYLE_MSGBOX, GetUserName(playerid), str, "OK", "");
	return true;
}

CMD:solicitar(playerid, params[])
{
	new Tipo[30];

	if(sscanf(params, "s[30]", Tipo))
			return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/solicitar [mecanico]");

	if(!strcmp(Tipo, "mecanico", true))
	{
		new zone[MAX_ZONE_NAME], Float: _pos[3];
		GetPlayerPos(playerid, _pos[0], _pos[1], _pos[2]);
		GetLocalName(_pos[0], _pos[1], zone, MAX_ZONE_NAME);

		foreach(new i: Player)
	 	{
	  		if(call::PLAYER->GetPlayerVarInt(i, Profissao) == MECANICO)
	    	{
	     		SendClientMessage(i, COR_SISTEMA, "* {"COR_BRANCO_INC"}O(a) jogador(a) {"COR_VERDE_INC"}%s{"COR_BRANCO_INC"} est� solicitando um Mecânico ( Local: {"COR_AVISO_INC"}%s ).", GetUserName(playerid), zone);
	       	}
	    }
	}
	return true;
}

CMD:presos(playerid)
{
	new str[1045] = "id\tnome\ttempo\tpris�o\n", count, presotxt[10];
	if(IsPlayerInRangeOfPoint(playerid, 2.0, 248.7233, 67.9884, 1003.6406) || IsPlayerInRangeOfPoint(playerid, 2.0, 298.5375,178.8100,1007.1719) || IsPlayerInRangeOfPoint(playerid, 2.0, 246.5317,118.3733,1003.2188))
	{
		foreach(new i: Player)
		{
			if ( call::PLAYER->GetPlayerVarInt(i, Preso) > 0 )
			{
				new hours, minutes, seconds, str2[50];
				
				formatSeconds(call::PLAYER->GetPlayerVarInt(i, TempoPreso), hours, minutes, seconds);

				if(call::PLAYER->GetPlayerVarInt(i, Preso) == PRESO_POLICIA) { presotxt = "Militar"; }
				if(call::PLAYER->GetPlayerVarInt(i, Preso) == PRESO_ADM) { presotxt = "Federal"; }

				format(str2, sizeof(str2), "%02d:%02d:%02d", hours, minutes, seconds);
				format(str, sizeof(str), "%s%d\t{"COR_VERMELHO_INC"}%s\t{"COR_BRANCO_INC"}%s\t{"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"}\n", str, i, GetUserName(i), str2, presotxt);
				count++;
			}
		}
		if(count)
			return ShowPlayerDialog(playerid, 0, DIALOG_STYLE_TABLIST_HEADERS, "LISTA DE PRESOS", str, "OK", "");

		SendClientMessage(playerid, COR_ERRO, "N�o h� nenhum preso online.");
	}
	else SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em Departamento de Pol�cia.");
	return true;
}


// ============================== [ COMANDOS VIP ] ============================== //

alias:cmdsvip("comandosvip");
CMD:cmdsvip(playerid, params[])
{
	if ( Jogador[playerid][Vip] )
	{
		ShowPlayerDialog(playerid, 0, DIALOG_STYLE_MSGBOX, "{1FB1AF}COMANDOS VIP",
"{"COR_VERMELHO_INC"}/vencimentovip{"COR_BRANCO_INC"} � Para visualizar seu vencimento vip.\n\
{"COR_VERMELHO_INC"}/v{"COR_BRANCO_INC"}� Chat vip\n\
{"COR_VERMELHO_INC"}/virar{"COR_BRANCO_INC"}� Para virar seu veiculo.\n\
{"COR_VERMELHO_INC"}/pintar{"COR_BRANCO_INC"}� Para pintar seu veiculo.\n\
{"COR_VERMELHO_INC"}/autotuning{"COR_BRANCO_INC"}� Auto tunar seu veiculo.\n\
{"COR_VERMELHO_INC"}/neon{"COR_BRANCO_INC"}� Colocar luz no seu veiculo.\n\
{"COR_VERMELHO_INC"}/tunar{"COR_BRANCO_INC"}� Tunar seu veiculo.\n\
{"COR_VERMELHO_INC"}/repararv{"COR_BRANCO_INC"}� Para reparar seu veiculo.\n",
		"OK", "");
		return true;
	}
	SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);
	return true;
}

CMD:vencimentovip(playerid, params[])
{
	if ( Jogador[playerid][Vip] )
	{
		ShowPlayerDialog(playerid, 0, DIALOG_STYLE_MSGBOX, "{1FB1AF}VENCIMENTO VIP", "{"COR_VERMELHO_INC"}�{"COR_BRANCO_INC"} Seus dia(s) vip acabam em {"COR_AZUL_INC"}%d{"COR_BRANCO_INC"} dias.", "OK", "", IsPlayerTimeVip(playerid));
		return true;
	}
	SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);
	return true;
}

CMD:vantagensvip(playerid)
{
	new str[1024 * 4];
	ReadFile("textos/help/vip.txt", str, sizeof(str));
	ShowPlayerDialog(playerid, 0, DIALOG_STYLE_MSGBOX, "AJUDA � {00BBFF}VIP", str, "Fechar", "");
	return true;
}

CMD:v(playerid, params[])
{
	if ( Jogador[playerid][Vip] )
	{
		if ( isnull(params) )
			return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/v [mensagem]");

		checaXingamento(playerid, params);

		foreach(new i: Player) {
			if ( Jogador[i][Vip] && Jogador[i][ChatVip] ) {
				SendClientMessage(i, 0xF4A460FF, "[chat VIP] %s[%d] disse: %s", GetUserName(playerid), playerid, params);
			}
		}
		return true;
	}
	SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);
	return true;
}

CMD:jetpack(playerid)
{
	if ( !Jogador[playerid][Vip] )
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

	if(GetPlayerWantedLevel(playerid) != 0)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar jetpack, pois est� sendo procurado pela policia.");

	if (IsPlayerInAnyVehicle(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando dentro de um veiculo.");
	
	if ( call::PLAYER->GetPlayerVarBool(playerid, Perseguido) || call::PLAYER->GetPlayerVarBool(playerid, Perseguindo) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em persegui��o.");

	if ( call::PLAYER->GetPlayerVarBool(playerid, Algemado) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando algemado.");

	if(GetPlayerSpecialAction(playerid) == SPECIAL_ACTION_USEJETPACK)
	{
		static Float:x, Float:y, Float:z;
		GetPlayerPos(playerid, x, y, z);
		SetPlayerPos(playerid, x, y, z);
		SendClientMessage(playerid, COR_BRANCO, "� Jetpack foi {"COR_VERMELHO_INC"}destruido{"COR_BRANCO_INC"}.");
		return true;
	}
	SetPlayerSpecialAction(playerid, SPECIAL_ACTION_USEJETPACK);
	SendClientMessage(playerid, COR_BRANCO, "� Jetpack criado com {"COR_VERDE_INC"}sucesso{"COR_BRANCO_INC"}, use o comando novamente se quiser destrui-lo.");
	return true;	
}

CMD:autotuning(playerid)
{
	if(!Jogador[playerid][Vip] && call::ADMIN->GetPlayerAdminLevel(playerid) < AJUDANTE)
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

	if(Autotunning[playerid] == 0) {
		Autotunning[playerid] = 1;
		if(call::ADMIN->GetPlayerAdminLevel(playerid) >= AJUDANTE || Jogador[playerid][Vip] == true) {
			SendClientMessage(playerid, COR_AVISO, "| VIP | Voc� ativou o modo AutoTuning para seus ve�culos!");
		}
		return true;
	}
	if(Autotunning[playerid] == 1) {
		Autotunning[playerid] = 0;
		if(call::ADMIN->GetPlayerAdminLevel(playerid) >= AJUDANTE || Jogador[playerid][Vip] == true) {
			SendClientMessage(playerid, COR_ERRO, "| VIP | Voc� desativou o modo AutoTuning!");
		}
		return true;
	}
	return true;
}

/*CMD:autotunning(playerid, params[])
{
	if ( !Jogador[playerid][Vip] )
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

	if (!IsPlayerInAnyVehicle(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em um ve�culo.");
	
	if ( call::PLAYER->GetPlayerVarBool(playerid, Perseguido) || call::PLAYER->GetPlayerVarBool(playerid, Perseguindo) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em persegui��o.");

	if ( call::PLAYER->GetPlayerVarBool(playerid, Algemado) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando algemado.");

	new Vehicleid = GetPlayerVehicleID(playerid);

	AddVehicleComponent(Vehicleid, 1035), AddVehicleComponent(Vehicleid, 1078), AddVehicleComponent(Vehicleid, 1010);
    AddVehicleComponent(Vehicleid, 1087), AddVehicleComponent(Vehicleid, 1046), AddVehicleComponent(Vehicleid, 1171);
    AddVehicleComponent(Vehicleid, 1149), AddVehicleComponent(Vehicleid, 1147);
    AddVehicleComponent(Vehicleid, 1036), AddVehicleComponent(Vehicleid, 1040);
	ChangeVehiclePaintjob(Vehicleid, random(3)), ChangeVehicleColor(Vehicleid, random(500), random(500));
	RepairVehicle(Vehicleid), SetVehicleHealth(Vehicleid, 1000);
	SendClientMessage(playerid, COR_AVISO, "* Veiculo tunado com sucesso.");
	return true;
}*/

CMD:cortag(playerid, params[])
{
	if ( !Jogador[playerid][Vip] )
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

 	if( isnull(params) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/cortag [Código-Html]");
	
	if ( !IsValidHex(params, VERIFY_HEX_8) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voce n�o digitou um código html v�lido, use o tipo RRGGBBAA.");

	Jogador[playerid][CorTag] = HexToInt(params);
	SendClientMessage(playerid, COR_AVISO, "� Voc� alterou a cor da tag vip para ( {%06x}Cor{"COR_BRANCO_INC"} ).", (Jogador[playerid][CorTag] >>> 8) );
	
	new file[MAX_FILE_CONFIG_LEN];
	format(file, sizeof(file), CONFIG_PLAYER_PATH, GetUserName(playerid));
	
	if ( !fexist(file) )
		DOF2::CreateFile(file);

    DOF2::SetInt(file, TAG_CORTAGVIP, Jogador[playerid][CorTag]);     
    DOF2::SaveFile(); 
	return 1;
}

CMD:virar(playerid, params[])
{
	if ( !Jogador[playerid][Vip] )
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

	new Float: _pos[4];
	if(IsPlayerInAnyVehicle(playerid))
	{
		SetCameraBehindPlayer(playerid);
		GetPlayerPos(playerid, _pos[0], _pos[1], _pos[2]);
		SetVehiclePos(GetPlayerVehicleID(playerid), _pos[0], _pos[1], _pos[2]);
		GetPlayerFacingAngle(playerid, _pos[3]);
		SetVehicleZAngle(GetPlayerVehicleID(playerid), _pos[3]);
	}
	return true;
}

CMD:pintar(playerid, params[])
{
	if ( !Jogador[playerid][Vip] )
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

	if (!IsPlayerInAnyVehicle(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em um ve�culo.");
	
	new id, id2;
	if ( sscanf(params, "dd", id, id2))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/pintar [cor1] [cor2]");

	if(id < 0 || id > 255)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Apenas ids de 0 a 255.");

	if(id2 < 0 || id2 > 255)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Apenas ids de 0 a 255.");

	ChangeVehicleColor(GetPlayerVehicleID(playerid), id, id2);
	return true;
}

CMD:neon(playerid, params[])
{
	if ( !Jogador[playerid][Vip] )
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

	if (!IsPlayerInAnyVehicle(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em um ve�culo.");

 	ShowPlayerDialog(playerid, Dialog_Neons, DIALOG_STYLE_LIST, "Neons", "Vermelho\nAzul\nVerde\nAmarelo\nRosa\nBranco\nSirene\nRemover Neon", "Confirmar", "Fechar");
	return true;
}

CMD:repararv(playerid, params[])
{
	if ( !Jogador[playerid][Vip] )
		return SendClientMessage(playerid, COR_ERRO, CMD_NOT_PERMISSION);

	if (!IsPlayerInAnyVehicle(playerid))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em um ve�culo.");

	if ( call::PLAYER->GetPlayerVarBool(playerid, Perseguido) || call::PLAYER->GetPlayerVarBool(playerid, Perseguindo) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar este comando em persegui��o.");
		
	RepairVehicle(GetPlayerVehicleID(playerid));
 	SetVehicleHealth(GetPlayerVehicleID(playerid), 1000);
  	SendClientMessage(playerid, COR_VERDE, "Veiculo reparado.");
	return true;
}

CMD:radio(playerid)
{
	if ( !IsPlayerInAnyVehicle(playerid) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� em um veiculo.");

	new str[512], nome[MAX_STATION_NAME];
	for(new i, stations = CountStations(); i < stations; i++){
		GetStationName(i, nome);
		format(str, sizeof(str), "%s%s\n", str, nome);
	}
	format(str, sizeof(str), "%s{F05555}Desligar", str);
	ShowPlayerDialog(playerid, VEHICLE_RADIO_STATION, DIALOG_STYLE_TABLIST, "ESTAÇÕES DE RÁDIO", str, "Sintonizar", "Sair");
	return true;
}


CMD:world(playerid)
{
	SendClientMessage(playerid, 0xB9C9BFFF, "Current World: %d", GetPlayerVirtualWorld(playerid));
	return true;
}

alias:me("y");
CMD:me(playerid, params[])
{
	if( !isnull(params) )
	{
		static Float:x, Float:y, Float:z;
		GetPlayerPos(playerid, x, y, z);
		
		checaXingamento(playerid, params);

		foreach(new i: Player)
		{
			if ( !call::PLAYER->IsPlayerLogged(i) )
				continue;

			if ( IsPlayerInRangeOfPoint(i, 15.0, x, y, z) )
			{
				SendClientMessage(i, 0xDA68FFFF, "** %s %s", GetUserName(playerid), params);
			}
		}
		return true;
	}
	SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/me [texto]");
	return true;
}

static pagarTime[MAX_PLAYERS];
alias:pagar("dardinheiro");
CMD:pagar(playerid, params[])
{
	new id, valor;
	if ( sscanf(params, "dd", id, valor) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/pagar [id] [valor]");

	if(GetPlayerScore(id) < 10) 
		return SendClientMessage(playerid, COR_ERRO, "Erro: Este jogador n�o tem level suficiente.");

	if ( !call::PLAYER->IsPlayerLogged(id) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) jogador(a) n�o est� logado.");

	new Float:x, Float:y, Float:z;

	GetPlayerPos(playerid, x, y, z);

	if ( GetPlayerDistanceFromPoint(id, x, y, z) > 10 )
		return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) jogador(a) n�o est� próximo a voc�.");

	if ( valor <= 0)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode retirar dinheiro do jogador.");

	if ( valor > 20000 )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� só pode pagar no m�ximo R$20.000 ao jogador.");

	if ( GetPlayerMoney(playerid) < valor )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem o dinheiro suficiente.");

	if ( pagarTime[playerid] > gettime() )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Aguarde %d segundos para usar o comando novamente.", pagarTime[playerid] - gettime() );
	
	pagarTime[playerid] = gettime() + (60 * 3);

	GivePlayerMoney(playerid, -valor);
	GivePlayerMoney(id, valor);

	
	SendClientMessage(playerid, COR_AZUL, "* {"COR_BRANCO_INC"}Voc� deu {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"} para o(a) jogador(a) {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"}.", RealStr(valor), GetUserName(id));
	SendClientMessage(id, COR_AZUL, "* {"COR_BRANCO_INC"}O(A) jogador {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"} deu {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"} para voc�.", GetUserName(playerid), RealStr(valor));

	new str[128];
	format(str, sizeof(str), "O jogador %s deu R$%s para %s", GetUserName(playerid), RealStr(valor), GetUserName(id));
	WriteLog("pagar.txt", str);
	return true;
}

CMD:vender(playerid, params[])
{
	new id, valor, tipo[15], qtd;
	if ( sscanf(params, "s[15]idi", tipo, qtd, id, valor))
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/vender [Maconha/Cocaina/Crack] [quantidade] [id] [valor cada uma]");

	if(!strcmp(tipo, "Maconha", true))
	{
		if(qtd > call::PLAYER->GetPlayerVarInt(playerid, Maconha)) 
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem toda essa quantia de maconha.");

		if(valor < 200 || valor > 600) 
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� só pode digitar valores de 200$ a 600$.");

		new valorapagar = qtd * valor;
		SendClientMessage(playerid, COR_AVISO, "* Voc� ofereceu ao %s %d kgs de {"COR_VERMELHO_INC"}Maconha{"COR_AVISO_INC"} no Valor de: R$%s.", GetUserName(id), qtd, RealStr(valorapagar));

		SendClientMessage(id, COR_BRANCO, "* %s Est� te oferecendo %d kgs de {"COR_VERMELHO_INC"}Maconha{"COR_BRANCO_INC"} no Valor de: {088A08}R$%s.", GetUserName(playerid), qtd, RealStr(valorapagar));
		SendClientMessage(id, COR_AZUL, "* Para aceitar digite: /aceitar maconha e para recusar digite: /recusa rmaconha.");
		SetTimerEx("ExpirouMaconha", 30000, true, "i", id);
		SetPVarInt(id, "OferecedorMID", playerid), SetPVarInt(id, "DinheiroMaconha", valorapagar), SetPVarInt(id, "MaconhaOferecida", 1), SetPVarInt(id, "MQuantidade", qtd);
	}
	else if(!strcmp(tipo, "Cocaina", true))
	{
		if(qtd > call::PLAYER->GetPlayerVarInt(playerid, Cocaina)) 
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem toda essa quantia de cocaina.");

		if(valor < 200 || valor > 600) 
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� só pode digitar valores de 200$ a 600$.");

		new valorapagar = qtd * valor;
		SendClientMessage(playerid, COR_AVISO, "* Voc� ofereceu ao %s %d kgs de {"COR_VERMELHO_INC"}Cocaina{"COR_AVISO_INC"} no Valor de: R$%s.", GetUserName(id), qtd, RealStr(valorapagar));

		SendClientMessage(id, COR_BRANCO, "* %s Est� te oferecendo %d kgs de {"COR_VERMELHO_INC"}Cocaina{"COR_BRANCO_INC"} no Valor de: {088A08}R$%s.", GetUserName(playerid), qtd, RealStr(valorapagar));
		SendClientMessage(id, COR_AZUL, "* Para aceitar digite: /aceitar cocaina e para recusar digite: /recusar cocaina.");
		SetTimerEx("ExpirouCocaina", 30000, true, "i", id);
		SetPVarInt(id, "OferecedorCID", playerid), SetPVarInt(id, "DinheiroCocaina", valorapagar), SetPVarInt(id, "CocainaOferecida", 1), SetPVarInt(id, "CQuantidade", qtd);
	}
	else if(!strcmp(tipo, "Crack", true))
	{
		if(qtd > call::PLAYER->GetPlayerVarInt(playerid, Crack)) 
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem toda essa quantia de crack.");

		if(valor < 200 || valor > 600) 
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� só pode digitar valores de 200$ a 600$.");

		new valorapagar = qtd * valor;
		SendClientMessage(playerid, COR_AVISO, "* Voc� ofereceu ao %s %d kgs de {"COR_VERMELHO_INC"}Crack{"COR_AVISO_INC"} no Valor de: R$%s.", GetUserName(id), qtd, RealStr(valorapagar));

		SendClientMessage(id, COR_BRANCO, "* %s Est� te oferecendo %d kgs de {"COR_VERMELHO_INC"}Crack{"COR_BRANCO_INC"} no Valor de: {088A08}R$%s.", GetUserName(playerid), qtd, RealStr(valorapagar));
		SendClientMessage(id, COR_AZUL, "* Para aceitar digite: /aceitar crack e para recusar digite: /recusar crack.");
		SetTimerEx("ExpirouCrack", 30000, true, "i", id);
		SetPVarInt(id, "OferecedorCraID", playerid), SetPVarInt(id, "DinheiroCrack", valorapagar), SetPVarInt(id, "CrackOferecida", 1), SetPVarInt(id, "CraQuantidade", qtd);
	}
	return true;
}

CMD:aceitar(playerid, params[])
{
	new Tipo[30];

	if(sscanf(params, "s[30]", Tipo))
			return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/aceitar [servico/conserto/maconha/cocaina/crack]");
		
	if(!strcmp(Tipo, "servico", true))
	{
		if( call::PLAYER->GetPlayerVarInt(playerid, AdvogadoID) == INVALID_PLAYER_ID ) 
			return SendClientMessage(playerid, COR_ERRO, "Erro: Nenhum advogado lhe ofereceu servi�o.");

		new valor = call::PLAYER->GetPlayerVarInt(playerid, AdvogadoValor), valor_advogado = ( valor > 15000 ? 15000 : valor ), ad_id = call::PLAYER->GetPlayerVarInt(playerid, AdvogadoID);

		if ( call::BANK->IsPlayerAccountOpenned(playerid) )
		{
			if ( valor > call::BANK->GetPlayerValueBankAccount(playerid, "saldo") )
				return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem o saldo suficiente para pagar o advogado.");

			new query[128];
			mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_BANK" SET `saldo`=`saldo`-'%d' WHERE `id`='%d' LIMIT 1;", valor, call::PLAYER->GetPlayerVarInt(playerid, PlayerID) );
			mysql_tquery(getConexao(), query);
		}
		else
		{

			if ( valor > GetPlayerMoney(playerid) )
				return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem {"COR_BRANCO_INC"}R$%s{"COR_ERRO_INC"} em m�os para pagar o advogado.", RealStr(valor));

			GivePlayerMoney(playerid, -valor);
		}
		

		/**
		*
		* 	advogado set's
		*
		**/
		call::PLAYER->SetPlayerVarInt(playerid, AdvogadoID, INVALID_PLAYER_ID);
		call::PLAYER->SetPlayerVarInt(playerid, AdvogadoValor, 0);

		call::PM->SoltarPrisioneiro(playerid);


		/**
		*
		* 	advogado set's
		*
		**/
		
		call::PLAYER->SetPlayerVarInt(ad_id, PrisioneiroID, INVALID_PLAYER_ID);
		GivePlayerMoney(ad_id, valor_advogado);
		SendClientMessage(ad_id, COR_AVISO, "* O(A) jogador(a) {"COR_BRANCO_INC"}%s{"COR_AVISO_INC"} aceitou seus servi�os e voc� ganhou {"COR_VERDE_INC"}%s.", GetUserName(playerid), RealStr(valor_advogado));
		return true;
	}
	else if(!strcmp(Tipo, "conserto", true))
	{
		if(GetPVarInt(playerid, "MecSolicitado") < 1)
			return SendClientMessage(playerid, COR_ERRO, "Erro: N�o te ofereceram uma repara��o.");

		new valorapagar = GetPVarInt(playerid, "DinheiroReparo"), mecanicoid = GetPVarInt(playerid, "MecanicoID"), vehicleid = Jogador[mecanicoid][ReparandoVeiculo];

		if(GetPlayerMoney(playerid) < valorapagar)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem dinheiro suficiente.");

		if(mecanicoid == playerid)
		{
			SetVehicleHealth(vehicleid, 1000.0), RepairVehicle(vehicleid);
			SendClientMessage(playerid, COR_BRANCO, "* Voc� consertou seu ve�culo.");
			call::MECANICO->CancelarServico(mecanicoid);
			DeletePVar(playerid, "MecanicoID"), DeletePVar(playerid, "DinheiroReparo"), DeletePVar(playerid, "MecSolicitado");
		}
		else
		{
			SendClientMessage(mecanicoid, COR_BRANCO, "* {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"} Aceitou seu conserto e voc� recebeu: {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"}.", GetUserName(playerid), RealStr(valorapagar));
			GivePlayerMoney(mecanicoid, valorapagar);
			RepairVehicle(vehicleid), SetVehicleHealth(vehicleid, 1000.0);
			SendClientMessage(playerid, COR_BRANCO, "* Voc� aceitou o conserto de {"COR_VERDE_INC"}%s{"COR_BRANCO_INC"} e pagou o Valor de: {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"}.", GetUserName(mecanicoid), RealStr(valorapagar));
			GivePlayerMoney(playerid, -valorapagar);
			call::MECANICO->CancelarServico(mecanicoid);
			DeletePVar(playerid, "MecanicoID"), DeletePVar(playerid, "DinheiroReparo"), DeletePVar(playerid, "MecSolicitado");
		}
		return true;
	}
	else if(!strcmp(Tipo, "maconha", true))
	{
		if(GetPVarInt(playerid, "MaconhaOferecida") < 1)
			return SendClientMessage(playerid, COR_ERRO, "Erro: N�o te ofereceram Maconha.");

		new valorapagar = GetPVarInt(playerid, "DinheiroMaconha");

		SendClientMessage(GetPVarInt(playerid, "OferecedorMID"), COR_BRANCO, "* %s Aceitou sua Maconha e voc� recebeu: {088A08}R$%s", GetUserName(playerid), RealStr(valorapagar));
		GivePlayerMoney(GetPVarInt(playerid, "OferecedorMID"), valorapagar);
		call::PLAYER->SetPlayerVarInt(GetPVarInt(playerid, "OferecedorMID"), Maconha, call::PLAYER->GetPlayerVarInt(GetPVarInt(playerid, "OferecedorMID"), Maconha) - GetPVarInt(playerid, "MQuantidade"));

		SendClientMessage(playerid, COR_AVISO, "* Voc� aceitou a Maconha de %s e pagou o Valor de: {088A08}R$%s", GetUserName(GetPVarInt(playerid, "OferecedorMID")), RealStr(valorapagar));
		GivePlayerMoney(playerid, -valorapagar);
		call::PLAYER->SetPlayerVarInt(playerid, Maconha, call::PLAYER->GetPlayerVarInt(playerid, Maconha) + GetPVarInt(playerid, "MQuantidade"));

		DeletePVar(playerid, "OferecedorMID"), DeletePVar(playerid, "DinheiroMaconha"), DeletePVar(playerid, "MaconhaOferecida"), DeletePVar(playerid, "MQuantidade");
	}
	else if(!strcmp(Tipo, "cocaina", true))
	{
		if(GetPVarInt(playerid, "CocainaOferecida") < 1)
			return SendClientMessage(playerid, COR_ERRO, "Erro: N�o te ofereceram Cocaina.");

		new valorapagar = GetPVarInt(playerid, "DinheiroCocaina");

		SendClientMessage(GetPVarInt(playerid, "OferecedorCID"), COR_BRANCO, "* %s Aceitou sua Cocaina e voc� recebeu: {088A08}R$%s", GetUserName(playerid), RealStr(valorapagar));
		GivePlayerMoney(GetPVarInt(playerid, "OferecedorCID"), valorapagar);
		call::PLAYER->SetPlayerVarInt(GetPVarInt(playerid, "OferecedorCID"), Cocaina, call::PLAYER->GetPlayerVarInt(GetPVarInt(playerid, "OferecedorCID"), Cocaina) - GetPVarInt(playerid, "CQuantidade"));

		SendClientMessage(playerid, COR_AVISO, "* Voc� aceitou a Cocaina de %s e pagou o Valor de: {088A08}R$%s", GetUserName(GetPVarInt(playerid, "OferecedorCID")), RealStr(valorapagar));
		GivePlayerMoney(playerid, -valorapagar);
		call::PLAYER->SetPlayerVarInt(playerid, Cocaina, call::PLAYER->GetPlayerVarInt(playerid, Cocaina) + GetPVarInt(playerid, "CQuantidade"));

		DeletePVar(playerid, "OferecedorCID"), DeletePVar(playerid, "DinheiroCocaina"), DeletePVar(playerid, "CocainaOferecida"), DeletePVar(playerid, "CQuantidade");
	}
	else if(!strcmp(Tipo, "crack", true))
	{
		if(GetPVarInt(playerid, "CrackOferecida") < 1)
			return SendClientMessage(playerid, COR_ERRO, "Erro: N�o te ofereceram Crack.");

		new valorapagar = GetPVarInt(playerid, "DinheiroCrack");

		SendClientMessage(GetPVarInt(playerid, "OferecedorCraID"), COR_BRANCO, "* %s Aceitou seu Crack e voc� recebeu: {088A08}R$%s", GetUserName(playerid), RealStr(valorapagar));
		GivePlayerMoney(GetPVarInt(playerid, "OferecedorCraID"), valorapagar);
		call::PLAYER->SetPlayerVarInt(GetPVarInt(playerid, "OferecedorCraID"), Crack, call::PLAYER->GetPlayerVarInt(GetPVarInt(playerid, "OferecedorCraID"), Crack) - GetPVarInt(playerid, "CraQuantidade"));

		SendClientMessage(playerid, COR_AVISO, "* Voc� aceitou o Crack de %s e pagou o Valor de: {088A08}R$%s", GetUserName(GetPVarInt(playerid, "OferecedorCraID")), RealStr(valorapagar));
		GivePlayerMoney(playerid, -valorapagar);
		call::PLAYER->SetPlayerVarInt(playerid, Crack, call::PLAYER->GetPlayerVarInt(playerid, Crack) + GetPVarInt(playerid, "CraQuantidade"));

		DeletePVar(playerid, "OferecedorCraID"), DeletePVar(playerid, "DinheiroCrack"), DeletePVar(playerid, "CrackOferecida"), DeletePVar(playerid, "CraQuantidade");
	}
	return true;
}

CMD:recusar(playerid, params[])
{
	new Tipo[30];

	if(sscanf(params, "s[30]", Tipo))
			return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/recusar [servico/conserto/maconha/cocaina/crack]");

	if(!strcmp(Tipo, "conserto", true))
	{
		if(GetPVarInt(playerid, "MecSolicitado") < 1)
			return SendClientMessage(playerid, COR_ERRO, "Erro: N�o te ofereceram uma repara��o.");

		new mecanicoid = GetPVarInt(playerid, "MecanicoID"), vehicleid = Jogador[mecanicoid][ReparandoVeiculo];

		UpdateVehicleDoorsDamageStatus(vehicleid, State[vehicleid][Doors][0], State[vehicleid][Doors][1], State[vehicleid][Doors][2], State[vehicleid][Doors][3]);
		UpdateVehicleLightsDamageStatus(vehicleid, State[vehicleid][Lights][0], State[vehicleid][Lights][1], State[vehicleid][Lights][2], State[vehicleid][Lights][3]);
		UpdateVehicleTiresDamageStatus(vehicleid, State[vehicleid][Tires][0], State[vehicleid][Tires][1], State[vehicleid][Tires][2], State[vehicleid][Tires][3]);

		SendClientMessage(mecanicoid, COR_AVISO, "* %s Recusou o conserto do ve�culo.", GetUserName(playerid));

		SendClientMessage(playerid, COR_AVISO, "* Voc� recusou a repara��o de %s e seu carro voltou ao estado normal.", GetUserName(mecanicoid));

		call::MECANICO->CancelarServico(mecanicoid);
		DeletePVar(playerid, "MecanicoID"), DeletePVar(playerid, "DinheiroReparo"), DeletePVar(playerid, "MecSolicitado");
		return true;
	}
	else if(!strcmp(Tipo, "servico", true))
	{
		if(GetPVarInt(playerid, "AdvSolicitado") == 0) 
			return SendClientMessage(playerid, COR_ERRO, "Erro: Servi�os inexistentes.");

		SendClientMessage(GetPVarInt(playerid, "AdvogadoID"), COR_AVISO, "* O jogador {"COR_BRANCO_INC"}%s{"COR_AVISO_INC"} recusou seus servi�os.", GetUserName(playerid));
		DeletePVar(playerid, "AdvSolicitado"), DeletePVar(playerid, "AdvogadoID");
		return true;
	}
	else if(!strcmp(Tipo, "maconha", true))
	{
		if(GetPVarInt(playerid, "MaconhaOferecida") < 1)
			return SendClientMessage(playerid, COR_ERRO, "Erro: N�o te ofereceram Maconha.");

		SendClientMessage(GetPVarInt(playerid, "OferecedorID"), COR_VERMELHO, "* %s Recusou sua Maconha.", GetUserName(playerid));

		SendClientMessage(playerid, COR_VERMELHO, "* Voc� recusou a Maconha de %s.", GetUserName(GetPVarInt(playerid, "OferecedorID")));

		DeletePVar(playerid, "OferecedorMID"), DeletePVar(playerid, "DinheiroMaconha"), DeletePVar(playerid, "MaconhaOferecida"), DeletePVar(playerid, "MQuantidade");
	}
	else if(!strcmp(Tipo, "cocaina", true))
	{
		if(GetPVarInt(playerid, "CocainaOferecida") < 1)
			return SendClientMessage(playerid, COR_ERRO, "Erro: N�o te ofereceram Cocaina.");

		SendClientMessage(GetPVarInt(playerid, "OferecedorCID"), COR_VERMELHO, "* %s Recusou sua Cocaina.", GetUserName(playerid));

		SendClientMessage(playerid, COR_VERMELHO, "* Voc� recusou a Cocaina de %s.", GetUserName(GetPVarInt(playerid, "OferecedorCID")));

		DeletePVar(playerid, "OferecedorCID"), DeletePVar(playerid, "DinheiroCocaina"), DeletePVar(playerid, "CocainaOferecida"), DeletePVar(playerid, "CQuantidade");
	}
	else if(!strcmp(Tipo, "crack", true))
	{
		if(GetPVarInt(playerid, "CrackOferecida") < 1)
			return SendClientMessage(playerid, COR_ERRO, "Erro: N�o te ofereceram Crack.");

		SendClientMessage(GetPVarInt(playerid, "OferecedorCraID"), COR_VERMELHO, "* %s Recusou seu Crack.", GetUserName(playerid));

		SendClientMessage(playerid, COR_VERMELHO, "* Voc� recusou o Crack de %s.", GetUserName(GetPVarInt(playerid, "OferecedorCraID")));

		DeletePVar(playerid, "OferecedorCraID"), DeletePVar(playerid, "DinheiroCrack"), DeletePVar(playerid, "CrackOferecida"), DeletePVar(playerid, "CraQuantidade");
	}
	return true;
}


CMD:usar(playerid, params[])
{
	if ( isnull(params) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/usar [maconha/crack/cocaina]");

	if ( strcmp(params, "maconha", true) == 0 )
	{
		if ( !Jogador[playerid][Maconha] )
		{
			SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem maconha para fumar.");
			return 1;
		}

		static Float:health;
		GetPlayerHealth(playerid, health);
		if ( health < 100 )
			SetPlayerHealth(playerid, health + 3.5);

		--Jogador[playerid][Maconha];
		SendClientMessage(playerid, 0x009264FF, "* Voc� fumou um baseado.");
		return true;
	}
	else if ( strcmp(params, "cocaina", true) == 0 )
	{
		if ( !Jogador[playerid][Cocaina] )
		{
			SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem cocaina para cheirar.");
			return 0;
		}

		static Float:health, Float:armour;

		GetPlayerHealth(playerid, health);

		GetPlayerArmour(playerid, armour);
		
		if ( health < 100 )
			SetPlayerHealth(playerid, health + 5.0);

		if ( armour < 50 )
			SetPlayerArmour(playerid, armour + 3.0);

		SendClientMessage(playerid, 0x009264FF, "* Voc� cheirou sua cocaina.");
		--Jogador[playerid][Cocaina];
		return true;
	}
	else if ( strcmp(params, "crack", true) == 0 )
	{
		if ( !Jogador[playerid][Crack] )
		{
			SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem crack para fumar.");
			return 1;
		}

		static Float:armour;
		GetPlayerArmour(playerid, armour);

		if ( armour < 60 )
			SetPlayerArmour(playerid, armour + 10.0);

		--Jogador[playerid][Crack];
		SendClientMessage(playerid, 0x009264FF, "* Voc� fumou uma pedra de crack.");
		return true;
	}
	return true;
}


CMD:menubtc(playerid)
{
	ShowPlayerDialog(playerid, MENU_CASH, DIALOG_STYLE_TABLIST, "MENU BITCOINS", "Ativar BitCoins", "Confirmar", "Fechar");
	return true;
}
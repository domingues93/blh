#include <YSI_Coding\y_hooks>

hook OnGameModeInit()
{
	SetTimer("OnTimeOneSeconds", 1000, true); // Timer de um segundo.
}

hook OnTimeOneSecondsPlayer(playerid)
{
	if( call::PLAYER->IsPlayerLogged(playerid) )
	{
		if( !GetPlayerAFK(playerid) )
		{
			if( call::ADMIN->IsAdminInJob(playerid) )
			{
				++Jogador[playerid][TempoAtividade][SEGUNDOS];
				if(Jogador[playerid][TempoAtividade][SEGUNDOS] >= 60)
				{
					++Jogador[playerid][TempoAtividade][MINUTOS];
					Jogador[playerid][TempoAtividade][SEGUNDOS] = 0;

					if(Jogador[playerid][TempoAtividade][MINUTOS] >= 60)
						++Jogador[playerid][TempoAtividade][HORAS], Jogador[playerid][TempoAtividade][MINUTOS] = 0;
				}
			}
			if( IsPlayerInAnyVehicle(playerid) )
			{
				call::TD->UpdatePlayerVelocimetro(playerid);
			}

			/**
			*
			*	Liberar mercadoria ao passar 1 hora.
			*
			**/
			if (  call::PLAYER->GetPlayerVarInt(playerid, EmpresaID) != INVALID_BUSINESS_ID )
			{
				if ( Jogador[playerid][Mercadoria] > MERCADORIA_LIBERADA)
				{
					--Jogador[playerid][Mercadoria];
					
					if ( Jogador[playerid][Mercadoria] == MERCADORIA_LIBERADA ){
						SendClientMessage(playerid, 0x1AD096FF, "Chegou mais mercadorias em Bayside.");
					}
				}
			}

			if ( Jogador[playerid][Preso] == SOLTO )
			{
				--Jogador[playerid][UPs];
			}
			
			new actorid = GetPlayerTargetDynamicActor(playerid), empresaid = GetPlayerVirtualWorld(playerid);

			if(IsValidDynamicActor(actorid) && GetPlayerWeapon(playerid) >= 22)
			{
				if(actorid == 367 || actorid == 368)
				{
					if(Jogador[playerid][Profissao] != ASSALTANTE)
						return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o faz parte da profiss�o Assaltante.");

					if (!call::BUSINESS->IsValidBusiness(empresaid) )
						return false;

					if(call::BUSINESS->GetBusinessParamsInt(empresaid, Cofre) <= 0)
						return SendClientMessage(playerid, COR_ERRO, "Erro: Esse hotel n�o pode ser assaltado no momento.");

					OnPlayerTargetDynamicActor(playerid);
				}
			}

			if(Jogador[playerid][UPs] <= 0)
			{
				if(Jogador[playerid][UPm] <= 0)
				{
					Jogador[playerid][UPm] = 10;
					Jogador[playerid][UPs] = 0;
					
					++Jogador[playerid][EXP];
					SendClientMessage(playerid, COR_BRANCO, "� {"COR_VERDE_INC"}Voc� ganhou {"COR_BRANCO_INC"}+1{"COR_VERDE_INC"} ponto de experi�ncia, voc� acumulou %d/%d.", Jogador[playerid][EXP], call::PLAYER->requestXP(playerid));
					PlayerPlaySound(playerid, 1058, 0.0, 0.0, 0.0);

					if(Jogador[playerid][EXP] >= call::PLAYER->requestXP(playerid) )
					{
						Jogador[playerid][EXP] = 0;

						SetPlayerScore(playerid, GetPlayerScore(playerid) + 1);
						call::TD->UpdateTextDrawHudLevel(playerid);
						
						SendClientMessage(playerid, COR_BRANCO, "� {"COR_VERDE_INC"}Parab�ns voc� ganhou {"COR_BRANCO_INC"}+1{"COR_VERDE_INC"} de level por acumular todas as experi�ncia necess�rias.");
						PlayerPlaySound(playerid,1057,0.0,0.0,0.0);

						switch( GetPlayerScore(playerid) )
						{
							case 100:{
								GivePlayerMoney(playerid, 10000);
								SendClientMessage(playerid, COR_VERDE, "Info: Voc� recebeu +R$10.000 por chegar aos seus 100 leveis.");
							}
							case 250:{
								GivePlayerMoney(playerid, 20000);
								SendClientMessage(playerid, COR_VERDE, "Info: Voc� recebeu +R$20.000 por chegar aos seus 250 leveis.");
							}
							case 500:{
								GivePlayerMoney(playerid, 40000);
								SendClientMessage(playerid, COR_VERDE, "Info: Voc� recebeu +R$40.000 por chegar aos seus 500 leveis.");
							}
							case 1000:{
								GivePlayerMoney(playerid, 75000); // mais vip 30 dias vips
								SendClientMessage(playerid, COR_VERDE, "Info: Voc� recebeu +R$75.000 por chegar aos seus 1000 leveis.");
							}
							case 1500:{
								GivePlayerMoney(playerid, 125000); // mais vip 30 dias vips
								SendClientMessage(playerid, COR_VERDE, "Info: Voc� recebeu +R$125.000 por chegar aos seus 1500 leveis.");
							}
							case 2000:{
								GivePlayerMoney(playerid, 180000); // mais vip 30 dias vips
								SendClientMessage(playerid, COR_VERDE, "Info: Voc� recebeu +R$180.000 por chegar aos seus 2000 leveis.");
							}
						}
					}
					call::TD->UpdateTextDrawHudExp(playerid);
					
					new query[128];
					mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `exp`='%d',`level`='%d' WHERE `id`='%d'", Jogador[playerid][EXP], GetPlayerScore(playerid), call::PLAYER->GetPlayerVarInt(playerid, PlayerID));
					mysql_tquery(getConexao(), query);
				}
				else{
					--Jogador[playerid][UPm];
					Jogador[playerid][UPs] = 59;
				}
			}
			if(Jogador[playerid][Fome] > 100)
				Jogador[playerid][Fome] = 100;
			
			if(Jogador[playerid][Sede] > 100)
				Jogador[playerid][Sede] = 100;
			
			if(Jogador[playerid][Sono] > 100)
				Jogador[playerid][Sono] = 100;

			if(call::PLAYER->GetPlayerVarInt(playerid, pDormindo) == 0)
			{
				Jogador[playerid][Fome] = Jogador[playerid][Fome] - 0.009;
				Jogador[playerid][Sede] = Jogador[playerid][Sede] - 0.013;
				Jogador[playerid][Sono] = Jogador[playerid][Sono] - 0.005;

				if( floatround(call::PLAYER->GetPlayerVarFloat(playerid, Sono)) <= 10)
				{
					if( floatround(call::PLAYER->GetPlayerVarFloat(playerid, Sono)) == 10)
					{
						call::PLAYER->SetPlayerVarFloat(playerid, Sono, 9.0);
						call::PLAYER->InitPlayerFadeEffect(playerid);
						SendClientMessage(playerid, COR_ERRO, "Voc� est� ficando com sono.");
					}
					else if( floatround(call::PLAYER->GetPlayerVarFloat(playerid, Sono)) == 5)
					{
						call::PLAYER->SetPlayerVarFloat(playerid, Sono, 4.0);
						call::PLAYER->InitPlayerFadeEffect(playerid);
						SendClientMessage(playerid, COR_ERRO, "Voc� est� ficando com muito sono.");
					}
					else if(call::PLAYER->GetPlayerVarFloat(playerid, Sono) <= 0.000)
					{
						callcmd::dormir(playerid);
						SendClientMessage(playerid, COR_ERRO, "Voc� desmaiou de sono.");
					}
				}
			}
			else
			{
				static Float:health;
				GetPlayerHealth(playerid, health);
				if ( health <= 80 )
				{
					SetPlayerHealth(playerid, health + 0.03);
				}

				Jogador[playerid][Fome] = Jogador[playerid][Fome] - 0.015;
				Jogador[playerid][Sede] = Jogador[playerid][Sede] - 0.03;
				Jogador[playerid][Sono] = Jogador[playerid][Sono] + 0.5;
				
				if(Jogador[playerid][Sono] >= 100)
					callcmd::acordar(playerid), Jogador[playerid][Sono] = 100.0;
			}

			if(call::PLAYER->GetPlayerVarFloat(playerid, Sede) <= 0.0)
			{
				SetPlayerHealth(playerid, 0), call::PLAYER->SetPlayerVarFloat(playerid, Sede, 5.0);
				SendClientMessage(playerid, COR_ERRO, "Voc� desmaiou por desidrata��o, e foi levado para um hospital.");
			}

			if(call::PLAYER->GetPlayerVarFloat(playerid, Fome) <= 0.0)
			{
				SetPlayerHealth(playerid, 0), call::PLAYER->SetPlayerVarFloat(playerid, Fome, 5.0);
				SendClientMessage(playerid, COR_ERRO, "Voc� desmaiou de fome, e foi levado para um hospital.");
			}

			static hour, minute;
			gettime(hour, minute);
			switch(hour)
			{
				case 18:
					SetPlayerTime(playerid, hour+2, minute);
				case 19..22:
					SetPlayerTime(playerid, hour+4, minute);
				default:
					SetPlayerTime(playerid, hour, minute);
			}
			call::TD->UpdateTextDrawPlayerHud(playerid);

			if(call::ADMIN->GetPlayerAdminLevel(playerid) >= AJUDANTE && call::PLAYER->GetPlayerVarInt(playerid, pDormindo) == 0 && call::ADMIN->IsAdminInJob(playerid))
			{
				SetPlayerChatBubble(playerid, call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid), true), -1, 10.0, 1000);
			}
			else
			{
				if(call::PLAYER->GetPlayerVarInt(playerid, pDormindo))
					SetPlayerChatBubble(playerid, "zZzzZZZzz", COR_BRANCO, 10.0, 1000);

				else if( Jogador[playerid][Vip] )
					SetPlayerChatBubble(playerid, "VIP", Jogador[playerid][CorTag], 10.0, 1000);
			}
		}

		/*foreach(new i: Player)
		{
			CheckSoundProx(i);
			CheckCarSoundProx(i);
		}*/
		
		if(call::PLAYER->GetPlayerVarInt(playerid, ObjectAttached) < gettime())
		{
			if(IsPlayerAttachedObjectSlotUsed(playerid, 0))
			{
				RemovePlayerAttachedObject(playerid, 0);
			}
		}
		return true;
	}
	return false;
}

new static 
	horas, minutos, bool:verificado;

public OnTimeOneSeconds()
{
	call::TD->UpdateTextDrawHud();

	gettime(horas, minutos);
	if(horas == 5 && minutos == 0 && !verificado)
	{
		verificado = true;
		SetTimer("GerarImpostos", 3 * (60 * 1000), false);

		SendClientMessageToAll(-1, "~=~=~=~=~=~=~=~=~=~=~=~=~=~=~= [ SERVER ] ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=");
		SendClientMessageToAll(-1, "O Servidor se reiniciar� em aproximadamente {"COR_AZUL_INC"}3{"COR_BRANCO_INC"} minutos");
		SendClientMessageToAll(-1, "~=~=~=~=~=~=~=~=~=~=~=~=~=~=~= [ ] ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=");
		return true;
	}
	return true;
}

forward GerarImpostos();
public GerarImpostos()
{
	CleanChat();
	SendClientMessageToAll(-1, "Servidor reiniciando, voltaremos em breve!");
	
	// gerar impostos
	print("gerando impostos...");
	mysql_query_file(getConexao(), "querys/impostos.sql", false);

	// Excluir contas inativas
	print("Verificando contas inativas...");
	new Cache:cache = mysql_query(getConexao(), "DELETE FROM "TABLE_USERS" WHERE DATEDIFF(NOW(), `ultimo_login`) > 180;", true);
	printf("Foram deletadas %d conta(s).", cache_affected_rows());
	cache_delete(cache);


	// reiniciar servidor
	SendRconCommand(#gmx);

	// Fazer o backup
	//HTTP(0, HTTP_POST, "localhost/metropolitan/backup", "backup", "OnBackupReturn");
	return true;
}  

/*
forward OnBackupReturn(index, response_code, data[]); public OnBackupReturn(index, response_code, data[])
{
	if ( response_code == 200)
	{
		print("� Backup efetuado com �xito.");
		printf("data: %s", data);
	}

	new str[128];
	format(str, sizeof(str), "O Servidor foi reiniciado automaticamente pelo sistema.");
	WriteLog("shutdown-server.txt", str, sizeof(str));
	SendRconCommand(#gmx);
	return true;
}*/
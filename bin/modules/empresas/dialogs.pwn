Dialog:BUSINESS_MENU(playerid, response, listitem, inputtext[])
{
	if(response)
	{
		new businessid = call::BUSINESS->GetBusinessInRangeP(playerid, 5.0), pName[MAX_PLAYER_NAME];
		
		if(businessid == INVALID_BUSINESS_ID)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está proximo a uma empresa."), callcmd::menuempresa(playerid);
			
		if(businessid != call::PLAYER->GetPlayerVarInt(playerid, EmpresaID))
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está proximo a sua empresa."), callcmd::menuempresa(playerid);

		call::BUSINESS->GetBusinessParams(businessid, Proprietario, pName);
		if(strcmp(pName, GetUserName(playerid), true) != 0)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não é proprietário desta empresa."), callcmd::menuempresa(playerid);

		switch(listitem)
		{
			case 0: // Contratar
			{
				new info[512];
				info[0] = EOS;
				info = "Nome\tFunção\n";
				LoopFuncionarios(i, call::BUSINESS->GetBusinessParamsInt(businessid, MaxFuncionarios)){
					format(info, sizeof(info), "%s%s\tFuncionário %d\n", info, call::BUSINESS->GetBusinessFuncName(businessid, i), i);
				}
				ShowPlayerDialog(playerid, bMENU_CONTRATAR, DIALOG_STYLE_TABLIST_HEADERS, "{"COR_DISABLE_INC"}MENU EMPRESA » {"COR_BRANCO_INC"}CONTRATAR", info, "Contratar", "Voltar");
				return true;
			}
			case 1: // Demitir
			{
				new info[512];
				info[0] = EOS;
				info = "Nome\tFunção\n";
				LoopFuncionarios(i, call::BUSINESS->GetBusinessParamsInt(businessid, MaxFuncionarios)){
					format(info, sizeof(info), "%s%s\tFuncionário %d\n", info, call::BUSINESS->GetBusinessFuncName(businessid, i), i);
				}
				ShowPlayerDialog(playerid, bMENU_DEMITIR, DIALOG_STYLE_TABLIST_HEADERS, "{"COR_DISABLE_INC"}MENU EMPRESA » {"COR_BRANCO_INC"}DEMISSÃO", info, "Demitir", "Voltar");
				return true;
			}
			case 2: // Alterar Mensagem
			{
				ShowPlayerDialog(playerid, bMENU_ALTER_MSG, DIALOG_STYLE_INPUT, "{"COR_DISABLE_INC"}MENU EMPRESA » {"COR_BRANCO_INC"}ALTERAR MENSAGEM", "{"COR_BRANCO_INC"}Digite a mensagem que será mostrado para seus visitantes.", "Alterar", "Voltar");
				return true;
			}
			case 3: // Alterar Salário
			{
				new valorTotalEntrega = call::BUSINESS->GetBusinessInfoEntrega(businessid),
					valorMin = (valorTotalEntrega * 20 / 100),
					valorMax = (valorTotalEntrega * 50 / 100);
				ShowPlayerDialog(playerid, bMENU_ALTER_SALARIO, DIALOG_STYLE_INPUT, "{"COR_DISABLE_INC"}MENU EMPRESA » {"COR_BRANCO_INC"}ALTERAR SALÁRIO", 
					"{"COR_BRANCO_INC"}Valor total por cada entrega: {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"}\n\nVocê pode definir um salário entre {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"} á {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"} para seus funcionários.", "Alterar", "Voltar", RealStr(valorTotalEntrega), RealStr(valorMin), RealStr(valorMax));
				return true;
			}
			case 4: // cofre
			{
				ShowPlayerDialog(playerid, B_MENU_COFRE, DIALOG_STYLE_INPUT, "{"COR_BRANCO_INC"}COFRE EMPRESA",
					"{"COR_BRANCO_INC"}Sua empresa possui {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"} no cofre.\n\nDigite a quantidade que deseja sacar.", 
					"Sacar", "Fechar", RealStr( call::BUSINESS->GetBusinessParamsInt(businessid, Cofre) ));
				return true;
			}
			case 5: // Upgrade
			{
				ShowPlayerDialog(playerid, B_MENU_UPGRADE, DIALOG_STYLE_MSGBOX, "{"COR_DISABLE_INC"}MENU EMPRESA » {"COR_BRANCO_INC"}UPGRADE", 
					"É necessário ter {"COR_AMARELO_INC"}%d{"COR_SISTEMA_INC"} produções e {"COR_VERDE_INC"}R$%s{"COR_SISTEMA_INC"} no cofre para\n\nAtualizar a sua empresa.", 
					"Upgrade", "Voltar", call::BUSINESS->BusinessRequeryXpUp(playerid, businessid), RealStr(call::BUSINESS->BusinessRequeryMoneyUp(playerid, businessid)));
				return true;
			}
		}
		return true;
	}
	return true;
}

Dialog:bMENU_CONTRATAR(playerid, response, listitem, inputtext[])
{
	if(response)
	{
		new businessid = call::BUSINESS->GetBusinessInRangeP(playerid, 5.0), pName[MAX_PLAYER_NAME];
		
		if(businessid == INVALID_BUSINESS_ID)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está proximo a uma empresa."), callcmd::menuempresa(playerid);
			
		if(businessid != call::PLAYER->GetPlayerVarInt(playerid, EmpresaID))
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está proximo a sua empresa."), callcmd::menuempresa(playerid);

		call::BUSINESS->GetBusinessParams(businessid, Proprietario, pName);
		if(strcmp(pName, GetUserName(playerid), true) != 0)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não é proprietário desta empresa."), callcmd::menuempresa(playerid);

		if(strcmp(call::BUSINESS->GetBusinessFuncName(businessid, listitem + 1), "Ninguem", true) != 0)
			return callcmd::menuempresa(playerid);

		SetPVarInt(playerid, "business_vagaid", listitem + 1);
		ShowPlayerDialog(playerid, bMENU_CONTRATAR_ID, DIALOG_STYLE_INPUT, "{"COR_DISABLE_INC"}MENU EMPRESA » CONTRATAR » {"COR_BRANCO_INC"}ID", "{"COR_BRANCO_INC"}Digite o id do jogador que queira contratar para sua empresa.\n\n\t{"COR_AVISO_INC"}Aviso: O Jogador tem que está a 5 metros de você.", "Contratar", "Cancelar");
		return true;
	}
	callcmd::menuempresa(playerid);
	return true;
}


Dialog:bMENU_CONTRATAR_ID(playerid, response, listitem, inputtext[])
{
	if(response)
	{
		new funcid = GetPVarInt(playerid, "business_vagaid");

		if(isnull(inputtext))
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você deve digitar um id válido."), callcmd::menuempresa(playerid);

		new id = strval(inputtext), businessid = call::BUSINESS->GetBusinessInRangeP(playerid, 5.0), pName[MAX_PLAYER_NAME];

		if ( id == playerid )
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não pode contratar você mesmo."), callcmd::menuempresa(playerid);

		if(!IsPlayerConnected(id) || !call::PLAYER->IsPlayerLogged(id))
			return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) não está conectado(a)."), callcmd::menuempresa(playerid);
		
		if(businessid == INVALID_BUSINESS_ID)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está proximo a uma empresa."), callcmd::menuempresa(playerid);
			
		if(businessid != call::PLAYER->GetPlayerVarInt(playerid, EmpresaID))
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está proximo a sua empresa."), callcmd::menuempresa(playerid);

		call::BUSINESS->GetBusinessParams(businessid, Proprietario, pName);
		if(strcmp(pName, GetUserName(playerid), true) != 0)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não é proprietário desta empresa."), callcmd::menuempresa(playerid);

		if(GetPVarInt(id, "business_time") != 0)
			return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) já recebeu uma proposta por favor aguarde."),callcmd::menuempresa(playerid);

		new Float:x, Float:y, Float:z;
		GetPlayerPos(id, x, y, z);
		if( !IsPlayerInRangeOfPoint(playerid, 5.0, x, y, z) )
			return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) não está proximo de você."), callcmd::menuempresa(playerid);

		if(call::PLAYER->GetPlayerVarInt(id, EmpresaID) != INVALID_BUSINESS_ID)
			return SendClientMessage(playerid, COR_ERRO, "Erro: O(A) Jogador(a) já está trabalhando em uma empresa ou possui uma."), callcmd::menuempresa(playerid);

		SetPVarInt(id, "businessid", businessid);
		SetPVarInt(id, "business_owner", playerid);
		SetPVarInt(id, "business_vagaid", funcid);
		SetPVarInt(id, "business_time", SetTimerEx("FecharContrato", 15000, false, "i", id));

		new info[1024];
		format(info, sizeof(info), "{"COR_BRANCO_INC"}O(A) Jogador(a) {"COR_AVISO_INC"}%s{"COR_BRANCO_INC"} deseja contratar você para trabalhar na empresa {"COR_AVISO_INC"}%s{"COR_BRANCO_INC"}.\n\n", GetUserName(playerid));
		format(info, sizeof(info), "%s_____________________________________________| PROPROSTA |_____________________________________________\n", info);
		format(info, sizeof(info), "%sEmpresa: {"COR_AVISO_INC"}%s{"COR_BRANCO_INC"}\nFunção: {"COR_AVISO_INC"}Funcionário %d{"COR_BRANCO_INC"}\nSalário: {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"}", info, call::BUSINESS->GetBusinessName(businessid), funcid, RealStr(call::BUSINESS->GetBusinessParamsInt(businessid, Salario)));
		ShowPlayerDialog(id, PLAYER_ACCEPT_JOB, DIALOG_STYLE_MSGBOX, "CONTRATO DE TRABALHO", info, "Aceitar", "Recusar");
		
		SendClientMessage(playerid, COR_AVISO, "Contrato: O Contrato foi oferecido para {"COR_BRANCO_INC"}%s{"COR_AVISO_INC"}, aguarde o mesmo responder.", GetUserName(id));
		return true;
	}
	callcmd::menuempresa(playerid);
	return true;
}

Dialog:PLAYER_ACCEPT_JOB(playerid, response, listitem, inputtext[])
{
	if(response)
	{
		new funcid = GetPVarInt(playerid, "business_vagaid"), id = GetPVarInt(playerid, "business_owner"), businessid = GetPVarInt(playerid, "businessid");
		DeletePVar(playerid, "business_vagaid"), DeletePVar(playerid, "business_owner"), DeletePVar(playerid, "businessid");

		if(GetPVarInt(playerid, "business_time") != 0)
			KillTimer(GetPVarInt(playerid, "business_time")), DeletePVar(playerid, "business_time");

		if(strcmp(call::BUSINESS->GetBusinessFuncName(businessid, funcid), "Ninguem", true) != 0)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Está vaga já foi preenchida"), SendClientMessage(id, COR_ERRO, "Erro: Não foi possivel contratar o(a) jogador(a)."), callcmd::menuempresa(id);

		call::BUSINESS->SetBusinessFuncName(businessid, funcid, GetUserName(playerid));
		Jogador[playerid][FuncionarioID] = funcid;
		call::PLAYER->SetPlayerVarInt(playerid, EmpresaID, businessid);
		
		SendClientMessage(playerid, COR_VERDE, "» {"COR_BRANCO_INC"}Agora você está trabalhando na empresa {"COR_VERDE_INC"}%s", call::BUSINESS->GetBusinessName(businessid));
		SendClientMessage(id, COR_VERDE, "» {"COR_BRANCO_INC"}O(A) Jogador(a) {"COR_VERDE_INC"}%s{"COR_BRANCO_INC"} aceitou trabalhar na sua empresa.", GetUserName(playerid));

		new query2[200];
		mysql_format(getConexao(), query2, sizeof(query2), "UPDATE "TABLE_USERS" SET `empresaid`='%d' WHERE `id`='%d' LIMIT 1;", businessid, Jogador[playerid][PlayerID]);
		mysql_tquery(getConexao(), query2);

		new query[200];
		mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_BUSINESS" SET `funcionario_%d`='%s',`entregas_funcionario_%d`='0' WHERE `id`='%d' LIMIT 1;", funcid, GetUserName(playerid), funcid, call::BUSINESS->GetBusinessColumID(businessid));
		mysql_tquery(getConexao(), query);
		call::BUSINESS->UpdateBusinessTexts(businessid);
		return true;
	}
	return true;
}

Dialog:bMENU_DEMITIR(playerid, response, listitem, inputtext[])
{
	if(response)
	{
		new query[200], businessid = call::BUSINESS->GetBusinessInRangeP(playerid, 5.0), funcid = listitem + 1, pName[MAX_PLAYER_NAME];

		if(businessid == INVALID_BUSINESS_ID)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está proximo a uma empresa."), callcmd::menuempresa(playerid);
			
		if(businessid != call::PLAYER->GetPlayerVarInt(playerid, EmpresaID))
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está proximo a sua empresa."), callcmd::menuempresa(playerid);

		call::BUSINESS->GetBusinessParams(businessid, Proprietario, pName);
		if(strcmp(pName, GetUserName(playerid), true) != 0)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não é proprietário desta empresa."), callcmd::menuempresa(playerid);

		new FuncionarioName[MAX_PLAYER_NAME];
		format(FuncionarioName, sizeof(FuncionarioName), call::BUSINESS->GetBusinessFuncName(businessid, funcid));
		
		if(strcmp(FuncionarioName, "Ninguem", true) == 0)
			return callcmd::menuempresa(playerid);

		new id = GetPlayerIDByName(FuncionarioName);
		if(id != INVALID_PLAYER_ID)
		{
			call::PLAYER->SetPlayerVarInt(id, FuncionarioID, 0);
			call::PLAYER->SetPlayerVarInt(id, EmpresaID, INVALID_BUSINESS_ID);
			SendClientMessage(id, COR_AVISO, "Aviso: Você foi demitido de suas funções na empresa {"COR_BRANCO_INC"}%s", call::BUSINESS->GetBusinessName(businessid));
		}
		
		SendClientMessage(playerid, COR_SISTEMA, "» {"COR_BRANCO_INC"}Você demitiu o funcionário {"COR_AVISO_INC"}%s{"COR_BRANCO_INC"} de sua empresa.", FuncionarioName);
		
		call::BUSINESS->SetBusinessFuncName(businessid, funcid, "Ninguem");
		call::BUSINESS->SetFuncionarioEntrega(businessid, funcid, 0);
		
		mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_BUSINESS" SET `funcionario_%d`='Ninguem',`entregas_funcionario_%d`='0' WHERE `id`='%d' LIMIT 1;", funcid, funcid, call::BUSINESS->GetBusinessColumID(businessid));
		mysql_tquery(getConexao(), query);
		call::BUSINESS->UpdateBusinessTexts(businessid);
	}
	callcmd::menuempresa(playerid);
	return true;
}

Dialog:bMENU_ALTER_MSG(playerid, response, listitem, inputtext[])
{
	new businessid = call::BUSINESS->GetBusinessInRangeP(playerid, 5.0), pName[MAX_PLAYER_NAME];

	if(businessid != call::PLAYER->GetPlayerVarInt(playerid, EmpresaID))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está proximo a sua empresa."), callcmd::menuempresa(playerid);

	call::BUSINESS->GetBusinessParams(businessid, Proprietario, pName);
	if(strcmp(pName, GetUserName(playerid), true) != 0)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não é proprietário desta empresa."), callcmd::menuempresa(playerid);

	if(response)
	{
		if(strlen(inputtext) < 3 || strlen(inputtext) > MAX_STRLEN_MSG || isnull(inputtext))
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você deve digitar uma mensagem entre 3 á "#MAX_STRLEN_MSG" caracteres."); // "

		new mensagem[MAX_STRLEN_MSG];
		
		format(mensagem, sizeof(mensagem), str_replace("'", "", inputtext)); // replace symbol '
		format(mensagem, sizeof(mensagem), str_replace("`", "", inputtext)); // replace symbol `

		call::BUSINESS->SetBusinessParams(businessid, Mensagem, mensagem);
		SendClientMessage(playerid, COR_SISTEMA, "Empresa: {"COR_BRANCO_INC"}Você alterou a mensagem da sua empresa.");
		
		new query[128];
		mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_BUSINESS" SET `mensagem`='%e' WHERE `id`='%d' LIMIT 1;", mensagem, call::BUSINESS->GetBusinessColumID(businessid));
		mysql_tquery(getConexao(), query);

		callcmd::menuempresa(playerid);
		return true;
	}
	callcmd::menuempresa(playerid);
	return true;
}


Dialog:bMENU_ALTER_SALARIO(playerid, response, listitem, inputtext[])
{
	if(response)
	{
		new businessid = call::BUSINESS->GetBusinessInRangeP(playerid, 5.0), pName[MAX_PLAYER_NAME];

		if(businessid != call::PLAYER->GetPlayerVarInt(playerid, EmpresaID))
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está proximo a sua empresa."), callcmd::menuempresa(playerid);

		call::BUSINESS->GetBusinessParams(businessid, Proprietario, pName);
		if(strcmp(pName, GetUserName(playerid), true) != 0)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não é proprietário desta empresa."), callcmd::menuempresa(playerid);

		if(isnull(inputtext) || !IsNumeric(inputtext))
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você tem que digitar um valor válido."), callcmd::menuempresa(playerid);

		new valorTotalEntrega = call::BUSINESS->GetBusinessInfoEntrega(businessid),
		valorMin = (valorTotalEntrega * 20 / 100),
		valorMax = (valorTotalEntrega * 50 / 100);
		
		new valor = strval(inputtext);
		if(valor < valorMin || valor > valorMax)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você tem que colocar um valor entre {"COR_VERDE_INC"}R$%s{"COR_ERRO_INC"} e {"COR_VERDE_INC"}R$%s{"COR_ERRO_INC"}.", RealStr(valorMin), RealStr(valorMax)), callcmd::menuempresa(playerid);
		
		call::BUSINESS->SetBusinessParamsInt(businessid, Salario, valor);
		
		new query[255];
		mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_BUSINESS" SET `salario`='%d' WHERE `id`='%d' LIMIT 1;", valor, call::BUSINESS->GetBusinessColumID(businessid));
		mysql_tquery(getConexao(), query);

		SendClientMessage(playerid, COR_SISTEMA, "Empresa: {"COR_BRANCO_INC"}Você definiu o salário dos funcionários para {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"}.", RealStr(valor));
		callcmd::menuempresa(playerid);
		return true;
	}
	callcmd::menuempresa(playerid);
	return true;
}

Dialog:bMENU_BUY_BUSINESS(playerid, response, listitem, inputtext[])
{
	if(response)
	{
		if(call::PLAYER->GetPlayerVarInt(playerid, EmpresaID) != INVALID_BUSINESS_ID)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você já possui uma empresa ou trabalha em uma.");

		new businessid = call::BUSINESS->GetBusinessInRangeP(playerid, 2.0);
		
		if(businessid == INVALID_BUSINESS_ID)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está proximo de uma empresa.");

		if(call::BUSINESS->GetBusinessParamsInt(businessid, ValorVenda) > 0)
		{
			return true;
		}
		else // Sem dono
		{
			if(GetPlayerMoney(playerid) < call::BUSINESS->GetBusinessParamsInt(businessid, Valor))
				return SendClientMessage(playerid, COR_ERRO, "Erro: Você não tem dinheiro para comprar está empresa.");

			GivePlayerMoney(playerid, -call::BUSINESS->GetBusinessParamsInt(businessid, Valor));
			call::BUSINESS->SetBusinessParams(businessid, Proprietario, GetUserName(playerid));
			call::BUSINESS->UpdateBusinessTexts(businessid);
			call::BUSINESS->SetBusinessParamsInt(businessid, Cofre, 0);

			call::PLAYER->SetPlayerVarInt(playerid, EmpresaID, businessid);

			new query[200];
			mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_BUSINESS" SET `proprietario`='%s',`cofre`='0' WHERE `id`='%d' LIMIT 1;", GetUserName(playerid), call::BUSINESS->GetBusinessColumID(businessid));
			mysql_tquery(getConexao(), query);

			mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET empresaid='%d' WHERE id='%d';", call::BUSINESS->GetBusinessParamsInt(businessid, ID), Jogador[playerid][PlayerID]);
			mysql_tquery(getConexao(), query);

			SendClientMessage(playerid, COR_SISTEMA, "Empresa: {"COR_BRANCO_INC"}Você comprou a empresa {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"} localizada em {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}.", call::BUSINESS->GetBusinessName(businessid), call::BUSINESS->GetBusinessLocalName(businessid));
			SendClientMessageToAll(COR_SISTEMA, "» {"COR_BRANCO_INC"}O(A) Jogador(a) {"COR_VERMELHO_INC"}%s{"COR_BRANCO_INC"} comprou a empresa id {"COR_AZUL_INC"}%d{"COR_BRANCO_INC"} localizada em {"COR_AMARELO_INC"}%s{"COR_BRANCO_INC"}.", GetUserName(playerid), businessid, call::BUSINESS->GetBusinessLocalName(businessid));
			return true;
		}
	}
	return true;
}


Dialog:bMENU_VENDA_BUSINESS(playerid, response, listitem, inputtext[])
{
	if(response)
	{
		new businessid = call::BUSINESS->GetBusinessInRangeP(playerid, 2.0), pName[MAX_PLAYER_NAME];
		if(businessid == INVALID_BUSINESS_ID)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está proximo de uma empresa.");

		if( businessid == Jogador[playerid][EmpresaID] )
		{
			call::BUSINESS->GetBusinessParams(businessid, Proprietario, pName);
			if(strcmp(pName, GetUserName(playerid), true) != 0)
				return SendClientMessage(playerid, COR_ERRO, "Erro: Você não é proprietário desta empresa.");

			new valorEmpresa = RETURN_VALOR_VENDA(businessid), cofre = call::BUSINESS->GetBusinessParamsInt(businessid, Cofre);
			SendClientMessage(playerid, COR_SISTEMA, "Empresa: {"COR_BRANCO_INC"}Valor da Empresa: {"COR_VERDE_INC"}R$%s", RealStr(valorEmpresa));
			SendClientMessage(playerid, COR_SISTEMA, "Empresa: {"COR_BRANCO_INC"}Valor no cofre: {"COR_VERDE_INC"}R$%s", RealStr(cofre));
			SendClientMessage(playerid, COR_SISTEMA, "Empresa: {"COR_BRANCO_INC"}Você recebeu: {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"} no total.", RealStr(valorEmpresa + cofre));
			GivePlayerMoney(playerid, (valorEmpresa + cofre));

			SendClientMessageToAll(COR_AZUL, "* {"COR_BRANCO_INC"}O(A) Jogador(a) {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"} vendeu sua empresa id {"COR_VERDE_INC"}%d{"COR_BRANCO_INC"} localizada em %s", GetUserName(playerid), businessid, call::BUSINESS->GetBusinessLocalName(businessid));

			call::BUSINESS->VenderBusiness(businessid, "Ninguem", true);
			Jogador[playerid][EmpresaID] = 0;
			
			new query[250];
			mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET empresaid='0' WHERE id='%d' LIMIT 1;", Jogador[playerid][PlayerID]);
			mysql_tquery(getConexao(), query);
			return true;
		}
		return true;
	}
	return true;
}

Dialog:B_MENU_COFRE(playerid, response, listitem, inputtext[])
{
	if ( response )
	{
		if ( !IsNumeric(inputtext) || isnull(inputtext) )
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você tem que digitar um valor númerico."), callcmd::menuempresa(playerid);

		new businessid = call::BUSINESS->GetBusinessInRangeP(playerid, 2.0), valor = strval(inputtext);

		if( businessid == INVALID_BUSINESS_ID )
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está proximo a uma empresa."), callcmd::menuempresa(playerid);
		
		new pName[MAX_PLAYER_NAME];
		call::BUSINESS->GetBusinessParams(businessid, Proprietario, pName);
		if ( strcmp(pName, GetUserName(playerid), true) != 0 )
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não é proprietário desta empresa.");

		if ( call::BUSINESS->GetBusinessParamsInt(businessid, Cofre) < valor)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Sua empresa não contém essa quantia no cofre."), callcmd::menuempresa(playerid);

		call::BUSINESS->SetBusinessParamsInt(businessid, Cofre, call::BUSINESS->GetBusinessParamsInt(businessid, Cofre) - valor);
		GivePlayerMoney(playerid, valor);
		SendClientMessage(playerid, COR_BRANCO, "Você sacou {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"} do cofre de sua empresa.", RealStr(valor) );
		return true;
	}
	callcmd::menuempresa(playerid);
	return true;
}

Dialog:B_MENU_UPGRADE(playerid, response, listitem, inputtext[])
{
	if ( response )
	{
		new businessid = call::BUSINESS->GetBusinessInRangeP(playerid, 2.0);
		
		if(businessid == INVALID_BUSINESS_ID)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está proximo a uma empresa."), callcmd::menuempresa(playerid);
		
		new pName[MAX_PLAYER_NAME];
		call::BUSINESS->GetBusinessParams(businessid, Proprietario, pName);
		if (strcmp(pName, GetUserName(playerid), true) != 0)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não é proprietário desta empresa.");

		if(businessid != call::PLAYER->GetPlayerVarInt(playerid, EmpresaID))
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não está proximo a sua empresa."), callcmd::menuempresa(playerid);

		if ( call::BUSINESS->GetBusinessParamsInt(businessid, Producao) < call::BUSINESS->BusinessRequeryXpUp(playerid, businessid))
			return SendClientMessage(playerid, COR_ERRO, "Erro: Sua empresa não tem produções suficientes para atualizar."), callcmd::menuempresa(playerid);

		if ( call::BUSINESS->GetBusinessParamsInt(businessid, Impostos) > 0)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Você não pode atualizar sua empresa enquanto não pagar os impostos."), callcmd::menuempresa(playerid);

		if ( call::BUSINESS->GetBusinessParamsInt(businessid, Cofre) < call::BUSINESS->BusinessRequeryMoneyUp(playerid, businessid))
			return SendClientMessage(playerid, COR_ERRO, "Erro: Sua empresa não tem dinheiro no cofre suficiente para atualizar."), callcmd::menuempresa(playerid);

		call::BUSINESS->SetBusinessParamsInt(businessid, Level, call::BUSINESS->GetBusinessParamsInt(businessid, Level) + 1);
		call::BUSINESS->SetBusinessParamsInt(businessid, Producao, -call::BUSINESS->BusinessRequeryXpUp(playerid, businessid));
		call::BUSINESS->SetBusinessParamsInt(businessid, Cofre, call::BUSINESS->GetBusinessParamsInt(businessid, Cofre) -call::BUSINESS->BusinessRequeryMoneyUp(playerid, businessid));
		call::BUSINESS->SetBusinessParamsInt(businessid, MaxFuncionarios, call::BUSINESS->GetBusinessParamsInt(businessid, MaxFuncionarios) +1);

		new query[128];
		mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_BUSINESS" SET `level`='%d',`producao`='%d',`cofre`='%d',`max-funcionarios`='%d' WHERE `id`='%d'", call::BUSINESS->GetBusinessParamsInt(businessid, Level), call::BUSINESS->GetBusinessParamsInt(businessid, Producao), call::BUSINESS->BusinessRequeryMoneyUp(playerid, businessid), call::BUSINESS->GetBusinessParamsInt(businessid, MaxFuncionarios), call::BUSINESS->GetBusinessColumID(businessid));
		mysql_tquery(getConexao(), query);

		SendClientMessage(playerid, COR_AZUL, "» {"COR_BRANCO_INC"}Você upou sua empresa para o level {"COR_AZUL_INC"}%d{"COR_BRANCO_INC"}.", call::BUSINESS->GetBusinessParamsInt(businessid, Level));
		return true;
	}
	callcmd::menuempresa(playerid);
	return true;
}
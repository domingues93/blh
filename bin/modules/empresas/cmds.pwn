CMD:pegarmercadoria(playerid)
{
	if( IsPlayerInDynamicCP(playerid, CP_BAYSIDE) )
	{
		if(call::PLAYER->GetPlayerVarInt(playerid, EmpresaID) == INVALID_BUSINESS_ID)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o trabalha em nenhuma empresa.");
		
		if ( Jogador[playerid][Mercadoria] > MERCADORIA_LIBERADA)
		{
			new horas, minutos, segundos;
			formatSeconds(Jogador[playerid][Mercadoria], horas, minutos, segundos);
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� precisa esperar {"COR_BRANCO_INC"}%d:%d:%d{"COR_ERRO_INC"} para pegar mais mercadoria.",  horas, minutos, segundos);
		}
			
		if ( Jogador[playerid][Mercadoria] == MERCADORIA_CARREGADA )
			return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� j� est� com a mercadoria, entregue a em sua empresa.");

		if( Jogador[playerid][Mercadoria] == MERCADORIA_LIBERADA )
		{
			Jogador[playerid][Mercadoria] = MERCADORIA_CARREGADA;
			SendClientMessage(playerid, COR_VERDE, "Voc� pegou a mercadoria, agora leve at� a empresa que voc� trabalha.");
			return true;
		}
		SendClientMessage(playerid, COR_ERRO, "Erro: Voc� precisa esperar at� o proximo up para pegar mais mercadorias.");
		return true;
	}
	SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� no Checkpoint em bayside.");
	return true;
}

CMD:entregarmercadoria(playerid)
{
	new businessid = call::BUSINESS->GetBusinessInRangeP(playerid, 3.0);
	
	if(businessid == INVALID_BUSINESS_ID)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� proximo a uma empresa.");

	if(businessid != call::PLAYER->GetPlayerVarInt(playerid, EmpresaID))
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o trabalha nest� empresa.");

	if( Jogador[playerid][Mercadoria] == MERCADORIA_CARREGADA)
	{
		Jogador[playerid][Mercadoria] = MERCADORIA_TEMPO;
		new funcid = Jogador[playerid][FuncionarioID], valorEntrega = call::BUSINESS->GetBusinessInfoEntrega(businessid);

		call::BUSINESS->SetBusinessParamsInt(businessid, Producao, call::BUSINESS->GetBusinessParamsInt(businessid, Producao) + 1);
		
		call::BUSINESS->SetBusinessParamsInt(businessid, Cofre, call::BUSINESS->GetBusinessParamsInt(businessid, Cofre) + (valorEntrega - call::BUSINESS->GetBusinessParamsInt(businessid, Salario)));
		GivePlayerMoney(playerid, call::BUSINESS->GetBusinessParamsInt(businessid, Salario));

		call::BUSINESS->UpdateBusinessTexts(businessid);
		
		SendClientMessage(playerid, COR_SISTEMA, "Empresa: {"COR_BRANCO_INC"}Voc� recebeu {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"} pela entrega.", RealStr(call::BUSINESS->GetBusinessParamsInt(businessid, Salario)));
		SendClientMessage(playerid, COR_SISTEMA, "Empresa: {"COR_BRANCO_INC"}Voc� efetuou {"COR_VERDE_INC"}+1{"COR_BRANCO_INC"} entrega.");
		
		new pName[MAX_PLAYER_NAME], query[255];
		call::BUSINESS->GetBusinessParams(businessid, Proprietario, pName);
		if ( strcmp(pName, GetUserName(playerid), true) == 0 )
		{
			mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_BUSINESS" SET `producao`='%d',`cofre`='%d' WHERE `id`='%d'", call::BUSINESS->GetBusinessParamsInt(businessid, Producao), call::BUSINESS->GetBusinessParamsInt(businessid, Cofre), call::BUSINESS->GetBusinessColumID(businessid));
			mysql_tquery(getConexao(), query);
		}
		else
		{
			call::BUSINESS->SetFuncionarioEntrega(businessid, funcid, call::BUSINESS->GetFuncionarioEntrega(businessid, funcid) + 1);
			
			mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_BUSINESS" SET `producao`='%d',`entregas_funcionario_%d`='%d',`cofre`='%d' WHERE `id`='%d'", call::BUSINESS->GetBusinessParamsInt(businessid, Producao), funcid, call::BUSINESS->GetFuncionarioEntrega(businessid, funcid), call::BUSINESS->GetBusinessParamsInt(businessid, Cofre), call::BUSINESS->GetBusinessColumID(businessid));
			mysql_tquery(getConexao(), query);
		}

		query[0] = EOS;
		mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_USERS" SET `mercadoria`='%d' WHERE `id`='%d' LIMIT 1;", Jogador[playerid][Mercadoria], Jogador[playerid][PlayerID]);
		mysql_tquery(getConexao(), query);
		
		return true;
	}
	SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� com a mercadoria.");
	return true;
}

CMD:infoempresa(playerid)
{
	new businessid = call::BUSINESS->GetBusinessInRangeP(playerid, 2.0);
	if(businessid != INVALID_BUSINESS_ID)
	{
		call::BUSINESS->ShowPlayerBusinessInfo(playerid, businessid);
		return true;
	}
	return true;
}

CMD:menuempresa(playerid)
{
	new businessid = call::BUSINESS->GetBusinessInRangeP(playerid, 2.0);
	if(businessid != INVALID_BUSINESS_ID)
	{
		call::BUSINESS->ShowPlayerBusinessMenu(playerid, businessid);
		return true;
	}
	return true;
}

CMD:comprarempresa(playerid)
{
	if(GetPlayerScore(playerid) < 80) 
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem level suficiente para comprar uma empresa.");

	if(call::PLAYER->GetPlayerVarInt(playerid, EmpresaID) != INVALID_BUSINESS_ID)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� j� possui uma empresa ou trabalha em uma.");

	new businessid = call::BUSINESS->GetBusinessInRangeP(playerid, 2.0), pName[MAX_PLAYER_NAME];
	if(businessid != INVALID_BUSINESS_ID)
	{
		call::BUSINESS->GetBusinessParams(businessid, Proprietario, pName);
		if(call::BUSINESS->GetBusinessParamsInt(businessid, ValorVenda) <= 0 && strcmp(pName, "Ninguem", true) != 0)
			return SendClientMessage(playerid, COR_ERRO, "Erro: Est� empresa n�o est� a venda.");
		
		if(GetPlayerMoney(playerid) < call::BUSINESS->GetBusinessParamsInt(businessid, Valor))
				return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o tem dinheiro para comprar est� empresa.");

		ShowPlayerDialog(playerid, bMENU_BUY_BUSINESS, DIALOG_STYLE_MSGBOX, "COMPRAR EMPRESA", "{"COR_BRANCO_INC"}Empresa: {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"}\nEnd {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"}, N� {"COR_AZUL_INC"}%d{"COR_BRANCO_INC"}\n\nValor: {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"}\n\n\t\tVoc� deseja comprar est� empresa?", "Comprar", "Sair", pName, call::BUSINESS->GetBusinessLocalName(businessid), businessid, RealStr(call::BUSINESS->GetBusinessParamsInt(businessid, Valor)));
		return true;
	}
	return true;
}


CMD:venderempresa(playerid, params[])
{
	if(call::PLAYER->GetPlayerVarInt(playerid, EmpresaID) == INVALID_BUSINESS_ID)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o possui uma empresa.");

	new businessid = call::BUSINESS->GetBusinessInRangeP(playerid, 2.0), pName[MAX_PLAYER_NAME];

	if(call::BUSINESS->GetBusinessParamsInt(businessid, Impostos) >= 1)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Sua empresa tem impostos para pagar.");

	if(businessid == INVALID_BUSINESS_ID)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� proximo a nenhuma empresa.");

	if(businessid == call::PLAYER->GetPlayerVarInt(playerid, EmpresaID))
	{
		call::BUSINESS->GetBusinessParams(businessid, Proprietario, pName);
		if(strcmp(pName, GetUserName(playerid), true) == 0)
		{
			new valor = RETURN_VALOR_VENDA(businessid);
			ShowPlayerDialog(playerid, bMENU_VENDA_BUSINESS, DIALOG_STYLE_MSGBOX, "VENDER EMPRESA", "{"COR_BRANCO_INC"}Voc� quer realmente vender sua empresa?\n\nVoc� ira receber {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"} pela venda.\n\n\t{"COR_ERRO_INC"}Essa a��o � permanente e irrevers�vel.", "Vender", "Sair", RealStr(valor));
			return true;
		}
		SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o � propriet�rio desta empresa.");
		return true;
	}
	return true;
}

CMD:demissao(playerid)
{
	if ( call::PLAYER->GetPlayerVarInt(playerid, EmpresaID) == INVALID_BUSINESS_ID )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o trabalha em nenhuma empresa.");

	new empresaid = call::BUSINESS->GetBusinessInRangeP(playerid, 2.0);
	if ( empresaid == INVALID_BUSINESS_ID )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o est� proximo a nenhuma empresa.");

	new pNome[MAX_PLAYER_NAME];
	call::BUSINESS->GetBusinessParams(empresaid, Proprietario, pNome, MAX_PLAYER_NAME);

	if ( strcmp(pNome, GetUserName(playerid), true) == 0 || isnull((pNome)) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� � o propriet�rio desta empresa.");

	if ( empresaid != call::PLAYER->GetPlayerVarInt(playerid, EmpresaID) )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o trabalha nessa empresa.");

	
	new query[200], funcid = call::PLAYER->GetPlayerVarInt(playerid, FuncionarioID);
	mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_BUSINESS" SET `funcionario_%d`='Ninguem',`entregas_funcionario_%d`='0' WHERE `id`='%d' LIMIT 1;", funcid, funcid, call::BUSINESS->GetBusinessColumID(empresaid));
	mysql_tquery(getConexao(), query);
	
	SendClientMessage(playerid, COR_VERDE, "� {"COR_BRANCO_INC"}Voc� n�o trabalha mais na empresa {"COR_VERDE_INC"}%s", call::BUSINESS->GetBusinessName(empresaid));

	call::BUSINESS->SetBusinessFuncName(empresaid, funcid, "Ninguem");
	call::BUSINESS->SetFuncionarioEntrega(empresaid, funcid, 0);
	
	call::PLAYER->SetPlayerVarInt(playerid, FuncionarioID, 0);
	call::PLAYER->SetPlayerVarInt(playerid, EmpresaID, INVALID_BUSINESS_ID);

	call::BUSINESS->UpdateBusinessTexts(empresaid);
	return true;
}
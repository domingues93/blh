
enum e_FUNCIONARIO_INFO 
{
	Nome[MAX_PLAYER_NAME],
	Entregas
}
static 
	Business[MAX_BUSINESS + 1][e_BUSINESS_INFO],
	Funcionario[MAX_BUSINESS][MAX_FUNCIONARIOS + 1][e_FUNCIONARIO_INFO]
;

#include ../bin/modules/empresas/cmds.pwn
#include ../bin/modules/empresas/dialogs.pwn
#include ../bin/modules/empresas/gerenciar.pwn


const BUSINESS_LENG_RENDER_VAR = 350;
	
// ================================================== [ functions ] ================================================== //

stock static render_string(businessid)
{
	new s[BUSINESS_LENG_RENDER_VAR];
	
	format(s, sizeof(s), 
		"Empresa: {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"}\nProprietario: {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"}\nEnd.: {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"}, N°: {"COR_AZUL_INC"}%d{"COR_BRANCO_INC"}\nValor: {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"}\nFuncionarios: {"COR_AZUL_INC"}%d/%d{"COR_BRANCO_INC"}\nEntregas: {"COR_AMARELO_INC"}%d{"COR_BRANCO_INC"}",
		Business[businessid][Nome],
		Business[businessid][Proprietario],
		call::BUSINESS->GetBusinessLocalName(businessid),
		businessid, RealStr(Business[businessid][Valor]),
		call::BUSINESS->CountFuncionarios(businessid),
		Business[businessid][MaxFuncionarios],
		Business[businessid][Producao]
	);
	return s;
}

function BUSINESS::CountFuncionarios(businessid)
{
	new c;
	LoopFuncionarios(i, Business[businessid][MaxFuncionarios])
	{
		if(strcmp(Funcionario[businessid][i][Nome], "Ninguem", true) != 0)
			++c;
	}
	return c;
}
function BUSINESS::render(businessid)
{
	Business[businessid][PickupID] = CreateDynamicPickup(RETURN_PICKUP_ID(businessid), 23, Business[businessid][Entrada][X], Business[businessid][Entrada][Y], Business[businessid][Entrada][Z] - 0.5, 0, 0, -1, BUSINESS_RENDER_STREAMER);

	Business[businessid][Label][0] = CreateDynamic3DTextLabel(render_string(businessid), COR_BRANCO, Business[businessid][Entrada][X], Business[businessid][Entrada][Y], Business[businessid][Entrada][Z] + 0.3, 20.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, 0, 0, -1, BUSINESS_RENDER_STREAMER);
	Business[businessid][Label][1] = CreateDynamic3DTextLabel("Use '{"COR_AMARELO_INC"}F{"COR_BRANCO_INC"}' para sair.", COR_BRANCO, Business[businessid][Saida][X], Business[businessid][Saida][Y], Business[businessid][Saida][Z] + 1.0, 20.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, businessid, Business[businessid][Interior], -1, BUSINESS_RENDER_STREAMER);

	Business[businessid][Map] = CreateDynamicMapIcon(Business[businessid][Entrada][X], Business[businessid][Entrada][Y], Business[businessid][Entrada][Z], Business[businessid][MapIcon], COR_BRANCO, 0, 0, -1, BUSINESS_RENDER_STREAMER, MAPICON_LOCAL);
	
	if(Business[businessid][UseActor]){
		Business[businessid][Actor_id] = CreateDynamicActor(Business[businessid][Skin], Business[businessid][ActorPos][X], Business[businessid][ActorPos][Y], Business[businessid][ActorPos][Z], Business[businessid][ActorPos][A], 1, 100.0, businessid, -1, -1);
		CreateDynamic3DTextLabel(Business[businessid][ActorName], 0xFD811DFF, Business[businessid][ActorPos][X], Business[businessid][ActorPos][Y], Business[businessid][ActorPos][Z]+0.7, 15.0, businessid, .interiorid=-1, .streamdistance=15.0);
	}
}
function BUSINESS::DestroyBusiness(businessid)
{
	DestroyDynamicPickup(Business[businessid][PickupID]);

	DestroyDynamic3DTextLabel(Business[businessid][Label][0]);
	DestroyDynamic3DTextLabel(Business[businessid][Label][1]);

	DestroyDynamicMapIcon(Business[businessid][Map]);

	new reset[e_BUSINESS_INFO];
	Business[businessid] = reset;

	Iter_Remove(Business, businessid);
	return true;
}

function BUSINESS::VenderBusiness(businessid, owner[], bool:reset)
{
	if(!call::BUSINESS->IsValidBusiness(businessid))
		return false;

	new pName[MAX_PLAYER_NAME];
	call::BUSINESS->GetBusinessParams(businessid, Proprietario, pName);
	
	new ownerid = GetPlayerIDByName(pName);

	if(call::PLAYER->IsPlayerLogged(ownerid))
		Jogador[ownerid][EmpresaID] = INVALID_BUSINESS_ID;

	call::BUSINESS->SetBusinessParams(businessid, Proprietario, owner);
	if(reset)
	{
		call::BUSINESS->SetBusinessParams(businessid, Mensagem, "");
		call::BUSINESS->SetBusinessParamsInt(businessid, Level, 0);
		call::BUSINESS->SetBusinessParamsInt(businessid, Producao, 0);
		call::BUSINESS->SetBusinessParamsInt(businessid, Cofre, 0);

		LoopFuncionarios(i, call::BUSINESS->GetBusinessParamsInt(businessid, MaxFuncionarios))
		{
			if(strcmp(call::BUSINESS->GetBusinessFuncName(businessid, i), "Ninguem", true) != 0)
			{
				 new id = GetPlayerIDByName(call::BUSINESS->GetBusinessFuncName(businessid, i));
				 if(call::PLAYER->IsPlayerLogged(id))
				 {
				 	SendClientMessage(id, COR_AVISO, "Aviso: {"COR_BRANCO_INC"}Você foi demitido de suas funções na empresa %s", call::BUSINESS->GetBusinessName(businessid));
				 	Jogador[id][EmpresaID] = INVALID_BUSINESS_ID;
				 }
				 call::BUSINESS->SetBusinessFuncName(businessid, i, "Ninguem");
				 call::BUSINESS->SetFuncionarioEntrega(businessid, i, 0);
			}
		}
		call::BUSINESS->SetBusinessParamsInt(businessid, MaxFuncionarios, 2);
	}
	call::BUSINESS->UpdateBusinessData(businessid);
	call::BUSINESS->UpdateBusinessTexts(businessid);
	return true;
}

function BUSINESS::UpdateBusinessData(businessid)
{
	if(!call::BUSINESS->IsValidBusiness(businessid))
		return false;

	new str[1024], query[2048];
	LoopFuncionarios(i, MAX_FUNCIONARIOS)
	{
		format(str, sizeof(str), "%s`funcionario_%d`='%s',`entregas_funcionario_%d`='%d',", str, i, Funcionario[businessid][i][Nome], i, Funcionario[businessid][i][Entregas]);
	}
	// remove o ultimo , da query acima.
	strdel(str, strlen(str) - 1, strlen(str));

	mysql_format(getConexao(), query, sizeof(query), "UPDATE "TABLE_BUSINESS" SET `proprietario`='%s',`mensagem`='%s',`level`='%d',`cofre`='%d',`producao`='%d',`max_funcionarios`='%d',%s,`entrada_x`='%f',`entrada_y`='%f',`entrada_z`='%f',`entrada_a`='%f',`saida_x`='%f',`saida_y`='%f',`saida_z`='%f',`saida_a`='%f',`interior`='%d' wHERE `id`='%d' LIMIT 1;", 
		Business[businessid][Proprietario], Business[businessid][Mensagem], Business[businessid][Level], Business[businessid][Cofre], Business[businessid][Producao], Business[businessid][MaxFuncionarios], str,
		Business[businessid][Entrada][X], Business[businessid][Entrada][Y], Business[businessid][Entrada][Z], Business[businessid][Entrada][A],
		Business[businessid][Saida][X], Business[businessid][Saida][Y], Business[businessid][Saida][Z], Business[businessid][Saida][A],
		Business[businessid][Interior],
		Business[businessid][ID]);
	mysql_tquery(getConexao(), query);
	return true;
}




function BUSINESS::SetBusinessExitPos(businessid, Float:x, Float:y, Float:z, Float:a, interior)
{
	if(!call::BUSINESS->IsValidBusiness(businessid))
		return false;

	Business[businessid][Saida][X] = x;
	Business[businessid][Saida][Y] = y;
	Business[businessid][Saida][Z] = z;
	Business[businessid][Saida][A] = a;
	Business[businessid][Interior] = interior;
	return true;
}

function BUSINESS::UpdateBusinessTexts(businessid)
{
	if(!call::BUSINESS->IsValidBusiness(businessid))
		return false;

	UpdateDynamic3DTextLabelText(Business[businessid][Label][0], COR_BRANCO, render_string(businessid));

	DestroyDynamicPickup(Business[businessid][PickupID]);
	Business[businessid][PickupID] = CreateDynamicPickup(RETURN_PICKUP_ID(businessid), 23, Business[businessid][Entrada][X], Business[businessid][Entrada][Y], Business[businessid][Entrada][Z], 0, 0, -1, BUSINESS_RENDER_STREAMER);
	return true;
}

function BUSINESS::GetBusinessColumID(businessid)
{
	if(!call::BUSINESS->IsValidBusiness(businessid))
		return false;

	return Business[businessid][ID];
}



function BUSINESS::GetBusinessName(businessid)
{
	new bName[MAX_BUSINESS_NAME];
	if(!call::BUSINESS->IsValidBusiness(businessid))
		return bName;
	format(bName, sizeof(bName), Business[businessid][Nome]);
	return bName;
}

function BUSINESS::GetBusinessLocalName(businessid)
{
	new endereco[MAX_ZONE_NAME];

	if(!call::BUSINESS->IsValidBusiness(businessid))
		return endereco;

	GetLocalName(Business[businessid][Entrada][X], Business[businessid][Entrada][Y], endereco, sizeof(endereco));
	return endereco;
}

function BUSINESS::GetBusinessPos(businessid, &Float:x, &Float:y, &Float:z)
{
	if(!call::BUSINESS->IsValidBusiness(businessid))
		return false;

	x = Business[businessid][Entrada][X];
	y = Business[businessid][Entrada][Y];
	z = Business[businessid][Entrada][Z];
	return true;
}

function BUSINESS::GetBusinessInRangeP(playerid, Float:range)
{
	foreach(new businessid: Business)
	{
		if(IsPlayerInRangeOfPoint(playerid, range, Business[businessid][Entrada][X], Business[businessid][Entrada][Y], Business[businessid][Entrada][Z]))
		{
			return businessid;
		}
	}
	return INVALID_BUSINESS_ID;
}

function BUSINESS::GetBusinessInfoEntrega(businessid)
{
	if(!call::BUSINESS->IsValidBusiness(businessid))
		return false;

	new valorTotal = (2000 + (Business[businessid][Level] * 350));
	return valorTotal;
}

GetBusinessIdByRow(rowid)
{
	foreach(new businessid: Business)
	{
		if ( Business[businessid][ID] == rowid)
		{
			return businessid;
		}
	}
	return INVALID_BUSINESS_ID;
}

function BUSINESS::IsValidBusiness(businessid){
	if(businessid > MAX_BUSINESS || businessid < 0)
		return false;

	return (Iter_Contains(Business, businessid) && !isnull(Business[businessid][Nome]));
}


function BUSINESS::ShowPlayerBusinessInfo(playerid, businessid)
{
	new info[1024 * 2];

	format(info, sizeof(info), "{"COR_BRANCO_INC"}Empresa: {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}\nEnd.: {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"} N°: {"COR_SISTEMA_INC"}%d{"COR_BRANCO_INC"}\n\nProprietário: {"COR_AZUL_INC"}%s{"COR_BRANCO_INC"}\nLevel: {"COR_AMARELO_INC"}%d{"COR_BRANCO_INC"}\n",
		Business[businessid][Nome], call::BUSINESS->GetBusinessLocalName(businessid), businessid, Business[businessid][Proprietario], Business[businessid][Level], Business[businessid][Proprietario]
	);

	if(Business[businessid][ValorCash] > 0)
		format(info, sizeof(info), "%sValor Cash: {"COR_AMARELO_INC"}R$%s{"COR_BRANCO_INC"}\n", info, RealStr(Business[businessid][ValorCash]));
	else
		format(info, sizeof(info), "%sValor: {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"}\n", info, RealStr(Business[businessid][Valor]));

	format(info, sizeof(info), "%sMensagem:\n{"COR_AVISO_INC"}%s{"COR_BRANCO_INC"}\n\n", info, Business[businessid][Mensagem]);


	if(businessid == call::PLAYER->GetPlayerVarInt(playerid, EmpresaID))
	{
		format(info, sizeof(info), "%sSalário Atual: {"COR_VERDE_INC"}R$%s{"COR_BRANCO_INC"}\n\n",info, RealStr(Business[businessid][Salario]));

		LoopFuncionarios(i, Business[businessid][MaxFuncionarios])
		{
			format(info, sizeof(info), "%sFuncionário: {"COR_SISTEMA_INC"}%s{"COR_BRANCO_INC"}\nEntregas: {"COR_VERMELHO_INC"}%d{"COR_BRANCO_INC"}\n\n", info, Funcionario[businessid][i][Nome], Funcionario[businessid][i][Entregas]);
		}
	}
	ShowPlayerDialog(playerid, 0, DIALOG_STYLE_MSGBOX, "{"COR_BRANCO_INC"}INFO EMPRESA", info, "OK", "");
	return true;
}

function BUSINESS::ShowPlayerBusinessMenu(playerid, businessid)
{
	if(businessid == call::PLAYER->GetPlayerVarInt(playerid, EmpresaID))
	{
		if(isnull(Business[businessid][Proprietario]) || strcmp(Business[businessid][Proprietario], GetUserName(playerid), true) != 0)
			return false;

		ShowPlayerDialog(playerid, BUSINESS_MENU, DIALOG_STYLE_LIST, "MENU EMPRESA", "Contratar\nDemitir\nAlterar Mensagem\nAlterar Salário\nCofre\n{"COR_VERDE_INC"}Atualizar", "Selecionar", "Sair");
		return true;
	}
	return false;
}

function BUSINESS::SetFuncionarioEntrega(businessid, funcid, entregas)
{
	if(!call::BUSINESS->IsValidBusiness(businessid))
		return false;

	Funcionario[businessid][funcid][Entregas] = entregas;
	return true;
}
function BUSINESS::GetFuncionarioEntrega(businessid, funcid)
{
	if(!call::BUSINESS->IsValidBusiness(businessid))
		return false;

	return Funcionario[businessid][funcid][Entregas];
}


function BUSINESS::SetBusinessFuncName(businessid, index, const pName[])
{
	if(!call::BUSINESS->IsValidBusiness(businessid))
		return false;

	format(Funcionario[businessid][index][Nome], MAX_PLAYER_NAME, pName);
	return true;
}

function BUSINESS::GetBusinessFuncName(businessid, index)
{
	new name[MAX_PLAYER_NAME];

	if(!call::BUSINESS->IsValidBusiness(businessid))
		return name;

	if(index < 1 || index > MAX_FUNCIONARIOS)
		return name;

	format(name, sizeof(name), Funcionario[businessid][index][Nome]);
	return name;
}

function BUSINESS::BusinessRequeryXpUp(playerid, businessid)
{
	if(businessid == call::PLAYER->GetPlayerVarInt(playerid, EmpresaID))
	{
		switch(call::BUSINESS->GetBusinessParamsInt(businessid, Level))
		{
			case 0..2: return 200;
			case 3..4: return 300;
			case 5..7: return 350;
			default: return 450;
		}
	}
	return true;
}

function BUSINESS::BusinessRequeryMoneyUp(playerid, businessid)
{
	if(businessid == call::PLAYER->GetPlayerVarInt(playerid, EmpresaID))
	{
		switch(call::BUSINESS->GetBusinessParamsInt(businessid, Level))
		{
			case 0..2: return 200000;
			case 3..4: return 300000;
			case 5..7: return 350000;
			default: return 450000;
		}
	}
	return true;
}
// ================================================== [ callbacks & hooks ] ================================================== //
#include <YSI_Coding\y_hooks>
forward OnLoadBusiness(playerid);
public OnLoadBusiness(playerid)
{
	if(cache_insert_id())
	{
		new query[60];
		mysql_format(getConexao(), query, sizeof(query), "SELECT * FROM "TABLE_BUSINESS" WHERE `id`='%d'", cache_insert_id());
		new Cache:cache = mysql_query(getConexao(), query, true), businessid = Iter_Count(Business) + 1;

		Iter_Add(Business, businessid);

		cache_get_value_name_int(0, "id", Business[businessid][ID]);
		
		cache_get_value_name(0, "nome", Business[businessid][Nome], MAX_BUSINESS_NAME);
		cache_get_value_name(0, "proprietario", Business[businessid][Proprietario], MAX_PLAYER_NAME);

		cache_get_value_name_float(0, "entrada_x", Business[businessid][Entrada][X]);
		cache_get_value_name_float(0, "entrada_y", Business[businessid][Entrada][Y]);
		cache_get_value_name_float(0, "entrada_z", Business[businessid][Entrada][Z]);
		cache_get_value_name_float(0, "entrada_a", Business[businessid][Entrada][A]);

		cache_get_value_name_float(0, "saida_x", Business[businessid][Saida][X]);
		cache_get_value_name_float(0, "saida_y", Business[businessid][Saida][Y]);
		cache_get_value_name_float(0, "saida_z", Business[businessid][Saida][Z]);
		cache_get_value_name_float(0, "saida_a", Business[businessid][Saida][A]);
		cache_get_value_name_int(0, "interior", Business[businessid][Interior]);

		cache_get_value_name(0, "actorname", Business[businessid][ActorName], MAX_PLAYER_NAME);
		cache_get_value_name_float(0, "actor_x", Business[businessid][ActorPos][X]);
		cache_get_value_name_float(0, "actor_y", Business[businessid][ActorPos][Y]);
		cache_get_value_name_float(0, "actor_z", Business[businessid][ActorPos][Z]);
		cache_get_value_name_float(0, "actor_a", Business[businessid][ActorPos][A]);
		cache_get_value_name_int(0, "useactor", Business[businessid][UseActor]);

		cache_get_value_name_int(0, "cofre", Business[businessid][Cofre]);
		cache_get_value_name_int(0, "producao", Business[businessid][Producao]);

		cache_get_value_name_int(0, "valor", Business[businessid][Valor]);
		cache_get_value_name_int(0, "valor_cash", Business[businessid][ValorCash]);

		cache_get_value_name(0, "mensagem", Business[businessid][Mensagem], MAX_STRLEN_MSG);
		cache_get_value_name_int(0, "max_funcionarios", Business[businessid][MaxFuncionarios]);
		cache_get_value_name_int(0, "level", Business[businessid][Level]);

		cache_get_value_name_int(0, "salario", Business[businessid][Salario]);
		cache_get_value_name_int(0, "valor_venda", Business[businessid][ValorVenda]);

		cache_get_value_name_int(0, "impostos", Business[businessid][Impostos]);


		new valorTotalEntrega = call::BUSINESS->GetBusinessInfoEntrega(businessid), valorMin = (valorTotalEntrega * 20 / 100), valorMax = (valorTotalEntrega * 50 / 100);
		if(Business[businessid][Salario] < valorMin){
			Business[businessid][Salario] = valorMin;
		}
		else if(Business[businessid][Salario] > valorMax){
			Business[businessid][Salario] = valorMax;
		}

		new str[30];
		LoopFuncionarios(i, MAX_FUNCIONARIOS)
		{
			format(str, sizeof(str), "funcionario_%d", i);
			cache_get_value_name(0, str, Funcionario[businessid][i][Nome], MAX_PLAYER_NAME);

			format(str, sizeof(str), "entregas_funcionario_%d", i);
			cache_get_value_name_int(0, str, Funcionario[businessid][i][Entregas]);
		}

		cache_get_value_name_int(0, "mapicon", Business[businessid][MapIcon]);

		call::BUSINESS->render(businessid);
		cache_delete(cache);

		SendClientMessage(playerid, COR_SISTEMA, "Empresa: {"COR_BRANCO_INC"}A Empresa foi criada com {"COR_VERDE_INC"}exito{"COR_BRANCO_INC"}.");
		return true;
	}
	SendClientMessage(playerid, COR_ERRO, "Erro: Não foi possivel criar a empresa.");
	return true;
}

hook OnGameModeInit()
{
	print("» Carregando empresas...\n");

	new Cache:cache = mysql_query(getConexao(), "SELECT * FROM "TABLE_BUSINESS";"), rows = cache_num_rows(), str[25], valorTotalEntrega, valorMin, valorMax;
	if(rows)
	{
		for(new businessid; businessid < rows; businessid++)
		{
			if(businessid >= MAX_BUSINESS){
				rows = MAX_BUSINESS;
				print("Sistema de Empresas chegou ao seu limite de "#MAX_BUSINESS#" empresas carregadas.");
				break;
			}

			Iter_Add(Business, businessid);

			cache_get_value_name_int(businessid, "id", Business[businessid][ID]);
			
			cache_get_value_name(businessid, "nome", Business[businessid][Nome], MAX_BUSINESS_NAME);
			cache_get_value_name(businessid, "proprietario", Business[businessid][Proprietario], MAX_PLAYER_NAME);

			cache_get_value_name_float(businessid, "entrada_x", Business[businessid][Entrada][X]);
			cache_get_value_name_float(businessid, "entrada_y", Business[businessid][Entrada][Y]);
			cache_get_value_name_float(businessid, "entrada_z", Business[businessid][Entrada][Z]);
			cache_get_value_name_float(businessid, "entrada_a", Business[businessid][Entrada][A]);

			cache_get_value_name_float(businessid, "saida_x", Business[businessid][Saida][X]);
			cache_get_value_name_float(businessid, "saida_y", Business[businessid][Saida][Y]);
			cache_get_value_name_float(businessid, "saida_z", Business[businessid][Saida][Z]);
			cache_get_value_name_float(businessid, "saida_a", Business[businessid][Saida][A]);
			cache_get_value_name_int(businessid, "interior", Business[businessid][Interior]);

			cache_get_value_name_int(businessid, "cofre", Business[businessid][Cofre]);
			cache_get_value_name_int(businessid, "producao", Business[businessid][Producao]);

			cache_get_value_name_int(businessid, "valor", Business[businessid][Valor]);
			cache_get_value_name_int(businessid, "valor_cash", Business[businessid][ValorCash]);

			cache_get_value_name(businessid, "mensagem", Business[businessid][Mensagem], MAX_STRLEN_MSG);
			cache_get_value_name_int(businessid, "max_funcionarios", Business[businessid][MaxFuncionarios]);
			cache_get_value_name_int(businessid, "level", Business[businessid][Level]);

			cache_get_value_name_int(businessid, "salario", Business[businessid][Salario]);
			cache_get_value_name_int(businessid, "valor_venda", Business[businessid][ValorVenda]);
			cache_get_value_name_int(businessid, "impostos", Business[businessid][Impostos]);

			valorTotalEntrega = call::BUSINESS->GetBusinessInfoEntrega(businessid), valorMin = (valorTotalEntrega * 20 / 100), valorMax = (valorTotalEntrega * 50 / 100);
			if(Business[businessid][Salario] < valorMin){
				Business[businessid][Salario] = valorMin;
			}
			else if(Business[businessid][Salario] > valorMax){
				Business[businessid][Salario] = valorMax;
			}
			LoopFuncionarios(i, MAX_FUNCIONARIOS)
			{
				format(str, sizeof(str), "funcionario_%d", i);
				cache_get_value_name(businessid, str, Funcionario[businessid][i][Nome], MAX_PLAYER_NAME);

				format(str, sizeof(str), "entregas_funcionario_%d", i);
				cache_get_value_name_int(businessid, str, Funcionario[businessid][i][Entregas]);
			}

			cache_get_value_name_int(businessid, "mapicon", Business[businessid][MapIcon]);

			call::BUSINESS->render(businessid);
		}
		printf("\t- Total de %d empresas carregadas.\n", Iter_Count(Business));
	}
	else
		print("\t- Nenhuma empresa carregada.\n");


	cache_delete(cache);
	return Y_HOOKS_CONTINUE_RETURN_1;
}

hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	if ( IsPlayerInAnyVehicle(playerid) )return Y_HOOKS_CONTINUE_RETURN_1;

	if(PRESSED(KEY_SECONDARY_ATTACK))
	{
		foreach(new businessid: Business)
		{
			if(IsPlayerInRangeOfPoint(playerid, 2.0, Business[businessid][Entrada][X], Business[businessid][Entrada][Y], Business[businessid][Entrada][Z]))
			{
				Teleport(
					playerid,
					Business[businessid][Saida][X],
					Business[businessid][Saida][Y],
					Business[businessid][Saida][Z],
					Business[businessid][Saida][A],
					Business[businessid][Interior],
					businessid,
					ENTROU_EMPRESA
				);
				return Y_HOOKS_BREAK_RETURN_1;
			}
			else if(IsPlayerInRangeOfPoint(playerid, 2.0, Business[businessid][Saida][X], Business[businessid][Saida][Y], Business[businessid][Saida][Z]) && GetPlayerVirtualWorld(playerid) == businessid)
			{
				call::PLAYER->SetPlayerVarInt(playerid, Entrou, ENTROU_NONE);
				Teleport(
					playerid,
					Business[businessid][Entrada][X],
					Business[businessid][Entrada][Y],
					Business[businessid][Entrada][Z],
					Business[businessid][Entrada][A],
					0,
					0,
					ENTROU_NONE
				);
				return Y_HOOKS_BREAK_RETURN_1;
			}
		}
	}
	return Y_HOOKS_CONTINUE_RETURN_1;
}



// ================================================== [ functions set & get ] ================================================== //

function BUSINESS::SetBusinessParams(businessid, e_BUSINESS_INFO:type, value[])
{
	if(!call::BUSINESS->IsValidBusiness(businessid))
		return false;
		
	format(Business[businessid][type], 200, value);
	return true;
}
function BUSINESS::GetBusinessParams(businessid, e_BUSINESS_INFO:type, value[], len = sizeof(value))
{
	if(!call::BUSINESS->IsValidBusiness(businessid))
		return false;

	format(value, len, Business[businessid][type]);
	return true;
}

function BUSINESS::SetBusinessParamsInt(businessid, e_BUSINESS_INFO:type, value)
{
	if(!call::BUSINESS->IsValidBusiness(businessid))
		return false;
		
	Business[businessid][type] = value;
	return true;
}
function BUSINESS::GetBusinessParamsInt(businessid, e_BUSINESS_INFO:type)
{
	if(!call::BUSINESS->IsValidBusiness(businessid))
		return false;

	return Business[businessid][type];
}
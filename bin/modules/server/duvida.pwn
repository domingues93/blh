/*#define MAX_DUVIDAS		200

enum e_DUVIDAS
{
	ID,
	Usuario[MAX_PLAYER_NAME],
	pDuvida[128]
}
new rDuvida[MAX_DUVIDAS][e_DUVIDAS], Iterator:rDuvida<MAX_HOUSE>;

#include <YSI_Coding\y_hooks>
hook OnGameModeInit()
{
	print("» Carregando duvidas...");
	new Cache:cache = mysql_query(getConexao(), "SELECT * FROM "TABLE_DUVIDAS""), rows = cache_num_rows();
	if( rows )
	{
		for(new i; i < rows; i++)
		{
			if ( i >= MAX_DUVIDAS )
			{
				print("[WARNING] O limite de duvidas foi atingido.");
				return 0;
			}

			Iter_Add(rDuvida, i);
			cache_get_value_name_int(i, "id", rDuvida[i][ID]);

			cache_get_value_name(0, "usuario", rDuvida[i][Usuario], MAX_PLAYER_NAME);
			cache_get_value_name(0, "duvida", rDuvida[i][pDuvida], 128);
		}
		printf("\t- Total de %d duvidas carregadas.\n", Iter_Count(rDuvida));
	}
	else
		print("\t- Nenhuma duvida foi carregada.\n");

	cache_delete(cache);
	return true;
}

CMD:duvida(playerid, params[])
{
	new mensagem[140];
	if ( sscanf(params, "s[140]", mensagem) )
		return SendClientMessage(playerid, COR_ERRO, CMD_CORRECT_USE, "/duvida [mensagem]");

	if ( call::ADMIN->GetPlayerAdminLevel(playerid) >= AJUDANTE)
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você não tem autorização para usar este comando.");

	if ( call::PLAYER->GetPlayerVarInt(playerid, Duvida) > gettime() )
		return SendClientMessage(playerid, COR_ERRO, "Erro: Você deve esperar {"COR_BRANCO_INC"}%d{"COR_ERRO_INC"} segundos para fazer uma nova pergunta.", call::PLAYER->GetPlayerVarInt(playerid, Duvida) - gettime());

	foreach(new i: PlayersAdmin)
	{
		if(call::ADMIN->IsAdminInJob(i))
		{
			SendClientMessage(i, 0x2C5B87FF, "DÚVIDA: {"COR_BRANCO_INC"}%s[%d]{2C5B87} enviou uma dúvida: /duvidas", GetUserName(playerid), playerid);
			GameTextForPlayer(i, "~h~~h~~h~~y~DÚVIDA", 3000, 4);
			PlayerPlaySound(i, 1057, 0.0, 0.0, 0.0);
		}
	}
	SendClientMessage(playerid, COR_VERDE, "» {"COR_BRANCO_INC"}Dúvida enviada, por favor aguarde.");
	call::PLAYER->SetPlayerVarInt(playerid, Duvida, gettime() + 30);

	new query[128];
	mysql_format(getConexao(), query, sizeof(query), "INSERT INTO "TABLE_DUVIDAS" (`usuario`,`duvida`)VALUES('%s','%s')", GetUserName(playerid), mensagem);
	mysql_tquery(getConexao(), query);
	return true;
}

flags:duvidas(AJUDANTE);
CMD:duvidas(playerid, params[])
{
	new info[2048], usuario[MAX_PLAYER_NAME], duvida[128], id, count;
	info = "ID\tNome\tDuvida\n";

	new Cache:cache = mysql_query(getConexao(), "SELECT * FROM "TABLE_DUVIDAS"");
	for(new i, rows = cache_num_rows(); i < rows; i++)
	{
		cache_get_value_name_int(i, "id", id);
		cache_get_value_name(i, "usuario", usuario, MAX_PLAYER_NAME);
		cache_get_value_name(i, "duvida", duvida, 128);
		format(info, sizeof(info), "%s%d\t%s\t%s\n", info, id, usuario, duvida);			
		count++;
	}
	if(count)
		return cache_delete(cache), ShowPlayerDialog(playerid, LISTA_DUVIDAS, DIALOG_STYLE_TABLIST_HEADERS, "{"COR_VERMELHO_INC"}"SERVER_NAME"» {"COR_BRANCO_INC"}DÚVIDAS", info, "Confirmar", "Cancelar");

	SendClientMessage(playerid, COR_ERRO, "Não há nenhuma lista.");
	return true;
}

Dialog:LISTA_DUVIDAS(playerid, response, listitem, inputtext[])
{
	if(response)
	{
		new str[128], usuario[MAX_PLAYER_NAME], id = strval(inputtext), query[100 + MAX_PLAYER_NAME];
		SetPVarInt(playerid, "DuvidaID", id);

		mysql_format(getConexao(), query, sizeof(query), "SELECT `usuario` FROM "TABLE_DUVIDAS" WHERE `id`='%d' LIMIT 1;", id);
		new Cache:cache = mysql_query(getConexao(), query, true);
		for(new i, rows = cache_num_rows(); i < rows; i++)
		{
			cache_get_value_name(i, "usuario", usuario, MAX_PLAYER_NAME);
			format(str, sizeof(str), "Respondendo a dúvida id: %d de: %s.", id, usuario);
		}
		cache_delete(cache);
		ShowPlayerDialog(playerid, RDUVIDA, DIALOG_STYLE_INPUT, "{"COR_VERMELHO_INC"}"SERVER_NAME"» {"COR_BRANCO_INC"}DÚVIDAS", str, "Responder", "Cancelar");
	}
	return true;
}

Dialog:RDUVIDA(playerid, response, listitem, inputtext[])
{
	if(response)
	{
		new id = GetPVarInt(playerid, "DuvidaID"), usuario[MAX_PLAYER_NAME], pid, query[128 + MAX_PLAYER_NAME], query2[128], query3[128];

		mysql_format(getConexao(), query, sizeof(query), "SELECT `usuario` FROM "TABLE_DUVIDAS" WHERE `id`='%d' LIMIT 1;", id);
		new Cache:cache = mysql_query(getConexao(), query, true);
		for(new i, rows = cache_num_rows(); i < rows; i++)
		{
			cache_get_value_name(i, "usuario", usuario, MAX_PLAYER_NAME);
		 	pid = GetPlayerIDByName(usuario);
		}

		if ( IsPlayerConnected(pid) )
		{
			SendClientMessage(pid, COR_BRANCO, "» O %s %s respondeu sua dúvida.", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)), GetUserName(playerid));
			SendClientMessage(pid, COR_BRANCO, "» Mensagem: %s.", inputtext);
		}

		call::PLAYER->SetPlayerVarInt(playerid, Hits, call::PLAYER->GetPlayerVarInt(playerid, Hits) + 1);
		mysql_format(getConexao(), query2, sizeof(query2), "UPDATE "TABLE_USERS" SET `hits`='%d' WHERE `username`='%s'", call::PLAYER->GetPlayerVarInt(playerid, Hits), GetUserName(playerid));
		mysql_tquery(getConexao(), query2);

		foreach(new i: Player)
		{
			if ( call::ADMIN->GetPlayerAdminLevel(i) >= AJUDANTE)
			{
				SendClientMessage(i, 0x476B8CFF, "O(A) %s %s[%d] respondeu a dúvida id: %d do(a) jogador(a): %s[%d].", call::ADMIN->CargoAdmin(call::ADMIN->GetPlayerAdminLevel(playerid)),  GetUserName(playerid), playerid, id, usuario, pid);
				GameTextForPlayer(i, "~b~Duvida Respondida!", 3000, 1);
			}
		}

		cache_delete(cache);
		mysql_format(getConexao(), query3, sizeof(query3), "DELETE FROM "TABLE_DUVIDAS" WHERE `id`='%d' LIMIT 1;", id);
		mysql_tquery(getConexao(), query3);
		return true;
	}
	return true;
}*/
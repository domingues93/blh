
// ============================== [ FORWARDs ] ============================== //
forward InitAnnouceSystem();
forward OnAnnounceCalled();


// ============================== [ VARIÁVEIS ] ============================== //

static NumAnnounce, old;

// ============================== [ PUBLICs ] ============================== //
#include <YSI_Coding\y_hooks>
hook OnGameModeInit()
{
	InitAnnouceSystem();
}

public InitAnnouceSystem()
{
	new query[128];

	new Cache:cache = mysql_query(getConexao(), "SELECT * FROM "TABLE_ANNOUNCE";", true);
	NumAnnounce = cache_num_rows();
	cache_delete(cache);

	++old;
	
	if(old > NumAnnounce)
		old = 1;

	mysql_format(getConexao(), query, sizeof(query), "SELECT * FROM "TABLE_ANNOUNCE" WHERE `id`='%d' LIMIT 1;", old);
	mysql_tquery(getConexao(), query, "OnAnnounceCalled");
	return true;
}

public OnAnnounceCalled()
{
	if(cache_num_rows())
	{
		new cor = -1, announce[130];
		cache_get_value_name(0, "texto", announce, sizeof(announce));
		cache_get_value_name_int(0, "cor", cor);

		SendClientMessageToAll(cor, "ANÚNCIO: %s", announce);
	}
	SetTimer("InitAnnouceSystem", (5 * (60 * 1000)), false);
	return true;
}
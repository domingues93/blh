#include <YSI_Coding\y_hooks>

#define 	MAX_DYN_ACTORS 			(50)

enum e_ACTOR_INFO
{
	ID,
	Actor_id,
	Nome[MAX_PLAYER_NAME],
	Float:Pos[e_POSITION],
	Interior,
	World,
	Skin,
	AnimLib[30],
	AnimName[30],
	fDelta,
	bool:Loop,
	bool:LockX,
	bool:lockY,
	bool:Freeze,
	Time,
}
static Actor[MAX_DYN_ACTORS][e_ACTOR_INFO];


hook OnGameModeInit()
{
	new Cache:cache = mysql_query(getConexao(), "SELECT * FROM actors;"), rows = cache_num_rows();
	rows = (rows > MAX_DYN_ACTORS ? MAX_DYN_ACTORS : rows);
	if ( rows )
	{
		for(new i; i < rows; i++)
		{
			cache_get_value_name_int(i, "id", Actor[i][ID]);

			cache_get_value_name(i, "nome", Actor[i][Nome]);

			cache_get_value_name_float(i, "x", Actor[i][Pos][X]);
			cache_get_value_name_float(i, "y", Actor[i][Pos][Y]);
			cache_get_value_name_float(i, "z", Actor[i][Pos][Z]);
			cache_get_value_name_float(i, "a", Actor[i][Pos][A]);
			
			cache_get_value_name_int(i, "world", Actor[i][World]);
			cache_get_value_name_int(i, "interior", Actor[i][Interior]);

			cache_get_value_name_int(i, "skin", Actor[i][Skin]);

			cache_get_value_name(i, "animlib", Actor[i][AnimLib], 30);
			cache_get_value_name(i, "animname", Actor[i][AnimName], 30);

			cache_get_value_name_int(i, "delta", Actor[i][fDelta]);
			cache_get_value_name_bool(i, "loop_", Actor[i][Loop]);
			cache_get_value_name_bool(i, "lockx", Actor[i][LockX]);
			cache_get_value_name_bool(i, "locky", Actor[i][lockY]);
			cache_get_value_name_bool(i, "freeze", Actor[i][Freeze]);
			cache_get_value_name_int(i, "time", Actor[i][Time]);

			CreateDynamic3DTextLabel(Actor[i][Nome], 0xFD811DFF, Actor[i][Pos][X], Actor[i][Pos][Y], Actor[i][Pos][Z]+0.7, 15.0, .worldid=Actor[i][World], .interiorid=Actor[i][Interior], .streamdistance=15.0, .testlos=1);
			Actor[i][Actor_id] = CreateDynamicActor(Actor[i][Skin], Actor[i][Pos][X], Actor[i][Pos][Y], Actor[i][Pos][Z], Actor[i][Pos][A], .invulnerable=0, .health = 100.0, .worldid=Actor[i][World], .interiorid=Actor[i][Interior], .streamdistance=50.0);
			
			if ( !isnull( Actor[i][AnimLib] ) && !isnull( Actor[i][AnimName] ) )
				ApplyDynamicActorAnimation(Actor[i][Actor_id], Actor[i][AnimLib], Actor[i][AnimName], Actor[i][fDelta], Actor[i][Loop], Actor[i][LockX], Actor[i][lockY], Actor[i][Freeze], Actor[i][Time]);

			Iter_Add(Actor, i);
		}
	}
	cache_delete(cache);
	return true;
}
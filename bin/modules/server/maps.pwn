
static const INTERIOR_ID = 3;


static 
	GATE_DP, bool:GATE_DP_OPENED, BikesNovatos[12]
;
#include <YSI_Coding\y_hooks>

hook OnPlayerConnect(playerid)
{
	// ============================= MOTEL ==================================== //
	new welcome = CreatePlayerObject(playerid, 19353, 1326.3205, -1061.3629, 3361.4370, 0.0000, 0.0000, 0.1119);
    SetPlayerObjectMaterialText(playerid, welcome, "Bem vindo", 0, 140, "Arial", 60, 1, -1, 0, 1);
    new GM = CreatePlayerObject(playerid, 19353, 1326.3111, -1060.4824, 3360.9765, 0.0000, 0.0000, 0.0119);
    SetPlayerObjectMaterialText(playerid, GM, "Hotel Brasil Life Honest", 0, 140, "Arial", 60, 1, -584707328, 0, 1);

	// ============================== AGENCIA DE EMPREGOS ============================== //
	RemoveBuildingForPlayer(playerid, 1502, 344.8750, 157.1953, 1013.1719, 0.25);
	// ============================================================================================================================================== //

	// ============================== DEPARTAMENTO DE POLICIA LOS SANTOS ============================== //
	RemoveBuildingForPlayer(playerid, 1266, 1538.5234, -1609.8047, 19.8438, 0.25);
	RemoveBuildingForPlayer(playerid, 4229, 1597.9063, -1699.7500, 30.2109, 0.25);
	RemoveBuildingForPlayer(playerid, 4230, 1597.9063, -1699.7500, 30.2109, 0.25);
	RemoveBuildingForPlayer(playerid, 1260, 1538.5234, -1609.8047, 19.8438, 0.25);
	// ============================================================================================================================================== //
	// ============================== NOVA FAZENDA ==================================================== //
	RemoveBuildingForPlayer(playerid, 1370, -951.9922, -527.0000, 25.5078, 0.25);
	RemoveBuildingForPlayer(playerid, 3374, -1206.1406, -1169.8359, 129.7031, 0.25);
	RemoveBuildingForPlayer(playerid, 3374, -1198.8672, -1169.8359, 129.7031, 0.25);
	RemoveBuildingForPlayer(playerid, 3374, -1191.6016, -1169.8359, 129.7031, 0.25);
	RemoveBuildingForPlayer(playerid, 3374, -1184.3281, -1169.8359, 129.7031, 0.25);
	RemoveBuildingForPlayer(playerid, 3261, -1183.7656, -1095.7969, 128.2188, 0.25);
	RemoveBuildingForPlayer(playerid, 3261, -1186.8828, -1095.7969, 128.2188, 0.25);
	RemoveBuildingForPlayer(playerid, 691, -1240.8047, -1013.2891, 156.7188, 0.25);
	RemoveBuildingForPlayer(playerid, 691, -1086.1484, -1308.5313, 128.3516, 0.25);
	RemoveBuildingForPlayer(playerid, 705, -1086.4141, -1291.3906, 128.0703, 0.25);
	RemoveBuildingForPlayer(playerid, 3276, -1088.4063, -1238.4375, 129.0625, 0.25);
	RemoveBuildingForPlayer(playerid, 691, -1073.8047, -1234.7578, 128.0781, 0.25);
	RemoveBuildingForPlayer(playerid, 3374, -1177.0547, -1169.8359, 129.7031, 0.25);
	RemoveBuildingForPlayer(playerid, 3261, -1152.2969, -1134.5078, 128.2188, 0.25);
	RemoveBuildingForPlayer(playerid, 3261, -1155.4063, -1134.5078, 128.2188, 0.25);
	RemoveBuildingForPlayer(playerid, 708, -1124.4922, -1140.8984, 128.4453, 0.25);
	RemoveBuildingForPlayer(playerid, 705, -1081.5938, -1143.2500, 128.3359, 0.25);
	RemoveBuildingForPlayer(playerid, 691, -1081.1484, -1162.4844, 128.1719, 0.25);
	RemoveBuildingForPlayer(playerid, 3261, -1180.6563, -1095.7969, 128.2188, 0.25);
	RemoveBuildingForPlayer(playerid, 3261, -1171.2813, -1095.7969, 128.2188, 0.25);
	RemoveBuildingForPlayer(playerid, 3261, -1174.4609, -1095.7969, 128.2188, 0.25);
	RemoveBuildingForPlayer(playerid, 3261, -1168.1016, -1095.7969, 128.2188, 0.25);
	RemoveBuildingForPlayer(playerid, 3261, -1177.5859, -1095.7969, 128.2188, 0.25);
	RemoveBuildingForPlayer(playerid, 3261, -1122.0859, -1095.5391, 128.2188, 0.25);
	RemoveBuildingForPlayer(playerid, 3261, -1122.0859, -1084.3672, 128.2188, 0.25);
	RemoveBuildingForPlayer(playerid, 705, -1156.6797, -1083.9219, 128.2734, 0.25);
	RemoveBuildingForPlayer(playerid, 705, -1105.5078, -1083.4375, 128.2734, 0.25);
	RemoveBuildingForPlayer(playerid, 708, -1145.2266, -1071.8984, 128.4453, 0.25);
	RemoveBuildingForPlayer(playerid, 691, -1023.8047, -1171.0938, 128.5859, 0.25);
	RemoveBuildingForPlayer(playerid, 691, -1069.4766, -1135.6797, 128.5859, 0.25);
	RemoveBuildingForPlayer(playerid, 705, -1022.8750, -1153.4453, 128.2734, 0.25);
	RemoveBuildingForPlayer(playerid, 708, -1057.0000, -1091.1484, 128.4453, 0.25);
	// =================================== HQ ADVOGADO ================================================= //
	RemoveBuildingForPlayer(playerid, 1250, 997.4141, 1707.5234, 10.8516, 0.25);
	RemoveBuildingForPlayer(playerid, 1251, 997.0469, 1710.9531, 11.2656, 0.25);
	RemoveBuildingForPlayer(playerid, 1251, 998.0938, 1755.6875, 11.2656, 0.25);
	RemoveBuildingForPlayer(playerid, 1250, 997.7266, 1759.1250, 10.8516, 0.25);
	// =================================== [ REFINARIA DE DROGAS ] ===================================== //
	RemoveBuildingForPlayer(playerid, 11088, -2166.8750, -236.5156, 40.8594, 0.25);
	RemoveBuildingForPlayer(playerid, 11091, -2133.5547, -132.7031, 36.1328, 0.25);
	RemoveBuildingForPlayer(playerid, 11282, -2166.8750, -236.5156, 40.8594, 0.25);
	RemoveBuildingForPlayer(playerid, 11376, -2144.3516, -132.9609, 38.3359, 0.25);
	RemoveBuildingForPlayer(playerid, 11280, -2137.6875, -198.0781, 34.4141, 0.25);
	RemoveBuildingForPlayer(playerid, 1278, -2097.6797, -178.2344, 48.3516, 0.25);
	RemoveBuildingForPlayer(playerid, 11011, -2144.3516, -132.9609, 38.3359, 0.25);
	RemoveBuildingForPlayer(playerid, 11009, -2128.5391, -142.8438, 39.1406, 0.25);
	RemoveBuildingForPlayer(playerid, 1278, -2137.6172, -110.9375, 48.3516, 0.25);
	RemoveBuildingForPlayer(playerid, 1441, -2184.6484, -226.8750, 36.1641, 0.25);
	RemoveBuildingForPlayer(playerid, 1438, -2164.2188, -231.1563, 35.5078, 0.25);
	// ================================================================================================================================================== //
	return Y_HOOKS_CONTINUE_RETURN_1;
}

hook OnGameModeInit()
{
	// ============================== AGENCIA DE EMPREGOS ============================== //
	// Pos interior 345.8872, 158.0593, 1014.1875, 355.1046, 3
	CreateDynamicObject(3089, 344.90079, 157.50110, 1014.35748,   0.00000, 0.00000, 0.00000, .interiorid=INTERIOR_ID, .streamdistance=50.0);
	CreateDynamicObject(1806, 344.93219, 164.07680, 1013.20068,   0.00000, 0.00000, 182.45889, .interiorid=INTERIOR_ID, .streamdistance=50.0);
	CreateDynamicObject(2309, 345.23560, 161.50000, 1013.19989,   0.00000, 0.00000, 363.82541, .interiorid=INTERIOR_ID, .streamdistance=50.0);
	CreateDynamicObject(2200, 347.87921, 166.74930, 1013.18323,   0.00000, 0.00000, 361.60001, .interiorid=INTERIOR_ID, .streamdistance=50.0);
	CreateDynamicObject(2198, 345.56000, 163.00000, 1013.16412,   0.00000, 0.00000, 182.00000, .interiorid=INTERIOR_ID, .streamdistance=50.0);
	CreateDynamicObject(1806, 349.05560, 164.07680, 1013.20068,   0.00000, 0.00000, 182.45889, .interiorid=INTERIOR_ID, .streamdistance=50.0);
	CreateDynamicObject(2308, 344.52341, 165.37770, 1013.18719,   0.00000, 0.00000, 358.71219, .interiorid=INTERIOR_ID, .streamdistance=50.0);
	CreateDynamicObject(2002, 344.59839, 159.86510, 1013.18213,   0.00000, 0.00000, -270.32001, .interiorid=INTERIOR_ID, .streamdistance=50.0);
	CreateDynamicObject(2198, 349.89999, 163.00000, 1013.16412,   0.00000, 0.00000, 182.00000, .interiorid=INTERIOR_ID, .streamdistance=50.0);
	CreateDynamicObject(2309, 334.24109, 159.41209, 1013.19989,   0.00000, 0.00000, 363.82541, .interiorid=INTERIOR_ID, .streamdistance=50.0);
	CreateDynamicObject(2309, 349.50000, 161.50000, 1013.19989,   0.00000, 0.00000, 363.82541, .interiorid=INTERIOR_ID, .streamdistance=50.0);
	CreateDynamicObject(2010, 356.81250, 160.51563, 1018.96875,   3.14159, 0.00000, 2.35619, .interiorid=INTERIOR_ID, .streamdistance=50.0);
	CreateDynamicObject(2001, 347.28302, 157.84050, 1013.18335,   0.00000, 0.00000, 0.00000, .interiorid=INTERIOR_ID, .streamdistance=50.0);
	CreateDynamicObject(2001, 350.09717, 165.18745, 1013.18335,   0.00000, 0.00000, 0.00000, .interiorid=INTERIOR_ID, .streamdistance=50.0);
	CreateDynamicObject(1703, 349.92419, 160.25999, 1013.16339,   0.00000, 0.00000, -90.59990, .interiorid=INTERIOR_ID, .streamdistance=50.0);
	CreateDynamicObject(2202, 347.88699, 163.74220, 1013.18250,   0.00000, 0.00000, -88.44000, .interiorid=INTERIOR_ID, .streamdistance=50.0);

	//CreateDynamicObject(14594, 346.3516, 173.9844, 1013.1719,     3.1416, 0.0000, 0.0000, .interiorid=INTERIOR_ID, .streamdistance=50.0); // estrutura
	// ============================================================================================================================================== //


	// ============================== PREFEITURA ============================== //
	CreateDynamicObject(14597, 1069.24268, -1060.59167, 1088.76196,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(14602, 1095.99829, -1065.19214, 1092.11230,   0.00000, 0.00000, 268.63611, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2161, 1082.84460, -1041.88831, 1089.88208,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2161, 1084.14465, -1041.90833, 1089.88208,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2161, 1082.84460, -1041.90833, 1088.56213,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2161, 1084.14465, -1041.90833, 1088.56213,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2161, 1085.44470, -1041.94824, 1089.88208,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2161, 1085.44470, -1041.94824, 1088.56213,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2161, 1086.74475, -1041.96814, 1089.88208,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2161, 1086.74475, -1041.96814, 1088.56213,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1082.89160, -1045.25403, 1089.15674,   0.00000, 0.00000, -1.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1087.21033, -1045.32715, 1089.15674,   0.00000, 0.00000, -1.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1091.47192, -1045.39685, 1089.15674,   0.00000, 0.00000, -1.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(3440, 1093.56555, -1045.58691, 1089.60449,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19450, 1085.46667, -1045.34619, 1092.37952,   0.00000, 0.00000, 269.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19450, 1088.46667, -1045.38623, 1092.37952,   0.00000, 0.00000, 269.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1082.90247, -1045.23438, 1089.15674,   0.00000, 0.00000, 179.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1087.24255, -1045.31445, 1089.15674,   0.00000, 0.00000, 179.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1091.56262, -1045.39453, 1089.15674,   0.00000, 0.00000, 179.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1714, 1096.12720, -1047.16577, 1086.72180,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2184, 1095.12244, -1048.48633, 1086.68103,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(14455, 1104.99829, -1042.57849, 1087.45544,   0.00000, 0.00000, 178.24510, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(14455, 1104.99829, -1042.57849, 1090.50244,   0.00000, 0.00000, 178.24510, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(14455, 1110.81226, -1042.75488, 1090.50244,   0.00000, 0.00000, 178.24510, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(14455, 1110.81226, -1042.75488, 1087.48096,   0.00000, 0.00000, 178.24510, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2165, 1096.73547, -1048.04749, 1086.65662,   0.00000, 0.00000, 183.38480, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(949, 1092.27161, -1047.03894, 1087.37256,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(949, 1100.26221, -1047.33240, 1087.37256,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2196, 1095.54187, -1047.62854, 1087.44666,   0.00000, 0.00000, 34.60920, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2855, 1093.54480, -1047.54980, 1087.50720,   0.00000, 0.00000, 334.82300, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(14705, 1097.17285, -1048.00513, 1087.67102,   0.00000, 0.00000, 355.71811, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(3440, 1099.06128, -1045.72229, 1089.60449,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1101.28052, -1045.58948, 1089.15674,   0.00000, 0.00000, 179.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1105.60046, -1045.66956, 1089.15674,   0.00000, 0.00000, 179.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1109.93481, -1045.75439, 1089.15674,   0.00000, 0.00000, 179.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1101.26465, -1045.60449, 1089.15674,   0.00000, 0.00000, 358.89691, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1105.55066, -1045.69360, 1089.15674,   0.00000, 0.00000, 358.89691, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1109.90161, -1045.77844, 1089.15674,   0.00000, 0.00000, 358.89691, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19450, 1103.87073, -1045.63123, 1092.37952,   0.00000, 0.00000, 269.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19450, 1107.45068, -1045.69116, 1092.37952,   0.00000, 0.00000, 269.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(14455, 1093.05249, -1042.30725, 1087.25891,   0.00000, 0.00000, 178.24510, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(14455, 1093.05249, -1042.30725, 1090.30396,   0.00000, 0.00000, 178.24510, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1536, 1111.95215, -1056.34448, 1086.75586,   0.00000, 0.00000, 89.59500, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2395, 1108.90466, -1066.30042, 1087.50708,   0.00000, 0.00000, 179.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19786, 1107.47864, -1066.37341, 1088.68542,   0.00000, 0.00000, 178.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1703, 1102.84595, -1063.87769, 1086.63586,   0.00000, 0.00000, 89.23040, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1703, 1106.97998, -1062.54578, 1086.63586,   0.00000, 0.00000, 269.10406, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1703, 1108.72546, -1058.87634, 1086.63586,   0.00000, 0.00000, 0.27100, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1703, 1107.93958, -1064.55396, 1086.63586,   0.00000, 0.00000, 89.55180, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(949, 1107.38745, -1058.89856, 1087.37256,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2257, 1111.66248, -1061.80029, 1088.53821,   0.00000, 0.00000, 268.74438, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1808, 1111.36609, -1065.71106, 1086.54333,   0.00000, 0.00000, 270.30469, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1101.79089, -1062.53821, 1088.35999,   0.00000, 0.00000, 268.74359, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1108.38354, -1057.91980, 1088.35999,   0.00000, 0.00000, 358.89691, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1112.65173, -1058.01025, 1088.35999,   0.00000, 0.00000, 358.89691, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1104.05115, -1057.83582, 1088.35999,   0.00000, 0.00000, 358.89691, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1101.68359, -1066.89673, 1088.35999,   0.00000, 0.00000, 268.74359, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1104.11108, -1057.85583, 1088.35999,   0.00000, 0.00000, 178.98000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1108.35461, -1057.93933, 1088.35999,   0.00000, 0.00000, 178.98000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1112.70227, -1058.03198, 1088.35999,   0.00000, 0.00000, 178.98000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1101.83044, -1062.57825, 1088.35999,   0.00000, 0.00000, 88.50470, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1101.71545, -1066.81372, 1088.35999,   0.00000, 0.00000, 88.50470, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(18762, 1099.52087, -1042.28442, 1089.21619,   0.00000, 0.00000, 358.95380, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(18762, 1094.13464, -1042.17322, 1089.21619,   0.00000, 0.00000, 358.95377, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(18762, 1094.13464, -1042.17322, 1093.59131,   0.00000, 0.00000, 358.95377, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(18762, 1099.52087, -1042.28442, 1093.71313,   0.00000, 0.00000, 358.95380, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(18762, 1097.10632, -1042.22278, 1091.66736,   0.00000, 90.00000, 359.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(18762, 1097.10632, -1042.22278, 1087.17993,   0.00000, 90.00000, 359.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19449, 1096.57849, -1042.20154, 1089.41736,   0.00000, 0.00000, 88.56820, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19166, 1096.82532, -1042.39014, 1089.28162,   90.00000, 0.00000, 359.49493, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(18762, 1098.52344, -1042.25952, 1089.21619,   0.00000, 0.00000, 358.95380, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(18762, 1095.12952, -1042.20264, 1089.21619,   0.00000, 0.00000, 358.95380, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1897, 1096.83838, -1042.47009, 1090.83899,   0.00000, 90.00000, 180.76620, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(957, 1096.07385, -1042.59216, 1090.76892,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(957, 1096.88586, -1042.59412, 1090.76892,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(957, 1097.69373, -1042.59607, 1090.76892,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1703, 1111.43701, -1047.87793, 1086.63586,   0.00000, 0.00000, 269.10406, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1703, 1111.45056, -1051.66980, 1086.63586,   0.00000, 0.00000, 269.10406, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2258, 1112.03833, -1050.64258, 1089.20081,   0.00000, 0.00000, 269.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2083, 1110.92383, -1050.24597, 1086.76013,   0.00000, 0.00000, 269.86520, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1665, 1111.58044, -1050.72034, 1087.27930,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2452, 1088.40259, -1056.92175, 1086.63489,   0.00000, 0.00000, 178.78729, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(5848, 1109.33423, -1074.35974, 1090.54333,   0.00000, 0.00000, 350.46780, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1536, 1094.30066, -1066.34412, 1086.74146,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1536, 1097.19678, -1066.34961, 1086.74146,   0.00000, 0.00000, 178.41040, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2011, 1100.90588, -1065.57581, 1086.75085,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2011, 1090.87244, -1065.45557, 1086.75085,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1703, 1099.35217, -1059.87195, 1086.63586,   0.00000, 0.00000, 269.10406, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1703, 1092.48511, -1061.39380, 1086.63586,   0.00000, 0.00000, 89.74129, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1703, 1081.80823, -1053.32446, 1086.63586,   0.00000, 0.00000, 88.80444, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1703, 1081.84229, -1049.39673, 1086.63586,   0.00000, 0.00000, 88.80444, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1827, 1083.73560, -1050.43335, 1086.72644,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1703, 1085.62732, -1051.42188, 1086.63391,   0.00000, 0.00000, 268.97504, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1703, 1085.81812, -1047.58362, 1086.63391,   0.00000, 0.00000, 268.97504, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2773, 1097.38757, -1051.16296, 1087.24146,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2773, 1095.21130, -1051.07532, 1087.24146,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(3440, 1081.99719, -1061.80164, 1087.46509,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(3440, 1082.06543, -1058.98730, 1087.47021,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(14599, 1072.68982, -1060.62732, 1088.87659,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2164, 1080.00879, -1074.13269, 1086.77795,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2164, 1080.02881, -1074.19275, 1088.59924,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2164, 1078.24133, -1074.11816, 1086.77795,   0.00000, 0.00000, 179.67513, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2164, 1078.23950, -1074.17566, 1088.59924,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2161, 1076.15601, -1073.75269, 1088.11035,   0.00000, 0.00000, 358.73169, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(16779, 1076.15833, -1068.87573, 1090.74731,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2011, 1073.80530, -1073.47339, 1086.75293,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2009, 1075.72229, -1067.08813, 1086.73669,   0.00000, 0.00000, 270.30270, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2009, 1076.72107, -1069.03931, 1086.73669,   0.00000, 0.00000, 179.51730, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1806, 1075.88440, -1068.07983, 1086.74585,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1806, 1075.75269, -1069.33459, 1086.74585,   0.00000, 0.00000, -180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2011, 1073.75061, -1064.76379, 1086.75293,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2571, 1079.30334, -1066.00586, 1086.72986,   0.00000, 0.00000, 268.20090, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1808, 1080.16980, -1064.38928, 1086.59656,   0.00000, 0.00000, 266.53870, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2571, 1062.94543, -1062.12183, 1086.72986,   0.00000, 0.00000, 180.00800, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2571, 1059.90442, -1059.00403, 1086.72986,   0.00000, 0.00000, 0.11220, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2011, 1064.01331, -1058.36304, 1086.75293,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2986, 1058.83777, -1063.44019, 1087.79199,   -90.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(18092, 1069.02527, -1070.78979, 1087.28088,   0.00000, 0.00000, 0.94660, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1714, 1069.06091, -1071.87317, 1086.77246,   0.00000, 0.00000, 181.77361, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2196, 1068.87781, -1070.18445, 1087.77734,   0.00000, 0.00000, 341.02704, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19893, 1069.04565, -1070.76062, 1087.78845,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(14705, 1070.91345, -1070.78418, 1087.98816,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2164, 1066.54749, -1074.14563, 1086.73486,   0.00000, 0.00000, 180.42149, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1703, 1065.82861, -1066.62903, 1086.63586,   0.00000, 0.00000, 89.42812, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1703, 1072.08276, -1064.31799, 1086.63586,   0.00000, 0.00000, 270.89523, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1808, 1072.05823, -1066.96057, 1086.59656,   0.00000, 0.00000, 266.53870, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1704, 1068.99988, -1069.33521, 1086.70044,   0.00000, 0.00000, 0.57059, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1704, 1067.89990, -1069.33521, 1086.70044,   0.00000, 0.00000, 0.57060, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2011, 1069.60510, -1064.49109, 1086.75293,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(16779, 1069.42102, -1068.53320, 1090.74731,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2424, 1073.52441, -1052.06738, 1086.69275,   0.00000, 0.00000, 179.40550, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2424, 1074.44446, -1052.06738, 1086.69275,   0.00000, 0.00000, 179.40550, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2424, 1075.36450, -1052.06738, 1086.69275,   0.00000, 0.00000, 179.40550, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2424, 1076.28455, -1052.06738, 1086.69275,   0.00000, 0.00000, 179.40550, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2424, 1077.20447, -1052.06738, 1086.69275,   0.00000, 0.00000, 179.40550, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2424, 1078.12451, -1052.06738, 1086.69275,   0.00000, 0.00000, 179.40550, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1075.06506, -1052.28345, 1089.31958,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19365, 1074.69678, -1052.19373, 1090.34326,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19365, 1076.97681, -1052.21375, 1090.34326,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1076.32507, -1052.26343, 1089.31958,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(3440, 1078.70190, -1052.17883, 1088.82666,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1502, 1078.93018, -1052.32397, 1086.76648,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1897, 1079.38513, -1052.16541, 1089.43506,   0.00000, 90.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1076.35962, -1052.15735, 1089.31958,   0.00000, 0.00000, 179.04520, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1649, 1075.02014, -1052.13538, 1089.31958,   0.00000, 0.00000, 179.04520, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2773, 1078.09351, -1053.61145, 1087.27356,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2773, 1073.58105, -1053.64429, 1087.27356,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2773, 1075.90723, -1053.66650, 1087.27356,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2009, 1077.21375, -1050.18738, 1086.63354,   0.00000, 0.00000, 179.28687, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2197, 1078.07349, -1050.09692, 1086.72595,   0.00000, 0.00000, 179.58250, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1806, 1076.40320, -1050.33191, 1086.78015,   0.00000, 0.00000, 163.80380, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2197, 1075.38525, -1050.09033, 1086.72595,   0.00000, 0.00000, 179.58250, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2009, 1074.52295, -1050.27722, 1086.63354,   0.00000, 0.00000, 179.28690, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1806, 1073.69238, -1050.42175, 1086.78015,   0.00000, 0.00000, 176.98550, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2612, 1077.48499, -1047.03687, 1088.67322,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2610, 1073.34912, -1047.39758, 1087.59045,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2610, 1073.82910, -1047.39758, 1087.59045,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2610, 1074.30908, -1047.39758, 1087.59045,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2610, 1074.78906, -1047.39758, 1087.59045,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2610, 1075.24915, -1047.39758, 1087.59045,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2011, 1080.01672, -1047.33264, 1086.75293,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1808, 1080.17407, -1056.12549, 1086.59656,   0.00000, 0.00000, 266.53870, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2009, 1072.10474, -1052.85791, 1086.63354,   0.00000, 0.00000, 179.28690, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1806, 1071.31445, -1052.88770, 1086.78015,   0.00000, 0.00000, 176.98550, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2009, 1072.12158, -1050.46631, 1086.63354,   0.00000, 0.00000, 179.28690, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1806, 1071.32031, -1050.37244, 1086.78015,   0.00000, 0.00000, 176.98550, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2009, 1069.05725, -1052.85144, 1086.63354,   0.00000, 0.00000, 179.28690, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1806, 1068.16089, -1053.00562, 1086.78015,   0.00000, 0.00000, 176.98550, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2009, 1069.09155, -1050.47156, 1086.63354,   0.00000, 0.00000, 179.28690, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1806, 1068.12134, -1050.50977, 1086.78015,   0.00000, 0.00000, 176.98550, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2009, 1072.10437, -1048.07898, 1086.63354,   0.00000, 0.00000, 179.28690, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1806, 1071.22852, -1048.11499, 1086.78015,   0.00000, 0.00000, 176.98550, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2009, 1069.13293, -1048.01282, 1086.63354,   0.00000, 0.00000, 179.28690, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1806, 1068.23181, -1047.79883, 1086.78015,   0.00000, 0.00000, 176.98550, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2011, 1065.86475, -1047.49683, 1086.75293,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1808, 1072.44849, -1055.17688, 1086.59656,   0.00000, 0.00000, 266.53870, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(9131, 1082.05249, -1060.97705, 1089.84729,   90.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(9131, 1082.06238, -1059.78101, 1089.84729,   90.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);

	// ============================== INTERIOR DO HOSPITAL ============================== //
	CreateDynamicObject(18030, 1163.30908, -1320.85718, -42.91639,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19449, 1172.91382, -1316.41748, -45.91558,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(18092, 1167.12793, -1326.00476, -44.79440,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2185, 1174.18933, -1315.81470, -45.28478,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2185, 1172.02869, -1315.81470, -45.28480,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2185, 1169.76953, -1315.81470, -45.28480,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1715, 1175.05432, -1314.24292, -45.28399,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1715, 1172.94055, -1314.29333, -45.28400,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1715, 1170.51819, -1314.29333, -45.28400,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19387, 1167.55603, -1314.69080, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19449, 1172.28540, -1313.12109, -43.53460,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19449, 1172.48450, -1308.97131, -43.53460,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19387, 1166.67578, -1308.96936, -43.53460,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19387, 1168.61646, -1310.65308, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19430, 1168.61841, -1312.36963, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19357, 1164.94580, -1314.89734, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19387, 1166.47534, -1316.41748, -43.53460,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19357, 1164.94580, -1311.68799, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19357, 1164.94775, -1310.40698, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2008, 1173.94373, -1312.51819, -45.28440,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1714, 1175.52881, -1312.00610, -45.15990,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1715, 1172.04395, -1311.28967, -45.28380,   0.00000, 0.00000, 77.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1715, 1172.01575, -1312.67920, -45.28380,   0.00000, 0.00000, 102.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2007, 1176.20386, -1309.62170, -44.80449,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2007, 1176.20386, -1310.62305, -44.80450,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2613, 1176.21399, -1311.97400, -45.28836,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(14455, 1173.70740, -1309.17566, -43.68773,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1808, 1174.65430, -1309.24780, -45.28379,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2007, 1176.12427, -1310.62305, -46.23550,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2007, 1176.12427, -1309.62170, -46.23550,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2194, 1176.55481, -1311.77795, -44.48066,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2194, 1176.55957, -1311.38916, -44.48070,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2194, 1176.58704, -1312.21606, -44.48066,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2194, 1176.57800, -1312.63745, -44.48066,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2010, 1169.05457, -1312.65100, -45.28379,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2007, 1176.31201, -1313.77576, -44.80449,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2007, 1176.30908, -1314.77551, -44.80449,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(14782, 1173.00916, -1307.89587, -44.05260,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(14782, 1174.62781, -1307.89978, -44.05460,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(14782, 1172.61865, -1304.66309, -44.05260,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19449, 1158.18201, -1328.43970, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19449, 1153.31311, -1333.16711, -43.53460,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19449, 1163.01538, -1328.54712, -43.53460,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19357, 1167.74402, -1330.15881, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19357, 1167.74402, -1333.36670, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19357, 1167.74207, -1333.82776, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(18092, 1165.32898, -1324.21838, -44.79640,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19357, 1160.99951, -1326.93005, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19357, 1161.00171, -1325.33936, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19357, 1163.07996, -1330.15881, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(18092, 1163.92639, -1340.67249, -44.79440,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(18092, 1162.12769, -1338.82788, -44.79640,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19387, 1166.06738, -1334.07019, -43.53460,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19357, 1163.07800, -1332.50647, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1801, 1164.97302, -1332.10742, -45.28364,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19357, 1163.07996, -1334.06824, -43.53460,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19387, 1159.88123, -1334.07019, -43.53460,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19357, 1158.18408, -1332.55688, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2333, 1164.66382, -1333.55737, -45.28360,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2251, 1163.41370, -1333.02954, -43.47816,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2167, 1163.71228, -1328.67188, -45.09077,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2167, 1162.44946, -1328.67188, -45.09080,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1801, 1160.06628, -1332.10742, -45.28360,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2333, 1161.52478, -1332.50928, -45.28360,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2251, 1162.51099, -1333.02771, -43.47820,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2596, 1161.47131, -1333.66870, -42.45050,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2596, 1164.78455, -1333.66870, -42.45050,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1823, 1166.46277, -1329.65588, -45.28392,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2853, 1167.10022, -1329.13501, -44.78861,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2647, 1166.60815, -1329.39026, -44.64582,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1823, 1158.49377, -1329.72803, -45.28392,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2853, 1158.98096, -1329.25317, -44.78861,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1997, 1160.33508, -1326.90918, -45.28415,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1997, 1160.37097, -1324.85217, -45.28415,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19387, 1156.31750, -1338.25305, -43.53460,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19449, 1150.00439, -1337.95544, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19449, 1157.84338, -1342.98792, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19357, 1154.80176, -1336.56409, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19357, 1154.80371, -1334.84326, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2333, 1155.97717, -1334.84863, -45.28360,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2251, 1156.36499, -1333.71423, -43.47820,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2194, 1157.06702, -1333.74585, -44.04884,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2194, 1155.90613, -1333.74585, -44.04880,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19387, 1156.49548, -1323.69507, -43.53460,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19449, 1150.09021, -1323.69702, -43.53460,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(3657, 1174.36011, -1329.02283, -44.82290,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(3657, 1174.36011, -1331.09692, -44.82290,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(3657, 1174.36011, -1333.21228, -44.82290,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2773, 1169.42297, -1317.64478, -44.74807,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2773, 1171.69397, -1317.65588, -44.74807,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2773, 1173.95789, -1317.67529, -44.74810,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2773, 1167.75281, -1317.70178, -44.74807,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(932, 1168.79993, -1316.92224, -45.28402,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(932, 1168.79626, -1317.72754, -45.28402,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(932, 1168.80029, -1318.47388, -45.28402,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19449, 1160.10242, -1316.41553, -43.53460,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19449, 1155.37683, -1308.57092, -43.53460,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2146, 1150.20959, -1310.55676, -44.88593,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2146, 1150.18469, -1313.28125, -44.88593,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(15036, 1153.24377, -1336.12036, -44.15332,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2994, 1156.03821, -1342.28552, -44.80390,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2994, 1154.22583, -1342.35107, -44.80390,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2637, 1150.66626, -1341.81287, -44.91124,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2639, 1152.38757, -1341.73608, -44.65756,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2134, 1158.38232, -1341.33875, -45.28400,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2341, 1158.38806, -1342.31018, -45.28420,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2132, 1160.38074, -1342.29651, -45.28400,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1808, 1158.14087, -1340.52881, -45.28400,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1964, 1163.95581, -1338.92383, -44.15190,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2190, 1164.21240, -1342.29810, -44.29410,   0.00000, 0.00000, 222.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2190, 1161.67725, -1338.29993, -44.29410,   0.00000, 0.00000, 4.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1715, 1161.84961, -1340.33984, -45.28400,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1715, 1162.46350, -1341.38782, -45.28400,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19325, 1174.43591, -1335.66663, -43.36420,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(3440, 1171.17017, -1335.67932, -43.11276,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19325, 1171.17017, -1341.70142, -43.36420,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(3440, 1171.17017, -1338.24036, -43.11280,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1768, 1162.06641, -1317.11865, -45.30917,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1768, 1174.99902, -1342.16565, -45.30920,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1768, 1175.75293, -1339.46497, -45.30920,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(18014, 1176.57068, -1338.02930, -44.29411,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(18014, 1176.59045, -1342.60986, -44.29610,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2011, 1175.77478, -1342.40247, -45.28350,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1814, 1173.32361, -1340.85718, -45.28356,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1808, 1173.80859, -1335.88562, -45.28352,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1808, 1168.43970, -1316.11914, -45.28380,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19449, 1172.91382, -1316.41748, -41.23349,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19325, 1171.33215, -1316.41748, -41.69795,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19325, 1177.92041, -1316.41943, -41.69800,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(16101, 1173.93445, -1316.41748, -53.15890,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(16101, 1171.69397, -1316.41748, -53.15890,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(16101, 1169.42297, -1316.41748, -53.15890,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2011, 1164.75708, -1316.84375, -45.28350,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2011, 1161.39612, -1316.84717, -45.28350,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2146, 1151.28992, -1310.53223, -44.88593,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2146, 1151.30078, -1313.17261, -44.88593,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1997, 1159.41003, -1326.91821, -45.28415,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2341, 1161.58716, -1327.98389, -45.28420,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2134, 1161.57910, -1327.01428, -45.28400,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2132, 1163.57104, -1327.96790, -45.28400,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1964, 1167.20032, -1324.59448, -44.15190,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2190, 1167.41223, -1327.56140, -44.29410,   0.00000, 0.00000, 222.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2190, 1164.45313, -1323.74353, -44.29410,   0.00000, 0.00000, 4.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1715, 1164.80518, -1325.74646, -45.28400,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1715, 1165.41968, -1326.87598, -45.28400,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1808, 1161.30652, -1326.21045, -45.28400,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1997, 1155.95654, -1310.14563, -45.28415,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(3383, 1160.25964, -1312.26855, -45.28420,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2341, 1164.33374, -1309.56396, -45.28420,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2132, 1162.34326, -1309.57849, -45.28400,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2132, 1164.31970, -1310.53235, -45.28400,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(3396, 1164.25378, -1314.72180, -45.28381,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2606, 1164.57068, -1314.40405, -42.90788,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2133, 1161.34106, -1309.57849, -45.28400,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2133, 1160.34106, -1309.55859, -45.28400,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2603, 1150.52405, -1329.01318, -44.90500,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2603, 1150.52405, -1326.89209, -44.90500,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2603, 1150.52405, -1324.85083, -44.90500,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2603, 1150.52405, -1331.16467, -44.90500,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2603, 1156.95081, -1331.16467, -44.90500,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2603, 1156.95081, -1329.01318, -44.90500,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2603, 1156.95081, -1326.89209, -44.90500,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2596, 1153.56042, -1332.75562, -42.78730,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2596, 1153.56042, -1324.10510, -42.78730,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1533, 1176.77271, -1324.46033, -45.28440,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1533, 1176.75879, -1325.93994, -45.28440,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1886, 1168.35229, -1325.56812, -41.27430,   16.00000, 0.00000, 91.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1886, 1159.97913, -1336.37793, -41.27430,   16.00000, 0.00000, 76.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1886, 1159.81689, -1318.24207, -41.27430,   16.00000, 0.00000, 76.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1886, 1159.07678, -1322.03967, -41.27430,   16.00000, 0.00000, -92.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2251, 1173.69666, -1340.36841, -43.93500,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1523, 1165.68774, -1316.44983, -45.28395,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1523, 1167.52722, -1313.90332, -45.28390,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1523, 1168.58203, -1309.85535, -45.28390,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1523, 1165.88379, -1309.00793, -45.28390,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1523, 1169.75037, -1305.09265, -45.06800,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(14401, 1162.85083, -1305.62805, -45.06398,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(14401, 1154.72559, -1293.60718, -45.06600,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1523, 1155.35254, -1313.38110, -45.28390,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1523, 1155.70508, -1323.71460, -45.28395,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1523, 1155.41321, -1316.41907, -45.28390,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19449, 1155.37292, -1311.68201, -40.99503,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19449, 1160.10425, -1316.41345, -40.46843,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19357, 1164.94409, -1311.68689, -40.99500,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19357, 1164.94409, -1314.89624, -40.99500,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(19357, 1164.94604, -1310.40588, -40.99500,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1533, 1149.42163, -1321.80444, -45.28440,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1533, 1149.42529, -1320.36682, -45.28440,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2774, 1148.82996, -1318.05542, -45.20044,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(2774, 1148.82996, -1322.63342, -45.20040,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1502, 1159.09863, -1334.08252, -45.28405,   0.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1502, 1166.81567, -1334.04443, -45.28400,   0.00000, 0.00000, 180.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(18072, 1176.72900, -1325.89856, -43.19949,   0.00000, 0.00000, 90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1651, 1167.81055, -1304.27026, -43.44110,   0.00000, 0.00000, -90.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1749, 1174.62805, -1316.42896, -41.56990,   14.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1749, 1171.32861, -1316.42896, -41.56990,   14.00000, 0.00000, 0.00000, .interiorid=0, .streamdistance=100.0);


	// ============================== DP ============================== //

	CreateDynamicObject(3749, 1551.30005, -1627.50000, 18.20000,  	 0.00000, 0.00000, 90.00000, .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(987, 1539.59998, -1617.69995, 15.90000,   	0.00000, 0.00000, 0.00000,   .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(987, 1539.69995, -1605.69995, 15.90000,   	0.00000, 0.00000, 270.00000, .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(987, 1539.80005, -1602.50000, 15.90000,   	0.00000, 0.00000, 270.00000, .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(987, 1551.80005, -1602.59998, 15.90000,   	0.00000, 0.00000, 180.00000, .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(987, 1563.80005, -1602.59998, 15.90000,   	0.00000, 0.00000, 179.99500, .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(987, 1575.69995, -1602.59998, 15.90000,   	0.00000, 0.00000, 179.99500, .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(987, 1587.69995, -1602.59998, 15.90000,   	0.00000, 0.00000, 179.99500, .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(987, 1599.59998, -1602.59998, 15.90000,   	0.00000, 0.00000, 179.99500, .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(987, 1608.00000, -1602.30005, 15.90000,   	0.00000, 0.00000, 179.99500, .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(987, 1608.09998, -1614.30005, 15.90000,   	0.00000, 0.00000, 90.00000,  .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(987, 1608.00000, -1626.19995, 15.90000,   	0.00000, 0.00000, 90.00000,  .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(987, 1607.90002, -1638.09998, 15.90000,   	0.00000, 0.00000, 90.00000,  .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(987, 1596.00000, -1638.09998, 15.90000,   	0.00000, 0.00000, 0.00000,   .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(987, 1577.40002, -1638.69995, 18.90000,   	0.00000, 0.00000, 0.00000,   .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(987, 1586.40002, -1638.09998, 19.10000,   	0.00000, 0.00000, 0.00000,   .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(973, 1537.69995, -1668.50000, 13.40000,   	0.00000, 0.00000, 90.00000,  .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(973, 1537.69995, -1676.90002, 13.40000,   	0.00000, 0.00000, 90.00000,  .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(3279, 1545.09998, -1610.19995, 12.40000,   	0.00000, 0.00000, 0.00000,   .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1257, 1521.09998, -1699.80005, 13.80000,   	0.00000, 0.00000, 181.00000, .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(1257, 1521.00000, -1677.40002, 13.80000,   	0.00000, 0.00000, 181.00000, .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(3934, 1564.69995, -1650.80005, 27.40000,   	0.00000, 0.00000, 0.00000,   .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(3934, 1565.09998, -1699.09998, 27.40000,   	0.00000, 0.00000, 0.00000,   .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(978, 1561.18750, -1623.02209, 13.35273,   	0.00000, 0.00000, 180.82135, .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(978, 1570.47144, -1622.88025, 13.35270,   	0.00000, 0.00000, 180.82140, .worldid=0, .interiorid=0, .streamdistance=100.0);
	
	GATE_DP = CreateDynamicObject(980, 1551.19995, -1627.59998, 15.20000,   	0.00000, 0.00000, 90.00000,  .worldid=0, .interiorid=0, .streamdistance=100.0);

	// Celas
	CreateDynamicObject(2930, 266.37369, 83.83120, 1002.66089,   	0.00000, 0.00000, 0.00000,   .interiorid=6, .streamdistance=100.0);
	CreateDynamicObject(2930, 266.38519, 88.33640, 1002.66089,   	0.00000, 0.00000, 0.00000,   .interiorid=6, .streamdistance=100.0);

	// ============================================= [ BARCO DE BAYSIDE] ============================================= //
	CreateDynamicObject(10230, -2195.73706, 2457.47998, 7.04280,   0.00000, 0.00000, 139.69603, .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(10231, -2194.02539, 2457.95508, 8.34390,   0.00000, 0.00000, 139.74190, .worldid=0, .interiorid=0, .streamdistance=100.0);
	CreateDynamicObject(10229, -2194.66650, 2458.38647, 5.77210,   0.02000, 0.10000, 139.58875, .worldid=0, .interiorid=0, .streamdistance=100.0);

	// ===================================== NOVA FAZENDA =========================================== //
	CreateDynamicObject(3578, 2074.01563, -2099.44531, 13.32031,   3.14159, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3749, 664.92188, -1309.35156, 18.36719,   356.85840, 0.00000, 3.14159, .streamdistance=100.0);
	CreateDynamicObject(12911, -61.45313, -36.99219, 1.97656,   356.85840, 0.00000, -3.04517, .streamdistance=100.0);
	CreateDynamicObject(3374, -50.01563, 3.17969, 3.47656,   3.14159, 0.00000, 1.99840, .streamdistance=100.0);
	CreateDynamicObject(12913, -21.94531, 101.39063, 4.53125,   3.14159, 0.00000, 3.10449, .streamdistance=100.0);
	CreateDynamicObject(12911, -1096.05554, -1272.48438, 128.19560,   0.00000, 0.00000, 459.07001, .streamdistance=100.0);
	CreateDynamicObject(3749, -1049.17712, -1313.63074, 134.39700,   0.00000, 0.00000, -1.98000, .streamdistance=100.0);
	CreateDynamicObject(3276, -1064.52808, -1313.52332, 129.06250,   356.85840, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3276, -1034.51733, -1315.04626, 129.06250,   356.85840, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(17324, -1054.22278, -1083.60107, 128.19920,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3374, -1030.44165, -1139.53064, 129.62050,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3374, -1030.47192, -1145.07361, 129.62050,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3374, -1025.52795, -1139.48328, 129.62050,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3374, -1025.67566, -1145.02747, 129.62050,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(12913, -1026.08496, -1174.70203, 128.21344,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(12913, -1033.17664, -1174.73364, 128.21344,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(17054, -1440.81250, -1460.10938, 100.63281,   356.85840, 0.00000, 3.14159, .streamdistance=100.0);
	CreateDynamicObject(17054, -1198.76978, -1183.35181, 128.19800,   0.00000, 0.00000, 1.00000, .streamdistance=100.0);
	CreateDynamicObject(705, -1109.28992, -1090.51794, 128.19928,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(17054, -1198.76978, -1183.35181, 128.19800,   0.00000, 0.00000, 1.00000, .streamdistance=100.0);
	CreateDynamicObject(705, -1168.84863, -1087.31665, 128.20830,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3374, -1169.85266, -1131.83459, 129.54710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3374, -1163.89587, -1131.85864, 129.54710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3374, -1169.97437, -1137.98193, 129.54710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3374, -1170.10498, -1144.57910, 129.54710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3374, -1163.75256, -1144.50720, 129.54710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3374, -1163.82605, -1138.02246, 129.54710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3261, -1136.62500, -1134.50781, 128.21875,   3.14159, 0.00000, 1.57080, .streamdistance=100.0);
	CreateDynamicObject(3252, -442.32813, 606.42188, 14.72656,   356.88345, -0.06517, -0.36734, .streamdistance=100.0);
	CreateDynamicObject(3425, -1200.81091, -1213.80859, 139.12030,   0.00000, 0.00000, -0.02000, .streamdistance=100.0);
	CreateDynamicObject(16442, -1137.55176, -1215.00244, 129.64690,   0.76000, 0.00000, 34.00000, .streamdistance=100.0);
	CreateDynamicObject(16442, -1151.12756, -1184.44312, 129.64690,   0.00000, 3.28000, 456.00000, .streamdistance=100.0);
	CreateDynamicObject(16442, -1111.90735, -1169.21228, 129.64690,   0.76000, 0.00000, 7089.00000, .streamdistance=100.0);
	CreateDynamicObject(16442, -1176.07312, -1204.71118, 129.64690,   0.00000, 3.28000, 584.00000, .streamdistance=100.0);
	CreateDynamicObject(16442, -1137.13940, -1172.07446, 129.64690,   0.76000, 0.00000, 664.00000, .streamdistance=100.0);
	CreateDynamicObject(16442, -1131.53918, -1192.81018, 129.64690,   0.76000, 0.00000, 4454.00000, .streamdistance=100.0);
	CreateDynamicObject(16442, -1169.10510, -1165.06274, 129.64690,   0.00000, 3.28000, 45876.00000, .streamdistance=100.0);
	CreateDynamicObject(16442, -1148.85559, -1199.52625, 129.64690,   0.00000, 3.28000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(708, -1095.41492, -1156.68701, 128.00781,   356.85840, 0.00000, 3.14159, .streamdistance=100.0);
	CreateDynamicObject(790, -1143.75781, -1228.93750, 130.71094,   3.14159, 0.00000, 1.72113, .streamdistance=100.0);
	CreateDynamicObject(12913, -1198.20447, -1162.57117, 128.20274,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3252, -1207.34204, -1156.44238, 128.20087,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3276, -1088.40625, -1249.92188, 129.06250,   3.14159, 0.00000, 1.57080, .streamdistance=100.0);
	CreateDynamicObject(3276, -1088.40625, -1249.92188, 129.06250,   3.14159, 0.00000, 1.57080, .streamdistance=100.0);
	CreateDynamicObject(3276, -1088.40625, -1249.92188, 129.06250,   3.14159, 0.00000, 1.57080, .streamdistance=100.0);
	CreateDynamicObject(3276, -1088.46301, -1225.49597, 129.06250,   3.14159, 0.00000, 1.57080, .streamdistance=100.0);
	CreateDynamicObject(691, -1037.38647, -1346.82080, 128.35156,   3.14159, 0.00000, 1.39626, .streamdistance=100.0);
	CreateDynamicObject(705, -1092.02209, -1309.64050, 128.07031,   3.14159, 0.00000, 0.95120, .streamdistance=100.0);
	CreateDynamicObject(691, -1010.20313, -1146.10156, 127.84375,   356.85840, 0.00000, 3.14159, .streamdistance=100.0);
	CreateDynamicObject(691, -1029.77222, -1157.81396, 127.84375,   356.85840, 0.00000, 3.14159, .streamdistance=100.0);
	CreateDynamicObject(1438, -1071.85657, -1159.87024, 128.22505,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1438, -1068.28564, -1159.83484, 128.22505,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1451, -1067.01062, -1215.52795, 129.01210,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1451, -1062.86548, -1215.42102, 129.01210,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(691, -1070.15283, -1330.92773, 128.09160,   3.14160, 0.00000, 1.39630, .streamdistance=100.0);
	// ================================== HQ ADVOGADO ================================================ //
	CreateDynamicObject(1343, 942.49841, 1727.76184, 8.60270,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(2690, 941.21802, 1738.58276, 9.93060,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(19859, 936.75427, 1734.74573, 9.04210,   0.00000, 0.00000, -90.00000, .streamdistance=100.0);
	CreateDynamicObject(19859, 936.71210, 1731.73572, 9.04210,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(1415, 987.22510, 1734.08447, 7.80080,   0.00000, 0.00000, -90.00000, .streamdistance=100.0);
	CreateDynamicObject(1415, 987.34161, 1740.18811, 7.80080,   0.00000, 0.00000, -90.00000, .streamdistance=100.0);
	CreateDynamicObject(1415, 987.27899, 1736.95850, 7.80080,   0.00000, 0.00000, -90.00000, .streamdistance=100.0);
	CreateDynamicObject(3035, 984.40662, 1718.49414, 8.27980,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	// ================================== HQ PLANTADOR ================================================= //
	CreateDynamicObject(3261,-1463.8000000,-1454.1000000,100.7000000,0.0000000,0.0000000,1.5000000, .streamdistance=100.0); //object(grasshouse) (0)
	CreateDynamicObject(3261,-1463.7000000,-1458.7000000,100.7000000,0.0000000,0.0000000,1.5000000, .streamdistance=100.0); //object(grasshouse) (1)
	CreateDynamicObject(3261,-1463.6000000,-1463.3000000,100.7000000,0.0000000,0.0000000,1.5000000, .streamdistance=100.0); //object(grasshouse) (2)
	CreateDynamicObject(3261,-1458.5000000,-1463.2000000,100.6000000,0.0000000,0.0000000,1.5000000, .streamdistance=100.0); //object(grasshouse) (3)
	CreateDynamicObject(3261,-1458.5000000,-1458.6000000,100.7000000,0.0000000,0.0000000,1.5000000, .streamdistance=100.0); //object(grasshouse) (4)
	CreateDynamicObject(3261,-1458.7000000,-1453.9000000,100.7000000,0.0000000,0.0000000,1.5000000, .streamdistance=100.0); //object(grasshouse) (5)
	CreateDynamicObject(3261,-1453.5000000,-1463.1000000,100.7000000,0.0000000,0.0000000,1.5000000, .streamdistance=100.0); //object(grasshouse) (6)
	CreateDynamicObject(3261,-1453.4000000,-1458.5000000,100.7000000,0.0000000,0.0000000,1.5000000, .streamdistance=100.0); //object(grasshouse) (7)
	CreateDynamicObject(3261,-1453.6000000,-1453.8000000,100.7000000,0.0000000,0.0000000,1.5000000, .streamdistance=100.0); //object(grasshouse) (8)
	// ================================== [ AREA DE DM ] =============================================== //
	// ================================= REFINARIA DE DRGOAS =========================================== //
	CreateDynamicObject(935, -2150.04443, -223.15434, 34.86920,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(935, -2149.00781, -224.42461, 34.86920,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(935, -2147.79688, -224.97913, 34.86920,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(935, -2150.51880, -224.38878, 34.86920,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(935, -2146.72412, -223.57808, 34.86920,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(935, -2148.48218, -223.23169, 34.86920,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(11014, -2101.98145, -241.89584, 36.96880,   356.85840, 0.00000, -90.00000, .streamdistance=100.0);
	CreateDynamicObject(10843, -2135.82373, -128.00314, 42.21570,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(11010, -2113.34888, -206.27513, 40.28125,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(11010, -2113.32031, -186.79688, 40.28125,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(11010, -2113.35156, -166.92224, 40.28125,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3749, -2127.20898, -80.82556, 39.94030,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1497, -2128.36206, -161.68700, 34.33970,   0.00000, 0.00000, -90.00000, .streamdistance=100.0);
	CreateDynamicObject(1497, -2128.28735, -164.66084, 34.33970,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(1497, -2128.31714, -201.60367, 34.33970,   0.00000, 0.00000, -90.00000, .streamdistance=100.0);
	CreateDynamicObject(1497, -2128.29199, -204.56973, 34.33970,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(1497, -2128.31128, -182.12474, 34.33970,   0.00000, 0.00000, -90.00000, .streamdistance=100.0);
	CreateDynamicObject(1497, -2128.29248, -185.13986, 34.33970,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(1497, -2163.59644, -225.51622, 35.51302,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(11081, -2150.94922, -154.91223, 41.00000,   356.85840, 0.00000, -90.00000, .streamdistance=100.0);
	CreateDynamicObject(939, -2183.76978, -224.35570, 37.96094,   356.85840, 0.00000, 3.14159, .streamdistance=100.0);
	CreateDynamicObject(944, -2181.84351, -213.95190, 36.39840,   356.85840, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(935, -2188.00830, -208.94841, 36.07410,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(935, -2189.00732, -206.26363, 36.07410,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(935, -2188.89453, -207.89392, 36.07410,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(935, -2187.34888, -205.71835, 36.07410,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(935, -2187.82788, -206.62956, 36.07410,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(935, -2187.90649, -207.64633, 36.07410,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(935, -2186.22339, -205.61081, 36.07410,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(935, -2189.47046, -209.03651, 36.07410,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3577, -2188.52197, -213.54533, 36.21640,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(919, -2116.31470, -209.83148, 45.72710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(919, -2108.83398, -204.26289, 45.72710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(919, -2108.71680, -210.10423, 45.72710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(919, -2107.71655, -189.23820, 45.72710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(919, -2116.43604, -204.25934, 45.72710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(919, -2115.16187, -189.58334, 45.72710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(919, -2115.16626, -184.22607, 45.72710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(919, -2113.03052, -163.46559, 45.72710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(919, -2107.74683, -184.47215, 45.72710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(919, -2113.26514, -169.64857, 45.72710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(919, -2105.92798, -163.52872, 45.72710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(919, -2106.47241, -169.80301, 45.72710,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1596, -2124.19507, -210.16850, 47.85490,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1596, -2124.57007, -170.47441, 47.85490,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1596, -2124.37671, -190.95323, 47.85490,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	// ================================= SPAWN NOVATOS ================================================= //
	BikesNovatos[0] = CreateVehicle(510, 1698.5206, -1872.5115, 13.1821, 90.0000, -1, -1, 100);
	BikesNovatos[1] = CreateVehicle(510, 1698.3521, -1880.9784, 13.1821, 90.0000, -1, -1, 100);
	BikesNovatos[2] = CreateVehicle(510, 1698.5140, -1873.2240, 13.1821, 90.0000, -1, -1, 100);
	BikesNovatos[3] = CreateVehicle(510, 1698.5107, -1873.9725, 13.1821, 90.0000, -1, -1, 100);
	BikesNovatos[4] = CreateVehicle(510, 1698.4762, -1874.7981, 13.1821, 90.0000, -1, -1, 100);
	BikesNovatos[5] = CreateVehicle(510, 1698.4767, -1875.5680, 13.1821, 90.0000, -1, -1, 100);
	BikesNovatos[6] = CreateVehicle(510, 1698.4534, -1876.2913, 13.1821, 90.0000, -1, -1, 100);
	BikesNovatos[7] = CreateVehicle(510, 1698.4061, -1876.9756, 13.1821, 90.0000, -1, -1, 100);
	BikesNovatos[8] = CreateVehicle(510, 1698.4225, -1877.6331, 13.1821, 90.0000, -1, -1, 100);
	BikesNovatos[9] = CreateVehicle(510, 1698.4613, -1878.3174, 13.1821, 90.0000, -1, -1, 100);
	BikesNovatos[10] = CreateVehicle(510, 1698.4469, -1879.1141, 13.1821, 90.0000, -1, -1, 100);
	BikesNovatos[11] = CreateVehicle(510, 1698.3868, -1880.0092, 13.1821, 90.0000, -1, -1, 100);
	// ========================================================================================================================================== //


	// ================================= Shopping ================================================= //
	new tmpobjid;
    tmpobjid = CreateDynamicObject(19357, 1832.119384, -1286.569213, 22.966100, 0.000000, 0.000000, 273.877990, -1, -1, -1, 100.00, 100.00);
    SetDynamicObjectMaterial(tmpobjid, 0, 19597, "lsbeachside", "wall7-256x256", 0x00000000);
    tmpobjid = CreateDynamicObject(19387, 1828.915161, -1286.789184, 22.966100, 0.000000, 0.000000, 273.877990, -1, -1, -1, 100.00, 100.00);
    SetDynamicObjectMaterial(tmpobjid, 0, 19597, "lsbeachside", "wall7-256x256", 0x00000000);
    tmpobjid = CreateDynamicObject(19357, 1825.714843, -1287.009155, 22.966100, 0.000000, 0.000000, 273.877990, -1, -1, -1, 100.00, 100.00);
    SetDynamicObjectMaterial(tmpobjid, 0, 19597, "lsbeachside", "wall7-256x256", 0x00000000);
    tmpobjid = CreateDynamicObject(19430, 1834.522949, -1286.400024, 22.966100, 0.000000, 0.000000, 273.877990, -1, -1, -1, 100.00, 100.00);
    SetDynamicObjectMaterial(tmpobjid, 0, 19597, "lsbeachside", "wall7-256x256", 0x00000000);
    tmpobjid = CreateDynamicObject(19430, 1834.942749, -1286.400024, 22.966100, 0.000000, 0.000000, 273.877990, -1, -1, -1, 100.00, 100.00);
    SetDynamicObjectMaterial(tmpobjid, 0, 19597, "lsbeachside", "wall7-256x256", 0x00000000);
    tmpobjid = CreateDynamicObject(19357, 1831.068969, -1294.834960, 22.966100, 0.000000, 0.000000, 272.858001, -1, -1, -1, 100.00, 100.00);
    SetDynamicObjectMaterial(tmpobjid, 0, 19597, "lsbeachside", "wall7-256x256", 0x00000000);
    tmpobjid = CreateDynamicObject(19430, 1828.257324, -1294.950805, 22.966100, 0.000000, 0.000000, 273.877990, -1, -1, -1, 100.00, 100.00);
    SetDynamicObjectMaterial(tmpobjid, 0, 19597, "lsbeachside", "wall7-256x256", 0x00000000);
    tmpobjid = CreateDynamicObject(19357, 1825.855102, -1295.097045, 22.966100, 0.000000, 0.000000, 272.858001, -1, -1, -1, 100.00, 100.00);
    SetDynamicObjectMaterial(tmpobjid, 0, 19597, "lsbeachside", "wall7-256x256", 0x00000000);
    tmpobjid = CreateDynamicObject(19357, 1834.148559, -1294.690307, 22.966100, 0.000000, 0.000000, 272.858001, -1, -1, -1, 100.00, 100.00);
    SetDynamicObjectMaterial(tmpobjid, 0, 19597, "lsbeachside", "wall7-256x256", 0x00000000);
    tmpobjid = CreateDynamicObject(19430, 1829.895751, -1287.621093, 22.966100, 0.000000, 0.000000, 363.338104, -1, -1, -1, 100.00, 100.00);
    SetDynamicObjectMaterial(tmpobjid, 0, 19597, "lsbeachside", "wall7-256x256", 0x00000000);
    tmpobjid = CreateDynamicObject(1502, 1828.156982, -1286.869140, 21.208299, 0.000000, 0.000000, 5.000000, -1, -1, -1, 100.00, 100.00);
    tmpobjid = CreateDynamicObject(2167, 1824.729248, -1295.117187, 21.201410, 0.000000, 0.000000, -175.079910, -1, -1, -1, 100.00, 100.00);
    tmpobjid = CreateDynamicObject(2165, 1828.302612, -1293.118164, 21.204969, 0.000000, 0.000000, 270.120178, -1, -1, -1, 100.00, 100.00);
    tmpobjid = CreateDynamicObject(1806, 1827.096435, -1293.739868, 21.201190, 0.000000, 0.000000, -92.279991, -1, -1, -1, 100.00, 100.00);
    tmpobjid = CreateDynamicObject(1806, 1829.679321, -1293.752563, 21.201190, 0.000000, 0.000000, 90.240028, -1, -1, -1, 100.00, 100.00);
    tmpobjid = CreateDynamicObject(2165, 1832.513916, -1293.760742, 21.204969, 0.000000, 0.000000, 90.060188, -1, -1, -1, 100.00, 100.00);
    tmpobjid = CreateDynamicObject(1806, 1833.808349, -1292.717041, 21.201190, 0.000000, 0.000000, 90.240028, -1, -1, -1, 100.00, 100.00);
    tmpobjid = CreateDynamicObject(1806, 1831.120483, -1292.842895, 21.201190, 0.000000, 0.000000, -92.279991, -1, -1, -1, 100.00, 100.00);

    tmpobjid = CreateDynamicObject(2165, 1831.879150, -1288.343139, 21.204969, 0.000000, 0.000000, 183.300170, -1, -1, -1, 100.00, 100.00);
    tmpobjid = CreateDynamicObject(1806, 1831.100463, -1286.905395, 21.201190, 0.000000, 0.000000, -178.020034, -1, -1, -1, 100.00, 100.00);
    tmpobjid = CreateDynamicObject(1806, 1831.679077, -1289.669433, 21.201190, 0.000000, 0.000000, 6.419980, -1, -1, -1, 100.00, 100.00);

    // =========================================== [Hq Mec�nico ] ============================================ //
    CreateDynamicObject(3036, 1569.34424, -2156.86060, 12.96720,   -90.00000, 0.00000, 90.00000);

	CreateDynamicObject(8947, 1571.63965, -2162.97534, 15.60000,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1728, 1577.93616, -2172.75098, 12.54310,   0.00000, 0.00000, 200.00000, .streamdistance=100.0);
	CreateDynamicObject(2236, 1578.56384, -2171.38745, 12.54770,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(2855, 1578.07556, -2170.70435, 12.61150,   0.00000, 0.00000, -70.00000, .streamdistance=100.0);
	CreateDynamicObject(2103, 1578.04272, -2170.91895, 13.05070,   0.00000, 0.00000, -90.00000, .streamdistance=100.0);
	CreateDynamicObject(941, 1565.45959, -2162.12720, 13.02000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(18633, 1565.68518, -2162.37158, 13.51180,   0.00000, 90.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(18635, 1565.03613, -2162.54321, 13.47400,   90.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(18634, 1565.62012, -2162.65918, 13.51390,   0.00000, 90.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(2601, 1565.02441, -2161.81641, 13.57430,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(2601, 1564.95422, -2162.94922, 13.57430,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(2601, 1565.21777, -2162.71875, 13.57430,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1650, 1565.66016, -2160.63110, 12.88000,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1650, 1565.26038, -2160.61182, 12.88000,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1650, 1565.96277, -2173.00146, 12.86000,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(18452, 1540.91431, -2173.55005, 15.36000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(1686, 1543.77039, -2173.57715, 12.60804,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1686, 1538.17444, -2173.56787, 12.60804,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(10281, 1571.83936, -2176.12622, 17.62310,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(19378, 1559.01648, -2170.40479, 12.47840,   0.00000, 90.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(19385, 1558.58191, -2175.13892, 14.30000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(19355, 1555.37634, -2175.14063, 14.30000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(19355, 1564.98865, -2175.14478, 14.30000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(19401, 1553.83740, -2173.62354, 14.30000,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(19355, 1553.84778, -2170.41187, 14.30000,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(19355, 1553.84070, -2167.20605, 14.30000,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(19355, 1555.37317, -2165.65088, 14.30000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(19355, 1558.58630, -2165.66138, 14.30000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(19385, 1561.79492, -2165.67285, 14.30000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(19428, 1563.92029, -2165.66943, 14.30000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(19466, 1553.82910, -2173.85303, 14.62000,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(2691, 1555.47705, -2175.03760, 14.57500,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(19355, 1562.65356, -2168.88916, 14.30000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(19428, 1560.26184, -2168.88110, 14.30000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(1806, 1554.31018, -2174.46118, 12.56480,   0.00000, 0.00000, -30.00000, .streamdistance=100.0);
	CreateDynamicObject(19385, 1559.51440, -2167.35986, 14.30000,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(2485, 1560.99341, -2169.37939, 13.64870,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(2655, 1553.98218, -2168.52930, 13.99920,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(2060, 1556.29822, -2166.04321, 12.66500,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(2167, 1561.02869, -2168.77222, 12.54630,   0.00000, 0.00000, 180.00000, .streamdistance=100.0);
	CreateDynamicObject(1616, 1563.83838, -2174.67114, 15.73108,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1616, 1554.35144, -2166.15063, 15.73110,   0.00000, 0.00000, 180.00000, .streamdistance=100.0);
	CreateDynamicObject(1885, 1559.81201, -2174.80225, 12.56631,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(2364, 1560.13660, -2175.53857, 12.54774,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3633, 1565.37878, -2164.14990, 13.02560,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(2049, 1563.46277, -2174.99854, 13.77900,   0.00000, 0.00000, 180.00000, .streamdistance=100.0);
	CreateDynamicObject(1650, 1560.31885, -2169.30225, 12.94000,   0.00000, 0.00000, 10.00000, .streamdistance=100.0);
	CreateDynamicObject(1650, 1560.10718, -2169.55103, 12.92000,   0.00000, 0.00000, -20.00000, .streamdistance=100.0);
	CreateDynamicObject(1650, 1559.85999, -2169.25513, 12.92000,   0.00000, 0.00000, 9.00000, .streamdistance=100.0);
	CreateDynamicObject(2385, 1553.90295, -2170.85010, 12.56350,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(1280, 1555.68579, -2175.58765, 12.94730,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(18635, 1553.99207, -2170.91650, 13.47400,   90.00000, 0.00000, 30.00000, .streamdistance=100.0);
	CreateDynamicObject(18635, 1554.03491, -2169.42310, 13.47400,   90.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(18633, 1554.21985, -2170.35400, 12.63550,   0.00000, 90.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(18638, 1562.58289, -2169.20581, 13.68020,   0.00000, -90.00000, 230.00000, .streamdistance=100.0);
	CreateDynamicObject(18645, 1561.66614, -2169.16968, 13.75090,   0.00000, 0.00000, -65.00000, .streamdistance=100.0);
	CreateDynamicObject(18644, 1554.07178, -2170.35229, 13.50660,   90.00000, 0.00000, -30.00000, .streamdistance=100.0);
	CreateDynamicObject(18641, 1561.59155, -2169.65430, 13.35050,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(18641, 1561.79419, -2169.55737, 13.29050,   -90.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(18641, 1562.11182, -2169.55249, 13.29050,   -90.00000, 0.00000, 80.00000, .streamdistance=100.0);
	CreateDynamicObject(1744, 1556.47192, -2165.61768, 13.91725,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(19330, 1563.17224, -2169.24194, 13.71080,   0.00000, -90.00000, 140.00000, .streamdistance=100.0);
	CreateDynamicObject(2690, 1554.17993, -2168.87842, 12.92650,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(2690, 1563.99597, -2172.39795, 12.92650,   0.00000, 0.00000, -90.00000, .streamdistance=100.0);
	CreateDynamicObject(19308, 1556.50696, -2165.95850, 14.39660,   0.00000, 0.00000, -90.00000, .streamdistance=100.0);
	CreateDynamicObject(19311, 1557.42151, -2165.96753, 14.39670,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(19377, 1559.02393, -2170.40381, 16.00000,   0.00000, 90.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(19401, 1561.78467, -2175.15186, 14.30000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(19466, 1561.67822, -2175.13867, 14.62000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(2714, 1558.58203, -2175.32129, 15.49450,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1523, 1557.77881, -2175.18066, 12.52300,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(19314, 1559.36047, -2167.35596, 15.26000,   90.00000, 90.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(3633, 1565.98596, -2166.01709, 13.20560,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(2893, 1570.32886, -2162.50366, 11.70000,   -30.00000, 0.00000, 180.00000, .streamdistance=100.0);
	CreateDynamicObject(2893, 1568.38831, -2162.50391, 11.70000,   -30.00000, 0.00000, 180.00000, .streamdistance=100.0);
	CreateDynamicObject(1002, 1565.47888, -2164.14819, 13.49540,   0.00000, 0.00000, -70.00000, .streamdistance=100.0);
	CreateDynamicObject(19371, 1573.78552, -2152.67773, 14.30000,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(19417, 1573.78503, -2155.86938, 14.30000,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(19398, 1575.30811, -2157.56323, 14.30000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(19444, 1577.70020, -2157.55103, 14.30000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(2972, 1565.40039, -2152.26099, 12.56550,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(2972, 1566.70093, -2152.24854, 12.56550,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(2972, 1566.01953, -2152.23999, 13.74550,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(2605, 1577.46045, -2153.07178, 12.94750,   0.00000, 0.00000, 180.00000, .streamdistance=100.0);
	CreateDynamicObject(2164, 1573.88403, -2152.45703, 12.54720,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(2309, 1577.86292, -2154.33716, 12.54820,   0.00000, 0.00000, 40.00000, .streamdistance=100.0);
	CreateDynamicObject(19466, 1573.77002, -2155.99707, 15.06120,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1712, 1578.05420, -2156.29663, 12.52790,   0.00000, 0.00000, -155.00000, .streamdistance=100.0);
	CreateDynamicObject(1897, 1571.05762, -2156.72192, 13.68000,   0.00000, 0.00000, -90.00000, .streamdistance=100.0);
	CreateDynamicObject(1897, 1571.06128, -2152.12695, 13.68000,   0.00000, 0.00000, -90.00000, .streamdistance=100.0);
	CreateDynamicObject(1897, 1567.62561, -2156.76196, 13.68000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(1897, 1567.61914, -2152.15674, 13.68000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(2254, 1577.25098, -2157.42871, 14.27690,   0.00000, 0.00000, 180.00000, .streamdistance=100.0);
	CreateDynamicObject(2255, 1565.20862, -2153.97485, 13.72360,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(1244, 1571.91089, -2152.59009, 13.36000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(1238, 1573.06030, -2152.67847, 12.86580,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1238, 1573.08105, -2152.65918, 12.98580,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1238, 1573.08093, -2152.65894, 13.16580,   0.00000, 0.00000, 30.00000, .streamdistance=100.0);
	CreateDynamicObject(19371, 1575.46484, -2155.86499, 15.96800,   0.00000, 90.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(19371, 1575.46973, -2152.65698, 15.96800,   0.00000, 90.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(19444, 1577.71338, -2155.87573, 15.97600,   0.00000, 90.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(19444, 1577.71460, -2152.44751, 15.97530,   0.00000, 90.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(19121, 1576.18994, -2150.96045, 14.55500,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1499, 1574.53870, -2157.57104, 12.53760,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2074, 1576.42334, -2154.66016, 15.63650,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2069, 1578.33411, -2157.21631, 12.58890,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1893, 1568.18396, -2170.43237, 18.18831,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1893, 1568.23901, -2168.24756, 18.18831,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1893, 1568.44702, -2165.10059, 18.18831,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1893, 1573.07654, -2171.14331, 18.18831,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1893, 1573.18091, -2168.98193, 18.18831,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1893, 1573.20532, -2166.90137, 18.18831,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3785, 1564.85400, -2161.88745, 14.22000,   90.00000, 0.00000, 0.00000);
	CreateDynamicObject(8948, 1571.63501, -2174.02148, 16.18000,   0.00000, 60.00000, 90.00000);
	CreateDynamicObject(3504, 1563.63269, -2164.66235, 13.90000,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(2607, 1562.86597, -2168.40747, 12.96000,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1806, 1563.24438, -2167.39795, 12.56660,   0.00000, 0.00000, 140.00000);
	CreateDynamicObject(2611, 1564.19165, -2167.41528, 14.03160,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(2368, 1561.03723, -2169.32178, 12.56530,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1744, 1553.79956, -2170.73755, 13.15090,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(1744, 1553.86499, -2170.73682, 12.73090,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(1744, 1553.86499, -2170.73682, 12.29090,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(2601, 1554.19006, -2171.12451, 13.17040,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2601, 1554.17151, -2170.81982, 13.17040,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(2601, 1554.32166, -2170.90063, 13.17040,   0.00000, 0.00000, 199.00000);
	CreateDynamicObject(2583, 1554.20886, -2167.15063, 13.44000,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(2060, 1557.45947, -2166.02246, 12.66500,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2060, 1556.86084, -2165.96167, 12.90500,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2010, 1563.82642, -2174.45264, 12.56545,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1808, 1563.85522, -2169.43726, 12.56622,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2267, 1554.01575, -2170.21313, 14.57010,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(2626, 1555.65527, -2173.88574, 13.08600,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(2059, 1554.57422, -2167.13086, 12.58460,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(2059, 1554.23389, -2166.72266, 12.58460,   0.00000, 0.00000, 6.00000);
	CreateDynamicObject(617, 1518.21594, -2163.69849, 12.53791,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(19124, 1537.98999, -2176.60645, 13.16720,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(19124, 1543.71545, -2176.59058, 13.16720,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1616, 1563.83398, -2151.56592, 18.22810,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1616, 1553.33130, -2166.39673, 15.99080,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1623, 1564.22986, -2153.22168, 18.00000,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1623, 1564.21362, -2155.88379, 18.00000,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2699, 1559.36987, -2172.04980, 13.20000,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2690, 1543.87061, -2172.72217, 12.96000,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(2690, 1538.28040, -2172.11206, 12.96000,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(7313, 1569.35132, -2151.06958, 16.38790,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2654, 1578.26660, -2158.43726, 12.76390,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(930, 1565.22961, -2172.53809, 13.04000,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(943, 1566.41943, -2173.76587, 13.32540,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(1650, 1565.83154, -2172.72803, 12.86000,   0.00000, 0.00000, 60.00000);
	CreateDynamicObject(2893, 1568.39294, -2156.87036, 13.04000,   80.00000, 0.00000, 180.00000);
	CreateDynamicObject(2893, 1570.33276, -2156.86255, 13.04000,   80.00000, 0.00000, 180.00000);
	CreateDynamicObject(1744, 1575.43970, -2150.92310, 14.00260,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2163, 1573.89050, -2154.75757, 12.57370,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(1549, 1574.17236, -2153.23145, 12.57360,   0.00000, 0.00000, 60.00000);
	CreateDynamicObject(1786, 1573.89612, -2154.50342, 13.49750,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(1671, 1577.74524, -2151.70923, 13.03380,   0.00000, 0.00000, -45.00000);
	CreateDynamicObject(2855, 1575.27625, -2151.28882, 14.34180,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(18638, 1575.66113, -2151.15601, 14.40190,   -5.00000, -90.00000, 60.00000);
	CreateDynamicObject(18631, 1573.70642, -2155.95776, 15.46850,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(19171, 1578.54956, -2166.03735, 15.72090,   90.00000, -90.00000, 0.00000);
	CreateDynamicObject(19170, 1578.54504, -2164.54541, 15.72730,   90.00000, -90.00000, 0.00000);
	CreateDynamicObject(19169, 1578.54993, -2166.03711, 17.21540,   90.00000, -90.00000, 0.00000);
	CreateDynamicObject(19168, 1578.54993, -2164.53760, 17.21000,   90.00000, -90.00000, 0.00000);
	CreateDynamicObject(1499, 1562.51648, -2165.67017, 12.53760,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(19347, 1562.60425, -2168.21997, 13.38500,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18641, 1562.29968, -2168.27002, 13.40470,   90.00000, 0.00000, 30.00000);
	CreateDynamicObject(2190, 1563.25586, -2168.81567, 13.37920,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(2074, 1561.70679, -2167.42603, 15.66950,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1495, 1559.46741, -2166.64307, 12.53610,   0.00000, 0.00000, 20.00000);
	CreateDynamicObject(1549, 1559.79456, -2168.56519, 12.56690,   0.00000, 0.00000, -30.00000);
	CreateDynamicObject(18977, 1559.50671, -2172.02490, 13.98820,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18978, 1559.22437, -2171.84595, 13.98840,   0.00000, 0.00000, 130.00000);
	CreateDynamicObject(18979, 1559.16382, -2172.18140, 13.98900,   0.00000, 0.00000, 230.00000);
	CreateDynamicObject(8843, 1549.52185, -2173.22754, 12.56390,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(8843, 1530.12085, -2172.66406, 12.56390,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(19380, 1594.43835, -2160.83398, 12.46460,   0.00000, 90.00000, 0.00000);
	CreateDynamicObject(8674, 1584.19849, -2156.12061, 14.00000,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(8674, 1594.50549, -2156.13550, 14.00000,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(19380, 1583.95459, -2160.83813, 12.46460,   0.00000, 90.00000, 0.00000);
	CreateDynamicObject(19380, 1594.44019, -2170.44019, 12.46460,   0.00000, 90.00000, 0.00000);
	CreateDynamicObject(19380, 1583.95984, -2170.44116, 12.46460,   0.00000, 90.00000, 0.00000);
	CreateDynamicObject(8674, 1599.66370, -2161.30713, 14.00000,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(8674, 1599.68286, -2166.42578, 14.00000,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(8674, 1594.50403, -2175.24463, 14.00000,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(8674, 1591.91846, -2175.23633, 14.00000,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2048, 1564.74951, -2166.59277, 15.00000,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(966, 1579.55505, -2174.99902, 12.55150,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(968, 1579.49268, -2175.02344, 13.30740,   0.00000, 6.00000, 0.00000);
	CreateDynamicObject(958, 1565.11157, -2169.23511, 13.42000,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(959, 1565.08240, -2169.23145, 13.39380,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(2900, 1574.44751, -2152.08789, 16.05346,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1271, 1575.31152, -2153.09790, 16.40000,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1271, 1574.56555, -2153.02905, 16.40000,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1271, 1574.95813, -2153.07104, 17.08000,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2478, 1574.93127, -2154.11426, 16.33510,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(2694, 1574.29175, -2155.86401, 16.15450,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2694, 1574.67944, -2155.88208, 16.15450,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2694, 1575.04688, -2156.06079, 16.15450,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2694, 1574.84814, -2156.02954, 16.39450,   0.00000, 0.00000, 30.00000);
	CreateDynamicObject(2694, 1574.39722, -2156.01587, 16.39450,   0.00000, 0.00000, 30.00000);
	CreateDynamicObject(1271, 1575.78223, -2156.78516, 16.40000,   0.00000, 0.00000, 60.00000);
	CreateDynamicObject(2900, 1577.16760, -2153.29370, 16.05346,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2900, 1577.27222, -2156.78418, 16.05346,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(4638, 1598.63184, -2173.40820, 14.26000,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(970, 1537.87256, -2173.55957, 13.14670,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(970, 1538.47522, -2173.55737, 13.14670,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(970, 1543.43140, -2173.53125, 13.14670,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(970, 1544.12488, -2173.62476, 13.14670,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(19360, 1577.03442, -2152.65063, 12.47620,   0.00000, 90.00000, 0.00000);
	CreateDynamicObject(19360, 1575.53650, -2152.65088, 12.47000,   0.00000, 90.00000, 0.00000);
	CreateDynamicObject(19360, 1577.03271, -2155.86938, 12.47620,   0.00000, 90.00000, 0.00000);
	CreateDynamicObject(19360, 1575.58203, -2155.86401, 12.47000,   0.00000, 90.00000, 0.00000);
	CreateDynamicObject(8661, 1568.90967, -2164.78833, 12.55320,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(8661, 1548.92407, -2164.82007, 12.55320,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(8661, 1528.96375, -2164.88574, 12.55320,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(8661, 1576.85217, -2164.79248, 12.55000,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(1439, 1558.39624, -2152.13965, 12.55170,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1330, 1565.20154, -2153.43774, 13.01340,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1300, 1598.66931, -2170.77490, 12.93110,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(19371, 1575.46753, -2151.00684, 14.30000,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(19371, 1576.92749, -2150.99463, 14.30000,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(19371, 1578.60022, -2152.58081, 14.30000,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(19371, 1578.60461, -2155.80054, 14.30000,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(19371, 1578.61365, -2155.98242, 14.30000,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3031, 1556.23779, -2171.58423, 17.66000,   0.00000, 0.00000, -150.00000);
	CreateDynamicObject(3042, 1579.01013, -2153.44458, 17.83510,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(2370, 1576.20642, -2164.97217, 12.55200,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3111, 1576.50281, -2164.60693, 13.41590,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(3017, 1576.54932, -2165.10083, 12.83710,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18866, 1576.86707, -2163.85742, 13.39680,   0.00000, 0.00000, -90.00000);
	CreateDynamicObject(18890, 1577.74036, -2157.77661, 13.40310,   -15.00000, 0.00000, 0.00000);
	CreateDynamicObject(19468, 1577.39429, -2157.83008, 12.63390,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1811, 1577.52454, -2166.35352, 13.16000,   0.00000, 0.00000, -30.00000);
	CreateDynamicObject(1811, 1576.66321, -2162.68970, 13.16000,   0.00000, 0.00000, 45.00000, .streamdistance=100.0);
	CreateDynamicObject(1811, 1574.69446, -2165.01147, 13.16000,   0.00000, 0.00000, 160.00000, .streamdistance=100.0);
	CreateDynamicObject(1078, 1566.93799, -2152.45361, 14.24330,   0.00000, 0.00000, -30.00000, .streamdistance=100.0);
	CreateDynamicObject(1077, 1565.97778, -2152.16235, 15.04330,   0.00000, -90.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1077, 1565.97778, -2152.16235, 15.26330,   0.00000, -90.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1008, 1565.01624, -2161.28540, 13.49440,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(1010, 1565.13574, -2169.96338, 14.30550,   0.00000, 0.00000, 75.00000, .streamdistance=100.0);
	CreateDynamicObject(19166, 1564.24353, -2171.09692, 14.51260,   90.00000, -90.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(19418, 1562.34497, -2168.64771, 13.38470,   90.00000, 0.00000, 60.00000, .streamdistance=100.0);
	CreateDynamicObject(2161, 1563.15747, -2165.80054, 12.56590,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(2270, 1561.95667, -2168.29102, 14.32850,   0.00000, 0.00000, 180.00000, .streamdistance=100.0);
	CreateDynamicObject(1775, 1563.76257, -2173.32104, 13.66000,   0.00000, 0.00000, -90.00000, .streamdistance=100.0);
	CreateDynamicObject(2894, 1555.61426, -2173.47266, 13.61470,   0.00000, 0.00000, -80.00000, .streamdistance=100.0);
	CreateDynamicObject(1424, 1580.12781, -2155.60840, 13.06780,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1424, 1579.95374, -2154.65918, 13.06780,   0.00000, 0.00000, -50.00000, .streamdistance=100.0);
	CreateDynamicObject(1459, 1579.70361, -2152.11157, 13.14770,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(1459, 1583.55542, -2155.37524, 13.14770,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1238, 1581.73181, -2155.46509, 12.86770,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1238, 1581.73181, -2155.46509, 13.02770,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1238, 1581.73181, -2155.46509, 13.24770,   0.00000, 0.00000, 20.00000, .streamdistance=100.0);
	CreateDynamicObject(1635, 1563.64954, -2170.48438, 16.92610,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3036, 1557.53491, -2160.66431, 14.29070,   0.00000, 0.00000, -90.00000, .streamdistance=100.0);
	CreateDynamicObject(10252, 1556.83728, -2161.89111, 14.16000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(974, 1557.51270, -2154.84912, 13.26930,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(974, 1560.86816, -2151.53491, 13.26930,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3504, 1563.65125, -2162.96436, 13.90000,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(1431, 1560.53516, -2152.03198, 13.10000,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1428, 1563.83215, -2153.30786, 13.98000,   0.00000, 0.00000, -90.00000, .streamdistance=100.0);
	CreateDynamicObject(1264, 1558.78479, -2152.08032, 13.36058,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1264, 1558.09253, -2152.18945, 13.36060,   0.00000, 0.00000, 90.00000, .streamdistance=100.0);
	CreateDynamicObject(1265, 1563.41553, -2152.12622, 12.95250,   0.00000, 0.00000, -60.00000, .streamdistance=100.0);
	CreateDynamicObject(3015, 1559.58191, -2152.74072, 12.71220,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(2969, 1560.31067, -2152.79053, 12.67390,   0.00000, 0.00000, 30.00000, .streamdistance=100.0);
	CreateDynamicObject(2969, 1560.31433, -2152.76245, 12.93390,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(3014, 1561.98022, -2152.06763, 12.77340,   0.00000, 0.00000, 60.00000, .streamdistance=100.0);
	CreateDynamicObject(3014, 1561.94373, -2151.99463, 13.25340,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1778, 1564.01563, -2161.78857, 12.55305,   0.00000, 0.00000, 0.00000, .streamdistance=100.0);
	CreateDynamicObject(1437, 1564.18604, -2156.69067, 12.80740,   20.00000, 93.50000, 79.50000, .streamdistance=100.0);
	// ==================================== [ Hotel LS ] ======================================= //
	CreateDynamicObject(19378,1330.8994141,-1050.0996094,3358.6220703,0.0000000,90.0000000,0.0000000); //object number 0
	CreateDynamicObject(19378,1330.8994141,-1059.6992188,3358.6230469,0.0000000,90.0000000,0.0000000); //object number 1
	CreateDynamicObject(19445,1336.0898438,-1059.6992188,3359.0000000,0.0000000,0.0000000,0.0000000); //object number 2
	CreateDynamicObject(19445,1336.0898438,-1059.6992188,3362.5000000,0.0000000,179.9945068,0.0000000); //object number 3
	CreateDynamicObject(19445,1336.0899658,-1050.0999756,3359.0000000,0.0000000,0.0000000,0.0000000); //object number 4
	CreateDynamicObject(19445,1336.0899658,-1050.0999756,3362.5000000,0.0000000,179.9945068,0.0000000); //object number 5
	CreateDynamicObject(2198,1336.5000000,-1058.4000244,3358.6999512,0.0000000,0.0000000,90.0000000); //object number 6
	CreateDynamicObject(2198,1336.5000000,-1056.5000000,3358.6999512,0.0000000,0.0000000,90.0000000); //object number 7
	CreateDynamicObject(2198,1336.5000000,-1054.5996094,3358.6999512,0.0000000,0.0000000,90.0000000); //object number 8
	CreateDynamicObject(2198,1336.5000000,-1056.1992188,3360.6000977,0.0000000,0.0000000,90.0000000); //object number 9
	CreateDynamicObject(2198,1336.5000000,-1054.2998047,3360.6000977,0.0000000,0.0000000,90.0000000); //object number 10
	CreateDynamicObject(2198,1336.5000000,-1052.4000244,3360.6000977,0.0000000,0.0000000,90.0000000); //object number 11
	CreateDynamicObject(2198,1336.5000000,-1050.5000000,3360.6000977,0.0000000,0.0000000,90.0000000); //object number 12
	CreateDynamicObject(2198,1336.5000000,-1048.5999756,3360.6000977,0.0000000,0.0000000,90.0000000); //object number 13
	CreateDynamicObject(2198,1336.5009766,-1047.0000000,3360.6000977,0.0000000,0.0000000,90.0000000); //object number 14
	CreateDynamicObject(19445,1331.1992188,-1064.4892578,3362.5000000,0.0000000,179.9945068,90.0000000); //object number 15
	CreateDynamicObject(19445,1331.1992188,-1064.4892578,3359.0000000,0.0000000,0.0000000,90.0000000); //object number 16
	CreateDynamicObject(2198,1334.5000000,-1064.8994141,3358.6999512,0.0000000,0.0000000,0.0000000); //object number 17
	CreateDynamicObject(2198,1332.5999756,-1064.9000244,3358.6999512,0.0000000,0.0000000,0.0000000); //object number 18
	CreateDynamicObject(1569,1332.1999512,-1064.4000244,3358.6999512,0.0000000,0.0000000,180.0000000); //object number 19
	CreateDynamicObject(1569,1329.1999512,-1064.4050293,3358.6999512,0.0000000,0.0000000,0.0000000); //object number 20
	CreateDynamicObject(2198,1332.0999756,-1064.9019775,3360.3000488,0.0000000,90.0000000,0.0000000); //object number 21
	CreateDynamicObject(2198,1332.0999756,-1064.9010010,3360.8000488,0.0000000,90.0000000,0.0000000); //object number 22
	CreateDynamicObject(2198,1330.6999512,-1064.9000244,3361.1000977,0.0000000,0.0000000,0.0000000); //object number 23
	CreateDynamicObject(2198,1329.5999756,-1064.9010010,3361.1000977,0.0000000,0.0000000,0.0000000); //object number 24
	CreateDynamicObject(2198,1329.0999756,-1064.9000244,3360.8000488,0.0000000,90.0000000,0.0000000); //object number 25
	CreateDynamicObject(2198,1329.0999756,-1064.9000244,3358.8999023,0.0000000,90.0000000,0.0000000); //object number 26
	CreateDynamicObject(2198,1327.6999512,-1064.9000244,3358.6999512,0.0000000,0.0000000,0.0000000); //object number 27
	CreateDynamicObject(2198,1325.7998047,-1064.8994141,3358.6999512,0.0000000,0.0000000,0.0000000); //object number 28
	CreateDynamicObject(2198,1333.0000000,-1064.9003906,3360.6000977,0.0000000,0.0000000,0.0000000); //object number 29
	CreateDynamicObject(2198,1327.4000244,-1064.9000244,3360.6000977,0.0000000,0.0000000,0.0000000); //object number 30
	CreateDynamicObject(19445,1331.1999512,-1045.2099609,3359.0000000,0.0000000,0.0000000,90.0000000); //object number 31
	CreateDynamicObject(19445,1331.1992188,-1045.2099609,3362.5000000,0.0000000,179.9945068,90.0000000); //object number 32
	CreateDynamicObject(2198,1326.2998047,-1044.7998047,3358.6999512,0.0000000,0.0000000,179.9945068); //object number 33
	CreateDynamicObject(2198,1328.1992188,-1044.7998047,3358.6999512,0.0000000,0.0000000,179.9945068); //object number 34
	CreateDynamicObject(2198,1330.0996094,-1044.7998047,3358.6999512,0.0000000,0.0000000,179.9945068); //object number 35
	CreateDynamicObject(2198,1335.2998047,-1044.7998047,3360.6000977,0.0000000,0.0000000,179.9945068); //object number 36
	CreateDynamicObject(2198,1333.4000244,-1044.8000488,3360.6000977,0.0000000,0.0000000,179.9945068); //object number 37
	CreateDynamicObject(2198,1331.5000000,-1044.7998047,3360.6000977,0.0000000,0.0000000,179.9945068); //object number 38
	CreateDynamicObject(2198,1329.5996094,-1044.7998047,3360.6000977,0.0000000,0.0000000,179.9945068); //object number 39
	CreateDynamicObject(2198,1327.6992188,-1044.7998047,3360.6000977,0.0000000,0.0000000,179.9945068); //object number 40
	CreateDynamicObject(19445,1326.3100586,-1062.5000000,3362.5000000,0.0000000,179.9945068,0.0000000); //object number 41
	CreateDynamicObject(1726,1331.5000000,-1054.1999512,3358.8999023,0.0000000,0.0000000,90.0000000); //object number 42
	CreateDynamicObject(19377,1331.1400146,-1050.0980225,3358.8100586,0.0000000,90.0000000,0.0000000); //object number 43
	CreateDynamicObject(1726,1333.6992188,-1049.7998047,3358.9187012,0.0000000,0.0000000,179.9945068); //object number 44
	CreateDynamicObject(1726,1334.5000000,-1052.0999756,3358.8999023,0.0000000,0.0000000,270.0000000); //object number 45
	CreateDynamicObject(2198,1332.1992188,-1054.3994141,3358.8999023,0.0000000,180.0000000,180.0000000); //object number 46
	CreateDynamicObject(2198,1330.2646484,-1054.3994141,3358.8999023,0.0000000,179.9945068,179.9945068); //object number 47
	CreateDynamicObject(2198,1334.0996094,-1054.3994141,3358.8999023,0.0000000,179.9945068,179.9945068); //object number 48
	CreateDynamicObject(2198,1336.0000000,-1054.4000244,3358.8999023,0.0000000,179.9945068,179.9945068); //object number 49
	CreateDynamicObject(19445,1334.5996094,-1062.6992188,3358.0000000,0.0000000,0.0000000,0.0000000); //object number 50
	CreateDynamicObject(19445,1339.3291016,-1057.8994141,3358.0000000,0.0000000,0.0000000,90.0000000); //object number 51
	CreateDynamicObject(19372,1336.2998047,-1059.5996094,3359.6000977,0.0000000,90.0000000,0.0000000); //object number 52
	CreateDynamicObject(19372,1336.3000488,-1062.8000488,3359.6000977,0.0000000,90.0000000,0.0000000); //object number 53
	CreateDynamicObject(1601,1335.6999512,-1062.0000000,3360.1000977,0.0000000,0.0000000,0.0000000); //object number 54
	CreateDynamicObject(1605,1335.9000244,-1060.8000488,3360.1999512,0.0000000,0.0000000,184.0000000); //object number 55
	CreateDynamicObject(10444,1334.7199707,-1065.2299805,3345.6999512,270.0000000,0.0000000,90.0000000); //object number 56
	CreateDynamicObject(10444,1341.9345703,-1058.0000000,3345.6999512,270.0000000,179.9945068,180.0000000); //object number 57
	CreateDynamicObject(10444,1341.8994141,-1073.1992188,3360.8000488,0.0000000,179.9945068,0.0000000); //object number 58
	CreateDynamicObject(19445,1334.5996094,-1062.6992188,3362.6000977,0.0000000,179.9945068,0.0000000); //object number 59
	CreateDynamicObject(19445,1339.3291016,-1058.0000000,3362.6000977,0.0000000,179.9945068,90.0000000); //object number 60
	CreateDynamicObject(16101,1334.6992188,-1058.0000000,3352.3999023,0.0000000,0.0000000,0.0000000); //object number 61
	CreateDynamicObject(16101,1336.0000000,-1058.0000000,3352.3999023,0.0000000,0.0000000,0.0000000); //object number 62
	CreateDynamicObject(16101,1334.6999512,-1064.4000244,3352.3999023,0.0000000,0.0000000,0.0000000); //object number 63
	CreateDynamicObject(2198,1335.0195312,-1058.2850342,3358.8969727,0.0000000,179.9945068,90.0000000); //object number 64
	CreateDynamicObject(2198,1335.0195312,-1060.0996094,3358.8969727,0.0000000,179.9945068,90.0000000); //object number 65
	CreateDynamicObject(2198,1335.0195312,-1062.0000000,3358.8969727,0.0000000,179.9945068,90.0000000); //object number 66
	CreateDynamicObject(2198,1335.0195312,-1063.8994141,3358.8969727,0.0000000,179.9945068,90.0000000); //object number 67
	CreateDynamicObject(2198,1335.9000244,-1058.2850342,3358.8969727,0.0000000,179.9945068,90.0000000); //object number 68
	CreateDynamicObject(2198,1336.5009766,-1057.0999756,3360.6000977,0.0000000,0.0000000,90.0000000); //object number 69
	CreateDynamicObject(2315,1333.0000000,-1052.4000244,3358.8999023,0.0000000,0.0000000,270.0000000); //object number 70
	CreateDynamicObject(2315,1333.5000000,-1048.1999512,3358.8999023,0.0000000,0.0000000,179.9945068); //object number 71
	CreateDynamicObject(19372,1336.3000488,-1059.5999756,3361.1000977,0.0000000,90.0000000,0.0000000); //object number 72
	CreateDynamicObject(19372,1336.3000488,-1062.8000488,3361.1000977,0.0000000,90.0000000,0.0000000); //object number 73
	CreateDynamicObject(963,1335.2998047,-1064.3994141,3360.6999512,90.0000000,0.0000000,0.0000000); //object number 74
	CreateDynamicObject(963,1336.2397461,-1064.4000244,3360.6999512,90.0000000,0.0000000,0.0000000); //object number 75
	CreateDynamicObject(963,1336.0000000,-1058.4000244,3360.5000000,90.0000000,0.0000000,90.0000000); //object number 76
	CreateDynamicObject(963,1336.0000000,-1059.3388672,3360.5000000,90.0000000,0.0000000,90.0000000); //object number 77
	CreateDynamicObject(963,1336.0000000,-1060.2779541,3360.5000000,90.0000000,0.0000000,90.0000000); //object number 78
	CreateDynamicObject(963,1336.0000000,-1061.2170410,3360.5000000,90.0000000,0.0000000,90.0000000); //object number 79
	CreateDynamicObject(963,1336.0000000,-1062.1560059,3360.5000000,90.0000000,0.0000000,90.0000000); //object number 80
	CreateDynamicObject(963,1336.0000000,-1063.0949707,3360.5000000,90.0000000,0.0000000,90.0000000); //object number 81
	CreateDynamicObject(963,1336.0000000,-1064.0332031,3360.5000000,90.0000000,0.0000000,90.0000000); //object number 82
	CreateDynamicObject(2854,1328.1999512,-1049.9000244,3359.3828125,0.0000000,0.0000000,0.0000000); //object number 83
	CreateDynamicObject(2853,1333.6999512,-1048.1999512,3359.3999023,0.0000000,0.0000000,0.0000000); //object number 84
	CreateDynamicObject(2252,1332.1992188,-1048.1992188,3359.6999512,0.0000000,0.0000000,0.0000000); //object number 85
	CreateDynamicObject(2010,1332.8000488,-1064.0000000,3358.6999512,0.0000000,0.0000000,108.0000000); //object number 86
	CreateDynamicObject(2010,1328.5996094,-1064.0000000,3358.6999512,0.0000000,0.0000000,107.9901123); //object number 87
	CreateDynamicObject(18608,1330.8000488,-1054.1999512,3363.1000977,0.0000000,0.0000000,0.0000000); //object number 88
	CreateDynamicObject(2271,1335.5000000,-1049.5999756,3361.1000977,0.0000000,0.0000000,270.0000000); //object number 89
	CreateDynamicObject(2269,1323.3000488,-1045.8000488,3361.1999512,0.0000000,0.0000000,0.0000000); //object number 90
	CreateDynamicObject(2268,1333.0000000,-1045.8000488,3361.0000000,0.0000000,0.0000000,0.0000000); //object number 91
	CreateDynamicObject(2270,1328.5999756,-1045.8000488,3361.1999512,0.0000000,0.0000000,0.0000000); //object number 92
	CreateDynamicObject(1726,1331.6999512,-1046.6999512,3358.8999023,0.0000000,0.0000000,0.0000000); //object number 93
	CreateDynamicObject(19445,1326.3100586,-1062.5000000,3359.0000000,0.0000000,0.0000000,0.0000000); //object number 94
	CreateDynamicObject(2198,1328.3000488,-1054.4000244,3358.8999023,0.0000000,179.9945068,179.9945068); //object number 95
	CreateDynamicObject(2198,1326.3640137,-1054.4000244,3358.8999023,0.0000000,179.9945068,179.9945068); //object number 96
	CreateDynamicObject(19445,1321.5839844,-1057.6899414,3359.0000000,0.0000000,0.0000000,90.0000000); //object number 97
	CreateDynamicObject(19445,1321.5839844,-1057.6899414,3362.5000000,0.0000000,179.9945068,90.0000000); //object number 98
	CreateDynamicObject(19378,1320.5000000,-1059.6999512,3358.6240234,0.0000000,90.0000000,0.0000000); //object number 99
	CreateDynamicObject(19378,1320.5000000,-1050.0999756,3358.6250000,0.0000000,90.0000000,0.0000000); //object number 100
	CreateDynamicObject(2198,1326.4000244,-1052.5000000,3358.8999023,0.0000000,180.0000000,90.0000000); //object number 101
	CreateDynamicObject(2198,1326.4000244,-1050.5999756,3358.8999023,0.0000000,179.9945068,90.0000000); //object number 102
	CreateDynamicObject(2198,1326.4000244,-1048.6999512,3358.8999023,0.0000000,179.9945068,90.0000000); //object number 103
	CreateDynamicObject(2198,1326.4000244,-1046.8000488,3358.8999023,0.0000000,179.9945068,90.0000000); //object number 104
	CreateDynamicObject(2198,1326.4000244,-1044.9000244,3358.8999023,0.0000000,179.9945068,90.0000000); //object number 105
	CreateDynamicObject(19445,1321.5999756,-1045.2099609,3359.0000000,0.0000000,0.0000000,90.0000000); //object number 106
	CreateDynamicObject(19445,1321.5999756,-1045.2099609,3362.5000000,0.0000000,179.9945068,90.0000000); //object number 107
	CreateDynamicObject(14596,1310.3000488,-1047.7700195,3363.6000977,0.0000000,0.0000000,180.0000000); //object number 108
	CreateDynamicObject(19445,1316.7099609,-1057.4000244,3359.0000000,0.0000000,0.0000000,0.0000000); //object number 109
	CreateDynamicObject(19445,1316.7099609,-1057.4000244,3362.5000000,0.0000000,179.9945068,0.0000000); //object number 110
	CreateDynamicObject(19445,1316.7099609,-1045.6999512,3359.0000000,0.0000000,0.0000000,0.0000000); //object number 111
	CreateDynamicObject(19445,1316.7099609,-1045.6999512,3362.5000000,0.0000000,179.9945068,0.0000000); //object number 112
	CreateDynamicObject(2198,1325.9000244,-1058.0500488,3358.6999512,0.0000000,0.0000000,270.0000000); //object number 113
	CreateDynamicObject(2198,1325.9000244,-1059.9000244,3358.6999512,0.0000000,0.0000000,270.0000000); //object number 114
	CreateDynamicObject(2198,1325.9000244,-1061.8000488,3358.6999512,0.0000000,0.0000000,270.0000000); //object number 115
	CreateDynamicObject(2198,1325.9000244,-1063.6999512,3358.6999512,0.0000000,0.0000000,270.0000000); //object number 116
	CreateDynamicObject(2198,1324.0000000,-1058.0999756,3358.6999512,0.0000000,0.0000000,0.0000000); //object number 117
	CreateDynamicObject(2198,1316.4000244,-1058.0999756,3358.6999512,0.0000000,0.0000000,0.0000000); //object number 118
	CreateDynamicObject(2198,1324.6999512,-1058.1009521,3360.6000977,0.0000000,0.0000000,0.0000000); //object number 119
	CreateDynamicObject(2198,1325.9000244,-1058.3000488,3360.6000977,0.0000000,0.0000000,270.0000000); //object number 120
	CreateDynamicObject(2198,1325.9000244,-1060.1999512,3360.6000977,0.0000000,0.0000000,270.0000000); //object number 121
	CreateDynamicObject(2198,1325.9000244,-1062.0999756,3360.6000977,0.0000000,0.0000000,270.0000000); //object number 122
	CreateDynamicObject(2198,1325.9010010,-1062.5999756,3360.6000977,0.0000000,0.0000000,270.0000000); //object number 123
	CreateDynamicObject(2198,1327.1999512,-1064.9010010,3360.6000977,0.0000000,0.0000000,0.0000000); //object number 124
	CreateDynamicObject(2198,1316.3010254,-1054.0000000,3360.6000977,0.0000000,0.0000000,270.0000000); //object number 125
	CreateDynamicObject(2198,1316.3000488,-1053.3010254,3360.6000977,0.0000000,0.0000000,270.0000000); //object number 126
	CreateDynamicObject(2198,1316.3000488,-1054.1999512,3358.6999512,0.0000000,0.0000000,270.0000000); //object number 127
	CreateDynamicObject(2198,1316.3000488,-1049.0300293,3358.6999512,0.0000000,0.0000000,270.0000000); //object number 128
	CreateDynamicObject(2198,1316.3000488,-1047.1999512,3358.6999512,0.0000000,0.0000000,270.0000000); //object number 129
	CreateDynamicObject(2198,1316.3000488,-1045.3000488,3358.6999512,0.0000000,0.0000000,270.0000000); //object number 130
	CreateDynamicObject(19426,1315.9959717,-1052.6199951,3359.0000000,0.0000000,0.0000000,270.0000000); //object number 131
	CreateDynamicObject(2198,1315.5999756,-1053.0009766,3358.6999512,0.0000000,0.0000000,270.0000000); //object number 132
	CreateDynamicObject(19426,1315.9959717,-1052.6199951,3362.5000000,0.0000000,180.0000000,270.0000000); //object number 133
	CreateDynamicObject(19426,1315.9959717,-1050.4300537,3359.0000000,0.0000000,0.0000000,270.0000000); //object number 134
	CreateDynamicObject(19426,1315.9959717,-1050.4300537,3362.5000000,0.0000000,179.9945068,270.0000000); //object number 135
	CreateDynamicObject(2198,1315.5999756,-1049.0310059,3358.6999512,0.0000000,0.0000000,270.0000000); //object number 136
	CreateDynamicObject(2198,1316.3000488,-1048.9000244,3360.6000977,0.0000000,0.0000000,270.0000000); //object number 137
	CreateDynamicObject(2198,1316.3000488,-1047.0000000,3360.6000977,0.0000000,0.0000000,270.0000000); //object number 138
	CreateDynamicObject(2198,1316.3010254,-1046.0000000,3360.6000977,0.0000000,0.0000000,270.0000000); //object number 139
	CreateDynamicObject(2198,1324.4000244,-1044.8000488,3358.6999512,0.0000000,0.0000000,179.9945068); //object number 140
	CreateDynamicObject(2198,1322.5000000,-1044.8000488,3358.6999512,0.0000000,0.0000000,179.9945068); //object number 141
	CreateDynamicObject(2198,1320.5999756,-1044.8000488,3358.6999512,0.0000000,0.0000000,179.9945068); //object number 142
	CreateDynamicObject(2198,1318.6999512,-1044.8000488,3358.6999512,0.0000000,0.0000000,179.9945068); //object number 143
	CreateDynamicObject(2198,1316.8000488,-1044.8000488,3358.6999512,0.0000000,0.0000000,179.9945068); //object number 144
	CreateDynamicObject(2198,1325.8000488,-1044.8000488,3360.6000977,0.0000000,0.0000000,179.9945068); //object number 145
	CreateDynamicObject(2198,1323.9000244,-1044.8000488,3360.6000977,0.0000000,0.0000000,179.9945068); //object number 146
	CreateDynamicObject(2198,1318.5000000,-1044.8010254,3360.6000977,0.0000000,0.0000000,180.0000000); //object number 147
	CreateDynamicObject(2315,1315.2010498,-1052.0999756,3361.6669922,90.0000000,0.0000000,90.0000000); //object number 148
	CreateDynamicObject(1502,1315.5600586,-1050.7370605,3358.6999512,0.0000000,0.0000000,270.0000000); //object number 149
	CreateDynamicObject(2435,1322.7010498,-1049.9090576,3359.0200195,0.0000000,180.0000000,0.0000000); //object number 150
	CreateDynamicObject(2435,1321.8000488,-1049.9069824,3359.0200195,0.0000000,179.9945068,0.0000000); //object number 151
	CreateDynamicObject(2435,1320.9000244,-1049.9060059,3359.0200195,0.0000000,179.9945068,0.0000000); //object number 152
	CreateDynamicObject(2435,1320.0000000,-1049.9050293,3359.0200195,0.0000000,179.9945068,0.0000000); //object number 153
	CreateDynamicObject(2435,1319.0999756,-1049.9040527,3359.0200195,0.0000000,179.9945068,0.0000000); //object number 154
	CreateDynamicObject(2435,1318.1999512,-1049.9029541,3359.0200195,0.0000000,179.9945068,0.0000000); //object number 155
	CreateDynamicObject(2435,1317.3000488,-1049.9019775,3359.0200195,0.0000000,179.9945068,0.0000000); //object number 156
	CreateDynamicObject(2315,1318.6999512,-1049.8000488,3359.5000000,270.0000000,0.0000000,179.9945068); //object number 157
	CreateDynamicObject(2315,1321.0999756,-1049.8000488,3359.5000000,270.0000000,0.0000000,179.9945068); //object number 158
	CreateDynamicObject(2315,1323.5009766,-1048.3000488,3359.5000000,270.0000000,0.0000000,269.9945068); //object number 159
	CreateDynamicObject(2435,1323.5089111,-1049.9100342,3359.0200195,0.0000000,179.9945068,0.0000000); //object number 160
	CreateDynamicObject(2435,1323.6070557,-1049.8480225,3359.0200195,0.0000000,179.9945068,90.0000000); //object number 161
	CreateDynamicObject(2435,1323.6049805,-1048.9160156,3359.0200195,0.0000000,179.9945068,90.0000000); //object number 162
	CreateDynamicObject(2435,1323.6040039,-1048.0000000,3359.0200195,0.0000000,179.9945068,90.0000000); //object number 163
	CreateDynamicObject(2435,1323.6030273,-1047.0999756,3359.0200195,0.0000000,179.9945068,90.0000000); //object number 164
	CreateDynamicObject(2315,1323.5340576,-1049.8010254,3359.5000000,270.0000000,0.0000000,179.9945068); //object number 165
	CreateDynamicObject(2315,1323.5000000,-1045.9000244,3359.5000000,270.0000000,0.0000000,269.9890137); //object number 166
	CreateDynamicObject(2435,1323.6020508,-1046.1999512,3359.0200195,0.0000000,179.9945068,90.0000000); //object number 167
	CreateDynamicObject(2435,1323.6009521,-1045.3000488,3359.0200195,0.0000000,179.9945068,90.0000000); //object number 168
	CreateDynamicObject(2315,1323.5000000,-1043.5000000,3359.5000000,270.0000000,0.0000000,269.9890137); //object number 169
	CreateDynamicObject(2315,1317.1999512,-1049.9000244,3359.5000000,270.0000000,0.0000000,0.0000000); //object number 170
	CreateDynamicObject(2315,1319.5999756,-1049.9000244,3359.5000000,270.0000000,0.0000000,0.0000000); //object number 171
	CreateDynamicObject(2315,1322.0000000,-1049.9000244,3359.5000000,270.0000000,0.0000000,0.0000000); //object number 172
	CreateDynamicObject(2315,1323.5999756,-1049.0000000,3359.5000000,270.0000000,0.0000000,90.0000000); //object number 173
	CreateDynamicObject(2315,1323.5999756,-1046.5999756,3359.5000000,270.0000000,0.0000000,90.0000000); //object number 174
	CreateDynamicObject(2315,1317.1999512,-1049.9000244,3359.5000000,0.0000000,0.0000000,0.0000000); //object number 175
	CreateDynamicObject(2315,1319.5999756,-1049.9000244,3359.5000000,0.0000000,0.0000000,0.0000000); //object number 176
	CreateDynamicObject(2315,1322.0000000,-1049.9000244,3359.5000000,0.0000000,0.0000000,0.0000000); //object number 177
	CreateDynamicObject(2315,1323.5999756,-1049.8000488,3359.5009766,0.0000000,0.0000000,90.0000000); //object number 178
	CreateDynamicObject(2315,1323.5999756,-1047.4000244,3359.5000000,0.0000000,0.0000000,90.0000000); //object number 179
	CreateDynamicObject(2315,1323.5999756,-1045.0999756,3359.5000000,0.0000000,0.0000000,90.0000000); //object number 180
	CreateDynamicObject(2435,1316.4000244,-1049.9010010,3359.0200195,0.0000000,179.9945068,0.0000000); //object number 181
	CreateDynamicObject(2198,1322.5000000,-1044.8000488,3360.6000977,0.0000000,0.0000000,179.9945068); //object number 182
	CreateDynamicObject(2198,1320.8000488,-1044.8000488,3358.8999023,0.0000000,90.0000000,179.9945068); //object number 183
	CreateDynamicObject(2198,1319.3000488,-1044.8000488,3358.8999023,0.0000000,90.0000000,179.9945068); //object number 184
	CreateDynamicObject(2198,1319.3000488,-1044.8000488,3360.8000488,0.0000000,90.0000000,179.9945068); //object number 185
	CreateDynamicObject(2198,1320.8000488,-1044.8000488,3360.8000488,0.0000000,90.0000000,179.9945068); //object number 186
	CreateDynamicObject(2198,1319.6999512,-1043.8000488,3361.2900391,0.0000000,180.0000000,90.0000000); //object number 187
	CreateDynamicObject(2198,1320.1999512,-1043.8010254,3361.2900391,0.0000000,179.9945068,90.0000000); //object number 188
	CreateDynamicObject(1726,1326.6999512,-1051.5000000,3358.8999023,0.0000000,0.0000000,90.0000000); //object number 189
	CreateDynamicObject(1726,1329.8000488,-1049.5000000,3358.8999023,0.0000000,0.0000000,270.0000000); //object number 190
	CreateDynamicObject(2315,1328.1999512,-1049.6999512,3358.8999023,0.0000000,0.0000000,270.0000000); //object number 191
	CreateDynamicObject(1727,1327.6999512,-1048.0000000,3358.8999023,0.0000000,0.0000000,0.0000000); //object number 192
	CreateDynamicObject(2852,1333.0000000,-1052.8994141,3359.3999023,0.0000000,0.0000000,0.0000000); //object number 193
	CreateDynamicObject(2008,1322.4000244,-1049.8000488,3359.1799316,0.0000000,0.0000000,180.0000000); //object number 194
	CreateDynamicObject(2008,1319.5999756,-1049.8000488,3359.1799316,0.0000000,0.0000000,179.9945068); //object number 195
	CreateDynamicObject(1955,1316.6999512,-1048.8000488,3360.1999512,60.0000000,180.0000000,24.0000000); //object number 196
	CreateDynamicObject(1955,1316.6999512,-1048.5999756,3360.1999512,59.9963379,179.9945068,23.9996338); //object number 197
	CreateDynamicObject(1955,1316.6999512,-1048.4000244,3360.1999512,59.9963379,179.9945068,23.9996338); //object number 198
	CreateDynamicObject(1955,1316.6999512,-1048.1999512,3360.1999512,59.9963379,179.9945068,27.9996338); //object number 199
	CreateDynamicObject(1955,1316.6999512,-1047.4000244,3360.1999512,59.9908447,179.9945068,27.9986572); //object number 200
	CreateDynamicObject(1955,1316.6999512,-1047.0000000,3360.1999512,59.9908447,179.9945068,27.9986572); //object number 201
	CreateDynamicObject(1955,1316.6999512,-1047.0000000,3360.3999023,59.9908447,179.9945068,27.9986572); //object number 202
	CreateDynamicObject(1955,1316.6999512,-1047.8000488,3360.3999023,59.9908447,179.9945068,27.9986572); //object number 203
	CreateDynamicObject(1955,1316.6999512,-1047.8000488,3360.6000977,59.9908447,179.9945068,27.9986572); //object number 204
	CreateDynamicObject(1955,1316.6999512,-1047.5999756,3360.6000977,59.9908447,179.9945068,27.9986572); //object number 205
	CreateDynamicObject(1955,1316.6999512,-1046.8000488,3360.6000977,59.9908447,179.9945068,27.9986572); //object number 206
	CreateDynamicObject(1955,1316.6999512,-1046.5999756,3360.6000977,59.9908447,179.9945068,27.9986572); //object number 207
	CreateDynamicObject(1955,1316.6999512,-1046.4000244,3360.6000977,59.9908447,179.9945068,27.9986572); //object number 208
	CreateDynamicObject(1955,1316.6999512,-1046.4000244,3360.3999023,59.9908447,179.9945068,27.9986572); //object number 209
	CreateDynamicObject(1955,1316.6999512,-1046.0000000,3360.3999023,59.9908447,179.9945068,27.9986572); //object number 210
	CreateDynamicObject(1955,1316.6999512,-1046.0000000,3360.1999512,59.9908447,179.9945068,27.9986572); //object number 211
	CreateDynamicObject(1955,1316.6999512,-1045.8000488,3360.1999512,59.9908447,179.9945068,27.9986572); //object number 212
	CreateDynamicObject(1955,1316.6999512,-1045.8000488,3360.3999023,59.9908447,179.9945068,27.9986572); //object number 213
	CreateDynamicObject(1955,1316.6999512,-1045.8000488,3360.6000977,59.9908447,179.9945068,27.9986572); //object number 214
	CreateDynamicObject(1955,1316.6999512,-1045.5999756,3360.6000977,59.9908447,179.9945068,27.9986572); //object number 215
	CreateDynamicObject(1955,1316.6999512,-1045.5999756,3360.3999023,59.9908447,179.9945068,27.9986572); //object number 216
	CreateDynamicObject(1955,1316.6999512,-1045.5999756,3360.1999512,59.9908447,179.9945068,27.9986572); //object number 217
	CreateDynamicObject(1955,1316.6999512,-1046.5999756,3360.1999512,59.9908447,179.9945068,27.9986572); //object number 218
	CreateDynamicObject(2400,1316.4000244,-1045.7600098,3361.0200195,3.9990234,179.9945068,90.0000000); //object number 219
	CreateDynamicObject(2400,1316.4000244,-1045.5600586,3361.0200195,3.9990234,179.9945068,90.0000000); //object number 220
	CreateDynamicObject(2400,1316.4000244,-1045.9599609,3361.2099609,3.9990234,179.9945068,90.0000000); //object number 221
	CreateDynamicObject(2400,1316.4000244,-1045.5600586,3361.1999512,3.9990234,179.9945068,90.0000000); //object number 222
	CreateDynamicObject(2400,1316.4000244,-1045.7500000,3361.1999512,4.0000000,179.9945068,90.0000000); //object number 223
	CreateDynamicObject(2400,1316.4000244,-1045.9599609,3361.4199219,3.9990234,179.9945068,90.0000000); //object number 224
	CreateDynamicObject(2400,1316.4000244,-1045.7500000,3361.4199219,3.9990234,179.9945068,90.0000000); //object number 225
	CreateDynamicObject(2400,1316.4000244,-1045.5600586,3361.4199219,3.9990234,179.9945068,90.0000000); //object number 226
	CreateDynamicObject(2186,1317.5999756,-1045.8000488,3358.6999512,0.0000000,0.0000000,0.0000000); //object number 227
	CreateDynamicObject(2252,1323.5000000,-1045.8000488,3360.3000488,0.0000000,0.0000000,0.0000000); //object number 228
	CreateDynamicObject(2252,1317.3000488,-1049.9000244,3360.3000488,0.0000000,0.0000000,0.0000000); //object number 229
	CreateDynamicObject(18608,1320.0999756,-1054.1999512,3363.1000977,0.0000000,0.0000000,0.0000000); //object number 230
	CreateDynamicObject(1506,1310.9000244,-1054.6400146,3358.6999512,0.0000000,0.0000000,0.0000000); //object number 231
	CreateDynamicObject(1506,1305.5000000,-1054.6400146,3358.6999512,0.0000000,0.0000000,0.0000000); //object number 232
	CreateDynamicObject(1506,1303.8100586,-1051.5999756,3358.6999512,0.0000000,0.0000000,270.0000000); //object number 233
	CreateDynamicObject(2198,1316.2998047,-1053.0000000,3358.6999512,0.0000000,0.0000000,270.0000000); //object number 234
	CreateDynamicObject(2198,1312.4000244,-1055.0999756,3360.8999023,0.0000000,90.0000000,0.0000000); //object number 235
	CreateDynamicObject(2198,1310.7299805,-1055.0999756,3360.8999023,0.0000000,90.0000000,0.0000000); //object number 236
	CreateDynamicObject(2198,1312.1300049,-1040.4399414,3360.6999512,0.0000000,90.0000000,180.0000000); //object number 237
	CreateDynamicObject(2198,1312.4000244,-1055.0999756,3359.0000000,0.0000000,90.0000000,0.0000000); //object number 238
	CreateDynamicObject(2198,1312.0999756,-1056.0899658,3361.1899414,0.0000000,0.0000000,90.0000000); //object number 239
	CreateDynamicObject(2198,1311.3000488,-1056.0899658,3361.1899414,0.0000000,0.0000000,90.0000000); //object number 240
	CreateDynamicObject(2400,1316.3994141,-1045.9648438,3361.0200195,3.9990234,179.9945068,90.0000000); //object number 241
	CreateDynamicObject(19377,1331.4000244,-1050.0999756,3362.1999512,0.0000000,90.0000000,0.0000000); //object number 242
	CreateDynamicObject(19377,1320.9000244,-1050.0999756,3362.1999512,0.0000000,90.0000000,0.0000000); //object number 243
	CreateDynamicObject(19377,1320.5999756,-1059.6999512,3362.1999512,0.0000000,90.0000000,0.0000000); //object number 244
	CreateDynamicObject(19377,1331.0999756,-1059.6999512,3362.1999512,0.0000000,90.0000000,0.0000000); //object number 245
	CreateDynamicObject(2315,1315.3000488,-1052.3000488,3367.3000488,270.0000000,0.0000000,90.0000000); //object number 246
	CreateDynamicObject(2315,1315.1999512,-1052.7449951,3360.8000488,0.0000000,90.0000000,0.0000000); //object number 247
	CreateDynamicObject(2315,1315.1999512,-1052.7449951,3358.3999023,0.0000000,90.0000000,0.0000000); //object number 248
	CreateDynamicObject(2315,1315.1999512,-1050.2821938,3358.3999023,0.0000000,90.0000000,0.0000000); //object number 249
	CreateDynamicObject(2315,1315.3000488,-1052.6999512,3366.3999023,0.0000000,90.0000000,180.0000000); //object number 250
	CreateDynamicObject(19445,1319.3000488,-1055.5999756,3358.0000000,0.0000000,0.0000000,90.0000000); //object number 251
	CreateDynamicObject(19445,1324.0269775,-1060.4000244,3358.0000000,0.0000000,0.0000000,0.0000000); //object number 252
	CreateDynamicObject(19372,1322.1999512,-1057.1999512,3359.6000977,0.0000000,90.0000000,0.0000000); //object number 253
	CreateDynamicObject(19372,1318.6999512,-1057.1999512,3359.6000977,0.0000000,90.0000000,0.0000000); //object number 254
	CreateDynamicObject(19372,1315.1999512,-1057.1999512,3359.6000977,0.0000000,90.0000000,0.0000000); //object number 255
	CreateDynamicObject(963,1318.1999512,-1057.5999756,3360.6999512,90.0000000,0.0000000,0.0000000); //object number 256
	CreateDynamicObject(963,1323.5200195,-1057.6009521,3360.6999512,90.0000000,0.0000000,0.0000000); //object number 257
	CreateDynamicObject(963,1322.6999512,-1057.5999756,3360.6999512,90.0000000,0.0000000,0.0000000); //object number 258
	CreateDynamicObject(963,1321.8000488,-1057.5999756,3360.6999512,90.0000000,0.0000000,0.0000000); //object number 259
	CreateDynamicObject(963,1320.9000244,-1057.5999756,3360.6999512,90.0000000,0.0000000,0.0000000); //object number 260
	CreateDynamicObject(963,1320.0000000,-1057.5999756,3360.6999512,90.0000000,0.0000000,0.0000000); //object number 261
	CreateDynamicObject(963,1319.0999756,-1057.5999756,3360.6999512,90.0000000,0.0000000,0.0000000); //object number 262
	CreateDynamicObject(963,1317.3000488,-1057.5999756,3360.6999512,90.0000000,0.0000000,0.0000000); //object number 263
	CreateDynamicObject(963,1316.8010254,-1056.2340088,3360.6999512,90.0000000,180.0000000,90.0000000); //object number 264
	CreateDynamicObject(963,1316.8000488,-1057.0999756,3360.6999512,90.0000000,179.9945068,90.0000000); //object number 265
	CreateDynamicObject(963,1316.8000488,-1058.0000000,3360.6999512,90.0000000,179.9945068,90.0000000); //object number 266
	CreateDynamicObject(19445,1319.3699951,-1055.5999756,3362.6000977,0.0000000,179.9945068,90.0000000); //object number 267
	CreateDynamicObject(19445,1324.0999756,-1060.4000244,3362.6000977,0.0000000,179.9945068,0.0000000); //object number 268
	CreateDynamicObject(10444,1323.9200439,-1062.9300537,3345.6999512,270.0000000,180.0000000,90.0000000); //object number 269
	CreateDynamicObject(10444,1316.6999512,-1070.9000244,3360.8000488,0.0000000,179.9945068,0.0000000); //object number 270
	CreateDynamicObject(10444,1316.6999512,-1055.6999512,3345.6999512,270.0000000,179.9945068,180.0000000); //object number 271
	CreateDynamicObject(16101,1323.9000244,-1055.6999512,3352.3999023,0.0000000,0.0000000,0.0000000); //object number 272
	CreateDynamicObject(16101,1323.9000244,-1057.5999756,3352.3999023,0.0000000,0.0000000,0.0000000); //object number 273
	CreateDynamicObject(16101,1316.8000488,-1055.6999512,3352.3999023,0.0000000,0.0000000,0.0000000); //object number 274
	CreateDynamicObject(19372,1322.3000488,-1057.3000488,3361.3000488,0.0000000,90.0000000,0.0000000); //object number 275
	CreateDynamicObject(19372,1318.8000488,-1057.3000488,3361.3000488,0.0000000,90.0000000,0.0000000); //object number 276
	CreateDynamicObject(19372,1315.3000488,-1057.3000488,3361.3000488,0.0000000,90.0000000,0.0000000); //object number 277
	CreateDynamicObject(1606,1320.5999756,-1057.1999512,3360.1999512,0.0000000,0.0000000,80.0000000); //object number 278
	CreateDynamicObject(2198,1323.6099854,-1057.0100098,3358.8999023,0.0000000,180.0000000,270.0000000); //object number 279
	CreateDynamicObject(2198,1323.6099854,-1058.9000244,3358.8999023,0.0000000,179.9945068,270.0000000); //object number 280
	CreateDynamicObject(2198,1322.6999512,-1056.0200195,3358.8999023,0.0000000,179.9945068,0.0000000); //object number 281
	CreateDynamicObject(2198,1320.8000488,-1056.0200195,3358.8999023,0.0000000,179.9945068,0.0000000); //object number 282
	CreateDynamicObject(2198,1318.9000244,-1056.0200195,3358.8999023,0.0000000,179.9945068,0.0000000); //object number 283
	CreateDynamicObject(2198,1317.0000000,-1056.0200195,3358.8999023,0.0000000,179.9945068,0.0000000); //object number 284
	CreateDynamicObject(2811,1323.3000488,-1057.0999756,3359.1000977,0.0000000,0.0000000,327.9951172); //object number 285
	CreateDynamicObject(2811,1323.0999756,-1057.0999756,3359.1000977,0.0000000,0.0000000,307.9913330); //object number 286
	CreateDynamicObject(2811,1323.1999512,-1056.9000244,3359.1000977,0.0000000,0.0000000,307.9907227); //object number 287
	CreateDynamicObject(2811,1320.9000244,-1056.9000244,3359.1000977,0.0000000,0.0000000,307.9907227); //object number 288
	CreateDynamicObject(2811,1318.9000244,-1056.6999512,3359.1000977,0.0000000,0.0000000,307.9907227); //object number 289
	CreateDynamicObject(2811,1318.8000488,-1056.5000000,3359.1000977,0.0000000,0.0000000,307.9907227); //object number 290
	CreateDynamicObject(2811,1317.1999512,-1057.1999512,3359.1000977,0.0000000,0.0000000,307.9907227); //object number 291
	CreateDynamicObject(2811,1335.5999756,-1058.6999512,3359.1000977,0.0000000,0.0000000,307.9907227); //object number 292
	CreateDynamicObject(2811,1335.8000488,-1058.9000244,3359.1000977,0.0000000,0.0000000,307.9907227); //object number 293
	CreateDynamicObject(2811,1335.5000000,-1059.0999756,3359.1000977,0.0000000,0.0000000,307.9907227); //object number 294
	CreateDynamicObject(2811,1335.5000000,-1060.5999756,3359.1000977,0.0000000,0.0000000,307.9907227); //object number 295
	CreateDynamicObject(2811,1334.9000244,-1064.1999512,3359.1000977,0.0000000,0.0000000,307.9907227); //object number 296
	CreateDynamicObject(2811,1335.0999756,-1064.0999756,3359.1000977,0.0000000,0.0000000,307.9907227); //object number 297
	CreateDynamicObject(2811,1334.9000244,-1064.0000000,3359.1000977,0.0000000,0.0000000,307.9907227); //object number 298
	CreateDynamicObject(2811,1335.6999512,-1062.9000244,3359.1000977,0.0000000,0.0000000,307.9907227); //object number 299
	CreateDynamicObject(2811,1335.6999512,-1062.6999512,3359.1000977,0.0000000,0.0000000,307.9907227); //object number 300
	CreateDynamicObject(2195,1335.5000000,-1045.8000488,3359.5000000,0.0000000,0.0000000,100.0000000); //object number 301
	CreateDynamicObject(2010,1324.8000488,-1057.0000000,3358.6999512,0.0000000,0.0000000,107.9901123); //object number 302
	CreateDynamicObject(1506,1312.1099854,-1040.9399414,3360.5000000,0.0000000,0.0000000,0.0000000); //object number 303
	CreateDynamicObject(1506,1304.2299805,-1041.8000488,3362.5000000,0.0000000,0.0000000,270.0000000); //object number 304
	CreateDynamicObject(1506,1304.2299805,-1051.5999756,3364.5000000,0.0000000,0.0000000,270.0000000); //object number 305
	CreateDynamicObject(1506,1305.5000000,-1054.6400146,3364.5000000,0.0000000,0.0000000,0.0000000); //object number 306
	CreateDynamicObject(1506,1310.9000244,-1054.6400146,3364.5000000,0.0000000,0.0000000,0.0000000); //object number 307
	CreateDynamicObject(1506,1314.8499756,-1050.7800293,3364.5000000,0.0000000,0.0000000,270.0000000); //object number 308
	CreateDynamicObject(2315,1315.1992188,-1050.2822266,3360.8000488,0.0000000,90.0000000,0.0000000); //object number 309
	CreateDynamicObject(2315,1315.3000488,-1050.3000488,3366.3999023,0.0000000,90.0000000,179.9945068); //object number 310
	CreateDynamicObject(1506,1313.9000244,-1040.9100342,3366.3000488,0.0000000,0.0000000,179.9945068); //object number 311
	CreateDynamicObject(1506,1304.6700439,-1043.1999512,3368.3000488,0.0000000,0.0000000,90.0000000); //object number 312
	CreateDynamicObject(1506,1304.6400146,-1051.5999756,3370.3000488,0.0000000,0.0000000,270.0000000); //object number 313
	CreateDynamicObject(1506,1305.8000488,-1054.6400146,3370.3000488,0.0000000,0.0000000,0.0000000); //object number 314
	CreateDynamicObject(1506,1310.9000244,-1054.6400146,3370.3000488,0.0000000,0.0000000,0.0000000); //object number 315
	CreateDynamicObject(1506,1315.1999512,-1050.7900391,3370.3000488,0.0000000,0.0000000,270.0000000); //object number 316
	CreateDynamicObject(2315,1315.6999512,-1052.6999512,3372.0000000,0.0000000,90.0000000,179.9945068); //object number 317
	CreateDynamicObject(2315,1315.6999512,-1050.4000244,3372.0000000,0.0000000,90.0000000,179.9945068); //object number 318
	CreateDynamicObject(2315,1315.6999512,-1052.3000488,3372.8999023,270.0000000,0.0000000,90.0000000); //object number 319
	CreateDynamicObject(2198,1307.0000000,-1055.0999756,3360.8999023,0.0000000,90.0000000,0.0000000); //object number 320
	CreateDynamicObject(2198,1307.0000000,-1055.0999756,3359.0000000,0.0000000,90.0000000,0.0000000); //object number 321
	CreateDynamicObject(2198,1305.3239746,-1055.0999756,3359.0000000,0.0000000,90.0000000,0.0000000); //object number 322
	CreateDynamicObject(2198,1305.3239746,-1055.0999756,3360.8999023,0.0000000,90.0000000,0.0000000); //object number 323
	CreateDynamicObject(2198,1306.6009521,-1056.0710449,3361.1940918,0.0000000,0.0000000,90.0000000); //object number 324
	CreateDynamicObject(2198,1305.9000244,-1056.0699463,3361.1940918,0.0000000,0.0000000,90.0000000); //object number 325
	CreateDynamicObject(2198,1303.3399658,-1053.0999756,3360.8000488,0.0000000,90.0000000,270.0000000); //object number 326
	CreateDynamicObject(2198,1303.3399658,-1053.0999756,3358.8999023,0.0000000,90.0000000,270.0000000); //object number 327
	CreateDynamicObject(2198,1303.3399658,-1051.4300537,3360.8000488,0.0000000,90.0000000,270.0000000); //object number 328
	CreateDynamicObject(2198,1303.3399658,-1051.4300537,3358.8999023,0.0000000,90.0000000,270.0000000); //object number 329
	CreateDynamicObject(2198,1302.3609619,-1051.9560547,3361.1999512,0.0000000,0.0000000,0.0000000); //object number 330
	CreateDynamicObject(2198,1302.3599854,-1052.8339844,3361.1999512,0.0000000,0.0000000,0.0000000); //object number 331
	CreateDynamicObject(2010,1314.0000000,-1054.3000488,3358.6999512,0.0000000,0.0000000,107.9901123); //object number 332
	CreateDynamicObject(2010,1304.1999512,-1054.3000488,3358.6999512,0.0000000,0.0000000,51.9901123); //object number 333
	CreateDynamicObject(2198,1310.7294922,-1055.0996094,3359.0000000,0.0000000,90.0000000,0.0000000); //object number 334
	CreateDynamicObject(2198,1312.1300049,-1040.4399414,3362.6000977,0.0000000,90.0000000,179.9945068); //object number 335
	CreateDynamicObject(2198,1313.8000488,-1040.4399414,3360.6999512,0.0000000,90.0000000,179.9945068); //object number 336
	CreateDynamicObject(2198,1313.8000488,-1040.4399414,3362.6000977,0.0000000,90.0000000,179.9945068); //object number 337
	CreateDynamicObject(2198,1312.4610596,-1040.4799805,3363.0000000,0.0000000,0.0000000,90.0000000); //object number 338
	CreateDynamicObject(2198,1313.3339844,-1040.4809570,3363.0000000,0.0000000,0.0000000,90.0000000); //object number 339
	CreateDynamicObject(2198,1303.7600098,-1043.3000488,3364.6000977,0.0000000,90.0000000,270.0000000); //object number 340
	CreateDynamicObject(2198,1303.7600098,-1043.3000488,3362.6999512,0.0000000,90.0000000,270.0000000); //object number 341
	CreateDynamicObject(2198,1303.7600098,-1041.6240234,3362.6999512,0.0000000,90.0000000,270.0000000); //object number 342
	CreateDynamicObject(2198,1303.7600098,-1041.6240234,3364.6000977,0.0000000,90.0000000,270.0000000); //object number 343
	CreateDynamicObject(2198,1302.7800293,-1043.0310059,3365.0000000,0.0000000,0.0000000,0.0000000); //object number 344
	CreateDynamicObject(2198,1302.7810059,-1042.1500244,3365.0000000,0.0000000,0.0000000,0.0000000); //object number 345
	CreateDynamicObject(2198,1303.7600098,-1051.4200439,3366.6000977,0.0000000,90.0000000,270.0000000); //object number 346
	CreateDynamicObject(2198,1303.7600098,-1051.4200439,3364.6999512,0.0000000,90.0000000,270.0000000); //object number 347
	CreateDynamicObject(2198,1303.7600098,-1053.0999756,3364.6999512,0.0000000,90.0000000,270.0000000); //object number 348
	CreateDynamicObject(2198,1303.7600098,-1053.0999756,3366.6000977,0.0000000,90.0000000,270.0000000); //object number 349
	CreateDynamicObject(2198,1302.7810059,-1051.9470215,3367.0000000,0.0000000,0.0000000,0.0000000); //object number 350
	CreateDynamicObject(2198,1302.7800293,-1052.8339844,3367.0000000,0.0000000,0.0000000,0.0000000); //object number 351
	CreateDynamicObject(2198,1305.3199463,-1055.0999756,3366.6000977,0.0000000,90.0000000,0.0000000); //object number 352
	CreateDynamicObject(2198,1305.3199463,-1055.0999756,3364.6999512,0.0000000,90.0000000,0.0000000); //object number 353
	CreateDynamicObject(2198,1307.0000000,-1055.0999756,3364.6999512,0.0000000,90.0000000,0.0000000); //object number 354
	CreateDynamicObject(2198,1307.0000000,-1055.0999756,3366.6000977,0.0000000,90.0000000,0.0000000); //object number 355
	CreateDynamicObject(2198,1310.7209473,-1055.0999756,3366.6000977,0.0000000,90.0000000,0.0000000); //object number 356
	CreateDynamicObject(2198,1310.7209473,-1055.0999756,3364.6999512,0.0000000,90.0000000,0.0000000); //object number 357
	CreateDynamicObject(2198,1312.4000244,-1055.0999756,3364.6999512,0.0000000,90.0000000,0.0000000); //object number 358
	CreateDynamicObject(2198,1312.4000244,-1055.0999756,3366.6000977,0.0000000,90.0000000,0.0000000); //object number 359
	CreateDynamicObject(2198,1305.8499756,-1056.0699463,3367.0000000,0.0000000,0.0000000,90.0000000); //object number 360
	CreateDynamicObject(2198,1306.7309570,-1056.0699463,3367.0000000,0.0000000,0.0000000,90.0000000); //object number 361
	CreateDynamicObject(2198,1311.2500000,-1056.0699463,3367.0000000,0.0000000,0.0000000,90.0000000); //object number 362
	CreateDynamicObject(2198,1312.1330566,-1056.0699463,3367.0000000,0.0000000,0.0000000,90.0000000); //object number 363
	CreateDynamicObject(2198,1314.0780029,-1040.4399414,3368.3999023,0.0000000,90.0000000,179.9945068); //object number 364
	CreateDynamicObject(2198,1314.0780029,-1040.4399414,3366.5000000,0.0000000,90.0000000,179.9945068); //object number 365
	CreateDynamicObject(2198,1312.4000244,-1040.4399414,3366.5000000,0.0000000,90.0000000,179.9945068); //object number 366
	CreateDynamicObject(2198,1312.4000244,-1040.4399414,3368.3999023,0.0000000,90.0000000,179.9945068); //object number 367
	CreateDynamicObject(2198,1312.7320557,-1040.4799805,3368.8000488,0.0000000,0.0000000,90.0000000); //object number 368
	CreateDynamicObject(2198,1313.6149902,-1040.4809570,3368.8000488,0.0000000,0.0000000,90.0000000); //object number 369
	CreateDynamicObject(2198,1304.1800537,-1041.5100098,3370.3999023,0.0000000,90.0000000,270.0000000); //object number 370
	CreateDynamicObject(2198,1304.1800537,-1041.5100098,3368.5000000,0.0000000,90.0000000,270.0000000); //object number 371
	CreateDynamicObject(2198,1304.1800537,-1043.1800537,3368.5000000,0.0000000,90.0000000,270.0000000); //object number 372
	CreateDynamicObject(2198,1304.1800537,-1043.1800537,3370.3999023,0.0000000,90.0000000,270.0000000); //object number 373
	CreateDynamicObject(2198,1303.2099609,-1042.9071938,3370.8000488,0.0000000,0.0000000,0.0000000); //object number 374
	CreateDynamicObject(2198,1303.1999512,-1042.0360107,3370.8000488,0.0000000,0.0000000,0.0000000); //object number 375
	CreateDynamicObject(2198,1304.1800537,-1053.0999756,3372.3999023,0.0000000,90.0000000,270.0000000); //object number 376
	CreateDynamicObject(2198,1304.1800537,-1053.0999756,3370.5000000,0.0000000,90.0000000,270.0000000); //object number 377
	CreateDynamicObject(2198,1304.1800537,-1051.4200439,3370.5000000,0.0000000,90.0000000,270.0000000); //object number 378
	CreateDynamicObject(2198,1304.1800537,-1051.4200439,3372.3999023,0.0000000,90.0000000,270.0000000); //object number 379
	CreateDynamicObject(2198,1303.1999512,-1051.9460449,3372.8000488,0.0000000,0.0000000,0.0000000); //object number 380
	CreateDynamicObject(2198,1303.2010498,-1052.8320312,3372.8000488,0.0000000,0.0000000,0.0000000); //object number 381
	CreateDynamicObject(2198,1307.3000488,-1055.0999756,3372.3999023,0.0000000,90.0000000,0.0000000); //object number 382
	CreateDynamicObject(2198,1307.3000488,-1055.0999756,3370.5000000,0.0000000,90.0000000,0.0000000); //object number 383
	CreateDynamicObject(2198,1305.6199951,-1055.0999756,3370.5000000,0.0000000,90.0000000,0.0000000); //object number 384
	CreateDynamicObject(2198,1305.6199951,-1055.0999756,3372.3999023,0.0000000,90.0000000,0.0000000); //object number 385
	CreateDynamicObject(2198,1310.7199707,-1055.0999756,3372.3999023,0.0000000,90.0000000,0.0000000); //object number 386
	CreateDynamicObject(2198,1310.7199707,-1055.0999756,3370.5000000,0.0000000,90.0000000,0.0000000); //object number 387
	CreateDynamicObject(2198,1312.4000244,-1055.0999756,3370.5000000,0.0000000,90.0000000,0.0000000); //object number 388
	CreateDynamicObject(2198,1312.4000244,-1055.0999756,3372.3999023,0.0000000,90.0000000,0.0000000); //object number 389
	CreateDynamicObject(2198,1307.0340576,-1056.0810547,3372.8000488,0.0000000,0.0000000,90.0000000); //object number 390
	CreateDynamicObject(2198,1306.1450195,-1056.0799561,3372.8000488,0.0000000,0.0000000,90.0000000); //object number 391
	CreateDynamicObject(2198,1311.2480469,-1056.0799561,3372.8000488,0.0000000,0.0000000,90.0000000); //object number 392
	CreateDynamicObject(2198,1312.1319580,-1056.0810547,3372.8000488,0.0000000,0.0000000,90.0000000); //object number 393
	CreateDynamicObject(2010,1314.4000244,-1054.0999756,3364.5000000,0.0000000,0.0000000,107.9901123); //object number 394
	CreateDynamicObject(2010,1304.8000488,-1054.0999756,3364.5000000,0.0000000,0.0000000,107.9901123); //object number 395
	CreateDynamicObject(2010,1305.0999756,-1054.0999756,3370.3000488,0.0000000,0.0000000,143.9901123); //object number 396
	CreateDynamicObject(2010,1314.9000244,-1054.0999756,3370.3000488,0.0000000,0.0000000,209.9868164); //object number 397
	CreateDynamicObject(19435,1303.3000488,-1052.3599854,3358.6201172,0.0000000,90.0000000,0.0000000); //object number 398
	CreateDynamicObject(19435,1306.2600098,-1055.1999512,3358.6201172,0.0000000,90.0000000,90.0000000); //object number 399
	CreateDynamicObject(19435,1311.6600342,-1055.1999512,3358.6201172,0.0000000,90.0000000,90.0000000); //object number 400
	CreateDynamicObject(19435,1312.8599854,-1040.3000488,3360.4199219,0.0000000,90.0000000,90.0000000); //object number 401
	CreateDynamicObject(19435,1306.2600098,-1055.1999512,3364.4199219,0.0000000,90.0000000,90.0000000); //object number 402
	CreateDynamicObject(19435,1311.6500244,-1055.1999512,3364.4199219,0.0000000,90.0000000,90.0000000); //object number 403
	CreateDynamicObject(19435,1303.6999512,-1042.5600586,3362.4199219,0.0000000,90.0000000,0.0000000); //object number 404
	CreateDynamicObject(19435,1303.6999512,-1052.3599854,3364.4199219,0.0000000,90.0000000,0.0000000); //object number 405
	CreateDynamicObject(19435,1315.3000488,-1051.5400391,3364.4199219,0.0000000,90.0000000,0.0000000); //object number 406
	CreateDynamicObject(19435,1313.1300049,-1040.4000244,3366.2199707,0.0000000,90.0000000,90.0000000); //object number 407
	CreateDynamicObject(19435,1304.0000000,-1042.4399414,3368.2199707,0.0000000,90.0000000,0.0000000); //object number 408
	CreateDynamicObject(19435,1304.0999756,-1052.3599854,3370.2199707,0.0000000,90.0000000,0.0000000); //object number 409
	CreateDynamicObject(19435,1315.8000488,-1051.5400391,3370.2199707,0.0000000,90.0000000,0.0000000); //object number 410
	CreateDynamicObject(19435,1306.5699463,-1055.1999512,3370.2199707,0.0000000,90.0000000,90.0000000); //object number 411
	CreateDynamicObject(19435,1311.6600342,-1055.1999512,3370.2199707,0.0000000,90.0000000,90.0000000); //object number 412
	CreateDynamicObject(2949,1313.0999756,-1048.9399414,3352.8999023,0.0000000,0.0000000,270.0000000); //object number 413
	CreateDynamicObject(2315,1313.5999756,-1049.4010010,3354.8000488,0.0000000,90.0000000,89.9945068); //object number 414
	CreateDynamicObject(2315,1311.1999512,-1049.4010010,3354.8000488,0.0000000,90.0000000,89.9945068); //object number 415
	CreateDynamicObject(2315,1313.5999756,-1049.4000244,3355.6999512,90.0000000,90.0000000,89.9945068); //object number 416
	CreateDynamicObject(2315,1311.1999512,-1049.4000244,3355.6999512,90.0000000,90.0000000,89.9945068); //object number 417
	CreateDynamicObject(1502,1319.1992188,-1045.2998047,3358.6999512,0.0000000,0.0000000,0.0000000); //object number 418
	CreateDynamicObject(19435,1314.8000488,-1051.4000244,3358.6201172,359.0000000,90.0000000,90.0000000); //object number 419
	CreateDynamicObject(19435,1330.6999512,-1063.5999756,3358.6298828,0.0000000,90.0000000,0.0000000); //object number 420
	CreateDynamicObject(19435,1330.6999512,-1062.0000000,3358.6298828,0.0000000,90.0000000,0.0000000); //object number 421
	// ==========================================================================================================================================//

	return Y_HOOKS_CONTINUE_RETURN_1;
}

stock function MAP::AbrirPortaoDP(playerid)
{
	if ( GATE_DP_OPENED )
		return false;

	if ( !IsPlayerInRangeOfPoint(playerid, 25.0, 1551.19995, -1627.59998, 9.29930) )
		return false;

	GATE_DP_OPENED = true;
	MoveDynamicObject(GATE_DP, 1551.19995, -1627.59998, 9.29930, 3);
	SetTimerEx("MAP_FecharPortaoDP", 6000, false, "i", playerid);
	return true;
}
forward function MAP::FecharPortaoDP(playerid);
public function MAP::FecharPortaoDP(playerid)
{
	GATE_DP_OPENED = false;
	MoveDynamicObject(GATE_DP, 1551.19995, -1627.59998, 15.20000, 3);
	return true;
}


hook OnPlayerStateChange(playerid, newstate, oldstate)
{
	if ( newstate == PLAYER_STATE_DRIVER )
	{
		if ( GetVehicleModel(GetPlayerVehicleID(playerid)) == 510 )
		{
			for(new i; i < sizeof(BikesNovatos); i++)
			{
				if ( GetPlayerVehicleID(playerid) == BikesNovatos[i] )
				{
					if ( GetPlayerScore(playerid) > 5 )
					{
						RemovePlayerFromVehicle(playerid);
						SendClientMessage(playerid, COR_ERRO, "Erro: Voc� n�o pode usar mais esse veiculo.");
					}
				}
			}
		}
	}
	return Y_HOOKS_CONTINUE_RETURN_1;
}
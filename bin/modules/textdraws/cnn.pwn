
new static Text:TDEditor_TD[2], TimerID = -1;


#include <YSI_Coding\y_hooks>

hook OnGameModeInit()
{
	TDEditor_TD[0] = TextDrawCreate(5.000000, 191.166671, "_"); // Autor 
	TextDrawLetterSize(TDEditor_TD[0], 0.279998, 1.098332);
	TextDrawAlignment(TDEditor_TD[0], 1);
	TextDrawColor(TDEditor_TD[0], -1);
	TextDrawSetShadow(TDEditor_TD[0], 1);
	TextDrawSetOutline(TDEditor_TD[0], 0);
	TextDrawBackgroundColor(TDEditor_TD[0], 255);
	TextDrawFont(TDEditor_TD[0], 2);
	TextDrawSetProportional(TDEditor_TD[0], 1);
	TextDrawSetShadow(TDEditor_TD[0], 1);

	TDEditor_TD[1] = TextDrawCreate(320.625000, 206.183029, "_"); // mensagem
	TextDrawLetterSize(TDEditor_TD[1], 0.400000, 1.600000);
	TextDrawTextSize(TDEditor_TD[1], 10.000000, 640.000000);
	TextDrawAlignment(TDEditor_TD[1], 2);
	TextDrawColor(TDEditor_TD[1], -1);
	TextDrawUseBox(TDEditor_TD[1], 1);
	TextDrawBoxColor(TDEditor_TD[1], 50);
	TextDrawSetShadow(TDEditor_TD[1], 1);
	TextDrawSetOutline(TDEditor_TD[1], 0);
	TextDrawBackgroundColor(TDEditor_TD[1], 255);
	TextDrawFont(TDEditor_TD[1], 1);
	TextDrawSetProportional(TDEditor_TD[1], 1);
	TextDrawSetShadow(TDEditor_TD[1], 1);

	TimerID = -1;
}

stock function TD::ShowCnnMessage(playerid, mensagem[])
{
	new str[MAX_PLAYER_NAME + 20], cargo[50];

	switch( call::ADMIN->GetPlayerAdminLevel(playerid) )
	{
		case DIRECAO: cargo = "~g~]]]] Diretor~w~";
		case ADMINISTRADOR: cargo = "~b~]]] Adm~w~";
		case MODERADOR: cargo = "~y~]] Mod~w~";
		case AJUDANTE: cargo = "~y~~h~~h~~h~~h~~h~] Ajud~w~";
	}
	format(str, sizeof(str), "%s %s", cargo, GetUserName(playerid));
	TextDrawSetString(TDEditor_TD[0], str);
	TextDrawShowForAll(TDEditor_TD[0]);

	TextDrawSetString(TDEditor_TD[1], mensagem);
	TextDrawShowForAll(TDEditor_TD[1]);

	if ( TimerID != -1 )
		KillTimer(TimerID);

	TimerID = SetTimer("TD_HideCnnMessage", 5000, false); 
	return true;
}

forward function TD::HideCnnMessage();
public function TD::HideCnnMessage()
{
	if ( TimerID != -1)
	{
		KillTimer(TimerID);
		TimerID = -1;

		TextDrawHideForAll(TDEditor_TD[0]);
		TextDrawHideForAll(TDEditor_TD[1]);
		
		TextDrawSetString(TDEditor_TD[0], "_");
		TextDrawSetString(TDEditor_TD[1], "_");
	}
	return true;
}

/*
*			@title 		HUD
*			@author: 	SammyJ
*			@date: 		08/10/2017
*
*/


/*
*
*			Hooks included
*
*/
#include <YSI_Coding\y_hooks>

/*
*
*			variaveis
*
*/

enum pBarInfo
{
	PlayerBar:pBarFome,
	PlayerBar:pBarSede,
	PlayerBar:pBarSono
}
static Text:TDEditor_TD[12], PlayerText:TDEditor_PTD[MAX_PLAYERS][4], pBar[MAX_PLAYERS][pBarInfo];

/*
*
*			hooks callbacks
*
*/
hook OnGameModeInit()
{
	print("� Criando textdraws de HUD.\n");

	TDEditor_TD[0] = TextDrawCreate(634.000000, 8.249985, "-"); // data
	TextDrawLetterSize(TDEditor_TD[0], 0.371294, 1.045832);
	TextDrawTextSize(TDEditor_TD[0], 770.000000, 0.000000);
	TextDrawAlignment(TDEditor_TD[0], 3);
	TextDrawColor(TDEditor_TD[0], -1);
	TextDrawUseBox(TDEditor_TD[0], 1);
	TextDrawBoxColor(TDEditor_TD[0], 0);
	TextDrawSetShadow(TDEditor_TD[0], 1);
	TextDrawSetOutline(TDEditor_TD[0], 0);
	TextDrawBackgroundColor(TDEditor_TD[0], 255);
	TextDrawFont(TDEditor_TD[0], 1);
	TextDrawSetProportional(TDEditor_TD[0], 1);
	TextDrawSetShadow(TDEditor_TD[0], 1);

	TDEditor_TD[1] = TextDrawCreate(547.253234, 23.616699, "-"); // hora
	TextDrawLetterSize(TDEditor_TD[1], 0.400000, 1.600000);
	TextDrawAlignment(TDEditor_TD[1], 1);
	TextDrawColor(TDEditor_TD[1], -1);
	TextDrawSetShadow(TDEditor_TD[1], 1);
	TextDrawSetOutline(TDEditor_TD[1], 0);
	TextDrawBackgroundColor(TDEditor_TD[1], 255);
	TextDrawFont(TDEditor_TD[1], 2);
	TextDrawSetProportional(TDEditor_TD[1], 1);
	TextDrawSetShadow(TDEditor_TD[1], 1);

	TDEditor_TD[2] = TextDrawCreate(31.928283, 426.166534, SERVER_NAME);
	TextDrawLetterSize(TDEditor_TD[2], 0.275841, 0.964166);
	TextDrawTextSize(TDEditor_TD[2], 189.000000, 0.000000);
	TextDrawAlignment(TDEditor_TD[2], 1);
	TextDrawColor(TDEditor_TD[2], -1);
	TextDrawUseBox(TDEditor_TD[2], 1);
	TextDrawBoxColor(TDEditor_TD[2], 0);
	TextDrawSetShadow(TDEditor_TD[2], 1);
	TextDrawSetOutline(TDEditor_TD[2], 0);
	TextDrawBackgroundColor(TDEditor_TD[2], 255);
	TextDrawFont(TDEditor_TD[2], 2);
	TextDrawSetProportional(TDEditor_TD[2], 1);
	TextDrawSetShadow(TDEditor_TD[2], 1);

	TDEditor_TD[3] = TextDrawCreate(37.980426, 214.748489, "box");
	TextDrawLetterSize(TDEditor_TD[3], 0.000000, 12.282013);
	TextDrawTextSize(TDEditor_TD[3], 135.000000, 0.000000);
	TextDrawAlignment(TDEditor_TD[3], 1);
	TextDrawColor(TDEditor_TD[3], -1);
	TextDrawUseBox(TDEditor_TD[3], 1);
	TextDrawBoxColor(TDEditor_TD[3], 150);
	TextDrawSetShadow(TDEditor_TD[3], 0);
	TextDrawSetOutline(TDEditor_TD[3], 0);
	TextDrawBackgroundColor(TDEditor_TD[3], 255);
	TextDrawFont(TDEditor_TD[3], 1);
	TextDrawSetProportional(TDEditor_TD[3], 1);
	TextDrawSetShadow(TDEditor_TD[3], 0);

	TDEditor_TD[4] = TextDrawCreate(38.000000, 200.000000, "status");
	TextDrawLetterSize(TDEditor_TD[4], 0.539149, 1.955834);
	TextDrawAlignment(TDEditor_TD[4], 1);
	TextDrawColor(TDEditor_TD[4], -1);
	TextDrawSetShadow(TDEditor_TD[4], 1);
	TextDrawSetOutline(TDEditor_TD[4], 0);
	TextDrawBackgroundColor(TDEditor_TD[4], 255);
	TextDrawFont(TDEditor_TD[4], 0);
	TextDrawSetProportional(TDEditor_TD[4], 1);
	TextDrawSetShadow(TDEditor_TD[4], 1);

	TDEditor_TD[5] = TextDrawCreate(40.000000, 222.000000, "-");
	TextDrawLetterSize(TDEditor_TD[5], 7.000000, 0.400000);
	TextDrawAlignment(TDEditor_TD[5], 1);
	TextDrawColor(TDEditor_TD[5], -1);
	TextDrawSetShadow(TDEditor_TD[5], 0);
	TextDrawSetOutline(TDEditor_TD[5], 0);
	TextDrawBackgroundColor(TDEditor_TD[5], 255);
	TextDrawFont(TDEditor_TD[5], 1);
	TextDrawSetProportional(TDEditor_TD[5], 1);
	TextDrawSetShadow(TDEditor_TD[5], 0);

	TDEditor_TD[6] = TextDrawCreate(40.000000, 232.000000, "~h~~g~+~w~UP");
	TextDrawLetterSize(TDEditor_TD[6], 0.320818, 1.232499);
	TextDrawTextSize(TDEditor_TD[6], 135.000000, 0.000000);
	TextDrawAlignment(TDEditor_TD[6], 1);
	TextDrawColor(TDEditor_TD[6], -1);
	TextDrawUseBox(TDEditor_TD[6], 0);
	TextDrawBoxColor(TDEditor_TD[6], 6618980);
	TextDrawSetShadow(TDEditor_TD[6], -1);
	TextDrawSetOutline(TDEditor_TD[6], 1);
	TextDrawBackgroundColor(TDEditor_TD[6], 255);
	TextDrawFont(TDEditor_TD[6], 1);
	TextDrawSetProportional(TDEditor_TD[6], 1);
	TextDrawSetShadow(TDEditor_TD[6], -1);

	TDEditor_TD[7] = TextDrawCreate(40.000000, 247.000000, "Level");
	TextDrawLetterSize(TDEditor_TD[7], 0.320818, 1.232499);
	TextDrawTextSize(TDEditor_TD[7], 135.000000, 0.000000);
	TextDrawAlignment(TDEditor_TD[7], 1);
	TextDrawColor(TDEditor_TD[7], -1);
	TextDrawUseBox(TDEditor_TD[7], 0);
	TextDrawBoxColor(TDEditor_TD[7], 6618980);
	TextDrawSetShadow(TDEditor_TD[7], -1);
	TextDrawSetOutline(TDEditor_TD[7], 1);
	TextDrawBackgroundColor(TDEditor_TD[7], 255);
	TextDrawFont(TDEditor_TD[7], 1);
	TextDrawSetProportional(TDEditor_TD[7], 1);
	TextDrawSetShadow(TDEditor_TD[7], -1);

	TDEditor_TD[8] = TextDrawCreate(40.000000, 262.000000, "Exp");
	TextDrawLetterSize(TDEditor_TD[8], 0.320818, 1.232499);
	TextDrawTextSize(TDEditor_TD[8], 135.000000, 0.000000);
	TextDrawAlignment(TDEditor_TD[8], 1);
	TextDrawColor(TDEditor_TD[8], -1);
	TextDrawUseBox(TDEditor_TD[8], 0);
	TextDrawBoxColor(TDEditor_TD[8], 6618980);
	TextDrawSetShadow(TDEditor_TD[8], -1);
	TextDrawSetOutline(TDEditor_TD[8], 1);
	TextDrawBackgroundColor(TDEditor_TD[8], 255);
	TextDrawFont(TDEditor_TD[8], 1);
	TextDrawSetProportional(TDEditor_TD[8], 1);
	TextDrawSetShadow(TDEditor_TD[8], -1);

	TDEditor_TD[9] = TextDrawCreate(40.000000, 277.000000, "Fome");
	TextDrawLetterSize(TDEditor_TD[9], 0.320818, 1.232499);
	TextDrawTextSize(TDEditor_TD[9], 135.000000, 0.000000);
	TextDrawAlignment(TDEditor_TD[9], 1);
	TextDrawColor(TDEditor_TD[9], -1);
	TextDrawUseBox(TDEditor_TD[9], 0);
	TextDrawBoxColor(TDEditor_TD[9], 6618980);
	TextDrawSetShadow(TDEditor_TD[9], -1);
	TextDrawSetOutline(TDEditor_TD[9], 1);
	TextDrawBackgroundColor(TDEditor_TD[9], 255);
	TextDrawFont(TDEditor_TD[9], 1);
	TextDrawSetProportional(TDEditor_TD[9], 1);
	TextDrawSetShadow(TDEditor_TD[9], -1);

	TDEditor_TD[10] = TextDrawCreate(40.000000, 292.000000, "Sede");
	TextDrawLetterSize(TDEditor_TD[10], 0.320818, 1.232499);
	TextDrawTextSize(TDEditor_TD[10], 135.000000, 0.000000);
	TextDrawAlignment(TDEditor_TD[10], 1);
	TextDrawColor(TDEditor_TD[10], -1);
	TextDrawUseBox(TDEditor_TD[10], 0);
	TextDrawBoxColor(TDEditor_TD[10], 6618980);
	TextDrawSetShadow(TDEditor_TD[10], -1);
	TextDrawSetOutline(TDEditor_TD[10], 1);
	TextDrawBackgroundColor(TDEditor_TD[10], 255);
	TextDrawFont(TDEditor_TD[10], 1);
	TextDrawSetProportional(TDEditor_TD[10], 1);
	TextDrawSetShadow(TDEditor_TD[10], -1);

	TDEditor_TD[11] = TextDrawCreate(40.000000, 307.000000, "Sono");
	TextDrawLetterSize(TDEditor_TD[11], 0.320818, 1.232499);
	TextDrawTextSize(TDEditor_TD[11], 135.000000, 0.000000);
	TextDrawAlignment(TDEditor_TD[11], 1);
	TextDrawColor(TDEditor_TD[11], -1);
	TextDrawUseBox(TDEditor_TD[11], 0);
	TextDrawBoxColor(TDEditor_TD[11], 6618980);
	TextDrawSetShadow(TDEditor_TD[11], -1);
	TextDrawSetOutline(TDEditor_TD[11], 1);
	TextDrawBackgroundColor(TDEditor_TD[11], 255);
	TextDrawFont(TDEditor_TD[11], 1);
	TextDrawSetProportional(TDEditor_TD[11], 1);
	TextDrawSetShadow(TDEditor_TD[11], -1);

	SetSVarInt("dateInit", 0);
}

hook OnPlayerConnect(playerid)
{
	TDEditor_PTD[playerid][0] = CreatePlayerTextDraw(playerid, 78.000000, 231.000000, "_"); // relogio up
	PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][0], 0.320818, 1.232499);
	PlayerTextDrawTextSize(playerid, TDEditor_PTD[playerid][0], 135.010055, 0.000000);
	PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][0], 1);
	PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][0], -1);
	PlayerTextDrawUseBox(playerid, TDEditor_PTD[playerid][0], 1);
	PlayerTextDrawBoxColor(playerid, TDEditor_PTD[playerid][0], 0);
	PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][0], -1);
	PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][0], 1);
	PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][0], 255);
	PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][0], 1);
	PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][0], 1);
	PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][0], -1);

	TDEditor_PTD[playerid][1] = CreatePlayerTextDraw(playerid, 78.000000, 247.000000, "-");
	PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][1], 0.320818, 1.232499);
	PlayerTextDrawTextSize(playerid, TDEditor_PTD[playerid][1], 135.010055, 0.000000);
	PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][1], 1);
	PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][1], -1);
	PlayerTextDrawUseBox(playerid, TDEditor_PTD[playerid][1], 1);
	PlayerTextDrawBoxColor(playerid, TDEditor_PTD[playerid][1], 0);
	PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][1], -1);
	PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][1], 1);
	PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][1], 255);
	PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][1], 1);
	PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][1], 1);
	PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][1], -1);

	TDEditor_PTD[playerid][2] = CreatePlayerTextDraw(playerid, 78.000000, 263.000000, "-/-");
	PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][2], 0.320818, 1.232499);
	PlayerTextDrawTextSize(playerid, TDEditor_PTD[playerid][2], 134.199951, 0.000000);
	PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][2], 1);
	PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][2], -1);
	PlayerTextDrawUseBox(playerid, TDEditor_PTD[playerid][2], 1);
	PlayerTextDrawBoxColor(playerid, TDEditor_PTD[playerid][2], 0);
	PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][2], -1);
	PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][2], 1);
	PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][2], 255);
	PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][2], 1);
	PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][2], 1);
	PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][2], -1);

	pBar[playerid][pBarFome] = CreatePlayerProgressBar(playerid, 78.000000, 283.000000, 55.0, 2.0, 0xB2CC04FF, 100.0, BAR_DIRECTION_RIGHT); // Fome
	pBar[playerid][pBarSede] = CreatePlayerProgressBar(playerid, 78.000000, 298.000000, 55.0, 2.0, 0x20B6F5FF, 100.0, BAR_DIRECTION_RIGHT); // Sede
	pBar[playerid][pBarSono] = CreatePlayerProgressBar(playerid, 78.000000, 313.000000, 55.0, 2.0, 0x755591FF, 100.0, BAR_DIRECTION_RIGHT); // Sono

	return true;
}

/*
*
*			functions
*
*/

stock function TD::ShowPlayerHud(playerid)
{
	for(new i; i < sizeof(TDEditor_TD); i++)
		TextDrawShowForPlayer(playerid, TDEditor_TD[i]);
	
	for(new i; i < sizeof(TDEditor_PTD[]); i++)
		PlayerTextDrawShow(playerid, TDEditor_PTD[playerid][i]);

	for(new i; i < sizeof(pBar[]); i++)
		ShowPlayerProgressBar(playerid,  PlayerBar:pBar[playerid][pBarInfo:i]);

	call::TD->UpdateTextDrawHudExp(playerid);
	call::TD->UpdateTextDrawHudLevel(playerid);
	return true;
}

stock function TD::HidePlayerHud(playerid)
{
	for(new i; i < sizeof(TDEditor_TD); i++)
		TextDrawHideForPlayer(playerid, TDEditor_TD[i]);

	for(new i; i < sizeof(TDEditor_PTD[]); i++)
		PlayerTextDrawHide(playerid, TDEditor_PTD[playerid][i]);

	for(new i; i < sizeof(pBar[]); i++)
		HidePlayerProgressBar(playerid, PlayerBar:pBar[playerid][pBarInfo:i]);

	return true;
}

stock function TD::ShowPlayerHudData(playerid)
{
	TextDrawShowForPlayer(playerid, TDEditor_TD[0]); // Dada
	TextDrawShowForPlayer(playerid, TDEditor_TD[1]); // Hora
}
stock function TD::HidePlayerHudData(playerid)
{
	TextDrawHideForPlayer(playerid, TDEditor_TD[0]); // Dada
	TextDrawHideForPlayer(playerid, TDEditor_TD[1]); // Hora
}


stock function TD::ShowPlayerHudStatus(playerid)
{
	for(new i; i < sizeof(TDEditor_PTD[]); i++)
		PlayerTextDrawShow(playerid, TDEditor_PTD[playerid][i]);

	for(new i; i < sizeof(pBar[]); i++)
		ShowPlayerProgressBar(playerid,  PlayerBar:pBar[playerid][pBarInfo:i]);

	for(new i=3; i < sizeof(TDEditor_TD); i++)
		TextDrawShowForPlayer(playerid, TDEditor_TD[i]);
	return true;
}
stock function TD::HidePlayerHudStatus(playerid)
{
	for(new i; i < sizeof(TDEditor_PTD[]); i++)
		PlayerTextDrawHide(playerid, TDEditor_PTD[playerid][i]);

	for(new i; i < sizeof(pBar[]); i++)
		HidePlayerProgressBar(playerid,  PlayerBar:pBar[playerid][pBarInfo:i]);

	for(new i=3; i < sizeof(TDEditor_TD); i++)
		TextDrawHideForPlayer(playerid, TDEditor_TD[i]);
	return true;
}

function TD::UpdateTextDrawHud()
{
	new
		str[128], hora, minutos, segundos;

	gettime(hora,minutos,segundos);

	if(hora == 0 && minutos == 0 || !GetSVarInt("dateInit"))
	{
		static
			dia, mes, ano;

		getdate(ano,mes,dia);
		SetSVarInt("dateInit", 1);
		format(str, sizeof(str), "%02d de %s de %d", dia, ReturnMesFormat(mes), ano);
		TextDrawSetString(TDEditor_TD[0], str);
	}
	format(str, sizeof(str), "%02d:%02d:%02d", hora, minutos, segundos);
	TextDrawSetString(TDEditor_TD[1], str);
}

function TD::UpdateTextDrawPlayerHud(playerid)
{
	new str[30];
	format(str, sizeof(str), "%02d:%02d", Jogador[playerid][UPm], Jogador[playerid][UPs]);
	PlayerTextDrawSetString(playerid, TDEditor_PTD[playerid][0], str);
	
	SetPlayerProgressBarValue(playerid, pBar[playerid][pBarFome], Jogador[playerid][Fome]);
	SetPlayerProgressBarValue(playerid, pBar[playerid][pBarSede], Jogador[playerid][Sede]);
	SetPlayerProgressBarValue(playerid, pBar[playerid][pBarSono], Jogador[playerid][Sono]);
}

function TD::UpdateTextDrawHudLevel(playerid)
{
	new str[10];
	format(str, sizeof(str), "%d", GetPlayerScore(playerid));
	PlayerTextDrawSetString(playerid, TDEditor_PTD[playerid][1], str);
}

function TD::UpdateTextDrawHudExp(playerid){
	new str[30];
	format(str, sizeof(str), "%d/%d", Jogador[playerid][EXP], call::PLAYER->requestXP(playerid));
	PlayerTextDrawSetString(playerid, TDEditor_PTD[playerid][2], str);
}

static Text:TDEditor_TD[5], 
	PlayerText:TDEditor_PTD[MAX_PLAYERS][2];

#include <YSI_Coding\y_hooks>
hook OnGameModeInit()
{
	TDEditor_TD[0] = TextDrawCreate(215.000000, 147.416671, "box");
	TextDrawLetterSize(TDEditor_TD[0], 0.000000, 10.347990);
	TextDrawTextSize(TDEditor_TD[0], 445.000000, 0.000000);
	TextDrawAlignment(TDEditor_TD[0], 1);
	TextDrawColor(TDEditor_TD[0], -1);
	TextDrawUseBox(TDEditor_TD[0], 1);
	TextDrawBoxColor(TDEditor_TD[0], 100);
	TextDrawSetShadow(TDEditor_TD[0], 0);
	TextDrawSetOutline(TDEditor_TD[0], 0);
	TextDrawBackgroundColor(TDEditor_TD[0], 255);
	TextDrawFont(TDEditor_TD[0], 1);
	TextDrawSetProportional(TDEditor_TD[0], 1);
	TextDrawSetShadow(TDEditor_TD[0], 0);

	TDEditor_TD[1] = TextDrawCreate(323.500000, 150.333404, "Você está afk!");
	TextDrawLetterSize(TDEditor_TD[1], 0.281499, 1.075000);
	TextDrawAlignment(TDEditor_TD[1], 2);
	TextDrawColor(TDEditor_TD[1], -1);
	TextDrawSetShadow(TDEditor_TD[1], 0);
	TextDrawSetOutline(TDEditor_TD[1], 1);
	TextDrawBackgroundColor(TDEditor_TD[1], 255);
	TextDrawFont(TDEditor_TD[1], 2);
	TextDrawSetProportional(TDEditor_TD[1], 1);
	TextDrawSetShadow(TDEditor_TD[1], 0);

	TDEditor_TD[2] = TextDrawCreate(213.500000, 162.583282, "-");
	TextDrawLetterSize(TDEditor_TD[2], 15.000000, 0.300000);
	TextDrawAlignment(TDEditor_TD[2], 1);
	TextDrawColor(TDEditor_TD[2], -1);
	TextDrawSetShadow(TDEditor_TD[2], 0);
	TextDrawSetOutline(TDEditor_TD[2], 0);
	TextDrawBackgroundColor(TDEditor_TD[2], 255);
	TextDrawFont(TDEditor_TD[2], 1);
	TextDrawSetProportional(TDEditor_TD[2], 1);
	TextDrawSetShadow(TDEditor_TD[2], 0);

	TDEditor_TD[3] = TextDrawCreate(275.400146, 180.083312, "pressione a tecla");
	TextDrawLetterSize(TDEditor_TD[3], 0.380000, 1.063333);
	TextDrawTextSize(TDEditor_TD[3], 543.000000, 0.000000);
	TextDrawAlignment(TDEditor_TD[3], 1);
	TextDrawColor(TDEditor_TD[3], -1);
	TextDrawUseBox(TDEditor_TD[3], 1);
	TextDrawBoxColor(TDEditor_TD[3], 0);
	TextDrawSetShadow(TDEditor_TD[3], 0);
	TextDrawSetOutline(TDEditor_TD[3], 1);
	TextDrawBackgroundColor(TDEditor_TD[3], 255);
	TextDrawFont(TDEditor_TD[3], 3);
	TextDrawSetProportional(TDEditor_TD[3], 1);
	TextDrawSetShadow(TDEditor_TD[3], 0);

	TDEditor_TD[4] = TextDrawCreate(294.500000, 206.333206, "box");
	TextDrawLetterSize(TDEditor_TD[4], 0.000000, 1.800003);
	TextDrawTextSize(TDEditor_TD[4], 361.000000, 0.000000);
	TextDrawAlignment(TDEditor_TD[4], 1);
	TextDrawColor(TDEditor_TD[4], -1);
	TextDrawUseBox(TDEditor_TD[4], 1);
	TextDrawBoxColor(TDEditor_TD[4], -1378294017);
	TextDrawSetShadow(TDEditor_TD[4], 0);
	TextDrawSetOutline(TDEditor_TD[4], 0);
	TextDrawBackgroundColor(TDEditor_TD[4], 255);
	TextDrawFont(TDEditor_TD[4], 1);
	TextDrawSetProportional(TDEditor_TD[4], 1);
	TextDrawSetShadow(TDEditor_TD[4], 0);
}

hook OnPlayerConnect(playerid)
{
	for(new i; i < sizeof(TDEditor_PTD[]); i++)
	{
		TDEditor_PTD[playerid][i] = PlayerText:INVALID_TEXT_DRAW;
	}
}

function TD::ShowPlayerTextAFK(playerid, tecla[], msg[]="")
{
	
	for(new i; i < sizeof(TDEditor_PTD[]); i++)
	{
		if(TDEditor_PTD[playerid][i] != PlayerText:INVALID_TEXT_DRAW)
		{
			PlayerTextDrawDestroy(playerid, TDEditor_PTD[playerid][i]);
			TDEditor_PTD[playerid][i] = PlayerText:INVALID_TEXT_DRAW;
		}
	}

	TDEditor_PTD[playerid][0] = CreatePlayerTextDraw(playerid, 322.900085, 205.750015, tecla); // tecla
	PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][0], 0.337500, 1.699167);
	PlayerTextDrawTextSize(playerid, TDEditor_PTD[playerid][0], 0.000000, 67.000000);
	PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][0], 2);
	PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][0], -1);
	PlayerTextDrawUseBox(playerid, TDEditor_PTD[playerid][0], 1);
	PlayerTextDrawBoxColor(playerid, TDEditor_PTD[playerid][0], 0);
	PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][0], 0);
	PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][0], 1);
	PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][0], 255);
	PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][0], 2);
	PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][0], 1);
	PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][0], 0);

	TDEditor_PTD[playerid][1] = CreatePlayerTextDraw(playerid, 329.799987, 227.749359, msg); // Mensagem de erro
	PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][1], 0.286999, 1.086666);
	PlayerTextDrawTextSize(playerid, TDEditor_PTD[playerid][1], 0.000000, 231.000000);
	PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][1], 2);
	PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][1], -1);
	PlayerTextDrawUseBox(playerid, TDEditor_PTD[playerid][1], 1);
	PlayerTextDrawBoxColor(playerid, TDEditor_PTD[playerid][1], 0);
	PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][1], 0);
	PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][1], 0);
	PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][1], 255);
	PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][1], 1);
	PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][1], 1);
	PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][1], 0);

	for(new i; i < sizeof(TDEditor_PTD[]); i++){
		PlayerTextDrawShow(playerid, TDEditor_PTD[playerid][i]);
	}
	for(new i; i < sizeof(TDEditor_TD); i++){
		TextDrawShowForPlayer(playerid, TDEditor_TD[i]);
	}
}

function TD::HidePlayerTextAFK(playerid)
{
	for(new i; i < sizeof(TDEditor_PTD[]); i++){
		PlayerTextDrawDestroy(playerid, TDEditor_PTD[playerid][i]);
		TDEditor_PTD[playerid][i] = PlayerText:INVALID_TEXT_DRAW;
	}
	for(new i; i < sizeof(TDEditor_TD); i++){
		TextDrawHideForPlayer(playerid, TDEditor_TD[i]);
	}
}

const 
	MAX_STATION_NAME = 60,
	MAX_STATION_URL = 100
;

enum e_STATION_INFO
{
	Nome[MAX_STATION_NAME],
	Url[MAX_STATION_URL]
}
static stations[][e_STATION_INFO] =
{
	{"Brasil Life Honest", 					"http://stm16.voxtreaming.com.br:6906/;"},
	{"Hunter POP",							"https://live.hunter.fm/pop_normal"},
	{"Hunter Sertanejo",					"https://live.hunter.fm/sertanejo_normal"},
	{"Hunter Rock",							"https://live.hunter.fm/rock_normal"},
	{"Hunter POP2k", 						"https://live.hunter.fm/pop2k_normal"},
	{"Hunter Lofi", 						"https://live.hunter.fm/lofi_normal"},
	{"Hunter Tropical", 					"https://live.hunter.fm/tropical_normal"},
	{"Hunter 80s", 							"https://live.hunter.fm/80s_normal"},
	{"Hunter Smash", 						"https://live.hunter.fm/smash_normal"},
	{"Metropolitana FM Funk", 				"https://ice.fabricahost.com.br/metropoitanaspfunk"}
	
};

stock CountStations()
	return sizeof(stations);

stock GetStationName(idx, stationName[])
{
	if ( idx < 0 || idx >= sizeof(stations) )
		return false;
	
	format(stationName, MAX_STATION_NAME, stations[idx][Nome]);
	return true;
}

stock GetStationUrl(idx, url[])
{
	if ( idx < 0 || idx >= sizeof(stations) )
		return false;
		
	format(url, MAX_STATION_URL, stations[idx][Url]);
	return true;
}